﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TutorialNoNo
struct TutorialNoNo_t3242399842;

#include "codegen/il2cpp-codegen.h"

// System.Void TutorialNoNo::.ctor()
extern "C"  void TutorialNoNo__ctor_m1849162099 (TutorialNoNo_t3242399842 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TutorialNoNo::Start()
extern "C"  void TutorialNoNo_Start_m799199791 (TutorialNoNo_t3242399842 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TutorialNoNo::Update()
extern "C"  void TutorialNoNo_Update_m1424635276 (TutorialNoNo_t3242399842 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
