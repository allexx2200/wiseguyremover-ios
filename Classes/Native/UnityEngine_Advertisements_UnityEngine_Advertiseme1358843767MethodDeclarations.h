﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.Advertisements.ShowOptions
struct ShowOptions_t1358843767;
// System.Action`1<UnityEngine.Advertisements.ShowResult>
struct Action_1_t258469394;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"

// System.Action`1<UnityEngine.Advertisements.ShowResult> UnityEngine.Advertisements.ShowOptions::get_resultCallback()
extern "C"  Action_1_t258469394 * ShowOptions_get_resultCallback_m3731275459 (ShowOptions_t1358843767 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.Advertisements.ShowOptions::get_gamerSid()
extern "C"  String_t* ShowOptions_get_gamerSid_m3683233454 (ShowOptions_t1358843767 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
