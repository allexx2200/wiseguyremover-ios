﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// LevelLoader
struct LevelLoader_t433131637;

#include "codegen/il2cpp-codegen.h"

// System.Void LevelLoader::.ctor()
extern "C"  void LevelLoader__ctor_m3293766304 (LevelLoader_t433131637 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LevelLoader::Start()
extern "C"  void LevelLoader_Start_m862454864 (LevelLoader_t433131637 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LevelLoader::Update()
extern "C"  void LevelLoader_Update_m3214809813 (LevelLoader_t433131637 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
