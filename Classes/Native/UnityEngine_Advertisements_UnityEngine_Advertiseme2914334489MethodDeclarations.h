﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.Advertisements.iOS.Platform/<UnityAdsDidStart>c__AnonStorey2
struct U3CUnityAdsDidStartU3Ec__AnonStorey2_t2914334489;
// UnityEngine.Advertisements.CallbackExecutor
struct CallbackExecutor_t2947320436;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_Advertisements_UnityEngine_Advertiseme2947320436.h"

// System.Void UnityEngine.Advertisements.iOS.Platform/<UnityAdsDidStart>c__AnonStorey2::.ctor()
extern "C"  void U3CUnityAdsDidStartU3Ec__AnonStorey2__ctor_m221156269 (U3CUnityAdsDidStartU3Ec__AnonStorey2_t2914334489 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Advertisements.iOS.Platform/<UnityAdsDidStart>c__AnonStorey2::<>m__0(UnityEngine.Advertisements.CallbackExecutor)
extern "C"  void U3CUnityAdsDidStartU3Ec__AnonStorey2_U3CU3Em__0_m535831003 (U3CUnityAdsDidStartU3Ec__AnonStorey2_t2914334489 * __this, CallbackExecutor_t2947320436 * ___executor0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
