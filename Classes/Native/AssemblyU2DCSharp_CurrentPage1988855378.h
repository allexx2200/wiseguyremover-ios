﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Sprite
struct Sprite_t309593783;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CurrentPage
struct  CurrentPage_t1988855378  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.Sprite CurrentPage::activePage
	Sprite_t309593783 * ___activePage_2;
	// UnityEngine.Sprite CurrentPage::inactivePage
	Sprite_t309593783 * ___inactivePage_3;
	// System.Boolean CurrentPage::isCurrentPage
	bool ___isCurrentPage_4;

public:
	inline static int32_t get_offset_of_activePage_2() { return static_cast<int32_t>(offsetof(CurrentPage_t1988855378, ___activePage_2)); }
	inline Sprite_t309593783 * get_activePage_2() const { return ___activePage_2; }
	inline Sprite_t309593783 ** get_address_of_activePage_2() { return &___activePage_2; }
	inline void set_activePage_2(Sprite_t309593783 * value)
	{
		___activePage_2 = value;
		Il2CppCodeGenWriteBarrier(&___activePage_2, value);
	}

	inline static int32_t get_offset_of_inactivePage_3() { return static_cast<int32_t>(offsetof(CurrentPage_t1988855378, ___inactivePage_3)); }
	inline Sprite_t309593783 * get_inactivePage_3() const { return ___inactivePage_3; }
	inline Sprite_t309593783 ** get_address_of_inactivePage_3() { return &___inactivePage_3; }
	inline void set_inactivePage_3(Sprite_t309593783 * value)
	{
		___inactivePage_3 = value;
		Il2CppCodeGenWriteBarrier(&___inactivePage_3, value);
	}

	inline static int32_t get_offset_of_isCurrentPage_4() { return static_cast<int32_t>(offsetof(CurrentPage_t1988855378, ___isCurrentPage_4)); }
	inline bool get_isCurrentPage_4() const { return ___isCurrentPage_4; }
	inline bool* get_address_of_isCurrentPage_4() { return &___isCurrentPage_4; }
	inline void set_isCurrentPage_4(bool value)
	{
		___isCurrentPage_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
