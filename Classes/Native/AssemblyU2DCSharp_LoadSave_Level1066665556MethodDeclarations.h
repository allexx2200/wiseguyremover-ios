﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// LoadSave.Level
struct Level_t1066665556;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_LoadSave_Level1066665556.h"

// System.Void LoadSave.Level::.ctor()
extern "C"  void Level__ctor_m2252576272 (Level_t1066665556 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LoadSave.Level::.ctor(LoadSave.Level)
extern "C"  void Level__ctor_m2689317099 (Level_t1066665556 * __this, Level_t1066665556 * ___other0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
