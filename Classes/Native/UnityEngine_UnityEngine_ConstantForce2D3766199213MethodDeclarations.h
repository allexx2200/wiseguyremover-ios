﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.ConstantForce2D
struct ConstantForce2D_t3766199213;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector22243707579.h"

// System.Void UnityEngine.ConstantForce2D::set_force(UnityEngine.Vector2)
extern "C"  void ConstantForce2D_set_force_m1425292042 (ConstantForce2D_t3766199213 * __this, Vector2_t2243707579  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ConstantForce2D::INTERNAL_set_force(UnityEngine.Vector2&)
extern "C"  void ConstantForce2D_INTERNAL_set_force_m702904410 (ConstantForce2D_t3766199213 * __this, Vector2_t2243707579 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
