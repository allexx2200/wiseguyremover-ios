﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.Advertisements.CallbackExecutor
struct CallbackExecutor_t2947320436;
// System.Action`1<UnityEngine.Advertisements.CallbackExecutor>
struct Action_1_t2749119818;

#include "codegen/il2cpp-codegen.h"

// System.Void UnityEngine.Advertisements.CallbackExecutor::.ctor()
extern "C"  void CallbackExecutor__ctor_m3057232423 (CallbackExecutor_t2947320436 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Advertisements.CallbackExecutor::Post(System.Action`1<UnityEngine.Advertisements.CallbackExecutor>)
extern "C"  void CallbackExecutor_Post_m4193517434 (CallbackExecutor_t2947320436 * __this, Action_1_t2749119818 * ___action0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Advertisements.CallbackExecutor::Update()
extern "C"  void CallbackExecutor_Update_m3496238336 (CallbackExecutor_t2947320436 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
