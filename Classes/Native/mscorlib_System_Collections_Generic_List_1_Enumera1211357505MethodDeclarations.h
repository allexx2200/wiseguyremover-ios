﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1593300101MethodDeclarations.h"

// System.Void System.Collections.Generic.List`1/Enumerator<ImportExportUtilities.Character>::.ctor(System.Collections.Generic.List`1<T>)
#define Enumerator__ctor_m416811207(__this, ___l0, method) ((  void (*) (Enumerator_t1211357505 *, List_1_t1676627831 *, const MethodInfo*))Enumerator__ctor_m3769601633_gshared)(__this, ___l0, method)
// System.Void System.Collections.Generic.List`1/Enumerator<ImportExportUtilities.Character>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m978368979(__this, method) ((  void (*) (Enumerator_t1211357505 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m3440386353_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<ImportExportUtilities.Character>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m361692383(__this, method) ((  Il2CppObject * (*) (Enumerator_t1211357505 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m2853089017_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<ImportExportUtilities.Character>::Dispose()
#define Enumerator_Dispose_m2730639556(__this, method) ((  void (*) (Enumerator_t1211357505 *, const MethodInfo*))Enumerator_Dispose_m3736175406_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<ImportExportUtilities.Character>::VerifyState()
#define Enumerator_VerifyState_m3399817133(__this, method) ((  void (*) (Enumerator_t1211357505 *, const MethodInfo*))Enumerator_VerifyState_m825848279_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<ImportExportUtilities.Character>::MoveNext()
#define Enumerator_MoveNext_m105615144(__this, method) ((  bool (*) (Enumerator_t1211357505 *, const MethodInfo*))Enumerator_MoveNext_m44995089_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<ImportExportUtilities.Character>::get_Current()
#define Enumerator_get_Current_m2360690042(__this, method) ((  Character_t2307506699 * (*) (Enumerator_t1211357505 *, const MethodInfo*))Enumerator_get_Current_m3108634708_gshared)(__this, method)
