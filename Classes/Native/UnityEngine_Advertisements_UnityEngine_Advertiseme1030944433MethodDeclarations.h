﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.Advertisements.StartEventArgs
struct StartEventArgs_t1030944433;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void UnityEngine.Advertisements.StartEventArgs::.ctor(System.String)
extern "C"  void StartEventArgs__ctor_m1199484170 (StartEventArgs_t1030944433 * __this, String_t* ___placementId0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Advertisements.StartEventArgs::set_placementId(System.String)
extern "C"  void StartEventArgs_set_placementId_m1448799765 (StartEventArgs_t1030944433 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
