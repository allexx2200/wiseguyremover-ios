﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GameMaster
struct GameMaster_t3159518556;

#include "codegen/il2cpp-codegen.h"

// System.Void GameMaster::.ctor()
extern "C"  void GameMaster__ctor_m1568498945 (GameMaster_t3159518556 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameMaster::Start()
extern "C"  void GameMaster_Start_m204021929 (GameMaster_t3159518556 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameMaster::Update()
extern "C"  void GameMaster_Update_m3840188074 (GameMaster_t3159518556 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameMaster::CreateBluesTable()
extern "C"  void GameMaster_CreateBluesTable_m1065156852 (GameMaster_t3159518556 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameMaster::CheckForDeadBlues()
extern "C"  void GameMaster_CheckForDeadBlues_m2405166521 (GameMaster_t3159518556 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameMaster::FailLevel()
extern "C"  void GameMaster_FailLevel_m2296886933 (GameMaster_t3159518556 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameMaster::CreateRedsTable()
extern "C"  void GameMaster_CreateRedsTable_m3145821583 (GameMaster_t3159518556 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameMaster::CheckForDeadReds()
extern "C"  void GameMaster_CheckForDeadReds_m4155658138 (GameMaster_t3159518556 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameMaster::SucceedLevel()
extern "C"  void GameMaster_SucceedLevel_m2200462369 (GameMaster_t3159518556 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameMaster::CheckForTouches()
extern "C"  void GameMaster_CheckForTouches_m1815965391 (GameMaster_t3159518556 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameMaster::goToNextLevel()
extern "C"  void GameMaster_goToNextLevel_m4166812671 (GameMaster_t3159518556 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
