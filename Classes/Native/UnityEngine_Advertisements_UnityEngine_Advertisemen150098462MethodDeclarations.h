﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.Advertisements.iOS.Platform/<UnityAdsDidError>c__AnonStorey1
struct U3CUnityAdsDidErrorU3Ec__AnonStorey1_t150098462;
// UnityEngine.Advertisements.CallbackExecutor
struct CallbackExecutor_t2947320436;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_Advertisements_UnityEngine_Advertiseme2947320436.h"

// System.Void UnityEngine.Advertisements.iOS.Platform/<UnityAdsDidError>c__AnonStorey1::.ctor()
extern "C"  void U3CUnityAdsDidErrorU3Ec__AnonStorey1__ctor_m3238226576 (U3CUnityAdsDidErrorU3Ec__AnonStorey1_t150098462 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Advertisements.iOS.Platform/<UnityAdsDidError>c__AnonStorey1::<>m__0(UnityEngine.Advertisements.CallbackExecutor)
extern "C"  void U3CUnityAdsDidErrorU3Ec__AnonStorey1_U3CU3Em__0_m2695248046 (U3CUnityAdsDidErrorU3Ec__AnonStorey1_t150098462 * __this, CallbackExecutor_t2947320436 * ___executor0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
