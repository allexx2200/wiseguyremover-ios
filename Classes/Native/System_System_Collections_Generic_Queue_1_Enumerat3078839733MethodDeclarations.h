﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "System_System_Collections_Generic_Queue_1_Enumerat3019169210MethodDeclarations.h"

// System.Void System.Collections.Generic.Queue`1/Enumerator<System.Action`1<UnityEngine.Advertisements.CallbackExecutor>>::.ctor(System.Collections.Generic.Queue`1<T>)
#define Enumerator__ctor_m758812242(__this, ___q0, method) ((  void (*) (Enumerator_t3078839733 *, Queue_1_t2568776653 *, const MethodInfo*))Enumerator__ctor_m677001007_gshared)(__this, ___q0, method)
// System.Void System.Collections.Generic.Queue`1/Enumerator<System.Action`1<UnityEngine.Advertisements.CallbackExecutor>>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m1497342237(__this, method) ((  void (*) (Enumerator_t3078839733 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m373072478_gshared)(__this, method)
// System.Object System.Collections.Generic.Queue`1/Enumerator<System.Action`1<UnityEngine.Advertisements.CallbackExecutor>>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m3811153741(__this, method) ((  Il2CppObject * (*) (Enumerator_t3078839733 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m2167685344_gshared)(__this, method)
// System.Void System.Collections.Generic.Queue`1/Enumerator<System.Action`1<UnityEngine.Advertisements.CallbackExecutor>>::Dispose()
#define Enumerator_Dispose_m1387411042(__this, method) ((  void (*) (Enumerator_t3078839733 *, const MethodInfo*))Enumerator_Dispose_m575349149_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Queue`1/Enumerator<System.Action`1<UnityEngine.Advertisements.CallbackExecutor>>::MoveNext()
#define Enumerator_MoveNext_m423325657(__this, method) ((  bool (*) (Enumerator_t3078839733 *, const MethodInfo*))Enumerator_MoveNext_m742418190_gshared)(__this, method)
// T System.Collections.Generic.Queue`1/Enumerator<System.Action`1<UnityEngine.Advertisements.CallbackExecutor>>::get_Current()
#define Enumerator_get_Current_m2743410824(__this, method) ((  Action_1_t2749119818 * (*) (Enumerator_t3078839733 *, const MethodInfo*))Enumerator_get_Current_m1613610405_gshared)(__this, method)
