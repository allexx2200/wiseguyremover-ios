﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// StaticData.Constants
struct  Constants_t3271648343  : public Il2CppObject
{
public:

public:
};

struct Constants_t3271648343_StaticFields
{
public:
	// System.String StaticData.Constants::SAVE_GAME_PATH
	String_t* ___SAVE_GAME_PATH_0;

public:
	inline static int32_t get_offset_of_SAVE_GAME_PATH_0() { return static_cast<int32_t>(offsetof(Constants_t3271648343_StaticFields, ___SAVE_GAME_PATH_0)); }
	inline String_t* get_SAVE_GAME_PATH_0() const { return ___SAVE_GAME_PATH_0; }
	inline String_t** get_address_of_SAVE_GAME_PATH_0() { return &___SAVE_GAME_PATH_0; }
	inline void set_SAVE_GAME_PATH_0(String_t* value)
	{
		___SAVE_GAME_PATH_0 = value;
		Il2CppCodeGenWriteBarrier(&___SAVE_GAME_PATH_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
