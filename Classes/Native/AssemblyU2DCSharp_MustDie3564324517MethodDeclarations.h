﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MustDie
struct MustDie_t3564324517;

#include "codegen/il2cpp-codegen.h"

// System.Void MustDie::.ctor()
extern "C"  void MustDie__ctor_m2648985360 (MustDie_t3564324517 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MustDie::Start()
extern "C"  void MustDie_Start_m746272128 (MustDie_t3564324517 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MustDie::Update()
extern "C"  void MustDie_Update_m39114213 (MustDie_t3564324517 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
