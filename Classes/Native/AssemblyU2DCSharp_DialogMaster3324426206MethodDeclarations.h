﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// DialogMaster
struct DialogMaster_t3324426206;

#include "codegen/il2cpp-codegen.h"

// System.Void DialogMaster::.ctor()
extern "C"  void DialogMaster__ctor_m3879060931 (DialogMaster_t3324426206 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DialogMaster::Start()
extern "C"  void DialogMaster_Start_m3599621375 (DialogMaster_t3324426206 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DialogMaster::Update()
extern "C"  void DialogMaster_Update_m3687772396 (DialogMaster_t3324426206 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
