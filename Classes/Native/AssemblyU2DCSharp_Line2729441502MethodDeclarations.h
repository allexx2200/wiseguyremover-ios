﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Line
struct Line_t2729441502;

#include "codegen/il2cpp-codegen.h"

// System.Void Line::.ctor()
extern "C"  void Line__ctor_m3651664013 (Line_t2729441502 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
