﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// LoadSave.Level[]
struct LevelU5BU5D_t1839895005;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LoadSave.GameState
struct  GameState_t2029468607  : public Il2CppObject
{
public:
	// LoadSave.Level[] LoadSave.GameState::levels
	LevelU5BU5D_t1839895005* ___levels_0;

public:
	inline static int32_t get_offset_of_levels_0() { return static_cast<int32_t>(offsetof(GameState_t2029468607, ___levels_0)); }
	inline LevelU5BU5D_t1839895005* get_levels_0() const { return ___levels_0; }
	inline LevelU5BU5D_t1839895005** get_address_of_levels_0() { return &___levels_0; }
	inline void set_levels_0(LevelU5BU5D_t1839895005* value)
	{
		___levels_0 = value;
		Il2CppCodeGenWriteBarrier(&___levels_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
