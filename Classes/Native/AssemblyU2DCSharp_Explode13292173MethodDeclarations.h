﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Explode
struct Explode_t13292173;

#include "codegen/il2cpp-codegen.h"

// System.Void Explode::.ctor()
extern "C"  void Explode__ctor_m1586866248 (Explode_t13292173 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Explode::Start()
extern "C"  void Explode_Start_m1253663336 (Explode_t13292173 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Explode::Update()
extern "C"  void Explode_Update_m3143739981 (Explode_t13292173 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Explode::Explosion()
extern "C"  void Explode_Explosion_m1359633813 (Explode_t13292173 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
