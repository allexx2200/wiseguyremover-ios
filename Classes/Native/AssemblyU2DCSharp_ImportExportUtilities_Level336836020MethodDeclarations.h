﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ImportExportUtilities.Level
struct Level_t336836020;
// System.String
struct String_t;
// System.Collections.Generic.List`1<ImportExportUtilities.Character>
struct List_1_t1676627831;
// System.Collections.Generic.List`1<ImportExportUtilities.Platform>
struct List_1_t372332161;
// ImportExportUtilities.Character
struct Character_t2307506699;
// ImportExportUtilities.Platform
struct Platform_t1003211029;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "AssemblyU2DCSharp_ImportExportUtilities_Level336836020.h"
#include "AssemblyU2DCSharp_ImportExportUtilities_Character2307506699.h"
#include "AssemblyU2DCSharp_ImportExportUtilities_Platform1003211029.h"

// System.Void ImportExportUtilities.Level::.ctor()
extern "C"  void Level__ctor_m3742255388 (Level_t336836020 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ImportExportUtilities.Level::.ctor(System.String)
extern "C"  void Level__ctor_m442090598 (Level_t336836020 * __this, String_t* ___path0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ImportExportUtilities.Level::.ctor(ImportExportUtilities.Level)
extern "C"  void Level__ctor_m1909254975 (Level_t336836020 * __this, Level_t336836020 * ___level0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<ImportExportUtilities.Character> ImportExportUtilities.Level::get_characters()
extern "C"  List_1_t1676627831 * Level_get_characters_m1863299254 (Level_t336836020 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<ImportExportUtilities.Platform> ImportExportUtilities.Level::get_platforms()
extern "C"  List_1_t372332161 * Level_get_platforms_m4054011684 (Level_t336836020 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ImportExportUtilities.Level::addCharacter(ImportExportUtilities.Character)
extern "C"  void Level_addCharacter_m1611917466 (Level_t336836020 * __this, Character_t2307506699 * ___obj0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ImportExportUtilities.Level::addPlatform(ImportExportUtilities.Platform)
extern "C"  void Level_addPlatform_m2853649974 (Level_t336836020 * __this, Platform_t1003211029 * ___obj0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ImportExportUtilities.Level::Export(System.String)
extern "C"  void Level_Export_m2384450406 (Level_t336836020 * __this, String_t* ___path0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ImportExportUtilities.Level::GenerateToScene(ImportExportUtilities.Level)
extern "C"  void Level_GenerateToScene_m306945417 (Il2CppObject * __this /* static, unused */, Level_t336836020 * ___level0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ImportExportUtilities.Level ImportExportUtilities.Level::GenerateFromScene()
extern "C"  Level_t336836020 * Level_GenerateFromScene_m2982569127 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
