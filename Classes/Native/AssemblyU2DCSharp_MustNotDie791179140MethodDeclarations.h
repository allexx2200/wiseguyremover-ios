﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MustNotDie
struct MustNotDie_t791179140;

#include "codegen/il2cpp-codegen.h"

// System.Void MustNotDie::.ctor()
extern "C"  void MustNotDie__ctor_m897372945 (MustNotDie_t791179140 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MustNotDie::Start()
extern "C"  void MustNotDie_Start_m2660101073 (MustNotDie_t791179140 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MustNotDie::Update()
extern "C"  void MustNotDie_Update_m2193848914 (MustNotDie_t791179140 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
