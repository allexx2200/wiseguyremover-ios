﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// ImportExportUtilities.Character[]
struct CharacterU5BU5D_t2288121194;
// ImportExportUtilities.Platform[]
struct PlatformU5BU5D_t3340127992;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ImportExportUtilities.ExportedLevel
struct  ExportedLevel_t702369075  : public Il2CppObject
{
public:
	// ImportExportUtilities.Character[] ImportExportUtilities.ExportedLevel::characters
	CharacterU5BU5D_t2288121194* ___characters_0;
	// ImportExportUtilities.Platform[] ImportExportUtilities.ExportedLevel::platforms
	PlatformU5BU5D_t3340127992* ___platforms_1;
	// System.Int32 ImportExportUtilities.ExportedLevel::numberOfCharacters
	int32_t ___numberOfCharacters_2;
	// System.Int32 ImportExportUtilities.ExportedLevel::numberOfPlatforms
	int32_t ___numberOfPlatforms_3;

public:
	inline static int32_t get_offset_of_characters_0() { return static_cast<int32_t>(offsetof(ExportedLevel_t702369075, ___characters_0)); }
	inline CharacterU5BU5D_t2288121194* get_characters_0() const { return ___characters_0; }
	inline CharacterU5BU5D_t2288121194** get_address_of_characters_0() { return &___characters_0; }
	inline void set_characters_0(CharacterU5BU5D_t2288121194* value)
	{
		___characters_0 = value;
		Il2CppCodeGenWriteBarrier(&___characters_0, value);
	}

	inline static int32_t get_offset_of_platforms_1() { return static_cast<int32_t>(offsetof(ExportedLevel_t702369075, ___platforms_1)); }
	inline PlatformU5BU5D_t3340127992* get_platforms_1() const { return ___platforms_1; }
	inline PlatformU5BU5D_t3340127992** get_address_of_platforms_1() { return &___platforms_1; }
	inline void set_platforms_1(PlatformU5BU5D_t3340127992* value)
	{
		___platforms_1 = value;
		Il2CppCodeGenWriteBarrier(&___platforms_1, value);
	}

	inline static int32_t get_offset_of_numberOfCharacters_2() { return static_cast<int32_t>(offsetof(ExportedLevel_t702369075, ___numberOfCharacters_2)); }
	inline int32_t get_numberOfCharacters_2() const { return ___numberOfCharacters_2; }
	inline int32_t* get_address_of_numberOfCharacters_2() { return &___numberOfCharacters_2; }
	inline void set_numberOfCharacters_2(int32_t value)
	{
		___numberOfCharacters_2 = value;
	}

	inline static int32_t get_offset_of_numberOfPlatforms_3() { return static_cast<int32_t>(offsetof(ExportedLevel_t702369075, ___numberOfPlatforms_3)); }
	inline int32_t get_numberOfPlatforms_3() const { return ___numberOfPlatforms_3; }
	inline int32_t* get_address_of_numberOfPlatforms_3() { return &___numberOfPlatforms_3; }
	inline void set_numberOfPlatforms_3(int32_t value)
	{
		___numberOfPlatforms_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
