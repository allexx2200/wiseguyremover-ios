﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "UnityEngine_UnityEngine_PhysicsUpdateBehaviour2D1773575811.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.ConstantForce2D
struct  ConstantForce2D_t3766199213  : public PhysicsUpdateBehaviour2D_t1773575811
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
