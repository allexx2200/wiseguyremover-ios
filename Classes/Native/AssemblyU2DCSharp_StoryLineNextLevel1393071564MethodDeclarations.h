﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// StoryLineNextLevel
struct StoryLineNextLevel_t1393071564;

#include "codegen/il2cpp-codegen.h"

// System.Void StoryLineNextLevel::.ctor()
extern "C"  void StoryLineNextLevel__ctor_m3258329023 (StoryLineNextLevel_t1393071564 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void StoryLineNextLevel::Start()
extern "C"  void StoryLineNextLevel_Start_m1126760907 (StoryLineNextLevel_t1393071564 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void StoryLineNextLevel::Update()
extern "C"  void StoryLineNextLevel_Update_m2797851598 (StoryLineNextLevel_t1393071564 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void StoryLineNextLevel::ChangeToNextLevel()
extern "C"  void StoryLineNextLevel_ChangeToNextLevel_m3463279619 (StoryLineNextLevel_t1393071564 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
