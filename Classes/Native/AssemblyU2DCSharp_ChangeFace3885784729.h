﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Sprite
struct Sprite_t309593783;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ChangeFace
struct  ChangeFace_t3885784729  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.Sprite ChangeFace::normalFace
	Sprite_t309593783 * ___normalFace_2;
	// UnityEngine.Sprite ChangeFace::scaredFace
	Sprite_t309593783 * ___scaredFace_3;
	// System.Single ChangeFace::speedThreashold
	float ___speedThreashold_4;

public:
	inline static int32_t get_offset_of_normalFace_2() { return static_cast<int32_t>(offsetof(ChangeFace_t3885784729, ___normalFace_2)); }
	inline Sprite_t309593783 * get_normalFace_2() const { return ___normalFace_2; }
	inline Sprite_t309593783 ** get_address_of_normalFace_2() { return &___normalFace_2; }
	inline void set_normalFace_2(Sprite_t309593783 * value)
	{
		___normalFace_2 = value;
		Il2CppCodeGenWriteBarrier(&___normalFace_2, value);
	}

	inline static int32_t get_offset_of_scaredFace_3() { return static_cast<int32_t>(offsetof(ChangeFace_t3885784729, ___scaredFace_3)); }
	inline Sprite_t309593783 * get_scaredFace_3() const { return ___scaredFace_3; }
	inline Sprite_t309593783 ** get_address_of_scaredFace_3() { return &___scaredFace_3; }
	inline void set_scaredFace_3(Sprite_t309593783 * value)
	{
		___scaredFace_3 = value;
		Il2CppCodeGenWriteBarrier(&___scaredFace_3, value);
	}

	inline static int32_t get_offset_of_speedThreashold_4() { return static_cast<int32_t>(offsetof(ChangeFace_t3885784729, ___speedThreashold_4)); }
	inline float get_speedThreashold_4() const { return ___speedThreashold_4; }
	inline float* get_address_of_speedThreashold_4() { return &___speedThreashold_4; }
	inline void set_speedThreashold_4(float value)
	{
		___speedThreashold_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
