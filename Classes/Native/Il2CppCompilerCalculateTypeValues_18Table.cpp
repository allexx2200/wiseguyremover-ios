﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>


#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UI_UnityEngine_UI_CanvasScaler2574720772.h"
#include "UnityEngine_UI_UnityEngine_UI_CanvasScaler_ScaleMod987318053.h"
#include "UnityEngine_UI_UnityEngine_UI_CanvasScaler_ScreenM1916789528.h"
#include "UnityEngine_UI_UnityEngine_UI_CanvasScaler_Unit3220761768.h"
#include "UnityEngine_UI_UnityEngine_UI_ContentSizeFitter1325211874.h"
#include "UnityEngine_UI_UnityEngine_UI_ContentSizeFitter_Fi4030874534.h"
#include "UnityEngine_UI_UnityEngine_UI_GridLayoutGroup1515633077.h"
#include "UnityEngine_UI_UnityEngine_UI_GridLayoutGroup_Corn1077473318.h"
#include "UnityEngine_UI_UnityEngine_UI_GridLayoutGroup_Axis1431825778.h"
#include "UnityEngine_UI_UnityEngine_UI_GridLayoutGroup_Cons3558160636.h"
#include "UnityEngine_UI_UnityEngine_UI_HorizontalLayoutGrou2875670365.h"
#include "UnityEngine_UI_UnityEngine_UI_HorizontalOrVertical1968298610.h"
#include "UnityEngine_UI_UnityEngine_UI_LayoutElement2808691390.h"
#include "UnityEngine_UI_UnityEngine_UI_LayoutGroup3962498969.h"
#include "UnityEngine_UI_UnityEngine_UI_LayoutGroup_U3CDelay3228926346.h"
#include "UnityEngine_UI_UnityEngine_UI_LayoutRebuilder2155218138.h"
#include "UnityEngine_UI_UnityEngine_UI_LayoutUtility4076838048.h"
#include "UnityEngine_UI_UnityEngine_UI_VerticalLayoutGroup2468316403.h"
#include "UnityEngine_UI_UnityEngine_UI_ReflectionMethodsCac3343836395.h"
#include "UnityEngine_UI_UnityEngine_UI_ReflectionMethodsCac3928470916.h"
#include "UnityEngine_UI_UnityEngine_UI_ReflectionMethodsCac2260664863.h"
#include "UnityEngine_UI_UnityEngine_UI_ReflectionMethodsCac3435657708.h"
#include "UnityEngine_UI_UnityEngine_UI_ReflectionMethodsCac2213949596.h"
#include "UnityEngine_UI_UnityEngine_UI_VertexHelper385374196.h"
#include "UnityEngine_UI_UnityEngine_UI_BaseVertexEffect2504093552.h"
#include "UnityEngine_UI_UnityEngine_UI_BaseMeshEffect1728560551.h"
#include "UnityEngine_UI_UnityEngine_UI_Outline1417504278.h"
#include "UnityEngine_UI_UnityEngine_UI_PositionAsUV11102546563.h"
#include "UnityEngine_UI_UnityEngine_UI_Shadow4269599528.h"
#include "UnityEngine_UI_U3CPrivateImplementationDetailsU3E1486305137.h"
#include "UnityEngine_UI_U3CPrivateImplementationDetailsU3E_1568637717.h"
#include "UnityEngine_Analytics_U3CModuleU3E3783534214.h"
#include "UnityEngine_Analytics_UnityEngine_Analytics_Analyt2191537572.h"
#include "UnityEngine_Analytics_UnityEngine_Analytics_Analyt1068911718.h"
#include "UnityEngine_Analytics_UnityEngine_Analytics_Tracka1304606600.h"
#include "UnityEngine_Analytics_UnityEngine_Analytics_Tracka2256174789.h"
#include "AssemblyU2DCSharp_U3CModuleU3E3783534214.h"
#include "AssemblyU2DCSharp_DeveloperTools_DeveloperLog2073088872.h"
#include "AssemblyU2DCSharp_ImportExportUtilities_Character2307506699.h"
#include "AssemblyU2DCSharp_ImportExportUtilities_Platform1003211029.h"
#include "AssemblyU2DCSharp_ImportExportUtilities_Level336836020.h"
#include "AssemblyU2DCSharp_ImportExportUtilities_ExportedLev702369075.h"
#include "AssemblyU2DCSharp_LoadSave_Level1066665556.h"
#include "AssemblyU2DCSharp_LoadSave_GameState2029468607.h"
#include "AssemblyU2DCSharp_StaticData_Constants3271648343.h"
#include "AssemblyU2DCSharp_StaticData_StaticLevelContainer3714456515.h"
#include "AssemblyU2DCSharp_ChangeFace3885784729.h"
#include "AssemblyU2DCSharp_CustomGravity2378668691.h"
#include "AssemblyU2DCSharp_CustomGravity_Direction673380269.h"
#include "AssemblyU2DCSharp_DestroyOnContact1939146997.h"
#include "AssemblyU2DCSharp_Explode13292173.h"
#include "AssemblyU2DCSharp_GameMaster3159518556.h"
#include "AssemblyU2DCSharp_GoToScene3084403383.h"
#include "AssemblyU2DCSharp_CurrentPage1988855378.h"
#include "AssemblyU2DCSharp_Spawner534830648.h"
#include "AssemblyU2DCSharp_StarShower3709226430.h"
#include "AssemblyU2DCSharp_LevelLoader433131637.h"
#include "AssemblyU2DCSharp_MustDie3564324517.h"
#include "AssemblyU2DCSharp_MustNotDie791179140.h"
#include "AssemblyU2DCSharp_PausePress1918135203.h"
#include "AssemblyU2DCSharp_Removable2119088671.h"
#include "AssemblyU2DCSharp_SkipToNext3002738695.h"
#include "AssemblyU2DCSharp_StoryLineNextLevel1393071564.h"
#include "AssemblyU2DCSharp_TutorialFollow1280665443.h"
#include "AssemblyU2DCSharp_TutorialNoNo3242399842.h"
#include "AssemblyU2DCSharp_DialogPosition3314033503.h"
#include "AssemblyU2DCSharp_Line2729441502.h"
#include "AssemblyU2DCSharp_DialogMaster3324426206.h"
#include "AssemblyU2DCSharp_U3CPrivateImplementationDetailsU1486305137.h"
#include "AssemblyU2DCSharp_U3CPrivateImplementationDetailsU2731437132.h"



#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1800 = { sizeof (CanvasScaler_t2574720772), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1800[14] = 
{
	CanvasScaler_t2574720772::get_offset_of_m_UiScaleMode_2(),
	CanvasScaler_t2574720772::get_offset_of_m_ReferencePixelsPerUnit_3(),
	CanvasScaler_t2574720772::get_offset_of_m_ScaleFactor_4(),
	CanvasScaler_t2574720772::get_offset_of_m_ReferenceResolution_5(),
	CanvasScaler_t2574720772::get_offset_of_m_ScreenMatchMode_6(),
	CanvasScaler_t2574720772::get_offset_of_m_MatchWidthOrHeight_7(),
	0,
	CanvasScaler_t2574720772::get_offset_of_m_PhysicalUnit_9(),
	CanvasScaler_t2574720772::get_offset_of_m_FallbackScreenDPI_10(),
	CanvasScaler_t2574720772::get_offset_of_m_DefaultSpriteDPI_11(),
	CanvasScaler_t2574720772::get_offset_of_m_DynamicPixelsPerUnit_12(),
	CanvasScaler_t2574720772::get_offset_of_m_Canvas_13(),
	CanvasScaler_t2574720772::get_offset_of_m_PrevScaleFactor_14(),
	CanvasScaler_t2574720772::get_offset_of_m_PrevReferencePixelsPerUnit_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1801 = { sizeof (ScaleMode_t987318053)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1801[4] = 
{
	ScaleMode_t987318053::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1802 = { sizeof (ScreenMatchMode_t1916789528)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1802[4] = 
{
	ScreenMatchMode_t1916789528::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1803 = { sizeof (Unit_t3220761768)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1803[6] = 
{
	Unit_t3220761768::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1804 = { sizeof (ContentSizeFitter_t1325211874), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1804[4] = 
{
	ContentSizeFitter_t1325211874::get_offset_of_m_HorizontalFit_2(),
	ContentSizeFitter_t1325211874::get_offset_of_m_VerticalFit_3(),
	ContentSizeFitter_t1325211874::get_offset_of_m_Rect_4(),
	ContentSizeFitter_t1325211874::get_offset_of_m_Tracker_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1805 = { sizeof (FitMode_t4030874534)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1805[4] = 
{
	FitMode_t4030874534::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1806 = { sizeof (GridLayoutGroup_t1515633077), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1806[6] = 
{
	GridLayoutGroup_t1515633077::get_offset_of_m_StartCorner_10(),
	GridLayoutGroup_t1515633077::get_offset_of_m_StartAxis_11(),
	GridLayoutGroup_t1515633077::get_offset_of_m_CellSize_12(),
	GridLayoutGroup_t1515633077::get_offset_of_m_Spacing_13(),
	GridLayoutGroup_t1515633077::get_offset_of_m_Constraint_14(),
	GridLayoutGroup_t1515633077::get_offset_of_m_ConstraintCount_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1807 = { sizeof (Corner_t1077473318)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1807[5] = 
{
	Corner_t1077473318::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1808 = { sizeof (Axis_t1431825778)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1808[3] = 
{
	Axis_t1431825778::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1809 = { sizeof (Constraint_t3558160636)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1809[4] = 
{
	Constraint_t3558160636::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1810 = { sizeof (HorizontalLayoutGroup_t2875670365), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1811 = { sizeof (HorizontalOrVerticalLayoutGroup_t1968298610), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1811[5] = 
{
	HorizontalOrVerticalLayoutGroup_t1968298610::get_offset_of_m_Spacing_10(),
	HorizontalOrVerticalLayoutGroup_t1968298610::get_offset_of_m_ChildForceExpandWidth_11(),
	HorizontalOrVerticalLayoutGroup_t1968298610::get_offset_of_m_ChildForceExpandHeight_12(),
	HorizontalOrVerticalLayoutGroup_t1968298610::get_offset_of_m_ChildControlWidth_13(),
	HorizontalOrVerticalLayoutGroup_t1968298610::get_offset_of_m_ChildControlHeight_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1812 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1813 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1814 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1815 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1816 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1817 = { sizeof (LayoutElement_t2808691390), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1817[7] = 
{
	LayoutElement_t2808691390::get_offset_of_m_IgnoreLayout_2(),
	LayoutElement_t2808691390::get_offset_of_m_MinWidth_3(),
	LayoutElement_t2808691390::get_offset_of_m_MinHeight_4(),
	LayoutElement_t2808691390::get_offset_of_m_PreferredWidth_5(),
	LayoutElement_t2808691390::get_offset_of_m_PreferredHeight_6(),
	LayoutElement_t2808691390::get_offset_of_m_FlexibleWidth_7(),
	LayoutElement_t2808691390::get_offset_of_m_FlexibleHeight_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1818 = { sizeof (LayoutGroup_t3962498969), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1818[8] = 
{
	LayoutGroup_t3962498969::get_offset_of_m_Padding_2(),
	LayoutGroup_t3962498969::get_offset_of_m_ChildAlignment_3(),
	LayoutGroup_t3962498969::get_offset_of_m_Rect_4(),
	LayoutGroup_t3962498969::get_offset_of_m_Tracker_5(),
	LayoutGroup_t3962498969::get_offset_of_m_TotalMinSize_6(),
	LayoutGroup_t3962498969::get_offset_of_m_TotalPreferredSize_7(),
	LayoutGroup_t3962498969::get_offset_of_m_TotalFlexibleSize_8(),
	LayoutGroup_t3962498969::get_offset_of_m_RectChildren_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1819 = { sizeof (U3CDelayedSetDirtyU3Ec__Iterator0_t3228926346), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1819[4] = 
{
	U3CDelayedSetDirtyU3Ec__Iterator0_t3228926346::get_offset_of_rectTransform_0(),
	U3CDelayedSetDirtyU3Ec__Iterator0_t3228926346::get_offset_of_U24current_1(),
	U3CDelayedSetDirtyU3Ec__Iterator0_t3228926346::get_offset_of_U24disposing_2(),
	U3CDelayedSetDirtyU3Ec__Iterator0_t3228926346::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1820 = { sizeof (LayoutRebuilder_t2155218138), -1, sizeof(LayoutRebuilder_t2155218138_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1820[9] = 
{
	LayoutRebuilder_t2155218138::get_offset_of_m_ToRebuild_0(),
	LayoutRebuilder_t2155218138::get_offset_of_m_CachedHashFromTransform_1(),
	LayoutRebuilder_t2155218138_StaticFields::get_offset_of_s_Rebuilders_2(),
	LayoutRebuilder_t2155218138_StaticFields::get_offset_of_U3CU3Ef__mgU24cache0_3(),
	LayoutRebuilder_t2155218138_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_4(),
	LayoutRebuilder_t2155218138_StaticFields::get_offset_of_U3CU3Ef__amU24cache1_5(),
	LayoutRebuilder_t2155218138_StaticFields::get_offset_of_U3CU3Ef__amU24cache2_6(),
	LayoutRebuilder_t2155218138_StaticFields::get_offset_of_U3CU3Ef__amU24cache3_7(),
	LayoutRebuilder_t2155218138_StaticFields::get_offset_of_U3CU3Ef__amU24cache4_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1821 = { sizeof (LayoutUtility_t4076838048), -1, sizeof(LayoutUtility_t4076838048_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1821[8] = 
{
	LayoutUtility_t4076838048_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_0(),
	LayoutUtility_t4076838048_StaticFields::get_offset_of_U3CU3Ef__amU24cache1_1(),
	LayoutUtility_t4076838048_StaticFields::get_offset_of_U3CU3Ef__amU24cache2_2(),
	LayoutUtility_t4076838048_StaticFields::get_offset_of_U3CU3Ef__amU24cache3_3(),
	LayoutUtility_t4076838048_StaticFields::get_offset_of_U3CU3Ef__amU24cache4_4(),
	LayoutUtility_t4076838048_StaticFields::get_offset_of_U3CU3Ef__amU24cache5_5(),
	LayoutUtility_t4076838048_StaticFields::get_offset_of_U3CU3Ef__amU24cache6_6(),
	LayoutUtility_t4076838048_StaticFields::get_offset_of_U3CU3Ef__amU24cache7_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1822 = { sizeof (VerticalLayoutGroup_t2468316403), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1823 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1824 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1824[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1825 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1825[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1826 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1826[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1827 = { sizeof (ReflectionMethodsCache_t3343836395), -1, sizeof(ReflectionMethodsCache_t3343836395_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1827[5] = 
{
	ReflectionMethodsCache_t3343836395::get_offset_of_raycast3D_0(),
	ReflectionMethodsCache_t3343836395::get_offset_of_raycast3DAll_1(),
	ReflectionMethodsCache_t3343836395::get_offset_of_raycast2D_2(),
	ReflectionMethodsCache_t3343836395::get_offset_of_getRayIntersectionAll_3(),
	ReflectionMethodsCache_t3343836395_StaticFields::get_offset_of_s_ReflectionMethodsCache_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1828 = { sizeof (Raycast3DCallback_t3928470916), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1829 = { sizeof (Raycast2DCallback_t2260664863), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1830 = { sizeof (RaycastAllCallback_t3435657708), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1831 = { sizeof (GetRayIntersectionAllCallback_t2213949596), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1832 = { sizeof (VertexHelper_t385374196), -1, sizeof(VertexHelper_t385374196_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1832[9] = 
{
	VertexHelper_t385374196::get_offset_of_m_Positions_0(),
	VertexHelper_t385374196::get_offset_of_m_Colors_1(),
	VertexHelper_t385374196::get_offset_of_m_Uv0S_2(),
	VertexHelper_t385374196::get_offset_of_m_Uv1S_3(),
	VertexHelper_t385374196::get_offset_of_m_Normals_4(),
	VertexHelper_t385374196::get_offset_of_m_Tangents_5(),
	VertexHelper_t385374196::get_offset_of_m_Indices_6(),
	VertexHelper_t385374196_StaticFields::get_offset_of_s_DefaultTangent_7(),
	VertexHelper_t385374196_StaticFields::get_offset_of_s_DefaultNormal_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1833 = { sizeof (BaseVertexEffect_t2504093552), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1834 = { sizeof (BaseMeshEffect_t1728560551), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1834[1] = 
{
	BaseMeshEffect_t1728560551::get_offset_of_m_Graphic_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1835 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1836 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1837 = { sizeof (Outline_t1417504278), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1838 = { sizeof (PositionAsUV1_t1102546563), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1839 = { sizeof (Shadow_t4269599528), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1839[4] = 
{
	Shadow_t4269599528::get_offset_of_m_EffectColor_3(),
	Shadow_t4269599528::get_offset_of_m_EffectDistance_4(),
	Shadow_t4269599528::get_offset_of_m_UseGraphicAlpha_5(),
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1840 = { sizeof (U3CPrivateImplementationDetailsU3E_t1486305141), -1, sizeof(U3CPrivateImplementationDetailsU3E_t1486305141_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1840[1] = 
{
	U3CPrivateImplementationDetailsU3E_t1486305141_StaticFields::get_offset_of_U24fieldU2D7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1841 = { sizeof (U24ArrayTypeU3D12_t1568637717)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU3D12_t1568637717 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1842 = { sizeof (U3CModuleU3E_t3783534221), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1843 = { sizeof (AnalyticsTracker_t2191537572), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1843[5] = 
{
	AnalyticsTracker_t2191537572::get_offset_of_m_EventName_2(),
	AnalyticsTracker_t2191537572::get_offset_of_m_Dict_3(),
	AnalyticsTracker_t2191537572::get_offset_of_m_PrevDictHash_4(),
	AnalyticsTracker_t2191537572::get_offset_of_m_TrackableProperty_5(),
	AnalyticsTracker_t2191537572::get_offset_of_m_Trigger_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1844 = { sizeof (Trigger_t1068911718)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1844[8] = 
{
	Trigger_t1068911718::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1845 = { sizeof (TrackableProperty_t1304606600), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1845[2] = 
{
	0,
	TrackableProperty_t1304606600::get_offset_of_m_Fields_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1846 = { sizeof (FieldWithTarget_t2256174789), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1846[6] = 
{
	FieldWithTarget_t2256174789::get_offset_of_m_ParamName_0(),
	FieldWithTarget_t2256174789::get_offset_of_m_Target_1(),
	FieldWithTarget_t2256174789::get_offset_of_m_FieldPath_2(),
	FieldWithTarget_t2256174789::get_offset_of_m_TypeString_3(),
	FieldWithTarget_t2256174789::get_offset_of_m_DoStatic_4(),
	FieldWithTarget_t2256174789::get_offset_of_m_StaticString_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1847 = { sizeof (U3CModuleU3E_t3783534222), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1848 = { sizeof (DeveloperLog_t2073088872), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1849 = { sizeof (Character_t2307506699), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1849[17] = 
{
	Character_t2307506699::get_offset_of_Position_0(),
	Character_t2307506699::get_offset_of_Rotation_1(),
	Character_t2307506699::get_offset_of_Scale_2(),
	0,
	0,
	0,
	0,
	Character_t2307506699::get_offset_of_GravityDirection_7(),
	0,
	0,
	0,
	0,
	0,
	Character_t2307506699::get_offset_of_Type_13(),
	0,
	0,
	Character_t2307506699::get_offset_of_Shape_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1850 = { sizeof (Platform_t1003211029), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1850[8] = 
{
	Platform_t1003211029::get_offset_of_Position_0(),
	Platform_t1003211029::get_offset_of_Rotation_1(),
	Platform_t1003211029::get_offset_of_Scale_2(),
	0,
	0,
	0,
	0,
	Platform_t1003211029::get_offset_of_Type_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1851 = { sizeof (Level_t336836020), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1851[2] = 
{
	Level_t336836020::get_offset_of__characters_0(),
	Level_t336836020::get_offset_of__platforms_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1852 = { sizeof (ExportedLevel_t702369075), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1852[4] = 
{
	ExportedLevel_t702369075::get_offset_of_characters_0(),
	ExportedLevel_t702369075::get_offset_of_platforms_1(),
	ExportedLevel_t702369075::get_offset_of_numberOfCharacters_2(),
	ExportedLevel_t702369075::get_offset_of_numberOfPlatforms_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1853 = { sizeof (Level_t1066665556), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1853[7] = 
{
	0,
	0,
	0,
	0,
	0,
	Level_t1066665556::get_offset_of_status_5(),
	Level_t1066665556::get_offset_of_number_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1854 = { sizeof (GameState_t2029468607), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1854[1] = 
{
	GameState_t2029468607::get_offset_of_levels_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1855 = { sizeof (Constants_t3271648343), -1, sizeof(Constants_t3271648343_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1855[1] = 
{
	Constants_t3271648343_StaticFields::get_offset_of_SAVE_GAME_PATH_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1856 = { sizeof (StaticLevelContainer_t3714456515), -1, sizeof(StaticLevelContainer_t3714456515_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1856[5] = 
{
	StaticLevelContainer_t3714456515::get_offset_of_levelToLoad_0(),
	StaticLevelContainer_t3714456515::get_offset_of_gameState_1(),
	StaticLevelContainer_t3714456515::get_offset_of_adsCounter_2(),
	StaticLevelContainer_t3714456515::get_offset_of_maxNumberOfReplays_3(),
	StaticLevelContainer_t3714456515_StaticFields::get_offset_of__instance_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1857 = { sizeof (ChangeFace_t3885784729), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1857[3] = 
{
	ChangeFace_t3885784729::get_offset_of_normalFace_2(),
	ChangeFace_t3885784729::get_offset_of_scaredFace_3(),
	ChangeFace_t3885784729::get_offset_of_speedThreashold_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1858 = { sizeof (CustomGravity_t2378668691), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1858[2] = 
{
	CustomGravity_t2378668691::get_offset_of_gravityDirection_2(),
	CustomGravity_t2378668691::get_offset_of_constantForce_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1859 = { sizeof (Direction_t673380269)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1859[5] = 
{
	Direction_t673380269::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1860 = { sizeof (DestroyOnContact_t1939146997), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1861 = { sizeof (Explode_t13292173), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1861[1] = 
{
	Explode_t13292173::get_offset_of_explosionCoeficient_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1862 = { sizeof (GameMaster_t3159518556), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1862[27] = 
{
	GameMaster_t3159518556::get_offset_of_mustNotDieObjects_2(),
	GameMaster_t3159518556::get_offset_of_mustDieObjects_3(),
	GameMaster_t3159518556::get_offset_of_gameOver_4(),
	GameMaster_t3159518556::get_offset_of_noMoverBlocksToRemove_5(),
	GameMaster_t3159518556::get_offset_of_levelFinished_6(),
	GameMaster_t3159518556::get_offset_of_numberOfStars_7(),
	GameMaster_t3159518556::get_offset_of_finishTimer_8(),
	GameMaster_t3159518556::get_offset_of_finishTimerMax_9(),
	GameMaster_t3159518556::get_offset_of_numberOfTaps_10(),
	GameMaster_t3159518556::get_offset_of_gravity_11(),
	GameMaster_t3159518556::get_offset_of_blueSquare_12(),
	GameMaster_t3159518556::get_offset_of_blueCircle_13(),
	GameMaster_t3159518556::get_offset_of_redSquare_14(),
	GameMaster_t3159518556::get_offset_of_redCircle_15(),
	GameMaster_t3159518556::get_offset_of_darkRedSquare_16(),
	GameMaster_t3159518556::get_offset_of_darkRedCircle_17(),
	GameMaster_t3159518556::get_offset_of_yellowSquare_18(),
	GameMaster_t3159518556::get_offset_of_yellowCircle_19(),
	GameMaster_t3159518556::get_offset_of_greenSquare_20(),
	GameMaster_t3159518556::get_offset_of_greenCircle_21(),
	GameMaster_t3159518556::get_offset_of_redPlatform_22(),
	GameMaster_t3159518556::get_offset_of_bluePlatform_23(),
	GameMaster_t3159518556::get_offset_of_purplePlatform_24(),
	GameMaster_t3159518556::get_offset_of_maxTapsFor3Stars_25(),
	GameMaster_t3159518556::get_offset_of_maxTapsFor2Stars_26(),
	GameMaster_t3159518556::get_offset_of_maxTapsFor1Star_27(),
	GameMaster_t3159518556::get_offset_of_defaultTutorialString_28(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1863 = { sizeof (GoToScene_t3084403383), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1864 = { sizeof (CurrentPage_t1988855378), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1864[3] = 
{
	CurrentPage_t1988855378::get_offset_of_activePage_2(),
	CurrentPage_t1988855378::get_offset_of_inactivePage_3(),
	CurrentPage_t1988855378::get_offset_of_isCurrentPage_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1865 = { sizeof (Spawner_t534830648), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1865[11] = 
{
	Spawner_t534830648::get_offset_of_numberOfLevels_2(),
	Spawner_t534830648::get_offset_of_numberOfPages_3(),
	Spawner_t534830648::get_offset_of_levelButtonPozitionsY_4(),
	Spawner_t534830648::get_offset_of_levelButtonPozitionsX_5(),
	Spawner_t534830648::get_offset_of_pageIdentifierPositionsY_6(),
	Spawner_t534830648::get_offset_of_distanceBetweenPages_7(),
	Spawner_t534830648::get_offset_of_levelButton_8(),
	Spawner_t534830648::get_offset_of_pageIdentifier_9(),
	Spawner_t534830648::get_offset_of_pages_10(),
	Spawner_t534830648::get_offset_of_currentPage_11(),
	Spawner_t534830648::get_offset_of_currentGameState_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1866 = { sizeof (StarShower_t3709226430), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1866[5] = 
{
	StarShower_t3709226430::get_offset_of_numberOfStars_2(),
	StarShower_t3709226430::get_offset_of_locked_3(),
	StarShower_t3709226430::get_offset_of_lockedSprite_4(),
	StarShower_t3709226430::get_offset_of_unlockedSprite_5(),
	StarShower_t3709226430::get_offset_of_levelNumber_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1867 = { sizeof (LevelLoader_t433131637), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1868 = { sizeof (MustDie_t3564324517), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1869 = { sizeof (MustNotDie_t791179140), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1870 = { sizeof (PausePress_t1918135203), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1870[1] = 
{
	PausePress_t1918135203::get_offset_of_isPaused_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1871 = { sizeof (Removable_t2119088671), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1871[1] = 
{
	Removable_t2119088671::get_offset_of_toBeDestroyed_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1872 = { sizeof (SkipToNext_t3002738695), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1872[2] = 
{
	SkipToNext_t3002738695::get_offset_of_nextScene_2(),
	SkipToNext_t3002738695::get_offset_of_timeoutToSkip_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1873 = { sizeof (StoryLineNextLevel_t1393071564), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1873[3] = 
{
	StoryLineNextLevel_t1393071564::get_offset_of_nextSceneName_2(),
	StoryLineNextLevel_t1393071564::get_offset_of_secondsTillEnd_3(),
	StoryLineNextLevel_t1393071564::get_offset_of_timer_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1874 = { sizeof (TutorialFollow_t1280665443), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1874[6] = 
{
	TutorialFollow_t1280665443::get_offset_of_followPosition_2(),
	TutorialFollow_t1280665443::get_offset_of_nextTutorial_3(),
	TutorialFollow_t1280665443::get_offset_of_isFirstTutorial_4(),
	TutorialFollow_t1280665443::get_offset_of_isCurrentTutorial_5(),
	TutorialFollow_t1280665443::get_offset_of_hand_6(),
	TutorialFollow_t1280665443::get_offset_of_tutorialText_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1875 = { sizeof (TutorialNoNo_t3242399842), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1876 = { sizeof (DialogPosition_t3314033503)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1876[3] = 
{
	DialogPosition_t3314033503::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1877 = { sizeof (Line_t2729441502), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1877[3] = 
{
	Line_t2729441502::get_offset_of_line_0(),
	Line_t2729441502::get_offset_of_position_1(),
	Line_t2729441502::get_offset_of_characterIndex_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1878 = { sizeof (DialogMaster_t3324426206), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1878[3] = 
{
	DialogMaster_t3324426206::get_offset_of_leftCharacters_2(),
	DialogMaster_t3324426206::get_offset_of_rightCharacters_3(),
	DialogMaster_t3324426206::get_offset_of_lines_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1879 = { sizeof (U3CPrivateImplementationDetailsU3E_t1486305142), -1, sizeof(U3CPrivateImplementationDetailsU3E_t1486305142_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1879[1] = 
{
	U3CPrivateImplementationDetailsU3E_t1486305142_StaticFields::get_offset_of_U24fieldU2DA55AA7C523DD2ED84D18AEF0C5B0A88A70618491_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1880 = { sizeof (U24ArrayTypeU3D20_t2731437132)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU3D20_t2731437132 ), 0, 0 };
#ifdef __clang__
#pragma clang diagnostic pop
#endif
