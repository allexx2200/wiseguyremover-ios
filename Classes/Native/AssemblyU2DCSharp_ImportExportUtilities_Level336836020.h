﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.List`1<ImportExportUtilities.Character>
struct List_1_t1676627831;
// System.Collections.Generic.List`1<ImportExportUtilities.Platform>
struct List_1_t372332161;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ImportExportUtilities.Level
struct  Level_t336836020  : public Il2CppObject
{
public:
	// System.Collections.Generic.List`1<ImportExportUtilities.Character> ImportExportUtilities.Level::_characters
	List_1_t1676627831 * ____characters_0;
	// System.Collections.Generic.List`1<ImportExportUtilities.Platform> ImportExportUtilities.Level::_platforms
	List_1_t372332161 * ____platforms_1;

public:
	inline static int32_t get_offset_of__characters_0() { return static_cast<int32_t>(offsetof(Level_t336836020, ____characters_0)); }
	inline List_1_t1676627831 * get__characters_0() const { return ____characters_0; }
	inline List_1_t1676627831 ** get_address_of__characters_0() { return &____characters_0; }
	inline void set__characters_0(List_1_t1676627831 * value)
	{
		____characters_0 = value;
		Il2CppCodeGenWriteBarrier(&____characters_0, value);
	}

	inline static int32_t get_offset_of__platforms_1() { return static_cast<int32_t>(offsetof(Level_t336836020, ____platforms_1)); }
	inline List_1_t372332161 * get__platforms_1() const { return ____platforms_1; }
	inline List_1_t372332161 ** get_address_of__platforms_1() { return &____platforms_1; }
	inline void set__platforms_1(List_1_t372332161 * value)
	{
		____platforms_1 = value;
		Il2CppCodeGenWriteBarrier(&____platforms_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
