﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.Advertisements.FinishEventArgs
struct FinishEventArgs_t694166726;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "UnityEngine_Advertisements_UnityEngine_Advertisemen456670012.h"

// System.Void UnityEngine.Advertisements.FinishEventArgs::.ctor(System.String,UnityEngine.Advertisements.ShowResult)
extern "C"  void FinishEventArgs__ctor_m36663444 (FinishEventArgs_t694166726 * __this, String_t* ___placementId0, int32_t ___showResult1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.Advertisements.FinishEventArgs::get_placementId()
extern "C"  String_t* FinishEventArgs_get_placementId_m2995134121 (FinishEventArgs_t694166726 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Advertisements.FinishEventArgs::set_placementId(System.String)
extern "C"  void FinishEventArgs_set_placementId_m1735742016 (FinishEventArgs_t694166726 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Advertisements.ShowResult UnityEngine.Advertisements.FinishEventArgs::get_showResult()
extern "C"  int32_t FinishEventArgs_get_showResult_m3706965440 (FinishEventArgs_t694166726 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Advertisements.FinishEventArgs::set_showResult(UnityEngine.Advertisements.ShowResult)
extern "C"  void FinishEventArgs_set_showResult_m954243639 (FinishEventArgs_t694166726 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
