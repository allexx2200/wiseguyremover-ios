﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.Advertisements.Advertisement/<Show>c__AnonStorey1
struct U3CShowU3Ec__AnonStorey1_t2698643214;
// System.Object
struct Il2CppObject;
// UnityEngine.Advertisements.FinishEventArgs
struct FinishEventArgs_t694166726;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "UnityEngine_Advertisements_UnityEngine_Advertisemen694166726.h"

// System.Void UnityEngine.Advertisements.Advertisement/<Show>c__AnonStorey1::.ctor()
extern "C"  void U3CShowU3Ec__AnonStorey1__ctor_m1808617094 (U3CShowU3Ec__AnonStorey1_t2698643214 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Advertisements.Advertisement/<Show>c__AnonStorey1::<>m__0(System.Object,UnityEngine.Advertisements.FinishEventArgs)
extern "C"  void U3CShowU3Ec__AnonStorey1_U3CU3Em__0_m2584523182 (U3CShowU3Ec__AnonStorey1_t2698643214 * __this, Il2CppObject * ___sender0, FinishEventArgs_t694166726 * ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
