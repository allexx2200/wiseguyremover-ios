﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Object2689449295.h"
#include "UnityEngine_UnityEngine_Vector22243707579.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ImportExportUtilities.Character
struct  Character_t2307506699  : public Il2CppObject
{
public:
	// UnityEngine.Vector2 ImportExportUtilities.Character::Position
	Vector2_t2243707579  ___Position_0;
	// System.Single ImportExportUtilities.Character::Rotation
	float ___Rotation_1;
	// System.Single ImportExportUtilities.Character::Scale
	float ___Scale_2;
	// System.Int32 ImportExportUtilities.Character::GravityDirection
	int32_t ___GravityDirection_7;
	// System.Int32 ImportExportUtilities.Character::Type
	int32_t ___Type_13;
	// System.Int32 ImportExportUtilities.Character::Shape
	int32_t ___Shape_16;

public:
	inline static int32_t get_offset_of_Position_0() { return static_cast<int32_t>(offsetof(Character_t2307506699, ___Position_0)); }
	inline Vector2_t2243707579  get_Position_0() const { return ___Position_0; }
	inline Vector2_t2243707579 * get_address_of_Position_0() { return &___Position_0; }
	inline void set_Position_0(Vector2_t2243707579  value)
	{
		___Position_0 = value;
	}

	inline static int32_t get_offset_of_Rotation_1() { return static_cast<int32_t>(offsetof(Character_t2307506699, ___Rotation_1)); }
	inline float get_Rotation_1() const { return ___Rotation_1; }
	inline float* get_address_of_Rotation_1() { return &___Rotation_1; }
	inline void set_Rotation_1(float value)
	{
		___Rotation_1 = value;
	}

	inline static int32_t get_offset_of_Scale_2() { return static_cast<int32_t>(offsetof(Character_t2307506699, ___Scale_2)); }
	inline float get_Scale_2() const { return ___Scale_2; }
	inline float* get_address_of_Scale_2() { return &___Scale_2; }
	inline void set_Scale_2(float value)
	{
		___Scale_2 = value;
	}

	inline static int32_t get_offset_of_GravityDirection_7() { return static_cast<int32_t>(offsetof(Character_t2307506699, ___GravityDirection_7)); }
	inline int32_t get_GravityDirection_7() const { return ___GravityDirection_7; }
	inline int32_t* get_address_of_GravityDirection_7() { return &___GravityDirection_7; }
	inline void set_GravityDirection_7(int32_t value)
	{
		___GravityDirection_7 = value;
	}

	inline static int32_t get_offset_of_Type_13() { return static_cast<int32_t>(offsetof(Character_t2307506699, ___Type_13)); }
	inline int32_t get_Type_13() const { return ___Type_13; }
	inline int32_t* get_address_of_Type_13() { return &___Type_13; }
	inline void set_Type_13(int32_t value)
	{
		___Type_13 = value;
	}

	inline static int32_t get_offset_of_Shape_16() { return static_cast<int32_t>(offsetof(Character_t2307506699, ___Shape_16)); }
	inline int32_t get_Shape_16() const { return ___Shape_16; }
	inline int32_t* get_address_of_Shape_16() { return &___Shape_16; }
	inline void set_Shape_16(int32_t value)
	{
		___Shape_16 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
