﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Explode
struct  Explode_t13292173  : public MonoBehaviour_t1158329972
{
public:
	// System.Single Explode::explosionCoeficient
	float ___explosionCoeficient_2;

public:
	inline static int32_t get_offset_of_explosionCoeficient_2() { return static_cast<int32_t>(offsetof(Explode_t13292173, ___explosionCoeficient_2)); }
	inline float get_explosionCoeficient_2() const { return ___explosionCoeficient_2; }
	inline float* get_address_of_explosionCoeficient_2() { return &___explosionCoeficient_2; }
	inline void set_explosionCoeficient_2(float value)
	{
		___explosionCoeficient_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
