﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PausePress
struct PausePress_t1918135203;

#include "codegen/il2cpp-codegen.h"

// System.Void PausePress::.ctor()
extern "C"  void PausePress__ctor_m2591755784 (PausePress_t1918135203 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PausePress::Start()
extern "C"  void PausePress_Start_m1942734468 (PausePress_t1918135203 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PausePress::Update()
extern "C"  void PausePress_Update_m3699104615 (PausePress_t1918135203 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PausePress::pressPauseButton()
extern "C"  void PausePress_pressPauseButton_m2414244273 (PausePress_t1918135203 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PausePress::RestartLevel()
extern "C"  void PausePress_RestartLevel_m1753174107 (PausePress_t1918135203 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PausePress::GoToMenu()
extern "C"  void PausePress_GoToMenu_m1225061222 (PausePress_t1918135203 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
