﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SkipToNext
struct  SkipToNext_t3002738695  : public MonoBehaviour_t1158329972
{
public:
	// System.String SkipToNext::nextScene
	String_t* ___nextScene_2;
	// System.Single SkipToNext::timeoutToSkip
	float ___timeoutToSkip_3;

public:
	inline static int32_t get_offset_of_nextScene_2() { return static_cast<int32_t>(offsetof(SkipToNext_t3002738695, ___nextScene_2)); }
	inline String_t* get_nextScene_2() const { return ___nextScene_2; }
	inline String_t** get_address_of_nextScene_2() { return &___nextScene_2; }
	inline void set_nextScene_2(String_t* value)
	{
		___nextScene_2 = value;
		Il2CppCodeGenWriteBarrier(&___nextScene_2, value);
	}

	inline static int32_t get_offset_of_timeoutToSkip_3() { return static_cast<int32_t>(offsetof(SkipToNext_t3002738695, ___timeoutToSkip_3)); }
	inline float get_timeoutToSkip_3() const { return ___timeoutToSkip_3; }
	inline float* get_address_of_timeoutToSkip_3() { return &___timeoutToSkip_3; }
	inline void set_timeoutToSkip_3(float value)
	{
		___timeoutToSkip_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
