﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object2689449295.h"
#include "AssemblyU2DCSharp_DialogPosition3314033503.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Line
struct  Line_t2729441502  : public Il2CppObject
{
public:
	// System.String Line::line
	String_t* ___line_0;
	// DialogPosition Line::position
	int32_t ___position_1;
	// System.Int32 Line::characterIndex
	int32_t ___characterIndex_2;

public:
	inline static int32_t get_offset_of_line_0() { return static_cast<int32_t>(offsetof(Line_t2729441502, ___line_0)); }
	inline String_t* get_line_0() const { return ___line_0; }
	inline String_t** get_address_of_line_0() { return &___line_0; }
	inline void set_line_0(String_t* value)
	{
		___line_0 = value;
		Il2CppCodeGenWriteBarrier(&___line_0, value);
	}

	inline static int32_t get_offset_of_position_1() { return static_cast<int32_t>(offsetof(Line_t2729441502, ___position_1)); }
	inline int32_t get_position_1() const { return ___position_1; }
	inline int32_t* get_address_of_position_1() { return &___position_1; }
	inline void set_position_1(int32_t value)
	{
		___position_1 = value;
	}

	inline static int32_t get_offset_of_characterIndex_2() { return static_cast<int32_t>(offsetof(Line_t2729441502, ___characterIndex_2)); }
	inline int32_t get_characterIndex_2() const { return ___characterIndex_2; }
	inline int32_t* get_address_of_characterIndex_2() { return &___characterIndex_2; }
	inline void set_characterIndex_2(int32_t value)
	{
		___characterIndex_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
