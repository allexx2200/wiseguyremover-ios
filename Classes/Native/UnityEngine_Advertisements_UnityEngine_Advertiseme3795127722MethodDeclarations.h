﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.Advertisements.iOS.Platform/<UnityAdsDidFinish>c__AnonStorey4
struct U3CUnityAdsDidFinishU3Ec__AnonStorey4_t3795127722;
// UnityEngine.Advertisements.CallbackExecutor
struct CallbackExecutor_t2947320436;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_Advertisements_UnityEngine_Advertiseme2947320436.h"

// System.Void UnityEngine.Advertisements.iOS.Platform/<UnityAdsDidFinish>c__AnonStorey4::.ctor()
extern "C"  void U3CUnityAdsDidFinishU3Ec__AnonStorey4__ctor_m2947401910 (U3CUnityAdsDidFinishU3Ec__AnonStorey4_t3795127722 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Advertisements.iOS.Platform/<UnityAdsDidFinish>c__AnonStorey4::<>m__0(UnityEngine.Advertisements.CallbackExecutor)
extern "C"  void U3CUnityAdsDidFinishU3Ec__AnonStorey4_U3CU3Em__0_m2622366564 (U3CUnityAdsDidFinishU3Ec__AnonStorey4_t3795127722 * __this, CallbackExecutor_t2947320436 * ___executor0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
