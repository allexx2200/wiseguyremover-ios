﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t1756533147;
// TutorialFollow
struct TutorialFollow_t1280665443;
// System.String
struct String_t;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TutorialFollow
struct  TutorialFollow_t1280665443  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.GameObject TutorialFollow::followPosition
	GameObject_t1756533147 * ___followPosition_2;
	// TutorialFollow TutorialFollow::nextTutorial
	TutorialFollow_t1280665443 * ___nextTutorial_3;
	// System.Boolean TutorialFollow::isFirstTutorial
	bool ___isFirstTutorial_4;
	// System.Boolean TutorialFollow::isCurrentTutorial
	bool ___isCurrentTutorial_5;
	// UnityEngine.GameObject TutorialFollow::hand
	GameObject_t1756533147 * ___hand_6;
	// System.String TutorialFollow::tutorialText
	String_t* ___tutorialText_7;

public:
	inline static int32_t get_offset_of_followPosition_2() { return static_cast<int32_t>(offsetof(TutorialFollow_t1280665443, ___followPosition_2)); }
	inline GameObject_t1756533147 * get_followPosition_2() const { return ___followPosition_2; }
	inline GameObject_t1756533147 ** get_address_of_followPosition_2() { return &___followPosition_2; }
	inline void set_followPosition_2(GameObject_t1756533147 * value)
	{
		___followPosition_2 = value;
		Il2CppCodeGenWriteBarrier(&___followPosition_2, value);
	}

	inline static int32_t get_offset_of_nextTutorial_3() { return static_cast<int32_t>(offsetof(TutorialFollow_t1280665443, ___nextTutorial_3)); }
	inline TutorialFollow_t1280665443 * get_nextTutorial_3() const { return ___nextTutorial_3; }
	inline TutorialFollow_t1280665443 ** get_address_of_nextTutorial_3() { return &___nextTutorial_3; }
	inline void set_nextTutorial_3(TutorialFollow_t1280665443 * value)
	{
		___nextTutorial_3 = value;
		Il2CppCodeGenWriteBarrier(&___nextTutorial_3, value);
	}

	inline static int32_t get_offset_of_isFirstTutorial_4() { return static_cast<int32_t>(offsetof(TutorialFollow_t1280665443, ___isFirstTutorial_4)); }
	inline bool get_isFirstTutorial_4() const { return ___isFirstTutorial_4; }
	inline bool* get_address_of_isFirstTutorial_4() { return &___isFirstTutorial_4; }
	inline void set_isFirstTutorial_4(bool value)
	{
		___isFirstTutorial_4 = value;
	}

	inline static int32_t get_offset_of_isCurrentTutorial_5() { return static_cast<int32_t>(offsetof(TutorialFollow_t1280665443, ___isCurrentTutorial_5)); }
	inline bool get_isCurrentTutorial_5() const { return ___isCurrentTutorial_5; }
	inline bool* get_address_of_isCurrentTutorial_5() { return &___isCurrentTutorial_5; }
	inline void set_isCurrentTutorial_5(bool value)
	{
		___isCurrentTutorial_5 = value;
	}

	inline static int32_t get_offset_of_hand_6() { return static_cast<int32_t>(offsetof(TutorialFollow_t1280665443, ___hand_6)); }
	inline GameObject_t1756533147 * get_hand_6() const { return ___hand_6; }
	inline GameObject_t1756533147 ** get_address_of_hand_6() { return &___hand_6; }
	inline void set_hand_6(GameObject_t1756533147 * value)
	{
		___hand_6 = value;
		Il2CppCodeGenWriteBarrier(&___hand_6, value);
	}

	inline static int32_t get_offset_of_tutorialText_7() { return static_cast<int32_t>(offsetof(TutorialFollow_t1280665443, ___tutorialText_7)); }
	inline String_t* get_tutorialText_7() const { return ___tutorialText_7; }
	inline String_t** get_address_of_tutorialText_7() { return &___tutorialText_7; }
	inline void set_tutorialText_7(String_t* value)
	{
		___tutorialText_7 = value;
		Il2CppCodeGenWriteBarrier(&___tutorialText_7, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
