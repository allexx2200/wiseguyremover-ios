﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// MustNotDie[]
struct MustNotDieU5BU5D_t1158737389;
// MustDie[]
struct MustDieU5BU5D_t3128154408;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// System.String
struct String_t;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameMaster
struct  GameMaster_t3159518556  : public MonoBehaviour_t1158329972
{
public:
	// MustNotDie[] GameMaster::mustNotDieObjects
	MustNotDieU5BU5D_t1158737389* ___mustNotDieObjects_2;
	// MustDie[] GameMaster::mustDieObjects
	MustDieU5BU5D_t3128154408* ___mustDieObjects_3;
	// System.Boolean GameMaster::gameOver
	bool ___gameOver_4;
	// System.Boolean GameMaster::noMoverBlocksToRemove
	bool ___noMoverBlocksToRemove_5;
	// System.Boolean GameMaster::levelFinished
	bool ___levelFinished_6;
	// System.Int32 GameMaster::numberOfStars
	int32_t ___numberOfStars_7;
	// System.Single GameMaster::finishTimer
	float ___finishTimer_8;
	// System.Single GameMaster::finishTimerMax
	float ___finishTimerMax_9;
	// System.Int32 GameMaster::numberOfTaps
	int32_t ___numberOfTaps_10;
	// System.Single GameMaster::gravity
	float ___gravity_11;
	// UnityEngine.GameObject GameMaster::blueSquare
	GameObject_t1756533147 * ___blueSquare_12;
	// UnityEngine.GameObject GameMaster::blueCircle
	GameObject_t1756533147 * ___blueCircle_13;
	// UnityEngine.GameObject GameMaster::redSquare
	GameObject_t1756533147 * ___redSquare_14;
	// UnityEngine.GameObject GameMaster::redCircle
	GameObject_t1756533147 * ___redCircle_15;
	// UnityEngine.GameObject GameMaster::darkRedSquare
	GameObject_t1756533147 * ___darkRedSquare_16;
	// UnityEngine.GameObject GameMaster::darkRedCircle
	GameObject_t1756533147 * ___darkRedCircle_17;
	// UnityEngine.GameObject GameMaster::yellowSquare
	GameObject_t1756533147 * ___yellowSquare_18;
	// UnityEngine.GameObject GameMaster::yellowCircle
	GameObject_t1756533147 * ___yellowCircle_19;
	// UnityEngine.GameObject GameMaster::greenSquare
	GameObject_t1756533147 * ___greenSquare_20;
	// UnityEngine.GameObject GameMaster::greenCircle
	GameObject_t1756533147 * ___greenCircle_21;
	// UnityEngine.GameObject GameMaster::redPlatform
	GameObject_t1756533147 * ___redPlatform_22;
	// UnityEngine.GameObject GameMaster::bluePlatform
	GameObject_t1756533147 * ___bluePlatform_23;
	// UnityEngine.GameObject GameMaster::purplePlatform
	GameObject_t1756533147 * ___purplePlatform_24;
	// System.Int32 GameMaster::maxTapsFor3Stars
	int32_t ___maxTapsFor3Stars_25;
	// System.Int32 GameMaster::maxTapsFor2Stars
	int32_t ___maxTapsFor2Stars_26;
	// System.Int32 GameMaster::maxTapsFor1Star
	int32_t ___maxTapsFor1Star_27;
	// System.String GameMaster::defaultTutorialString
	String_t* ___defaultTutorialString_28;

public:
	inline static int32_t get_offset_of_mustNotDieObjects_2() { return static_cast<int32_t>(offsetof(GameMaster_t3159518556, ___mustNotDieObjects_2)); }
	inline MustNotDieU5BU5D_t1158737389* get_mustNotDieObjects_2() const { return ___mustNotDieObjects_2; }
	inline MustNotDieU5BU5D_t1158737389** get_address_of_mustNotDieObjects_2() { return &___mustNotDieObjects_2; }
	inline void set_mustNotDieObjects_2(MustNotDieU5BU5D_t1158737389* value)
	{
		___mustNotDieObjects_2 = value;
		Il2CppCodeGenWriteBarrier(&___mustNotDieObjects_2, value);
	}

	inline static int32_t get_offset_of_mustDieObjects_3() { return static_cast<int32_t>(offsetof(GameMaster_t3159518556, ___mustDieObjects_3)); }
	inline MustDieU5BU5D_t3128154408* get_mustDieObjects_3() const { return ___mustDieObjects_3; }
	inline MustDieU5BU5D_t3128154408** get_address_of_mustDieObjects_3() { return &___mustDieObjects_3; }
	inline void set_mustDieObjects_3(MustDieU5BU5D_t3128154408* value)
	{
		___mustDieObjects_3 = value;
		Il2CppCodeGenWriteBarrier(&___mustDieObjects_3, value);
	}

	inline static int32_t get_offset_of_gameOver_4() { return static_cast<int32_t>(offsetof(GameMaster_t3159518556, ___gameOver_4)); }
	inline bool get_gameOver_4() const { return ___gameOver_4; }
	inline bool* get_address_of_gameOver_4() { return &___gameOver_4; }
	inline void set_gameOver_4(bool value)
	{
		___gameOver_4 = value;
	}

	inline static int32_t get_offset_of_noMoverBlocksToRemove_5() { return static_cast<int32_t>(offsetof(GameMaster_t3159518556, ___noMoverBlocksToRemove_5)); }
	inline bool get_noMoverBlocksToRemove_5() const { return ___noMoverBlocksToRemove_5; }
	inline bool* get_address_of_noMoverBlocksToRemove_5() { return &___noMoverBlocksToRemove_5; }
	inline void set_noMoverBlocksToRemove_5(bool value)
	{
		___noMoverBlocksToRemove_5 = value;
	}

	inline static int32_t get_offset_of_levelFinished_6() { return static_cast<int32_t>(offsetof(GameMaster_t3159518556, ___levelFinished_6)); }
	inline bool get_levelFinished_6() const { return ___levelFinished_6; }
	inline bool* get_address_of_levelFinished_6() { return &___levelFinished_6; }
	inline void set_levelFinished_6(bool value)
	{
		___levelFinished_6 = value;
	}

	inline static int32_t get_offset_of_numberOfStars_7() { return static_cast<int32_t>(offsetof(GameMaster_t3159518556, ___numberOfStars_7)); }
	inline int32_t get_numberOfStars_7() const { return ___numberOfStars_7; }
	inline int32_t* get_address_of_numberOfStars_7() { return &___numberOfStars_7; }
	inline void set_numberOfStars_7(int32_t value)
	{
		___numberOfStars_7 = value;
	}

	inline static int32_t get_offset_of_finishTimer_8() { return static_cast<int32_t>(offsetof(GameMaster_t3159518556, ___finishTimer_8)); }
	inline float get_finishTimer_8() const { return ___finishTimer_8; }
	inline float* get_address_of_finishTimer_8() { return &___finishTimer_8; }
	inline void set_finishTimer_8(float value)
	{
		___finishTimer_8 = value;
	}

	inline static int32_t get_offset_of_finishTimerMax_9() { return static_cast<int32_t>(offsetof(GameMaster_t3159518556, ___finishTimerMax_9)); }
	inline float get_finishTimerMax_9() const { return ___finishTimerMax_9; }
	inline float* get_address_of_finishTimerMax_9() { return &___finishTimerMax_9; }
	inline void set_finishTimerMax_9(float value)
	{
		___finishTimerMax_9 = value;
	}

	inline static int32_t get_offset_of_numberOfTaps_10() { return static_cast<int32_t>(offsetof(GameMaster_t3159518556, ___numberOfTaps_10)); }
	inline int32_t get_numberOfTaps_10() const { return ___numberOfTaps_10; }
	inline int32_t* get_address_of_numberOfTaps_10() { return &___numberOfTaps_10; }
	inline void set_numberOfTaps_10(int32_t value)
	{
		___numberOfTaps_10 = value;
	}

	inline static int32_t get_offset_of_gravity_11() { return static_cast<int32_t>(offsetof(GameMaster_t3159518556, ___gravity_11)); }
	inline float get_gravity_11() const { return ___gravity_11; }
	inline float* get_address_of_gravity_11() { return &___gravity_11; }
	inline void set_gravity_11(float value)
	{
		___gravity_11 = value;
	}

	inline static int32_t get_offset_of_blueSquare_12() { return static_cast<int32_t>(offsetof(GameMaster_t3159518556, ___blueSquare_12)); }
	inline GameObject_t1756533147 * get_blueSquare_12() const { return ___blueSquare_12; }
	inline GameObject_t1756533147 ** get_address_of_blueSquare_12() { return &___blueSquare_12; }
	inline void set_blueSquare_12(GameObject_t1756533147 * value)
	{
		___blueSquare_12 = value;
		Il2CppCodeGenWriteBarrier(&___blueSquare_12, value);
	}

	inline static int32_t get_offset_of_blueCircle_13() { return static_cast<int32_t>(offsetof(GameMaster_t3159518556, ___blueCircle_13)); }
	inline GameObject_t1756533147 * get_blueCircle_13() const { return ___blueCircle_13; }
	inline GameObject_t1756533147 ** get_address_of_blueCircle_13() { return &___blueCircle_13; }
	inline void set_blueCircle_13(GameObject_t1756533147 * value)
	{
		___blueCircle_13 = value;
		Il2CppCodeGenWriteBarrier(&___blueCircle_13, value);
	}

	inline static int32_t get_offset_of_redSquare_14() { return static_cast<int32_t>(offsetof(GameMaster_t3159518556, ___redSquare_14)); }
	inline GameObject_t1756533147 * get_redSquare_14() const { return ___redSquare_14; }
	inline GameObject_t1756533147 ** get_address_of_redSquare_14() { return &___redSquare_14; }
	inline void set_redSquare_14(GameObject_t1756533147 * value)
	{
		___redSquare_14 = value;
		Il2CppCodeGenWriteBarrier(&___redSquare_14, value);
	}

	inline static int32_t get_offset_of_redCircle_15() { return static_cast<int32_t>(offsetof(GameMaster_t3159518556, ___redCircle_15)); }
	inline GameObject_t1756533147 * get_redCircle_15() const { return ___redCircle_15; }
	inline GameObject_t1756533147 ** get_address_of_redCircle_15() { return &___redCircle_15; }
	inline void set_redCircle_15(GameObject_t1756533147 * value)
	{
		___redCircle_15 = value;
		Il2CppCodeGenWriteBarrier(&___redCircle_15, value);
	}

	inline static int32_t get_offset_of_darkRedSquare_16() { return static_cast<int32_t>(offsetof(GameMaster_t3159518556, ___darkRedSquare_16)); }
	inline GameObject_t1756533147 * get_darkRedSquare_16() const { return ___darkRedSquare_16; }
	inline GameObject_t1756533147 ** get_address_of_darkRedSquare_16() { return &___darkRedSquare_16; }
	inline void set_darkRedSquare_16(GameObject_t1756533147 * value)
	{
		___darkRedSquare_16 = value;
		Il2CppCodeGenWriteBarrier(&___darkRedSquare_16, value);
	}

	inline static int32_t get_offset_of_darkRedCircle_17() { return static_cast<int32_t>(offsetof(GameMaster_t3159518556, ___darkRedCircle_17)); }
	inline GameObject_t1756533147 * get_darkRedCircle_17() const { return ___darkRedCircle_17; }
	inline GameObject_t1756533147 ** get_address_of_darkRedCircle_17() { return &___darkRedCircle_17; }
	inline void set_darkRedCircle_17(GameObject_t1756533147 * value)
	{
		___darkRedCircle_17 = value;
		Il2CppCodeGenWriteBarrier(&___darkRedCircle_17, value);
	}

	inline static int32_t get_offset_of_yellowSquare_18() { return static_cast<int32_t>(offsetof(GameMaster_t3159518556, ___yellowSquare_18)); }
	inline GameObject_t1756533147 * get_yellowSquare_18() const { return ___yellowSquare_18; }
	inline GameObject_t1756533147 ** get_address_of_yellowSquare_18() { return &___yellowSquare_18; }
	inline void set_yellowSquare_18(GameObject_t1756533147 * value)
	{
		___yellowSquare_18 = value;
		Il2CppCodeGenWriteBarrier(&___yellowSquare_18, value);
	}

	inline static int32_t get_offset_of_yellowCircle_19() { return static_cast<int32_t>(offsetof(GameMaster_t3159518556, ___yellowCircle_19)); }
	inline GameObject_t1756533147 * get_yellowCircle_19() const { return ___yellowCircle_19; }
	inline GameObject_t1756533147 ** get_address_of_yellowCircle_19() { return &___yellowCircle_19; }
	inline void set_yellowCircle_19(GameObject_t1756533147 * value)
	{
		___yellowCircle_19 = value;
		Il2CppCodeGenWriteBarrier(&___yellowCircle_19, value);
	}

	inline static int32_t get_offset_of_greenSquare_20() { return static_cast<int32_t>(offsetof(GameMaster_t3159518556, ___greenSquare_20)); }
	inline GameObject_t1756533147 * get_greenSquare_20() const { return ___greenSquare_20; }
	inline GameObject_t1756533147 ** get_address_of_greenSquare_20() { return &___greenSquare_20; }
	inline void set_greenSquare_20(GameObject_t1756533147 * value)
	{
		___greenSquare_20 = value;
		Il2CppCodeGenWriteBarrier(&___greenSquare_20, value);
	}

	inline static int32_t get_offset_of_greenCircle_21() { return static_cast<int32_t>(offsetof(GameMaster_t3159518556, ___greenCircle_21)); }
	inline GameObject_t1756533147 * get_greenCircle_21() const { return ___greenCircle_21; }
	inline GameObject_t1756533147 ** get_address_of_greenCircle_21() { return &___greenCircle_21; }
	inline void set_greenCircle_21(GameObject_t1756533147 * value)
	{
		___greenCircle_21 = value;
		Il2CppCodeGenWriteBarrier(&___greenCircle_21, value);
	}

	inline static int32_t get_offset_of_redPlatform_22() { return static_cast<int32_t>(offsetof(GameMaster_t3159518556, ___redPlatform_22)); }
	inline GameObject_t1756533147 * get_redPlatform_22() const { return ___redPlatform_22; }
	inline GameObject_t1756533147 ** get_address_of_redPlatform_22() { return &___redPlatform_22; }
	inline void set_redPlatform_22(GameObject_t1756533147 * value)
	{
		___redPlatform_22 = value;
		Il2CppCodeGenWriteBarrier(&___redPlatform_22, value);
	}

	inline static int32_t get_offset_of_bluePlatform_23() { return static_cast<int32_t>(offsetof(GameMaster_t3159518556, ___bluePlatform_23)); }
	inline GameObject_t1756533147 * get_bluePlatform_23() const { return ___bluePlatform_23; }
	inline GameObject_t1756533147 ** get_address_of_bluePlatform_23() { return &___bluePlatform_23; }
	inline void set_bluePlatform_23(GameObject_t1756533147 * value)
	{
		___bluePlatform_23 = value;
		Il2CppCodeGenWriteBarrier(&___bluePlatform_23, value);
	}

	inline static int32_t get_offset_of_purplePlatform_24() { return static_cast<int32_t>(offsetof(GameMaster_t3159518556, ___purplePlatform_24)); }
	inline GameObject_t1756533147 * get_purplePlatform_24() const { return ___purplePlatform_24; }
	inline GameObject_t1756533147 ** get_address_of_purplePlatform_24() { return &___purplePlatform_24; }
	inline void set_purplePlatform_24(GameObject_t1756533147 * value)
	{
		___purplePlatform_24 = value;
		Il2CppCodeGenWriteBarrier(&___purplePlatform_24, value);
	}

	inline static int32_t get_offset_of_maxTapsFor3Stars_25() { return static_cast<int32_t>(offsetof(GameMaster_t3159518556, ___maxTapsFor3Stars_25)); }
	inline int32_t get_maxTapsFor3Stars_25() const { return ___maxTapsFor3Stars_25; }
	inline int32_t* get_address_of_maxTapsFor3Stars_25() { return &___maxTapsFor3Stars_25; }
	inline void set_maxTapsFor3Stars_25(int32_t value)
	{
		___maxTapsFor3Stars_25 = value;
	}

	inline static int32_t get_offset_of_maxTapsFor2Stars_26() { return static_cast<int32_t>(offsetof(GameMaster_t3159518556, ___maxTapsFor2Stars_26)); }
	inline int32_t get_maxTapsFor2Stars_26() const { return ___maxTapsFor2Stars_26; }
	inline int32_t* get_address_of_maxTapsFor2Stars_26() { return &___maxTapsFor2Stars_26; }
	inline void set_maxTapsFor2Stars_26(int32_t value)
	{
		___maxTapsFor2Stars_26 = value;
	}

	inline static int32_t get_offset_of_maxTapsFor1Star_27() { return static_cast<int32_t>(offsetof(GameMaster_t3159518556, ___maxTapsFor1Star_27)); }
	inline int32_t get_maxTapsFor1Star_27() const { return ___maxTapsFor1Star_27; }
	inline int32_t* get_address_of_maxTapsFor1Star_27() { return &___maxTapsFor1Star_27; }
	inline void set_maxTapsFor1Star_27(int32_t value)
	{
		___maxTapsFor1Star_27 = value;
	}

	inline static int32_t get_offset_of_defaultTutorialString_28() { return static_cast<int32_t>(offsetof(GameMaster_t3159518556, ___defaultTutorialString_28)); }
	inline String_t* get_defaultTutorialString_28() const { return ___defaultTutorialString_28; }
	inline String_t** get_address_of_defaultTutorialString_28() { return &___defaultTutorialString_28; }
	inline void set_defaultTutorialString_28(String_t* value)
	{
		___defaultTutorialString_28 = value;
		Il2CppCodeGenWriteBarrier(&___defaultTutorialString_28, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
