﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Sprite
struct Sprite_t309593783;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// StarShower
struct  StarShower_t3709226430  : public MonoBehaviour_t1158329972
{
public:
	// System.Int32 StarShower::numberOfStars
	int32_t ___numberOfStars_2;
	// System.Boolean StarShower::locked
	bool ___locked_3;
	// UnityEngine.Sprite StarShower::lockedSprite
	Sprite_t309593783 * ___lockedSprite_4;
	// UnityEngine.Sprite StarShower::unlockedSprite
	Sprite_t309593783 * ___unlockedSprite_5;
	// System.Int32 StarShower::levelNumber
	int32_t ___levelNumber_6;

public:
	inline static int32_t get_offset_of_numberOfStars_2() { return static_cast<int32_t>(offsetof(StarShower_t3709226430, ___numberOfStars_2)); }
	inline int32_t get_numberOfStars_2() const { return ___numberOfStars_2; }
	inline int32_t* get_address_of_numberOfStars_2() { return &___numberOfStars_2; }
	inline void set_numberOfStars_2(int32_t value)
	{
		___numberOfStars_2 = value;
	}

	inline static int32_t get_offset_of_locked_3() { return static_cast<int32_t>(offsetof(StarShower_t3709226430, ___locked_3)); }
	inline bool get_locked_3() const { return ___locked_3; }
	inline bool* get_address_of_locked_3() { return &___locked_3; }
	inline void set_locked_3(bool value)
	{
		___locked_3 = value;
	}

	inline static int32_t get_offset_of_lockedSprite_4() { return static_cast<int32_t>(offsetof(StarShower_t3709226430, ___lockedSprite_4)); }
	inline Sprite_t309593783 * get_lockedSprite_4() const { return ___lockedSprite_4; }
	inline Sprite_t309593783 ** get_address_of_lockedSprite_4() { return &___lockedSprite_4; }
	inline void set_lockedSprite_4(Sprite_t309593783 * value)
	{
		___lockedSprite_4 = value;
		Il2CppCodeGenWriteBarrier(&___lockedSprite_4, value);
	}

	inline static int32_t get_offset_of_unlockedSprite_5() { return static_cast<int32_t>(offsetof(StarShower_t3709226430, ___unlockedSprite_5)); }
	inline Sprite_t309593783 * get_unlockedSprite_5() const { return ___unlockedSprite_5; }
	inline Sprite_t309593783 ** get_address_of_unlockedSprite_5() { return &___unlockedSprite_5; }
	inline void set_unlockedSprite_5(Sprite_t309593783 * value)
	{
		___unlockedSprite_5 = value;
		Il2CppCodeGenWriteBarrier(&___unlockedSprite_5, value);
	}

	inline static int32_t get_offset_of_levelNumber_6() { return static_cast<int32_t>(offsetof(StarShower_t3709226430, ___levelNumber_6)); }
	inline int32_t get_levelNumber_6() const { return ___levelNumber_6; }
	inline int32_t* get_address_of_levelNumber_6() { return &___levelNumber_6; }
	inline void set_levelNumber_6(int32_t value)
	{
		___levelNumber_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
