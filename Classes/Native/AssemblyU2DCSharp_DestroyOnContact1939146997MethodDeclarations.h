﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// DestroyOnContact
struct DestroyOnContact_t1939146997;
// UnityEngine.Collision2D
struct Collision2D_t1539500754;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Collision2D1539500754.h"

// System.Void DestroyOnContact::.ctor()
extern "C"  void DestroyOnContact__ctor_m2995891028 (DestroyOnContact_t1939146997 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DestroyOnContact::Start()
extern "C"  void DestroyOnContact_Start_m3326067956 (DestroyOnContact_t1939146997 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DestroyOnContact::Update()
extern "C"  void DestroyOnContact_Update_m3727710045 (DestroyOnContact_t1939146997 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DestroyOnContact::OnCollisionEnter2D(UnityEngine.Collision2D)
extern "C"  void DestroyOnContact_OnCollisionEnter2D_m1438149862 (DestroyOnContact_t1939146997 * __this, Collision2D_t1539500754 * ___coll0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
