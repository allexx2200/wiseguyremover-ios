﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CurrentPage
struct CurrentPage_t1988855378;

#include "codegen/il2cpp-codegen.h"

// System.Void CurrentPage::.ctor()
extern "C"  void CurrentPage__ctor_m1756353849 (CurrentPage_t1988855378 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CurrentPage::Start()
extern "C"  void CurrentPage_Start_m2011402713 (CurrentPage_t1988855378 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CurrentPage::Update()
extern "C"  void CurrentPage_Update_m4037982612 (CurrentPage_t1988855378 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
