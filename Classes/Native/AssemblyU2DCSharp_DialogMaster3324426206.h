﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Sprite[]
struct SpriteU5BU5D_t3359083662;
// Line[]
struct LineU5BU5D_t3637666283;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DialogMaster
struct  DialogMaster_t3324426206  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.Sprite[] DialogMaster::leftCharacters
	SpriteU5BU5D_t3359083662* ___leftCharacters_2;
	// UnityEngine.Sprite[] DialogMaster::rightCharacters
	SpriteU5BU5D_t3359083662* ___rightCharacters_3;
	// Line[] DialogMaster::lines
	LineU5BU5D_t3637666283* ___lines_4;

public:
	inline static int32_t get_offset_of_leftCharacters_2() { return static_cast<int32_t>(offsetof(DialogMaster_t3324426206, ___leftCharacters_2)); }
	inline SpriteU5BU5D_t3359083662* get_leftCharacters_2() const { return ___leftCharacters_2; }
	inline SpriteU5BU5D_t3359083662** get_address_of_leftCharacters_2() { return &___leftCharacters_2; }
	inline void set_leftCharacters_2(SpriteU5BU5D_t3359083662* value)
	{
		___leftCharacters_2 = value;
		Il2CppCodeGenWriteBarrier(&___leftCharacters_2, value);
	}

	inline static int32_t get_offset_of_rightCharacters_3() { return static_cast<int32_t>(offsetof(DialogMaster_t3324426206, ___rightCharacters_3)); }
	inline SpriteU5BU5D_t3359083662* get_rightCharacters_3() const { return ___rightCharacters_3; }
	inline SpriteU5BU5D_t3359083662** get_address_of_rightCharacters_3() { return &___rightCharacters_3; }
	inline void set_rightCharacters_3(SpriteU5BU5D_t3359083662* value)
	{
		___rightCharacters_3 = value;
		Il2CppCodeGenWriteBarrier(&___rightCharacters_3, value);
	}

	inline static int32_t get_offset_of_lines_4() { return static_cast<int32_t>(offsetof(DialogMaster_t3324426206, ___lines_4)); }
	inline LineU5BU5D_t3637666283* get_lines_4() const { return ___lines_4; }
	inline LineU5BU5D_t3637666283** get_address_of_lines_4() { return &___lines_4; }
	inline void set_lines_4(LineU5BU5D_t3637666283* value)
	{
		___lines_4 = value;
		Il2CppCodeGenWriteBarrier(&___lines_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
