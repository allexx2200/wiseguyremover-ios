﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// LoadSave.GameState
struct GameState_t2029468607;
// StaticData.StaticLevelContainer
struct StaticLevelContainer_t3714456515;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// StaticData.StaticLevelContainer
struct  StaticLevelContainer_t3714456515  : public Il2CppObject
{
public:
	// System.Int32 StaticData.StaticLevelContainer::levelToLoad
	int32_t ___levelToLoad_0;
	// LoadSave.GameState StaticData.StaticLevelContainer::gameState
	GameState_t2029468607 * ___gameState_1;
	// System.Int32 StaticData.StaticLevelContainer::adsCounter
	int32_t ___adsCounter_2;
	// System.Int32 StaticData.StaticLevelContainer::maxNumberOfReplays
	int32_t ___maxNumberOfReplays_3;

public:
	inline static int32_t get_offset_of_levelToLoad_0() { return static_cast<int32_t>(offsetof(StaticLevelContainer_t3714456515, ___levelToLoad_0)); }
	inline int32_t get_levelToLoad_0() const { return ___levelToLoad_0; }
	inline int32_t* get_address_of_levelToLoad_0() { return &___levelToLoad_0; }
	inline void set_levelToLoad_0(int32_t value)
	{
		___levelToLoad_0 = value;
	}

	inline static int32_t get_offset_of_gameState_1() { return static_cast<int32_t>(offsetof(StaticLevelContainer_t3714456515, ___gameState_1)); }
	inline GameState_t2029468607 * get_gameState_1() const { return ___gameState_1; }
	inline GameState_t2029468607 ** get_address_of_gameState_1() { return &___gameState_1; }
	inline void set_gameState_1(GameState_t2029468607 * value)
	{
		___gameState_1 = value;
		Il2CppCodeGenWriteBarrier(&___gameState_1, value);
	}

	inline static int32_t get_offset_of_adsCounter_2() { return static_cast<int32_t>(offsetof(StaticLevelContainer_t3714456515, ___adsCounter_2)); }
	inline int32_t get_adsCounter_2() const { return ___adsCounter_2; }
	inline int32_t* get_address_of_adsCounter_2() { return &___adsCounter_2; }
	inline void set_adsCounter_2(int32_t value)
	{
		___adsCounter_2 = value;
	}

	inline static int32_t get_offset_of_maxNumberOfReplays_3() { return static_cast<int32_t>(offsetof(StaticLevelContainer_t3714456515, ___maxNumberOfReplays_3)); }
	inline int32_t get_maxNumberOfReplays_3() const { return ___maxNumberOfReplays_3; }
	inline int32_t* get_address_of_maxNumberOfReplays_3() { return &___maxNumberOfReplays_3; }
	inline void set_maxNumberOfReplays_3(int32_t value)
	{
		___maxNumberOfReplays_3 = value;
	}
};

struct StaticLevelContainer_t3714456515_StaticFields
{
public:
	// StaticData.StaticLevelContainer StaticData.StaticLevelContainer::_instance
	StaticLevelContainer_t3714456515 * ____instance_4;

public:
	inline static int32_t get_offset_of__instance_4() { return static_cast<int32_t>(offsetof(StaticLevelContainer_t3714456515_StaticFields, ____instance_4)); }
	inline StaticLevelContainer_t3714456515 * get__instance_4() const { return ____instance_4; }
	inline StaticLevelContainer_t3714456515 ** get_address_of__instance_4() { return &____instance_4; }
	inline void set__instance_4(StaticLevelContainer_t3714456515 * value)
	{
		____instance_4 = value;
		Il2CppCodeGenWriteBarrier(&____instance_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
