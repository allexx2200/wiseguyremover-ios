﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SkipToNext
struct SkipToNext_t3002738695;

#include "codegen/il2cpp-codegen.h"

// System.Void SkipToNext::.ctor()
extern "C"  void SkipToNext__ctor_m266695162 (SkipToNext_t3002738695 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SkipToNext::Update()
extern "C"  void SkipToNext_Update_m1537027891 (SkipToNext_t3002738695 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SkipToNext::SkipScene()
extern "C"  void SkipToNext_SkipScene_m1440608173 (SkipToNext_t3002738695 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
