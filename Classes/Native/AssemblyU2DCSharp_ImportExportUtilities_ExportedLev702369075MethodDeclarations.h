﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ImportExportUtilities.ExportedLevel
struct ExportedLevel_t702369075;
// ImportExportUtilities.Level
struct Level_t336836020;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_ImportExportUtilities_Level336836020.h"

// System.Void ImportExportUtilities.ExportedLevel::.ctor(ImportExportUtilities.Level)
extern "C"  void ExportedLevel__ctor_m3218447752 (ExportedLevel_t702369075 * __this, Level_t336836020 * ___level0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
