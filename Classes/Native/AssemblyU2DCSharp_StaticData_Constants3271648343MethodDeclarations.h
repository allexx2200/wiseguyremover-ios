﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// StaticData.Constants
struct Constants_t3271648343;

#include "codegen/il2cpp-codegen.h"

// System.Void StaticData.Constants::.ctor()
extern "C"  void Constants__ctor_m1506138744 (Constants_t3271648343 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void StaticData.Constants::.cctor()
extern "C"  void Constants__cctor_m3623256087 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
