﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// LoadSave.GameState
struct GameState_t2029468607;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_LoadSave_GameState2029468607.h"
#include "mscorlib_System_String2029220233.h"

// System.Void LoadSave.GameState::.ctor(System.Int32)
extern "C"  void GameState__ctor_m4157959086 (GameState_t2029468607 * __this, int32_t ___numberOfLevels0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LoadSave.GameState::.ctor(LoadSave.GameState)
extern "C"  void GameState__ctor_m3504319115 (GameState_t2029468607 * __this, GameState_t2029468607 * ___other0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LoadSave.GameState::.ctor(System.String)
extern "C"  void GameState__ctor_m3330283085 (GameState_t2029468607 * __this, String_t* ___path0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LoadSave.GameState::Export(System.String)
extern "C"  void GameState_Export_m2636018477 (GameState_t2029468607 * __this, String_t* ___path0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
