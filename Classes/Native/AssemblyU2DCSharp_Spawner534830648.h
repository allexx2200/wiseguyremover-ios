﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Single[]
struct SingleU5BU5D_t577127397;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// UnityEngine.GameObject[]
struct GameObjectU5BU5D_t3057952154;
// LoadSave.GameState
struct GameState_t2029468607;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spawner
struct  Spawner_t534830648  : public MonoBehaviour_t1158329972
{
public:
	// System.Int32 Spawner::numberOfLevels
	int32_t ___numberOfLevels_2;
	// System.Int32 Spawner::numberOfPages
	int32_t ___numberOfPages_3;
	// System.Single[] Spawner::levelButtonPozitionsY
	SingleU5BU5D_t577127397* ___levelButtonPozitionsY_4;
	// System.Single[] Spawner::levelButtonPozitionsX
	SingleU5BU5D_t577127397* ___levelButtonPozitionsX_5;
	// System.Single Spawner::pageIdentifierPositionsY
	float ___pageIdentifierPositionsY_6;
	// System.Single Spawner::distanceBetweenPages
	float ___distanceBetweenPages_7;
	// UnityEngine.GameObject Spawner::levelButton
	GameObject_t1756533147 * ___levelButton_8;
	// UnityEngine.GameObject Spawner::pageIdentifier
	GameObject_t1756533147 * ___pageIdentifier_9;
	// UnityEngine.GameObject[] Spawner::pages
	GameObjectU5BU5D_t3057952154* ___pages_10;
	// System.Int32 Spawner::currentPage
	int32_t ___currentPage_11;
	// LoadSave.GameState Spawner::currentGameState
	GameState_t2029468607 * ___currentGameState_12;

public:
	inline static int32_t get_offset_of_numberOfLevels_2() { return static_cast<int32_t>(offsetof(Spawner_t534830648, ___numberOfLevels_2)); }
	inline int32_t get_numberOfLevels_2() const { return ___numberOfLevels_2; }
	inline int32_t* get_address_of_numberOfLevels_2() { return &___numberOfLevels_2; }
	inline void set_numberOfLevels_2(int32_t value)
	{
		___numberOfLevels_2 = value;
	}

	inline static int32_t get_offset_of_numberOfPages_3() { return static_cast<int32_t>(offsetof(Spawner_t534830648, ___numberOfPages_3)); }
	inline int32_t get_numberOfPages_3() const { return ___numberOfPages_3; }
	inline int32_t* get_address_of_numberOfPages_3() { return &___numberOfPages_3; }
	inline void set_numberOfPages_3(int32_t value)
	{
		___numberOfPages_3 = value;
	}

	inline static int32_t get_offset_of_levelButtonPozitionsY_4() { return static_cast<int32_t>(offsetof(Spawner_t534830648, ___levelButtonPozitionsY_4)); }
	inline SingleU5BU5D_t577127397* get_levelButtonPozitionsY_4() const { return ___levelButtonPozitionsY_4; }
	inline SingleU5BU5D_t577127397** get_address_of_levelButtonPozitionsY_4() { return &___levelButtonPozitionsY_4; }
	inline void set_levelButtonPozitionsY_4(SingleU5BU5D_t577127397* value)
	{
		___levelButtonPozitionsY_4 = value;
		Il2CppCodeGenWriteBarrier(&___levelButtonPozitionsY_4, value);
	}

	inline static int32_t get_offset_of_levelButtonPozitionsX_5() { return static_cast<int32_t>(offsetof(Spawner_t534830648, ___levelButtonPozitionsX_5)); }
	inline SingleU5BU5D_t577127397* get_levelButtonPozitionsX_5() const { return ___levelButtonPozitionsX_5; }
	inline SingleU5BU5D_t577127397** get_address_of_levelButtonPozitionsX_5() { return &___levelButtonPozitionsX_5; }
	inline void set_levelButtonPozitionsX_5(SingleU5BU5D_t577127397* value)
	{
		___levelButtonPozitionsX_5 = value;
		Il2CppCodeGenWriteBarrier(&___levelButtonPozitionsX_5, value);
	}

	inline static int32_t get_offset_of_pageIdentifierPositionsY_6() { return static_cast<int32_t>(offsetof(Spawner_t534830648, ___pageIdentifierPositionsY_6)); }
	inline float get_pageIdentifierPositionsY_6() const { return ___pageIdentifierPositionsY_6; }
	inline float* get_address_of_pageIdentifierPositionsY_6() { return &___pageIdentifierPositionsY_6; }
	inline void set_pageIdentifierPositionsY_6(float value)
	{
		___pageIdentifierPositionsY_6 = value;
	}

	inline static int32_t get_offset_of_distanceBetweenPages_7() { return static_cast<int32_t>(offsetof(Spawner_t534830648, ___distanceBetweenPages_7)); }
	inline float get_distanceBetweenPages_7() const { return ___distanceBetweenPages_7; }
	inline float* get_address_of_distanceBetweenPages_7() { return &___distanceBetweenPages_7; }
	inline void set_distanceBetweenPages_7(float value)
	{
		___distanceBetweenPages_7 = value;
	}

	inline static int32_t get_offset_of_levelButton_8() { return static_cast<int32_t>(offsetof(Spawner_t534830648, ___levelButton_8)); }
	inline GameObject_t1756533147 * get_levelButton_8() const { return ___levelButton_8; }
	inline GameObject_t1756533147 ** get_address_of_levelButton_8() { return &___levelButton_8; }
	inline void set_levelButton_8(GameObject_t1756533147 * value)
	{
		___levelButton_8 = value;
		Il2CppCodeGenWriteBarrier(&___levelButton_8, value);
	}

	inline static int32_t get_offset_of_pageIdentifier_9() { return static_cast<int32_t>(offsetof(Spawner_t534830648, ___pageIdentifier_9)); }
	inline GameObject_t1756533147 * get_pageIdentifier_9() const { return ___pageIdentifier_9; }
	inline GameObject_t1756533147 ** get_address_of_pageIdentifier_9() { return &___pageIdentifier_9; }
	inline void set_pageIdentifier_9(GameObject_t1756533147 * value)
	{
		___pageIdentifier_9 = value;
		Il2CppCodeGenWriteBarrier(&___pageIdentifier_9, value);
	}

	inline static int32_t get_offset_of_pages_10() { return static_cast<int32_t>(offsetof(Spawner_t534830648, ___pages_10)); }
	inline GameObjectU5BU5D_t3057952154* get_pages_10() const { return ___pages_10; }
	inline GameObjectU5BU5D_t3057952154** get_address_of_pages_10() { return &___pages_10; }
	inline void set_pages_10(GameObjectU5BU5D_t3057952154* value)
	{
		___pages_10 = value;
		Il2CppCodeGenWriteBarrier(&___pages_10, value);
	}

	inline static int32_t get_offset_of_currentPage_11() { return static_cast<int32_t>(offsetof(Spawner_t534830648, ___currentPage_11)); }
	inline int32_t get_currentPage_11() const { return ___currentPage_11; }
	inline int32_t* get_address_of_currentPage_11() { return &___currentPage_11; }
	inline void set_currentPage_11(int32_t value)
	{
		___currentPage_11 = value;
	}

	inline static int32_t get_offset_of_currentGameState_12() { return static_cast<int32_t>(offsetof(Spawner_t534830648, ___currentGameState_12)); }
	inline GameState_t2029468607 * get_currentGameState_12() const { return ___currentGameState_12; }
	inline GameState_t2029468607 ** get_address_of_currentGameState_12() { return &___currentGameState_12; }
	inline void set_currentGameState_12(GameState_t2029468607 * value)
	{
		___currentGameState_12 = value;
		Il2CppCodeGenWriteBarrier(&___currentGameState_12, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
