﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// StoryLineNextLevel
struct  StoryLineNextLevel_t1393071564  : public MonoBehaviour_t1158329972
{
public:
	// System.String StoryLineNextLevel::nextSceneName
	String_t* ___nextSceneName_2;
	// System.Single StoryLineNextLevel::secondsTillEnd
	float ___secondsTillEnd_3;
	// System.Single StoryLineNextLevel::timer
	float ___timer_4;

public:
	inline static int32_t get_offset_of_nextSceneName_2() { return static_cast<int32_t>(offsetof(StoryLineNextLevel_t1393071564, ___nextSceneName_2)); }
	inline String_t* get_nextSceneName_2() const { return ___nextSceneName_2; }
	inline String_t** get_address_of_nextSceneName_2() { return &___nextSceneName_2; }
	inline void set_nextSceneName_2(String_t* value)
	{
		___nextSceneName_2 = value;
		Il2CppCodeGenWriteBarrier(&___nextSceneName_2, value);
	}

	inline static int32_t get_offset_of_secondsTillEnd_3() { return static_cast<int32_t>(offsetof(StoryLineNextLevel_t1393071564, ___secondsTillEnd_3)); }
	inline float get_secondsTillEnd_3() const { return ___secondsTillEnd_3; }
	inline float* get_address_of_secondsTillEnd_3() { return &___secondsTillEnd_3; }
	inline void set_secondsTillEnd_3(float value)
	{
		___secondsTillEnd_3 = value;
	}

	inline static int32_t get_offset_of_timer_4() { return static_cast<int32_t>(offsetof(StoryLineNextLevel_t1393071564, ___timer_4)); }
	inline float get_timer_4() const { return ___timer_4; }
	inline float* get_address_of_timer_4() { return &___timer_4; }
	inline void set_timer_4(float value)
	{
		___timer_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
