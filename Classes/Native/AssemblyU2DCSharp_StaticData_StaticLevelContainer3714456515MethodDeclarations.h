﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// StaticData.StaticLevelContainer
struct StaticLevelContainer_t3714456515;
// LoadSave.GameState
struct GameState_t2029468607;

#include "codegen/il2cpp-codegen.h"

// System.Void StaticData.StaticLevelContainer::.ctor()
extern "C"  void StaticLevelContainer__ctor_m1801706126 (StaticLevelContainer_t3714456515 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean StaticData.StaticLevelContainer::playAd()
extern "C"  bool StaticLevelContainer_playAd_m164333081 (StaticLevelContainer_t3714456515 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// StaticData.StaticLevelContainer StaticData.StaticLevelContainer::getInstance()
extern "C"  StaticLevelContainer_t3714456515 * StaticLevelContainer_getInstance_m3684656527 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// LoadSave.GameState StaticData.StaticLevelContainer::loadLevelFromStorage()
extern "C"  GameState_t2029468607 * StaticLevelContainer_loadLevelFromStorage_m1234179298 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void StaticData.StaticLevelContainer::.cctor()
extern "C"  void StaticLevelContainer__cctor_m2563202227 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
