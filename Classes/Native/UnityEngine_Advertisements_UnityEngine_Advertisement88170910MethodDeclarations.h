﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.Advertisements.iOS.Platform/unityAdsDidStart
struct unityAdsDidStart_t88170910;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "mscorlib_System_String2029220233.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void UnityEngine.Advertisements.iOS.Platform/unityAdsDidStart::.ctor(System.Object,System.IntPtr)
extern "C"  void unityAdsDidStart__ctor_m3948634436 (unityAdsDidStart_t88170910 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Advertisements.iOS.Platform/unityAdsDidStart::Invoke(System.String)
extern "C"  void unityAdsDidStart_Invoke_m1136890932 (unityAdsDidStart_t88170910 * __this, String_t* ___placementId0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult UnityEngine.Advertisements.iOS.Platform/unityAdsDidStart::BeginInvoke(System.String,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * unityAdsDidStart_BeginInvoke_m3748639203 (unityAdsDidStart_t88170910 * __this, String_t* ___placementId0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Advertisements.iOS.Platform/unityAdsDidStart::EndInvoke(System.IAsyncResult)
extern "C"  void unityAdsDidStart_EndInvoke_m500040306 (unityAdsDidStart_t88170910 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
