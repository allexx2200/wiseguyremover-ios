﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_EventHandler_1_gen1280756467MethodDeclarations.h"

// System.Void System.EventHandler`1<UnityEngine.Advertisements.ErrorEventArgs>::.ctor(System.Object,System.IntPtr)
#define EventHandler_1__ctor_m3958207098(__this, ___object0, ___method1, method) ((  void (*) (EventHandler_1_t4277752891 *, Il2CppObject *, IntPtr_t, const MethodInfo*))EventHandler_1__ctor_m805401670_gshared)(__this, ___object0, ___method1, method)
// System.Void System.EventHandler`1<UnityEngine.Advertisements.ErrorEventArgs>::Invoke(System.Object,TEventArgs)
#define EventHandler_1_Invoke_m4092882017(__this, ___sender0, ___e1, method) ((  void (*) (EventHandler_1_t4277752891 *, Il2CppObject *, ErrorEventArgs_t1391478423 *, const MethodInfo*))EventHandler_1_Invoke_m3162899003_gshared)(__this, ___sender0, ___e1, method)
// System.IAsyncResult System.EventHandler`1<UnityEngine.Advertisements.ErrorEventArgs>::BeginInvoke(System.Object,TEventArgs,System.AsyncCallback,System.Object)
#define EventHandler_1_BeginInvoke_m2225743964(__this, ___sender0, ___e1, ___callback2, ___object3, method) ((  Il2CppObject * (*) (EventHandler_1_t4277752891 *, Il2CppObject *, ErrorEventArgs_t1391478423 *, AsyncCallback_t163412349 *, Il2CppObject *, const MethodInfo*))EventHandler_1_BeginInvoke_m2005697352_gshared)(__this, ___sender0, ___e1, ___callback2, ___object3, method)
// System.Void System.EventHandler`1<UnityEngine.Advertisements.ErrorEventArgs>::EndInvoke(System.IAsyncResult)
#define EventHandler_1_EndInvoke_m2077893308(__this, ___result0, method) ((  void (*) (EventHandler_1_t4277752891 *, Il2CppObject *, const MethodInfo*))EventHandler_1_EndInvoke_m487063176_gshared)(__this, ___result0, method)
