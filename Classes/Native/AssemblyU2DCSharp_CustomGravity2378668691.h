﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.ConstantForce2D
struct ConstantForce2D_t3766199213;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "AssemblyU2DCSharp_CustomGravity_Direction673380269.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CustomGravity
struct  CustomGravity_t2378668691  : public MonoBehaviour_t1158329972
{
public:
	// CustomGravity/Direction CustomGravity::gravityDirection
	int32_t ___gravityDirection_2;
	// UnityEngine.ConstantForce2D CustomGravity::constantForce
	ConstantForce2D_t3766199213 * ___constantForce_3;

public:
	inline static int32_t get_offset_of_gravityDirection_2() { return static_cast<int32_t>(offsetof(CustomGravity_t2378668691, ___gravityDirection_2)); }
	inline int32_t get_gravityDirection_2() const { return ___gravityDirection_2; }
	inline int32_t* get_address_of_gravityDirection_2() { return &___gravityDirection_2; }
	inline void set_gravityDirection_2(int32_t value)
	{
		___gravityDirection_2 = value;
	}

	inline static int32_t get_offset_of_constantForce_3() { return static_cast<int32_t>(offsetof(CustomGravity_t2378668691, ___constantForce_3)); }
	inline ConstantForce2D_t3766199213 * get_constantForce_3() const { return ___constantForce_3; }
	inline ConstantForce2D_t3766199213 ** get_address_of_constantForce_3() { return &___constantForce_3; }
	inline void set_constantForce_3(ConstantForce2D_t3766199213 * value)
	{
		___constantForce_3 = value;
		Il2CppCodeGenWriteBarrier(&___constantForce_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
