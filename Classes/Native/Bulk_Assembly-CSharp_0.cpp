﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

// ChangeFace
struct ChangeFace_t3885784729;
// UnityEngine.Rigidbody2D
struct Rigidbody2D_t502193897;
// System.Object
struct Il2CppObject;
// UnityEngine.SpriteRenderer
struct SpriteRenderer_t1209076198;
// UnityEngine.Animator
struct Animator_t69676727;
// CurrentPage
struct CurrentPage_t1988855378;
// UnityEngine.UI.Image
struct Image_t2042527209;
// CustomGravity
struct CustomGravity_t2378668691;
// UnityEngine.ConstantForce2D
struct ConstantForce2D_t3766199213;
// GameMaster
struct GameMaster_t3159518556;
// UnityEngine.Transform
struct Transform_t3275118058;
// DestroyOnContact
struct DestroyOnContact_t1939146997;
// UnityEngine.Collision2D
struct Collision2D_t1539500754;
// DeveloperTools.DeveloperLog
struct DeveloperLog_t2073088872;
// System.String
struct String_t;
// UnityEngine.UI.Text
struct Text_t356221433;
// DialogMaster
struct DialogMaster_t3324426206;
// Explode
struct Explode_t13292173;
// MustNotDie
struct MustNotDie_t791179140;
// MustDie
struct MustDie_t3564324517;
// Removable
struct Removable_t2119088671;
// TutorialNoNo
struct TutorialNoNo_t3242399842;
// GoToScene
struct GoToScene_t3084403383;
// ImportExportUtilities.Character
struct Character_t2307506699;
// ImportExportUtilities.ExportedLevel
struct ExportedLevel_t702369075;
// ImportExportUtilities.Level
struct Level_t336836020;
// System.Collections.Generic.List`1<ImportExportUtilities.Character>
struct List_1_t1676627831;
// System.Collections.Generic.List`1<ImportExportUtilities.Platform>
struct List_1_t372332161;
// ImportExportUtilities.Platform
struct Platform_t1003211029;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// LevelLoader
struct LevelLoader_t433131637;
// Line
struct Line_t2729441502;
// LoadSave.GameState
struct GameState_t2029468607;
// LoadSave.Level
struct Level_t1066665556;
// PausePress
struct PausePress_t1918135203;
// UnityEngine.BoxCollider2D
struct BoxCollider2D_t948534547;
// UnityEngine.CircleCollider2D
struct CircleCollider2D_t13116344;
// SkipToNext
struct SkipToNext_t3002738695;
// Spawner
struct Spawner_t534830648;
// UnityEngine.RectTransform
struct RectTransform_t3349966182;
// StarShower
struct StarShower_t3709226430;
// UnityEngine.UI.Button
struct Button_t2872111280;
// StaticData.Constants
struct Constants_t3271648343;
// StaticData.StaticLevelContainer
struct StaticLevelContainer_t3714456515;
// StoryLineNextLevel
struct StoryLineNextLevel_t1393071564;
// TutorialFollow
struct TutorialFollow_t1280665443;

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array3829468939.h"
#include "AssemblyU2DCSharp_U3CModuleU3E3783534214.h"
#include "AssemblyU2DCSharp_U3CModuleU3E3783534214MethodDeclarations.h"
#include "AssemblyU2DCSharp_U3CPrivateImplementationDetailsU1486305137.h"
#include "AssemblyU2DCSharp_U3CPrivateImplementationDetailsU1486305137MethodDeclarations.h"
#include "AssemblyU2DCSharp_U3CPrivateImplementationDetailsU2731437132.h"
#include "AssemblyU2DCSharp_U3CPrivateImplementationDetailsU2731437132MethodDeclarations.h"
#include "AssemblyU2DCSharp_ChangeFace3885784729.h"
#include "AssemblyU2DCSharp_ChangeFace3885784729MethodDeclarations.h"
#include "mscorlib_System_Void1841601450.h"
#include "UnityEngine_UnityEngine_MonoBehaviour1158329972MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Component3819376471MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Transform3275118058MethodDeclarations.h"
#include "UnityEngine_UnityEngine_GameObject1756533147MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Rigidbody2D502193897MethodDeclarations.h"
#include "UnityEngine_UnityEngine_SpriteRenderer1209076198MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Object1021602117MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Animator69676727MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Rigidbody2D502193897.h"
#include "mscorlib_System_Single2076509932.h"
#include "UnityEngine_UnityEngine_Vector22243707579.h"
#include "mscorlib_System_Boolean3825574718.h"
#include "UnityEngine_UnityEngine_GameObject1756533147.h"
#include "UnityEngine_UnityEngine_Animator69676727.h"
#include "UnityEngine_UnityEngine_Transform3275118058.h"
#include "UnityEngine_UnityEngine_Vector22243707579MethodDeclarations.h"
#include "UnityEngine_UnityEngine_SpriteRenderer1209076198.h"
#include "UnityEngine_UnityEngine_Component3819376471.h"
#include "UnityEngine_UnityEngine_Sprite309593783.h"
#include "UnityEngine_UnityEngine_Object1021602117.h"
#include "mscorlib_System_String2029220233.h"
#include "AssemblyU2DCSharp_CurrentPage1988855378.h"
#include "AssemblyU2DCSharp_CurrentPage1988855378MethodDeclarations.h"
#include "UnityEngine_UI_UnityEngine_UI_Image2042527209MethodDeclarations.h"
#include "UnityEngine_UI_UnityEngine_UI_Image2042527209.h"
#include "AssemblyU2DCSharp_CustomGravity2378668691.h"
#include "AssemblyU2DCSharp_CustomGravity2378668691MethodDeclarations.h"
#include "UnityEngine_UnityEngine_ConstantForce2D3766199213.h"
#include "UnityEngine_UnityEngine_ConstantForce2D3766199213MethodDeclarations.h"
#include "AssemblyU2DCSharp_CustomGravity_Direction673380269.h"
#include "mscorlib_System_Int322071877448.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"
#include "AssemblyU2DCSharp_GameMaster3159518556.h"
#include "UnityEngine_UnityEngine_Vector32243707580MethodDeclarations.h"
#include "AssemblyU2DCSharp_CustomGravity_Direction673380269MethodDeclarations.h"
#include "AssemblyU2DCSharp_DestroyOnContact1939146997.h"
#include "AssemblyU2DCSharp_DestroyOnContact1939146997MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Collision2D1539500754.h"
#include "UnityEngine_UnityEngine_Collision2D1539500754MethodDeclarations.h"
#include "AssemblyU2DCSharp_DeveloperTools_DeveloperLog2073088872.h"
#include "AssemblyU2DCSharp_DeveloperTools_DeveloperLog2073088872MethodDeclarations.h"
#include "mscorlib_System_Object2689449295MethodDeclarations.h"
#include "mscorlib_System_String2029220233MethodDeclarations.h"
#include "UnityEngine_UI_UnityEngine_UI_Text356221433.h"
#include "UnityEngine_UI_UnityEngine_UI_Text356221433MethodDeclarations.h"
#include "AssemblyU2DCSharp_DialogMaster3324426206.h"
#include "AssemblyU2DCSharp_DialogMaster3324426206MethodDeclarations.h"
#include "AssemblyU2DCSharp_DialogPosition3314033503.h"
#include "AssemblyU2DCSharp_DialogPosition3314033503MethodDeclarations.h"
#include "AssemblyU2DCSharp_Explode13292173.h"
#include "AssemblyU2DCSharp_Explode13292173MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Debug1368543263MethodDeclarations.h"
#include "mscorlib_System_Object2689449295.h"
#include "AssemblyU2DCSharp_GameMaster3159518556MethodDeclarations.h"
#include "AssemblyU2DCSharp_StaticData_StaticLevelContainer3714456515MethodDeclarations.h"
#include "UnityEngine_Advertisements_UnityEngine_Advertiseme3914199195MethodDeclarations.h"
#include "UnityEngine_UnityEngine_SceneManagement_SceneManager90660965MethodDeclarations.h"
#include "mscorlib_System_Int322071877448MethodDeclarations.h"
#include "UnityEngine_UnityEngine_SceneManagement_Scene1684909666.h"
#include "mscorlib_System_FormatException2948921286.h"
#include "AssemblyU2DCSharp_StaticData_StaticLevelContainer3714456515.h"
#include "UnityEngine_UnityEngine_SceneManagement_Scene1684909666MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Time31991979MethodDeclarations.h"
#include "AssemblyU2DCSharp_MustNotDie791179140.h"
#include "Assembly-CSharp_ArrayTypes.h"
#include "AssemblyU2DCSharp_MustDie3564324517.h"
#include "AssemblyU2DCSharp_LoadSave_GameState2029468607MethodDeclarations.h"
#include "mscorlib_System_NullReferenceException3156209119.h"
#include "AssemblyU2DCSharp_LoadSave_GameState2029468607.h"
#include "AssemblyU2DCSharp_LoadSave_Level1066665556.h"
#include "AssemblyU2DCSharp_StaticData_Constants3271648343.h"
#include "AssemblyU2DCSharp_StaticData_Constants3271648343MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Input1785128008MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Camera189460977MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Physics2D2540166467MethodDeclarations.h"
#include "UnityEngine_UnityEngine_RaycastHit2D4063908774MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Behaviour955675639MethodDeclarations.h"
#include "AssemblyU2DCSharp_Removable2119088671MethodDeclarations.h"
#include "UnityEngine_UnityEngine_RaycastHit2D4063908774.h"
#include "AssemblyU2DCSharp_Removable2119088671.h"
#include "AssemblyU2DCSharp_TutorialNoNo3242399842.h"
#include "UnityEngine_UnityEngine_Camera189460977.h"
#include "UnityEngine_UnityEngine_Collider2D646061738.h"
#include "AssemblyU2DCSharp_GoToScene3084403383.h"
#include "AssemblyU2DCSharp_GoToScene3084403383MethodDeclarations.h"
#include "AssemblyU2DCSharp_ImportExportUtilities_Character2307506699.h"
#include "AssemblyU2DCSharp_ImportExportUtilities_Character2307506699MethodDeclarations.h"
#include "AssemblyU2DCSharp_ImportExportUtilities_ExportedLev702369075.h"
#include "AssemblyU2DCSharp_ImportExportUtilities_ExportedLev702369075MethodDeclarations.h"
#include "AssemblyU2DCSharp_ImportExportUtilities_Level336836020.h"
#include "AssemblyU2DCSharp_ImportExportUtilities_Level336836020MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1676627831MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen372332161MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1211357505.h"
#include "AssemblyU2DCSharp_ImportExportUtilities_Platform1003211029.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera4202029131.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1676627831.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1211357505MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen372332161.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera4202029131MethodDeclarations.h"
#include "mscorlib_System_IO_File1930543328MethodDeclarations.h"
#include "UnityEngine_UnityEngine_JsonUtility653638946MethodDeclarations.h"
#include "UnityEngine_UnityEngine_JsonUtility653638946.h"
#include "AssemblyU2DCSharp_ImportExportUtilities_Platform1003211029MethodDeclarations.h"
#include "mscorlib_System_IO_StreamWriter3858580635MethodDeclarations.h"
#include "mscorlib_System_IO_StreamWriter3858580635.h"
#include "mscorlib_System_IO_TextWriter4027217640MethodDeclarations.h"
#include "mscorlib_System_IO_TextWriter4027217640.h"
#include "UnityEngine_UnityEngine_Quaternion4030073918.h"
#include "AssemblyU2DCSharp_LevelLoader433131637.h"
#include "AssemblyU2DCSharp_LevelLoader433131637MethodDeclarations.h"
#include "AssemblyU2DCSharp_Line2729441502.h"
#include "AssemblyU2DCSharp_Line2729441502MethodDeclarations.h"
#include "AssemblyU2DCSharp_LoadSave_Level1066665556MethodDeclarations.h"
#include "AssemblyU2DCSharp_MustDie3564324517MethodDeclarations.h"
#include "AssemblyU2DCSharp_MustNotDie791179140MethodDeclarations.h"
#include "AssemblyU2DCSharp_PausePress1918135203.h"
#include "AssemblyU2DCSharp_PausePress1918135203MethodDeclarations.h"
#include "UnityEngine_UnityEngine_KeyCode2283395152.h"
#include "UnityEngine_UnityEngine_Renderer257310565MethodDeclarations.h"
#include "UnityEngine_UnityEngine_BoxCollider2D948534547.h"
#include "UnityEngine_UnityEngine_CircleCollider2D13116344.h"
#include "UnityEngine_UnityEngine_SceneManagement_LoadSceneM2981886439.h"
#include "UnityEngine_UnityEngine_Color2020392075.h"
#include "AssemblyU2DCSharp_SkipToNext3002738695.h"
#include "AssemblyU2DCSharp_SkipToNext3002738695MethodDeclarations.h"
#include "AssemblyU2DCSharp_Spawner534830648.h"
#include "AssemblyU2DCSharp_Spawner534830648MethodDeclarations.h"
#include "mscorlib_System_Runtime_CompilerServices_RuntimeHel266230107MethodDeclarations.h"
#include "mscorlib_ArrayTypes.h"
#include "mscorlib_System_RuntimeFieldHandle2331729674.h"
#include "UnityEngine_ArrayTypes.h"
#include "UnityEngine_UnityEngine_RectTransform3349966182.h"
#include "AssemblyU2DCSharp_StarShower3709226430MethodDeclarations.h"
#include "AssemblyU2DCSharp_StarShower3709226430.h"
#include "UnityEngine_UI_UnityEngine_UI_Button2872111280.h"
#include "UnityEngine_UI_UnityEngine_UI_Selectable1490392188MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Application354826772MethodDeclarations.h"
#include "mscorlib_System_IO_FileNotFoundException4200667904.h"
#include "mscorlib_System_Exception1927440687.h"
#include "mscorlib_System_IO_IsolatedStorage_IsolatedStorage2011779685.h"
#include "AssemblyU2DCSharp_StoryLineNextLevel1393071564.h"
#include "AssemblyU2DCSharp_StoryLineNextLevel1393071564MethodDeclarations.h"
#include "AssemblyU2DCSharp_TutorialFollow1280665443.h"
#include "AssemblyU2DCSharp_TutorialFollow1280665443MethodDeclarations.h"
#include "AssemblyU2DCSharp_TutorialNoNo3242399842MethodDeclarations.h"

// !!0 UnityEngine.GameObject::GetComponent<System.Object>()
extern "C"  Il2CppObject * GameObject_GetComponent_TisIl2CppObject_m2812611596_gshared (GameObject_t1756533147 * __this, const MethodInfo* method);
#define GameObject_GetComponent_TisIl2CppObject_m2812611596(__this, method) ((  Il2CppObject * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m2812611596_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponent<UnityEngine.Rigidbody2D>()
#define GameObject_GetComponent_TisRigidbody2D_t502193897_m812242143(__this, method) ((  Rigidbody2D_t502193897 * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m2812611596_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<System.Object>()
extern "C"  Il2CppObject * Component_GetComponent_TisIl2CppObject_m4109961936_gshared (Component_t3819376471 * __this, const MethodInfo* method);
#define Component_GetComponent_TisIl2CppObject_m4109961936(__this, method) ((  Il2CppObject * (*) (Component_t3819376471 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m4109961936_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<UnityEngine.SpriteRenderer>()
#define Component_GetComponent_TisSpriteRenderer_t1209076198_m2178781570(__this, method) ((  SpriteRenderer_t1209076198 * (*) (Component_t3819376471 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m4109961936_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponent<UnityEngine.Animator>()
#define GameObject_GetComponent_TisAnimator_t69676727_m2717502299(__this, method) ((  Animator_t69676727 * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m2812611596_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<UnityEngine.UI.Image>()
#define Component_GetComponent_TisImage_t2042527209_m2189462422(__this, method) ((  Image_t2042527209 * (*) (Component_t3819376471 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m4109961936_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<UnityEngine.ConstantForce2D>()
#define Component_GetComponent_TisConstantForce2D_t3766199213_m2952855213(__this, method) ((  ConstantForce2D_t3766199213 * (*) (Component_t3819376471 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m4109961936_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponent<GameMaster>()
#define GameObject_GetComponent_TisGameMaster_t3159518556_m3423764161(__this, method) ((  GameMaster_t3159518556 * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m2812611596_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponent<UnityEngine.Transform>()
#define GameObject_GetComponent_TisTransform_t3275118058_m791278180(__this, method) ((  Transform_t3275118058 * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m2812611596_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponent<UnityEngine.UI.Text>()
#define GameObject_GetComponent_TisText_t356221433_m1217399699(__this, method) ((  Text_t356221433 * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m2812611596_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponent<MustNotDie>()
#define GameObject_GetComponent_TisMustNotDie_t791179140_m3796240729(__this, method) ((  MustNotDie_t791179140 * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m2812611596_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponent<MustDie>()
#define GameObject_GetComponent_TisMustDie_t3564324517_m2755846400(__this, method) ((  MustDie_t3564324517 * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m2812611596_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponent<Removable>()
#define GameObject_GetComponent_TisRemovable_t2119088671_m2134440904(__this, method) ((  Removable_t2119088671 * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m2812611596_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponent<Explode>()
#define GameObject_GetComponent_TisExplode_t13292173_m1264009640(__this, method) ((  Explode_t13292173 * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m2812611596_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponent<TutorialNoNo>()
#define GameObject_GetComponent_TisTutorialNoNo_t3242399842_m3271948131(__this, method) ((  TutorialNoNo_t3242399842 * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m2812611596_gshared)(__this, method)
// !!0 UnityEngine.JsonUtility::FromJson<System.Object>(System.String)
extern "C"  Il2CppObject * JsonUtility_FromJson_TisIl2CppObject_m3451544451_gshared (Il2CppObject * __this /* static, unused */, String_t* p0, const MethodInfo* method);
#define JsonUtility_FromJson_TisIl2CppObject_m3451544451(__this /* static, unused */, p0, method) ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, String_t*, const MethodInfo*))JsonUtility_FromJson_TisIl2CppObject_m3451544451_gshared)(__this /* static, unused */, p0, method)
// !!0 UnityEngine.JsonUtility::FromJson<ImportExportUtilities.ExportedLevel>(System.String)
#define JsonUtility_FromJson_TisExportedLevel_t702369075_m2447477843(__this /* static, unused */, p0, method) ((  ExportedLevel_t702369075 * (*) (Il2CppObject * /* static, unused */, String_t*, const MethodInfo*))JsonUtility_FromJson_TisIl2CppObject_m3451544451_gshared)(__this /* static, unused */, p0, method)
// !!0 UnityEngine.Object::Instantiate<System.Object>(!!0)
extern "C"  Il2CppObject * Object_Instantiate_TisIl2CppObject_m447919519_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * p0, const MethodInfo* method);
#define Object_Instantiate_TisIl2CppObject_m447919519(__this /* static, unused */, p0, method) ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))Object_Instantiate_TisIl2CppObject_m447919519_gshared)(__this /* static, unused */, p0, method)
// !!0 UnityEngine.Object::Instantiate<UnityEngine.GameObject>(!!0)
#define Object_Instantiate_TisGameObject_t1756533147_m3664764861(__this /* static, unused */, p0, method) ((  GameObject_t1756533147 * (*) (Il2CppObject * /* static, unused */, GameObject_t1756533147 *, const MethodInfo*))Object_Instantiate_TisIl2CppObject_m447919519_gshared)(__this /* static, unused */, p0, method)
// !!0 UnityEngine.GameObject::GetComponent<CustomGravity>()
#define GameObject_GetComponent_TisCustomGravity_t2378668691_m2385254694(__this, method) ((  CustomGravity_t2378668691 * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m2812611596_gshared)(__this, method)
// !!0 UnityEngine.JsonUtility::FromJson<LoadSave.GameState>(System.String)
#define JsonUtility_FromJson_TisGameState_t2029468607_m3146932469(__this /* static, unused */, p0, method) ((  GameState_t2029468607 * (*) (Il2CppObject * /* static, unused */, String_t*, const MethodInfo*))JsonUtility_FromJson_TisIl2CppObject_m3451544451_gshared)(__this /* static, unused */, p0, method)
// !!0 UnityEngine.GameObject::GetComponent<UnityEngine.UI.Image>()
#define GameObject_GetComponent_TisImage_t2042527209_m4162535761(__this, method) ((  Image_t2042527209 * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m2812611596_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponent<UnityEngine.SpriteRenderer>()
#define GameObject_GetComponent_TisSpriteRenderer_t1209076198_m1184556631(__this, method) ((  SpriteRenderer_t1209076198 * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m2812611596_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponent<UnityEngine.BoxCollider2D>()
#define GameObject_GetComponent_TisBoxCollider2D_t948534547_m1829960061(__this, method) ((  BoxCollider2D_t948534547 * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m2812611596_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponent<UnityEngine.CircleCollider2D>()
#define GameObject_GetComponent_TisCircleCollider2D_t13116344_m3645672388(__this, method) ((  CircleCollider2D_t13116344 * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m2812611596_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponent<UnityEngine.ConstantForce2D>()
#define GameObject_GetComponent_TisConstantForce2D_t3766199213_m3058379513(__this, method) ((  ConstantForce2D_t3766199213 * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m2812611596_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<UnityEngine.Animator>()
#define Component_GetComponent_TisAnimator_t69676727_m475627522(__this, method) ((  Animator_t69676727 * (*) (Component_t3819376471 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m4109961936_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<UnityEngine.BoxCollider2D>()
#define Component_GetComponent_TisBoxCollider2D_t948534547_m324820273(__this, method) ((  BoxCollider2D_t948534547 * (*) (Component_t3819376471 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m4109961936_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<UnityEngine.CircleCollider2D>()
#define Component_GetComponent_TisCircleCollider2D_t13116344_m1279094442(__this, method) ((  CircleCollider2D_t13116344 * (*) (Component_t3819376471 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m4109961936_gshared)(__this, method)
// !!0 UnityEngine.Object::Instantiate<System.Object>(!!0,UnityEngine.Transform)
extern "C"  Il2CppObject * Object_Instantiate_TisIl2CppObject_m681991875_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * p0, Transform_t3275118058 * p1, const MethodInfo* method);
#define Object_Instantiate_TisIl2CppObject_m681991875(__this /* static, unused */, p0, p1, method) ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, Il2CppObject *, Transform_t3275118058 *, const MethodInfo*))Object_Instantiate_TisIl2CppObject_m681991875_gshared)(__this /* static, unused */, p0, p1, method)
// !!0 UnityEngine.Object::Instantiate<UnityEngine.GameObject>(!!0,UnityEngine.Transform)
#define Object_Instantiate_TisGameObject_t1756533147_m3066053529(__this /* static, unused */, p0, p1, method) ((  GameObject_t1756533147 * (*) (Il2CppObject * /* static, unused */, GameObject_t1756533147 *, Transform_t3275118058 *, const MethodInfo*))Object_Instantiate_TisIl2CppObject_m681991875_gshared)(__this /* static, unused */, p0, p1, method)
// !!0 UnityEngine.GameObject::GetComponent<UnityEngine.RectTransform>()
#define GameObject_GetComponent_TisRectTransform_t3349966182_m132995507(__this, method) ((  RectTransform_t3349966182 * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m2812611596_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponent<CurrentPage>()
#define GameObject_GetComponent_TisCurrentPage_t1988855378_m3044519405(__this, method) ((  CurrentPage_t1988855378 * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m2812611596_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<UnityEngine.UI.Text>()
#define Component_GetComponent_TisText_t356221433_m1342661039(__this, method) ((  Text_t356221433 * (*) (Component_t3819376471 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m4109961936_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponent<StarShower>()
#define GameObject_GetComponent_TisStarShower_t3709226430_m1692839511(__this, method) ((  StarShower_t3709226430 * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m2812611596_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponent<UnityEngine.UI.Button>()
#define GameObject_GetComponent_TisButton_t2872111280_m1008560876(__this, method) ((  Button_t2872111280 * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m2812611596_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<UnityEngine.RectTransform>()
#define Component_GetComponent_TisRectTransform_t3349966182_m1310250299(__this, method) ((  RectTransform_t3349966182 * (*) (Component_t3819376471 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m4109961936_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<UnityEngine.UI.Button>()
#define Component_GetComponent_TisButton_t2872111280_m3412601438(__this, method) ((  Button_t2872111280 * (*) (Component_t3819376471 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m4109961936_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponent<PausePress>()
#define GameObject_GetComponent_TisPausePress_t1918135203_m3658604164(__this, method) ((  PausePress_t1918135203 * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m2812611596_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponent<TutorialFollow>()
#define GameObject_GetComponent_TisTutorialFollow_t1280665443_m4229171454(__this, method) ((  TutorialFollow_t1280665443 * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m2812611596_gshared)(__this, method)
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void ChangeFace::.ctor()
extern "C"  void ChangeFace__ctor_m2504436316 (ChangeFace_t3885784729 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ChangeFace::Start()
extern "C"  void ChangeFace_Start_m3655810180 (ChangeFace_t3885784729 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void ChangeFace::Update()
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisRigidbody2D_t502193897_m812242143_MethodInfo_var;
extern const MethodInfo* Component_GetComponent_TisSpriteRenderer_t1209076198_m2178781570_MethodInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisAnimator_t69676727_m2717502299_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral1547773730;
extern const uint32_t ChangeFace_Update_m2745119365_MetadataUsageId;
extern "C"  void ChangeFace_Update_m2745119365 (ChangeFace_t3885784729 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ChangeFace_Update_m2745119365_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Rigidbody2D_t502193897 * V_0 = NULL;
	float V_1 = 0.0f;
	Vector2_t2243707579  V_2;
	memset(&V_2, 0, sizeof(V_2));
	bool V_3 = false;
	GameObject_t1756533147 * V_4 = NULL;
	Animator_t69676727 * V_5 = NULL;
	{
		Transform_t3275118058 * L_0 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		Transform_t3275118058 * L_1 = Transform_get_parent_m147407266(L_0, /*hidden argument*/NULL);
		NullCheck(L_1);
		GameObject_t1756533147 * L_2 = Component_get_gameObject_m3105766835(L_1, /*hidden argument*/NULL);
		NullCheck(L_2);
		Rigidbody2D_t502193897 * L_3 = GameObject_GetComponent_TisRigidbody2D_t502193897_m812242143(L_2, /*hidden argument*/GameObject_GetComponent_TisRigidbody2D_t502193897_m812242143_MethodInfo_var);
		V_0 = L_3;
		Rigidbody2D_t502193897 * L_4 = V_0;
		NullCheck(L_4);
		Vector2_t2243707579  L_5 = Rigidbody2D_get_velocity_m3310151195(L_4, /*hidden argument*/NULL);
		V_2 = L_5;
		float L_6 = Vector2_get_magnitude_m33802565((&V_2), /*hidden argument*/NULL);
		V_1 = L_6;
		float L_7 = V_1;
		float L_8 = __this->get_speedThreashold_4();
		V_3 = (bool)((((float)L_7) > ((float)L_8))? 1 : 0);
		bool L_9 = V_3;
		if (!L_9)
		{
			goto IL_004b;
		}
	}
	{
		SpriteRenderer_t1209076198 * L_10 = Component_GetComponent_TisSpriteRenderer_t1209076198_m2178781570(__this, /*hidden argument*/Component_GetComponent_TisSpriteRenderer_t1209076198_m2178781570_MethodInfo_var);
		Sprite_t309593783 * L_11 = __this->get_scaredFace_3();
		NullCheck(L_10);
		SpriteRenderer_set_sprite_m617298623(L_10, L_11, /*hidden argument*/NULL);
		goto IL_005c;
	}

IL_004b:
	{
		SpriteRenderer_t1209076198 * L_12 = Component_GetComponent_TisSpriteRenderer_t1209076198_m2178781570(__this, /*hidden argument*/Component_GetComponent_TisSpriteRenderer_t1209076198_m2178781570_MethodInfo_var);
		Sprite_t309593783 * L_13 = __this->get_normalFace_2();
		NullCheck(L_12);
		SpriteRenderer_set_sprite_m617298623(L_12, L_13, /*hidden argument*/NULL);
	}

IL_005c:
	{
		Transform_t3275118058 * L_14 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_14);
		Transform_t3275118058 * L_15 = Transform_get_parent_m147407266(L_14, /*hidden argument*/NULL);
		NullCheck(L_15);
		GameObject_t1756533147 * L_16 = Component_get_gameObject_m3105766835(L_15, /*hidden argument*/NULL);
		V_4 = L_16;
		GameObject_t1756533147 * L_17 = V_4;
		NullCheck(L_17);
		Animator_t69676727 * L_18 = GameObject_GetComponent_TisAnimator_t69676727_m2717502299(L_17, /*hidden argument*/GameObject_GetComponent_TisAnimator_t69676727_m2717502299_MethodInfo_var);
		V_5 = L_18;
		Animator_t69676727 * L_19 = V_5;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_20 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_19, /*hidden argument*/NULL);
		if (!L_20)
		{
			goto IL_0090;
		}
	}
	{
		Animator_t69676727 * L_21 = V_5;
		bool L_22 = V_3;
		NullCheck(L_21);
		Animator_SetBool_m2305662531(L_21, _stringLiteral1547773730, L_22, /*hidden argument*/NULL);
	}

IL_0090:
	{
		return;
	}
}
// System.Void CurrentPage::.ctor()
extern "C"  void CurrentPage__ctor_m1756353849 (CurrentPage_t1988855378 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void CurrentPage::Start()
extern "C"  void CurrentPage_Start_m2011402713 (CurrentPage_t1988855378 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void CurrentPage::Update()
extern const MethodInfo* Component_GetComponent_TisImage_t2042527209_m2189462422_MethodInfo_var;
extern const uint32_t CurrentPage_Update_m4037982612_MetadataUsageId;
extern "C"  void CurrentPage_Update_m4037982612 (CurrentPage_t1988855378 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CurrentPage_Update_m4037982612_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = __this->get_isCurrentPage_4();
		if (!L_0)
		{
			goto IL_0021;
		}
	}
	{
		Image_t2042527209 * L_1 = Component_GetComponent_TisImage_t2042527209_m2189462422(__this, /*hidden argument*/Component_GetComponent_TisImage_t2042527209_m2189462422_MethodInfo_var);
		Sprite_t309593783 * L_2 = __this->get_activePage_2();
		NullCheck(L_1);
		Image_set_sprite_m1800056820(L_1, L_2, /*hidden argument*/NULL);
		goto IL_0032;
	}

IL_0021:
	{
		Image_t2042527209 * L_3 = Component_GetComponent_TisImage_t2042527209_m2189462422(__this, /*hidden argument*/Component_GetComponent_TisImage_t2042527209_m2189462422_MethodInfo_var);
		Sprite_t309593783 * L_4 = __this->get_inactivePage_3();
		NullCheck(L_3);
		Image_set_sprite_m1800056820(L_3, L_4, /*hidden argument*/NULL);
	}

IL_0032:
	{
		return;
	}
}
// System.Void CustomGravity::.ctor()
extern "C"  void CustomGravity__ctor_m2276211818 (CustomGravity_t2378668691 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void CustomGravity::Start()
extern const MethodInfo* Component_GetComponent_TisConstantForce2D_t3766199213_m2952855213_MethodInfo_var;
extern const uint32_t CustomGravity_Start_m1628625966_MetadataUsageId;
extern "C"  void CustomGravity_Start_m1628625966 (CustomGravity_t2378668691 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CustomGravity_Start_m1628625966_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		ConstantForce2D_t3766199213 * L_0 = Component_GetComponent_TisConstantForce2D_t3766199213_m2952855213(__this, /*hidden argument*/Component_GetComponent_TisConstantForce2D_t3766199213_m2952855213_MethodInfo_var);
		__this->set_constantForce_3(L_0);
		return;
	}
}
// System.Void CustomGravity::Update()
extern const MethodInfo* GameObject_GetComponent_TisGameMaster_t3159518556_m3423764161_MethodInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisTransform_t3275118058_m791278180_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral3887597074;
extern const uint32_t CustomGravity_Update_m735476087_MetadataUsageId;
extern "C"  void CustomGravity_Update_m735476087 (CustomGravity_t2378668691 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CustomGravity_Update_m735476087_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	GameObject_t1756533147 * V_4 = NULL;
	Vector3_t2243707580  V_5;
	memset(&V_5, 0, sizeof(V_5));
	{
		GameObject_t1756533147 * L_0 = GameObject_Find_m836511350(NULL /*static, unused*/, _stringLiteral3887597074, /*hidden argument*/NULL);
		NullCheck(L_0);
		GameMaster_t3159518556 * L_1 = GameObject_GetComponent_TisGameMaster_t3159518556_m3423764161(L_0, /*hidden argument*/GameObject_GetComponent_TisGameMaster_t3159518556_m3423764161_MethodInfo_var);
		NullCheck(L_1);
		float L_2 = L_1->get_gravity_11();
		V_0 = L_2;
		V_1 = (0.0f);
		int32_t L_3 = __this->get_gravityDirection_2();
		V_2 = L_3;
		int32_t L_4 = V_2;
		if (L_4 == 0)
		{
			goto IL_003d;
		}
		if (L_4 == 1)
		{
			goto IL_005e;
		}
		if (L_4 == 2)
		{
			goto IL_0079;
		}
		if (L_4 == 3)
		{
			goto IL_009a;
		}
	}
	{
		goto IL_00bb;
	}

IL_003d:
	{
		ConstantForce2D_t3766199213 * L_5 = __this->get_constantForce_3();
		float L_6 = V_0;
		Vector2_t2243707579  L_7 = Vector2_get_up_m977201173(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector2_t2243707579  L_8 = Vector2_op_Multiply_m3393065202(NULL /*static, unused*/, L_6, L_7, /*hidden argument*/NULL);
		NullCheck(L_5);
		ConstantForce2D_set_force_m1425292042(L_5, L_8, /*hidden argument*/NULL);
		V_1 = (180.0f);
		goto IL_00bb;
	}

IL_005e:
	{
		ConstantForce2D_t3766199213 * L_9 = __this->get_constantForce_3();
		float L_10 = V_0;
		Vector2_t2243707579  L_11 = Vector2_get_down_m516190382(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector2_t2243707579  L_12 = Vector2_op_Multiply_m3393065202(NULL /*static, unused*/, L_10, L_11, /*hidden argument*/NULL);
		NullCheck(L_9);
		ConstantForce2D_set_force_m1425292042(L_9, L_12, /*hidden argument*/NULL);
		goto IL_00bb;
	}

IL_0079:
	{
		ConstantForce2D_t3766199213 * L_13 = __this->get_constantForce_3();
		float L_14 = V_0;
		Vector2_t2243707579  L_15 = Vector2_get_left_m573266379(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector2_t2243707579  L_16 = Vector2_op_Multiply_m3393065202(NULL /*static, unused*/, L_14, L_15, /*hidden argument*/NULL);
		NullCheck(L_13);
		ConstantForce2D_set_force_m1425292042(L_13, L_16, /*hidden argument*/NULL);
		V_1 = (270.0f);
		goto IL_00bb;
	}

IL_009a:
	{
		ConstantForce2D_t3766199213 * L_17 = __this->get_constantForce_3();
		float L_18 = V_0;
		Vector2_t2243707579  L_19 = Vector2_get_right_m28012078(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector2_t2243707579  L_20 = Vector2_op_Multiply_m3393065202(NULL /*static, unused*/, L_18, L_19, /*hidden argument*/NULL);
		NullCheck(L_17);
		ConstantForce2D_set_force_m1425292042(L_17, L_20, /*hidden argument*/NULL);
		V_1 = (90.0f);
		goto IL_00bb;
	}

IL_00bb:
	{
		V_3 = 0;
		goto IL_0107;
	}

IL_00c2:
	{
		Transform_t3275118058 * L_21 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		int32_t L_22 = V_3;
		NullCheck(L_21);
		Transform_t3275118058 * L_23 = Transform_GetChild_m3838588184(L_21, L_22, /*hidden argument*/NULL);
		NullCheck(L_23);
		GameObject_t1756533147 * L_24 = Component_get_gameObject_m3105766835(L_23, /*hidden argument*/NULL);
		V_4 = L_24;
		GameObject_t1756533147 * L_25 = V_4;
		NullCheck(L_25);
		Transform_t3275118058 * L_26 = GameObject_GetComponent_TisTransform_t3275118058_m791278180(L_25, /*hidden argument*/GameObject_GetComponent_TisTransform_t3275118058_m791278180_MethodInfo_var);
		NullCheck(L_26);
		Vector3_t2243707580  L_27 = Transform_get_localEulerAngles_m4231787854(L_26, /*hidden argument*/NULL);
		V_5 = L_27;
		GameObject_t1756533147 * L_28 = V_4;
		NullCheck(L_28);
		Transform_t3275118058 * L_29 = GameObject_GetComponent_TisTransform_t3275118058_m791278180(L_28, /*hidden argument*/GameObject_GetComponent_TisTransform_t3275118058_m791278180_MethodInfo_var);
		float L_30 = (&V_5)->get_x_1();
		float L_31 = (&V_5)->get_y_2();
		float L_32 = V_1;
		Vector3_t2243707580  L_33;
		memset(&L_33, 0, sizeof(L_33));
		Vector3__ctor_m2638739322(&L_33, L_30, L_31, L_32, /*hidden argument*/NULL);
		NullCheck(L_29);
		Transform_set_localEulerAngles_m2927195985(L_29, L_33, /*hidden argument*/NULL);
		int32_t L_34 = V_3;
		V_3 = ((int32_t)((int32_t)L_34+(int32_t)1));
	}

IL_0107:
	{
		int32_t L_35 = V_3;
		Transform_t3275118058 * L_36 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_36);
		int32_t L_37 = Transform_get_childCount_m881385315(L_36, /*hidden argument*/NULL);
		if ((((int32_t)L_35) < ((int32_t)L_37)))
		{
			goto IL_00c2;
		}
	}
	{
		return;
	}
}
// System.Void DestroyOnContact::.ctor()
extern "C"  void DestroyOnContact__ctor_m2995891028 (DestroyOnContact_t1939146997 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void DestroyOnContact::Start()
extern "C"  void DestroyOnContact_Start_m3326067956 (DestroyOnContact_t1939146997 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void DestroyOnContact::Update()
extern "C"  void DestroyOnContact_Update_m3727710045 (DestroyOnContact_t1939146997 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void DestroyOnContact::OnCollisionEnter2D(UnityEngine.Collision2D)
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const uint32_t DestroyOnContact_OnCollisionEnter2D_m1438149862_MetadataUsageId;
extern "C"  void DestroyOnContact_OnCollisionEnter2D_m1438149862 (DestroyOnContact_t1939146997 * __this, Collision2D_t1539500754 * ___coll0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DestroyOnContact_OnCollisionEnter2D_m1438149862_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Collision2D_t1539500754 * L_0 = ___coll0;
		NullCheck(L_0);
		GameObject_t1756533147 * L_1 = Collision2D_get_gameObject_m4234358314(L_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		Object_Destroy_m4145850038(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void DeveloperTools.DeveloperLog::.ctor()
extern "C"  void DeveloperLog__ctor_m1268392924 (DeveloperLog_t2073088872 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void DeveloperTools.DeveloperLog::LogMessage(System.String)
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisText_t356221433_m1217399699_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral161058078;
extern Il2CppCodeGenString* _stringLiteral372029352;
extern const uint32_t DeveloperLog_LogMessage_m3766158779_MetadataUsageId;
extern "C"  void DeveloperLog_LogMessage_m3766158779 (Il2CppObject * __this /* static, unused */, String_t* ___message0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DeveloperLog_LogMessage_m3766158779_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	GameObject_t1756533147 * V_0 = NULL;
	{
		GameObject_t1756533147 * L_0 = GameObject_Find_m836511350(NULL /*static, unused*/, _stringLiteral161058078, /*hidden argument*/NULL);
		V_0 = L_0;
		GameObject_t1756533147 * L_1 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_2 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0037;
		}
	}
	{
		GameObject_t1756533147 * L_3 = V_0;
		NullCheck(L_3);
		Text_t356221433 * L_4 = GameObject_GetComponent_TisText_t356221433_m1217399699(L_3, /*hidden argument*/GameObject_GetComponent_TisText_t356221433_m1217399699_MethodInfo_var);
		String_t* L_5 = ___message0;
		GameObject_t1756533147 * L_6 = V_0;
		NullCheck(L_6);
		Text_t356221433 * L_7 = GameObject_GetComponent_TisText_t356221433_m1217399699(L_6, /*hidden argument*/GameObject_GetComponent_TisText_t356221433_m1217399699_MethodInfo_var);
		NullCheck(L_7);
		String_t* L_8 = VirtFuncInvoker0< String_t* >::Invoke(71 /* System.String UnityEngine.UI.Text::get_text() */, L_7);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_9 = String_Concat_m612901809(NULL /*static, unused*/, L_5, _stringLiteral372029352, L_8, /*hidden argument*/NULL);
		NullCheck(L_4);
		VirtActionInvoker1< String_t* >::Invoke(72 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_4, L_9);
	}

IL_0037:
	{
		return;
	}
}
// System.Void DialogMaster::.ctor()
extern "C"  void DialogMaster__ctor_m3879060931 (DialogMaster_t3324426206 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void DialogMaster::Start()
extern "C"  void DialogMaster_Start_m3599621375 (DialogMaster_t3324426206 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void DialogMaster::Update()
extern "C"  void DialogMaster_Update_m3687772396 (DialogMaster_t3324426206 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void Explode::.ctor()
extern "C"  void Explode__ctor_m1586866248 (Explode_t13292173 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Explode::Start()
extern "C"  void Explode_Start_m1253663336 (Explode_t13292173 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void Explode::Update()
extern "C"  void Explode_Update_m3143739981 (Explode_t13292173 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void Explode::Explosion()
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern Il2CppClass* Vector2_t2243707579_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisRigidbody2D_t502193897_m812242143_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral2830371216;
extern Il2CppCodeGenString* _stringLiteral18119008;
extern const uint32_t Explode_Explosion_m1359633813_MetadataUsageId;
extern "C"  void Explode_Explosion_m1359633813 (Explode_t13292173 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Explode_Explosion_m1359633813_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	GameObject_t1756533147 * V_0 = NULL;
	int32_t V_1 = 0;
	GameObject_t1756533147 * V_2 = NULL;
	Rigidbody2D_t502193897 * V_3 = NULL;
	float V_4 = 0.0f;
	Vector3_t2243707580  V_5;
	memset(&V_5, 0, sizeof(V_5));
	Vector2_t2243707579  V_6;
	memset(&V_6, 0, sizeof(V_6));
	{
		GameObject_t1756533147 * L_0 = GameObject_Find_m836511350(NULL /*static, unused*/, _stringLiteral2830371216, /*hidden argument*/NULL);
		V_0 = L_0;
		GameObject_t1756533147 * L_1 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_2 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		if (L_2)
		{
			goto IL_0021;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_LogError_m3715728798(NULL /*static, unused*/, _stringLiteral18119008, /*hidden argument*/NULL);
		return;
	}

IL_0021:
	{
		V_1 = 0;
		goto IL_00e6;
	}

IL_0028:
	{
		GameObject_t1756533147 * L_3 = V_0;
		NullCheck(L_3);
		Transform_t3275118058 * L_4 = GameObject_get_transform_m909382139(L_3, /*hidden argument*/NULL);
		int32_t L_5 = V_1;
		NullCheck(L_4);
		Transform_t3275118058 * L_6 = Transform_GetChild_m3838588184(L_4, L_5, /*hidden argument*/NULL);
		NullCheck(L_6);
		GameObject_t1756533147 * L_7 = Component_get_gameObject_m3105766835(L_6, /*hidden argument*/NULL);
		V_2 = L_7;
		GameObject_t1756533147 * L_8 = V_2;
		NullCheck(L_8);
		Rigidbody2D_t502193897 * L_9 = GameObject_GetComponent_TisRigidbody2D_t502193897_m812242143(L_8, /*hidden argument*/GameObject_GetComponent_TisRigidbody2D_t502193897_m812242143_MethodInfo_var);
		V_3 = L_9;
		Rigidbody2D_t502193897 * L_10 = V_3;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_11 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_10, /*hidden argument*/NULL);
		if (!L_11)
		{
			goto IL_00e2;
		}
	}
	{
		Transform_t3275118058 * L_12 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_12);
		Vector3_t2243707580  L_13 = Transform_get_position_m1104419803(L_12, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_14 = V_2;
		NullCheck(L_14);
		Transform_t3275118058 * L_15 = GameObject_get_transform_m909382139(L_14, /*hidden argument*/NULL);
		NullCheck(L_15);
		Vector3_t2243707580  L_16 = Transform_get_position_m1104419803(L_15, /*hidden argument*/NULL);
		Vector3_t2243707580  L_17 = Vector3_op_Subtraction_m2407545601(NULL /*static, unused*/, L_13, L_16, /*hidden argument*/NULL);
		V_5 = L_17;
		float L_18 = Vector3_get_magnitude_m860342598((&V_5), /*hidden argument*/NULL);
		V_4 = L_18;
		GameObject_t1756533147 * L_19 = V_2;
		NullCheck(L_19);
		Transform_t3275118058 * L_20 = GameObject_get_transform_m909382139(L_19, /*hidden argument*/NULL);
		NullCheck(L_20);
		Vector3_t2243707580  L_21 = Transform_get_position_m1104419803(L_20, /*hidden argument*/NULL);
		Transform_t3275118058 * L_22 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_22);
		Vector3_t2243707580  L_23 = Transform_get_position_m1104419803(L_22, /*hidden argument*/NULL);
		Vector3_t2243707580  L_24 = Vector3_op_Subtraction_m2407545601(NULL /*static, unused*/, L_21, L_23, /*hidden argument*/NULL);
		Vector2_t2243707579  L_25 = Vector2_op_Implicit_m1064335535(NULL /*static, unused*/, L_24, /*hidden argument*/NULL);
		V_6 = L_25;
		Vector2_Normalize_m2465777963((&V_6), /*hidden argument*/NULL);
		Rigidbody2D_t502193897 * L_26 = V_3;
		Rigidbody2D_t502193897 * L_27 = V_3;
		NullCheck(L_27);
		Vector2_t2243707579  L_28 = Rigidbody2D_get_velocity_m3310151195(L_27, /*hidden argument*/NULL);
		float L_29 = __this->get_explosionCoeficient_2();
		float L_30 = V_4;
		Vector2_t2243707579  L_31 = V_6;
		Vector2_t2243707579  L_32 = Vector2_op_Multiply_m3393065202(NULL /*static, unused*/, ((float)((float)L_29/(float)((float)((float)(1.0f)+(float)L_30)))), L_31, /*hidden argument*/NULL);
		Vector2_t2243707579  L_33 = Vector2_op_Addition_m1389598521(NULL /*static, unused*/, L_28, L_32, /*hidden argument*/NULL);
		NullCheck(L_26);
		Rigidbody2D_set_velocity_m3592751374(L_26, L_33, /*hidden argument*/NULL);
		float L_34 = __this->get_explosionCoeficient_2();
		float L_35 = V_4;
		Vector2_t2243707579  L_36 = V_6;
		Vector2_t2243707579  L_37 = Vector2_op_Multiply_m3393065202(NULL /*static, unused*/, ((float)((float)L_34/(float)((float)((float)(1.0f)+(float)L_35)))), L_36, /*hidden argument*/NULL);
		Vector2_t2243707579  L_38 = L_37;
		Il2CppObject * L_39 = Box(Vector2_t2243707579_il2cpp_TypeInfo_var, &L_38);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, L_39, /*hidden argument*/NULL);
	}

IL_00e2:
	{
		int32_t L_40 = V_1;
		V_1 = ((int32_t)((int32_t)L_40+(int32_t)1));
	}

IL_00e6:
	{
		int32_t L_41 = V_1;
		GameObject_t1756533147 * L_42 = V_0;
		NullCheck(L_42);
		Transform_t3275118058 * L_43 = GameObject_get_transform_m909382139(L_42, /*hidden argument*/NULL);
		NullCheck(L_43);
		int32_t L_44 = Transform_get_childCount_m881385315(L_43, /*hidden argument*/NULL);
		if ((((int32_t)L_41) < ((int32_t)L_44)))
		{
			goto IL_0028;
		}
	}
	{
		return;
	}
}
// System.Void GameMaster::.ctor()
extern Il2CppCodeGenString* _stringLiteral791163804;
extern const uint32_t GameMaster__ctor_m1568498945_MetadataUsageId;
extern "C"  void GameMaster__ctor_m1568498945 (GameMaster_t3159518556 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GameMaster__ctor_m1568498945_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		__this->set_defaultTutorialString_28(_stringLiteral791163804);
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GameMaster::Start()
extern Il2CppClass* StaticLevelContainer_t3714456515_il2cpp_TypeInfo_var;
extern Il2CppClass* Advertisement_t3914199195_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern Il2CppClass* FormatException_t2948921286_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32_t2071877448_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisText_t356221433_m1217399699_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral1286078031;
extern Il2CppCodeGenString* _stringLiteral1594891052;
extern Il2CppCodeGenString* _stringLiteral3140075451;
extern Il2CppCodeGenString* _stringLiteral3599784509;
extern Il2CppCodeGenString* _stringLiteral1187738806;
extern const uint32_t GameMaster_Start_m204021929_MetadataUsageId;
extern "C"  void GameMaster_Start_m204021929 (GameMaster_t3159518556 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GameMaster_Start_m204021929_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	Scene_t1684909666  V_1;
	memset(&V_1, 0, sizeof(V_1));
	int32_t V_2 = 0;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		IL2CPP_RUNTIME_CLASS_INIT(StaticLevelContainer_t3714456515_il2cpp_TypeInfo_var);
		StaticLevelContainer_t3714456515 * L_0 = StaticLevelContainer_getInstance_m3684656527(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		bool L_1 = StaticLevelContainer_playAd_m164333081(L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_002d;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Advertisement_t3914199195_il2cpp_TypeInfo_var);
		bool L_2 = Advertisement_IsReady_m876308337(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0023;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Advertisement_t3914199195_il2cpp_TypeInfo_var);
		Advertisement_Show_m2036493855(NULL /*static, unused*/, /*hidden argument*/NULL);
		goto IL_002d;
	}

IL_0023:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, _stringLiteral1286078031, /*hidden argument*/NULL);
	}

IL_002d:
	{
		Scene_t1684909666  L_3 = SceneManager_GetActiveScene_m2964039490(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_1 = L_3;
		String_t* L_4 = Scene_get_name_m745914591((&V_1), /*hidden argument*/NULL);
		V_0 = L_4;
	}

IL_003b:
	try
	{ // begin try (depth: 1)
		String_t* L_5 = V_0;
		NullCheck(_stringLiteral1594891052);
		int32_t L_6 = String_get_Length_m1606060069(_stringLiteral1594891052, /*hidden argument*/NULL);
		NullCheck(L_5);
		String_t* L_7 = String_Substring_m2032624251(L_5, L_6, /*hidden argument*/NULL);
		int32_t L_8 = Int32_Parse_m3683414232(NULL /*static, unused*/, L_7, /*hidden argument*/NULL);
		V_2 = L_8;
		goto IL_0072;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1927440687 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (FormatException_t2948921286_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0056;
		throw e;
	}

CATCH_0056:
	{ // begin catch(System.FormatException)
		String_t* L_9 = V_0;
		NullCheck(_stringLiteral3140075451);
		int32_t L_10 = String_get_Length_m1606060069(_stringLiteral3140075451, /*hidden argument*/NULL);
		NullCheck(L_9);
		String_t* L_11 = String_Substring_m2032624251(L_9, L_10, /*hidden argument*/NULL);
		int32_t L_12 = Int32_Parse_m3683414232(NULL /*static, unused*/, L_11, /*hidden argument*/NULL);
		V_2 = L_12;
		goto IL_0072;
	} // end catch (depth: 1)

IL_0072:
	{
		IL2CPP_RUNTIME_CLASS_INIT(StaticLevelContainer_t3714456515_il2cpp_TypeInfo_var);
		StaticLevelContainer_t3714456515 * L_13 = StaticLevelContainer_getInstance_m3684656527(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_14 = V_2;
		NullCheck(L_13);
		L_13->set_levelToLoad_0(L_14);
		__this->set_gameOver_4((bool)0);
		__this->set_levelFinished_6((bool)0);
		__this->set_noMoverBlocksToRemove_5((bool)0);
		__this->set_numberOfStars_7(0);
		__this->set_numberOfTaps_10(0);
		GameObject_t1756533147 * L_15 = GameObject_Find_m836511350(NULL /*static, unused*/, _stringLiteral3599784509, /*hidden argument*/NULL);
		NullCheck(L_15);
		Text_t356221433 * L_16 = GameObject_GetComponent_TisText_t356221433_m1217399699(L_15, /*hidden argument*/GameObject_GetComponent_TisText_t356221433_m1217399699_MethodInfo_var);
		int32_t L_17 = __this->get_numberOfTaps_10();
		int32_t L_18 = L_17;
		Il2CppObject * L_19 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_18);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_20 = String_Concat_m56707527(NULL /*static, unused*/, L_19, _stringLiteral1187738806, /*hidden argument*/NULL);
		NullCheck(L_16);
		VirtActionInvoker1< String_t* >::Invoke(72 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_16, L_20);
		GameMaster_CreateBluesTable_m1065156852(__this, /*hidden argument*/NULL);
		GameMaster_CreateRedsTable_m3145821583(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GameMaster::Update()
extern "C"  void GameMaster_Update_m3840188074 (GameMaster_t3159518556 * __this, const MethodInfo* method)
{
	{
		bool L_0 = __this->get_noMoverBlocksToRemove_5();
		if (!L_0)
		{
			goto IL_0049;
		}
	}
	{
		bool L_1 = __this->get_gameOver_4();
		if (L_1)
		{
			goto IL_0049;
		}
	}
	{
		bool L_2 = __this->get_levelFinished_6();
		if (L_2)
		{
			goto IL_0049;
		}
	}
	{
		float L_3 = __this->get_finishTimer_8();
		float L_4 = Time_get_deltaTime_m2233168104(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_finishTimer_8(((float)((float)L_3-(float)L_4)));
		float L_5 = __this->get_finishTimer_8();
		if ((!(((float)L_5) < ((float)(0.0f)))))
		{
			goto IL_0049;
		}
	}
	{
		GameMaster_SucceedLevel_m2200462369(__this, /*hidden argument*/NULL);
	}

IL_0049:
	{
		bool L_6 = __this->get_gameOver_4();
		if (L_6)
		{
			goto IL_009d;
		}
	}
	{
		bool L_7 = __this->get_levelFinished_6();
		if (L_7)
		{
			goto IL_009d;
		}
	}
	{
		GameMaster_CheckForDeadBlues_m2405166521(__this, /*hidden argument*/NULL);
		bool L_8 = __this->get_gameOver_4();
		if (L_8)
		{
			goto IL_009d;
		}
	}
	{
		bool L_9 = __this->get_noMoverBlocksToRemove_5();
		if (L_9)
		{
			goto IL_009d;
		}
	}
	{
		GameMaster_CheckForDeadReds_m4155658138(__this, /*hidden argument*/NULL);
		bool L_10 = __this->get_gameOver_4();
		if (L_10)
		{
			goto IL_009d;
		}
	}
	{
		bool L_11 = __this->get_noMoverBlocksToRemove_5();
		if (L_11)
		{
			goto IL_009d;
		}
	}
	{
		GameMaster_CheckForTouches_m1815965391(__this, /*hidden argument*/NULL);
	}

IL_009d:
	{
		return;
	}
}
// System.Void GameMaster::CreateBluesTable()
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern Il2CppClass* MustNotDieU5BU5D_t1158737389_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisMustNotDie_t791179140_m3796240729_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral2830371216;
extern Il2CppCodeGenString* _stringLiteral2482826324;
extern const uint32_t GameMaster_CreateBluesTable_m1065156852_MetadataUsageId;
extern "C"  void GameMaster_CreateBluesTable_m1065156852 (GameMaster_t3159518556 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GameMaster_CreateBluesTable_m1065156852_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	GameObject_t1756533147 * V_0 = NULL;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	GameObject_t1756533147 * V_3 = NULL;
	MustNotDie_t791179140 * V_4 = NULL;
	int32_t V_5 = 0;
	int32_t V_6 = 0;
	GameObject_t1756533147 * V_7 = NULL;
	MustNotDie_t791179140 * V_8 = NULL;
	{
		GameObject_t1756533147 * L_0 = GameObject_Find_m836511350(NULL /*static, unused*/, _stringLiteral2830371216, /*hidden argument*/NULL);
		V_0 = L_0;
		GameObject_t1756533147 * L_1 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_2 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		if (L_2)
		{
			goto IL_0025;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_LogError_m3715728798(NULL /*static, unused*/, _stringLiteral2482826324, /*hidden argument*/NULL);
		goto IL_00e8;
	}

IL_0025:
	{
		V_1 = 0;
		V_2 = 0;
		goto IL_005c;
	}

IL_002e:
	{
		GameObject_t1756533147 * L_3 = V_0;
		NullCheck(L_3);
		Transform_t3275118058 * L_4 = GameObject_get_transform_m909382139(L_3, /*hidden argument*/NULL);
		int32_t L_5 = V_2;
		NullCheck(L_4);
		Transform_t3275118058 * L_6 = Transform_GetChild_m3838588184(L_4, L_5, /*hidden argument*/NULL);
		NullCheck(L_6);
		GameObject_t1756533147 * L_7 = Component_get_gameObject_m3105766835(L_6, /*hidden argument*/NULL);
		V_3 = L_7;
		GameObject_t1756533147 * L_8 = V_3;
		NullCheck(L_8);
		MustNotDie_t791179140 * L_9 = GameObject_GetComponent_TisMustNotDie_t791179140_m3796240729(L_8, /*hidden argument*/GameObject_GetComponent_TisMustNotDie_t791179140_m3796240729_MethodInfo_var);
		V_4 = L_9;
		MustNotDie_t791179140 * L_10 = V_4;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_11 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_10, /*hidden argument*/NULL);
		if (!L_11)
		{
			goto IL_0058;
		}
	}
	{
		int32_t L_12 = V_1;
		V_1 = ((int32_t)((int32_t)L_12+(int32_t)1));
	}

IL_0058:
	{
		int32_t L_13 = V_2;
		V_2 = ((int32_t)((int32_t)L_13+(int32_t)1));
	}

IL_005c:
	{
		int32_t L_14 = V_2;
		GameObject_t1756533147 * L_15 = V_0;
		NullCheck(L_15);
		Transform_t3275118058 * L_16 = GameObject_get_transform_m909382139(L_15, /*hidden argument*/NULL);
		NullCheck(L_16);
		int32_t L_17 = Transform_get_childCount_m881385315(L_16, /*hidden argument*/NULL);
		if ((((int32_t)L_14) < ((int32_t)L_17)))
		{
			goto IL_002e;
		}
	}
	{
		int32_t L_18 = V_1;
		if ((((int32_t)L_18) <= ((int32_t)0)))
		{
			goto IL_00e1;
		}
	}
	{
		int32_t L_19 = V_1;
		__this->set_mustNotDieObjects_2(((MustNotDieU5BU5D_t1158737389*)SZArrayNew(MustNotDieU5BU5D_t1158737389_il2cpp_TypeInfo_var, (uint32_t)L_19)));
		V_5 = 0;
		V_6 = 0;
		goto IL_00ca;
	}

IL_008b:
	{
		GameObject_t1756533147 * L_20 = V_0;
		NullCheck(L_20);
		Transform_t3275118058 * L_21 = GameObject_get_transform_m909382139(L_20, /*hidden argument*/NULL);
		int32_t L_22 = V_6;
		NullCheck(L_21);
		Transform_t3275118058 * L_23 = Transform_GetChild_m3838588184(L_21, L_22, /*hidden argument*/NULL);
		NullCheck(L_23);
		GameObject_t1756533147 * L_24 = Component_get_gameObject_m3105766835(L_23, /*hidden argument*/NULL);
		V_7 = L_24;
		GameObject_t1756533147 * L_25 = V_7;
		NullCheck(L_25);
		MustNotDie_t791179140 * L_26 = GameObject_GetComponent_TisMustNotDie_t791179140_m3796240729(L_25, /*hidden argument*/GameObject_GetComponent_TisMustNotDie_t791179140_m3796240729_MethodInfo_var);
		V_8 = L_26;
		MustNotDie_t791179140 * L_27 = V_8;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_28 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_27, /*hidden argument*/NULL);
		if (!L_28)
		{
			goto IL_00c4;
		}
	}
	{
		MustNotDieU5BU5D_t1158737389* L_29 = __this->get_mustNotDieObjects_2();
		int32_t L_30 = V_5;
		int32_t L_31 = L_30;
		V_5 = ((int32_t)((int32_t)L_31+(int32_t)1));
		MustNotDie_t791179140 * L_32 = V_8;
		NullCheck(L_29);
		ArrayElementTypeCheck (L_29, L_32);
		(L_29)->SetAt(static_cast<il2cpp_array_size_t>(L_31), (MustNotDie_t791179140 *)L_32);
	}

IL_00c4:
	{
		int32_t L_33 = V_6;
		V_6 = ((int32_t)((int32_t)L_33+(int32_t)1));
	}

IL_00ca:
	{
		int32_t L_34 = V_6;
		GameObject_t1756533147 * L_35 = V_0;
		NullCheck(L_35);
		Transform_t3275118058 * L_36 = GameObject_get_transform_m909382139(L_35, /*hidden argument*/NULL);
		NullCheck(L_36);
		int32_t L_37 = Transform_get_childCount_m881385315(L_36, /*hidden argument*/NULL);
		if ((((int32_t)L_34) < ((int32_t)L_37)))
		{
			goto IL_008b;
		}
	}
	{
		goto IL_00e8;
	}

IL_00e1:
	{
		__this->set_mustNotDieObjects_2((MustNotDieU5BU5D_t1158737389*)NULL);
	}

IL_00e8:
	{
		return;
	}
}
// System.Void GameMaster::CheckForDeadBlues()
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const uint32_t GameMaster_CheckForDeadBlues_m2405166521_MetadataUsageId;
extern "C"  void GameMaster_CheckForDeadBlues_m2405166521 (GameMaster_t3159518556 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GameMaster_CheckForDeadBlues_m2405166521_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	MustNotDie_t791179140 * V_0 = NULL;
	MustNotDieU5BU5D_t1158737389* V_1 = NULL;
	int32_t V_2 = 0;
	{
		MustNotDieU5BU5D_t1158737389* L_0 = __this->get_mustNotDieObjects_2();
		if (!L_0)
		{
			goto IL_0041;
		}
	}
	{
		MustNotDieU5BU5D_t1158737389* L_1 = __this->get_mustNotDieObjects_2();
		V_1 = L_1;
		V_2 = 0;
		goto IL_0038;
	}

IL_0019:
	{
		MustNotDieU5BU5D_t1158737389* L_2 = V_1;
		int32_t L_3 = V_2;
		NullCheck(L_2);
		int32_t L_4 = L_3;
		MustNotDie_t791179140 * L_5 = (L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4));
		V_0 = L_5;
		MustNotDie_t791179140 * L_6 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_7 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_6, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_0034;
		}
	}
	{
		GameMaster_FailLevel_m2296886933(__this, /*hidden argument*/NULL);
		goto IL_0041;
	}

IL_0034:
	{
		int32_t L_8 = V_2;
		V_2 = ((int32_t)((int32_t)L_8+(int32_t)1));
	}

IL_0038:
	{
		int32_t L_9 = V_2;
		MustNotDieU5BU5D_t1158737389* L_10 = V_1;
		NullCheck(L_10);
		if ((((int32_t)L_9) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_10)->max_length)))))))
		{
			goto IL_0019;
		}
	}

IL_0041:
	{
		return;
	}
}
// System.Void GameMaster::FailLevel()
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisAnimator_t69676727_m2717502299_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral2830371216;
extern Il2CppCodeGenString* _stringLiteral2843951784;
extern Il2CppCodeGenString* _stringLiteral3558534422;
extern Il2CppCodeGenString* _stringLiteral678270948;
extern Il2CppCodeGenString* _stringLiteral716724565;
extern Il2CppCodeGenString* _stringLiteral1884423134;
extern Il2CppCodeGenString* _stringLiteral1994460817;
extern Il2CppCodeGenString* _stringLiteral1876604234;
extern const uint32_t GameMaster_FailLevel_m2296886933_MetadataUsageId;
extern "C"  void GameMaster_FailLevel_m2296886933 (GameMaster_t3159518556 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GameMaster_FailLevel_m2296886933_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	GameObject_t1756533147 * V_0 = NULL;
	{
		__this->set_gameOver_4((bool)1);
		GameObject_t1756533147 * L_0 = GameObject_Find_m836511350(NULL /*static, unused*/, _stringLiteral2830371216, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		Object_Destroy_m4145850038(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_1 = GameObject_Find_m836511350(NULL /*static, unused*/, _stringLiteral2843951784, /*hidden argument*/NULL);
		NullCheck(L_1);
		Animator_t69676727 * L_2 = GameObject_GetComponent_TisAnimator_t69676727_m2717502299(L_1, /*hidden argument*/GameObject_GetComponent_TisAnimator_t69676727_m2717502299_MethodInfo_var);
		NullCheck(L_2);
		Animator_SetTrigger_m3418492570(L_2, _stringLiteral3558534422, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_3 = GameObject_Find_m836511350(NULL /*static, unused*/, _stringLiteral2843951784, /*hidden argument*/NULL);
		NullCheck(L_3);
		Animator_t69676727 * L_4 = GameObject_GetComponent_TisAnimator_t69676727_m2717502299(L_3, /*hidden argument*/GameObject_GetComponent_TisAnimator_t69676727_m2717502299_MethodInfo_var);
		NullCheck(L_4);
		Animator_SetBool_m2305662531(L_4, _stringLiteral678270948, (bool)1, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_5 = GameObject_Find_m836511350(NULL /*static, unused*/, _stringLiteral716724565, /*hidden argument*/NULL);
		NullCheck(L_5);
		Animator_t69676727 * L_6 = GameObject_GetComponent_TisAnimator_t69676727_m2717502299(L_5, /*hidden argument*/GameObject_GetComponent_TisAnimator_t69676727_m2717502299_MethodInfo_var);
		NullCheck(L_6);
		Animator_SetBool_m2305662531(L_6, _stringLiteral1884423134, (bool)1, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_7 = GameObject_Find_m836511350(NULL /*static, unused*/, _stringLiteral1994460817, /*hidden argument*/NULL);
		V_0 = L_7;
		GameObject_t1756533147 * L_8 = V_0;
		bool L_9 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
		if (!L_9)
		{
			goto IL_007f;
		}
	}
	{
		GameObject_t1756533147 * L_10 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		Object_Destroy_m4145850038(NULL /*static, unused*/, L_10, /*hidden argument*/NULL);
	}

IL_007f:
	{
		GameObject_t1756533147 * L_11 = GameObject_Find_m836511350(NULL /*static, unused*/, _stringLiteral1876604234, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		Object_Destroy_m4145850038(NULL /*static, unused*/, L_11, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GameMaster::CreateRedsTable()
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern Il2CppClass* MustDieU5BU5D_t3128154408_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisMustDie_t3564324517_m2755846400_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral2830371216;
extern Il2CppCodeGenString* _stringLiteral2482826324;
extern const uint32_t GameMaster_CreateRedsTable_m3145821583_MetadataUsageId;
extern "C"  void GameMaster_CreateRedsTable_m3145821583 (GameMaster_t3159518556 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GameMaster_CreateRedsTable_m3145821583_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	GameObject_t1756533147 * V_0 = NULL;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	GameObject_t1756533147 * V_3 = NULL;
	MustDie_t3564324517 * V_4 = NULL;
	int32_t V_5 = 0;
	int32_t V_6 = 0;
	GameObject_t1756533147 * V_7 = NULL;
	MustDie_t3564324517 * V_8 = NULL;
	{
		GameObject_t1756533147 * L_0 = GameObject_Find_m836511350(NULL /*static, unused*/, _stringLiteral2830371216, /*hidden argument*/NULL);
		V_0 = L_0;
		GameObject_t1756533147 * L_1 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_2 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		if (L_2)
		{
			goto IL_0025;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_LogError_m3715728798(NULL /*static, unused*/, _stringLiteral2482826324, /*hidden argument*/NULL);
		goto IL_00e8;
	}

IL_0025:
	{
		V_1 = 0;
		V_2 = 0;
		goto IL_005c;
	}

IL_002e:
	{
		GameObject_t1756533147 * L_3 = V_0;
		NullCheck(L_3);
		Transform_t3275118058 * L_4 = GameObject_get_transform_m909382139(L_3, /*hidden argument*/NULL);
		int32_t L_5 = V_2;
		NullCheck(L_4);
		Transform_t3275118058 * L_6 = Transform_GetChild_m3838588184(L_4, L_5, /*hidden argument*/NULL);
		NullCheck(L_6);
		GameObject_t1756533147 * L_7 = Component_get_gameObject_m3105766835(L_6, /*hidden argument*/NULL);
		V_3 = L_7;
		GameObject_t1756533147 * L_8 = V_3;
		NullCheck(L_8);
		MustDie_t3564324517 * L_9 = GameObject_GetComponent_TisMustDie_t3564324517_m2755846400(L_8, /*hidden argument*/GameObject_GetComponent_TisMustDie_t3564324517_m2755846400_MethodInfo_var);
		V_4 = L_9;
		MustDie_t3564324517 * L_10 = V_4;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_11 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_10, /*hidden argument*/NULL);
		if (!L_11)
		{
			goto IL_0058;
		}
	}
	{
		int32_t L_12 = V_1;
		V_1 = ((int32_t)((int32_t)L_12+(int32_t)1));
	}

IL_0058:
	{
		int32_t L_13 = V_2;
		V_2 = ((int32_t)((int32_t)L_13+(int32_t)1));
	}

IL_005c:
	{
		int32_t L_14 = V_2;
		GameObject_t1756533147 * L_15 = V_0;
		NullCheck(L_15);
		Transform_t3275118058 * L_16 = GameObject_get_transform_m909382139(L_15, /*hidden argument*/NULL);
		NullCheck(L_16);
		int32_t L_17 = Transform_get_childCount_m881385315(L_16, /*hidden argument*/NULL);
		if ((((int32_t)L_14) < ((int32_t)L_17)))
		{
			goto IL_002e;
		}
	}
	{
		int32_t L_18 = V_1;
		if ((((int32_t)L_18) <= ((int32_t)0)))
		{
			goto IL_00e1;
		}
	}
	{
		int32_t L_19 = V_1;
		__this->set_mustDieObjects_3(((MustDieU5BU5D_t3128154408*)SZArrayNew(MustDieU5BU5D_t3128154408_il2cpp_TypeInfo_var, (uint32_t)L_19)));
		V_5 = 0;
		V_6 = 0;
		goto IL_00ca;
	}

IL_008b:
	{
		GameObject_t1756533147 * L_20 = V_0;
		NullCheck(L_20);
		Transform_t3275118058 * L_21 = GameObject_get_transform_m909382139(L_20, /*hidden argument*/NULL);
		int32_t L_22 = V_6;
		NullCheck(L_21);
		Transform_t3275118058 * L_23 = Transform_GetChild_m3838588184(L_21, L_22, /*hidden argument*/NULL);
		NullCheck(L_23);
		GameObject_t1756533147 * L_24 = Component_get_gameObject_m3105766835(L_23, /*hidden argument*/NULL);
		V_7 = L_24;
		GameObject_t1756533147 * L_25 = V_7;
		NullCheck(L_25);
		MustDie_t3564324517 * L_26 = GameObject_GetComponent_TisMustDie_t3564324517_m2755846400(L_25, /*hidden argument*/GameObject_GetComponent_TisMustDie_t3564324517_m2755846400_MethodInfo_var);
		V_8 = L_26;
		MustDie_t3564324517 * L_27 = V_8;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_28 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_27, /*hidden argument*/NULL);
		if (!L_28)
		{
			goto IL_00c4;
		}
	}
	{
		MustDieU5BU5D_t3128154408* L_29 = __this->get_mustDieObjects_3();
		int32_t L_30 = V_5;
		int32_t L_31 = L_30;
		V_5 = ((int32_t)((int32_t)L_31+(int32_t)1));
		MustDie_t3564324517 * L_32 = V_8;
		NullCheck(L_29);
		ArrayElementTypeCheck (L_29, L_32);
		(L_29)->SetAt(static_cast<il2cpp_array_size_t>(L_31), (MustDie_t3564324517 *)L_32);
	}

IL_00c4:
	{
		int32_t L_33 = V_6;
		V_6 = ((int32_t)((int32_t)L_33+(int32_t)1));
	}

IL_00ca:
	{
		int32_t L_34 = V_6;
		GameObject_t1756533147 * L_35 = V_0;
		NullCheck(L_35);
		Transform_t3275118058 * L_36 = GameObject_get_transform_m909382139(L_35, /*hidden argument*/NULL);
		NullCheck(L_36);
		int32_t L_37 = Transform_get_childCount_m881385315(L_36, /*hidden argument*/NULL);
		if ((((int32_t)L_34) < ((int32_t)L_37)))
		{
			goto IL_008b;
		}
	}
	{
		goto IL_00e8;
	}

IL_00e1:
	{
		__this->set_mustDieObjects_3((MustDieU5BU5D_t3128154408*)NULL);
	}

IL_00e8:
	{
		return;
	}
}
// System.Void GameMaster::CheckForDeadReds()
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const uint32_t GameMaster_CheckForDeadReds_m4155658138_MetadataUsageId;
extern "C"  void GameMaster_CheckForDeadReds_m4155658138 (GameMaster_t3159518556 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GameMaster_CheckForDeadReds_m4155658138_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	MustDie_t3564324517 * V_0 = NULL;
	MustDieU5BU5D_t3128154408* V_1 = NULL;
	int32_t V_2 = 0;
	{
		MustDieU5BU5D_t3128154408* L_0 = __this->get_mustDieObjects_3();
		if (!L_0)
		{
			goto IL_004a;
		}
	}
	{
		MustDieU5BU5D_t3128154408* L_1 = __this->get_mustDieObjects_3();
		V_1 = L_1;
		V_2 = 0;
		goto IL_002e;
	}

IL_0019:
	{
		MustDieU5BU5D_t3128154408* L_2 = V_1;
		int32_t L_3 = V_2;
		NullCheck(L_2);
		int32_t L_4 = L_3;
		MustDie_t3564324517 * L_5 = (L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4));
		V_0 = L_5;
		MustDie_t3564324517 * L_6 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_7 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_6, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_002a;
		}
	}
	{
		return;
	}

IL_002a:
	{
		int32_t L_8 = V_2;
		V_2 = ((int32_t)((int32_t)L_8+(int32_t)1));
	}

IL_002e:
	{
		int32_t L_9 = V_2;
		MustDieU5BU5D_t3128154408* L_10 = V_1;
		NullCheck(L_10);
		if ((((int32_t)L_9) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_10)->max_length)))))))
		{
			goto IL_0019;
		}
	}
	{
		__this->set_noMoverBlocksToRemove_5((bool)1);
		float L_11 = __this->get_finishTimerMax_9();
		__this->set_finishTimer_8(L_11);
	}

IL_004a:
	{
		return;
	}
}
// System.Void GameMaster::SucceedLevel()
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern Il2CppClass* StaticLevelContainer_t3714456515_il2cpp_TypeInfo_var;
extern Il2CppClass* Constants_t3271648343_il2cpp_TypeInfo_var;
extern Il2CppClass* NullReferenceException_t3156209119_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisAnimator_t69676727_m2717502299_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral1876604234;
extern Il2CppCodeGenString* _stringLiteral1994460817;
extern Il2CppCodeGenString* _stringLiteral2843951784;
extern Il2CppCodeGenString* _stringLiteral3558534422;
extern Il2CppCodeGenString* _stringLiteral678270948;
extern Il2CppCodeGenString* _stringLiteral2885902666;
extern Il2CppCodeGenString* _stringLiteral2129337197;
extern Il2CppCodeGenString* _stringLiteral1756647240;
extern Il2CppCodeGenString* _stringLiteral459719467;
extern Il2CppCodeGenString* _stringLiteral2925530383;
extern const uint32_t GameMaster_SucceedLevel_m2200462369_MetadataUsageId;
extern "C"  void GameMaster_SucceedLevel_m2200462369 (GameMaster_t3159518556 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GameMaster_SucceedLevel_m2200462369_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	GameObject_t1756533147 * V_0 = NULL;
	StaticLevelContainer_t3714456515 * V_1 = NULL;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	GameObject_t1756533147 * V_4 = NULL;
	GameObject_t1756533147 * V_5 = NULL;
	NullReferenceException_t3156209119 * V_6 = NULL;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		GameObject_t1756533147 * L_0 = GameObject_Find_m836511350(NULL /*static, unused*/, _stringLiteral1876604234, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		Object_Destroy_m4145850038(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_1 = GameObject_Find_m836511350(NULL /*static, unused*/, _stringLiteral1994460817, /*hidden argument*/NULL);
		V_0 = L_1;
		GameObject_t1756533147 * L_2 = V_0;
		bool L_3 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_002b;
		}
	}
	{
		GameObject_t1756533147 * L_4 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		Object_Destroy_m4145850038(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
	}

IL_002b:
	{
		__this->set_levelFinished_6((bool)1);
		GameObject_t1756533147 * L_5 = GameObject_Find_m836511350(NULL /*static, unused*/, _stringLiteral2843951784, /*hidden argument*/NULL);
		NullCheck(L_5);
		Animator_t69676727 * L_6 = GameObject_GetComponent_TisAnimator_t69676727_m2717502299(L_5, /*hidden argument*/GameObject_GetComponent_TisAnimator_t69676727_m2717502299_MethodInfo_var);
		NullCheck(L_6);
		Animator_SetTrigger_m3418492570(L_6, _stringLiteral3558534422, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_7 = GameObject_Find_m836511350(NULL /*static, unused*/, _stringLiteral2843951784, /*hidden argument*/NULL);
		NullCheck(L_7);
		Animator_t69676727 * L_8 = GameObject_GetComponent_TisAnimator_t69676727_m2717502299(L_7, /*hidden argument*/GameObject_GetComponent_TisAnimator_t69676727_m2717502299_MethodInfo_var);
		NullCheck(L_8);
		Animator_SetBool_m2305662531(L_8, _stringLiteral678270948, (bool)1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(StaticLevelContainer_t3714456515_il2cpp_TypeInfo_var);
		StaticLevelContainer_t3714456515 * L_9 = StaticLevelContainer_getInstance_m3684656527(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_1 = L_9;
		StaticLevelContainer_t3714456515 * L_10 = V_1;
		NullCheck(L_10);
		int32_t L_11 = L_10->get_levelToLoad_0();
		V_2 = L_11;
		V_3 = 3;
		int32_t L_12 = __this->get_numberOfTaps_10();
		int32_t L_13 = __this->get_maxTapsFor1Star_27();
		if ((((int32_t)L_12) <= ((int32_t)L_13)))
		{
			goto IL_008c;
		}
	}
	{
		V_3 = 0;
		goto IL_00b7;
	}

IL_008c:
	{
		int32_t L_14 = __this->get_numberOfTaps_10();
		int32_t L_15 = __this->get_maxTapsFor2Stars_26();
		if ((((int32_t)L_14) <= ((int32_t)L_15)))
		{
			goto IL_00a4;
		}
	}
	{
		V_3 = 1;
		goto IL_00b7;
	}

IL_00a4:
	{
		int32_t L_16 = __this->get_numberOfTaps_10();
		int32_t L_17 = __this->get_maxTapsFor3Stars_25();
		if ((((int32_t)L_16) <= ((int32_t)L_17)))
		{
			goto IL_00b7;
		}
	}
	{
		V_3 = 2;
	}

IL_00b7:
	{
		GameObject_t1756533147 * L_18 = GameObject_Find_m836511350(NULL /*static, unused*/, _stringLiteral2885902666, /*hidden argument*/NULL);
		NullCheck(L_18);
		Animator_t69676727 * L_19 = GameObject_GetComponent_TisAnimator_t69676727_m2717502299(L_18, /*hidden argument*/GameObject_GetComponent_TisAnimator_t69676727_m2717502299_MethodInfo_var);
		int32_t L_20 = V_3;
		NullCheck(L_19);
		Animator_SetInteger_m528582597(L_19, _stringLiteral2129337197, L_20, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_21 = GameObject_Find_m836511350(NULL /*static, unused*/, _stringLiteral2885902666, /*hidden argument*/NULL);
		NullCheck(L_21);
		Animator_t69676727 * L_22 = GameObject_GetComponent_TisAnimator_t69676727_m2717502299(L_21, /*hidden argument*/GameObject_GetComponent_TisAnimator_t69676727_m2717502299_MethodInfo_var);
		NullCheck(L_22);
		Animator_SetBool_m2305662531(L_22, _stringLiteral1756647240, (bool)1, /*hidden argument*/NULL);
	}

IL_00eb:
	try
	{ // begin try (depth: 1)
		{
			StaticLevelContainer_t3714456515 * L_23 = V_1;
			IL2CPP_RUNTIME_CLASS_INIT(StaticLevelContainer_t3714456515_il2cpp_TypeInfo_var);
			GameState_t2029468607 * L_24 = StaticLevelContainer_loadLevelFromStorage_m1234179298(NULL /*static, unused*/, /*hidden argument*/NULL);
			NullCheck(L_23);
			L_23->set_gameState_1(L_24);
			int32_t L_25 = V_2;
			StaticLevelContainer_t3714456515 * L_26 = V_1;
			NullCheck(L_26);
			GameState_t2029468607 * L_27 = L_26->get_gameState_1();
			NullCheck(L_27);
			LevelU5BU5D_t1839895005* L_28 = L_27->get_levels_0();
			NullCheck(L_28);
			if ((((int32_t)L_25) >= ((int32_t)((int32_t)((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_28)->max_length))))-(int32_t)1)))))
			{
				goto IL_01ba;
			}
		}

IL_010b:
		{
			int32_t L_29 = V_3;
			if ((((int32_t)L_29) <= ((int32_t)0)))
			{
				goto IL_0141;
			}
		}

IL_0112:
		{
			StaticLevelContainer_t3714456515 * L_30 = V_1;
			NullCheck(L_30);
			GameState_t2029468607 * L_31 = L_30->get_gameState_1();
			NullCheck(L_31);
			LevelU5BU5D_t1839895005* L_32 = L_31->get_levels_0();
			int32_t L_33 = V_2;
			NullCheck(L_32);
			int32_t L_34 = ((int32_t)((int32_t)L_33+(int32_t)1));
			Level_t1066665556 * L_35 = (L_32)->GetAt(static_cast<il2cpp_array_size_t>(L_34));
			NullCheck(L_35);
			int32_t L_36 = L_35->get_status_5();
			if ((!(((uint32_t)L_36) == ((uint32_t)(-1)))))
			{
				goto IL_0141;
			}
		}

IL_012c:
		{
			StaticLevelContainer_t3714456515 * L_37 = V_1;
			NullCheck(L_37);
			GameState_t2029468607 * L_38 = L_37->get_gameState_1();
			NullCheck(L_38);
			LevelU5BU5D_t1839895005* L_39 = L_38->get_levels_0();
			int32_t L_40 = V_2;
			NullCheck(L_39);
			int32_t L_41 = ((int32_t)((int32_t)L_40+(int32_t)1));
			Level_t1066665556 * L_42 = (L_39)->GetAt(static_cast<il2cpp_array_size_t>(L_41));
			NullCheck(L_42);
			L_42->set_status_5(0);
		}

IL_0141:
		{
			StaticLevelContainer_t3714456515 * L_43 = V_1;
			NullCheck(L_43);
			GameState_t2029468607 * L_44 = L_43->get_gameState_1();
			NullCheck(L_44);
			LevelU5BU5D_t1839895005* L_45 = L_44->get_levels_0();
			int32_t L_46 = V_2;
			NullCheck(L_45);
			int32_t L_47 = ((int32_t)((int32_t)L_46+(int32_t)1));
			Level_t1066665556 * L_48 = (L_45)->GetAt(static_cast<il2cpp_array_size_t>(L_47));
			NullCheck(L_48);
			int32_t L_49 = L_48->get_status_5();
			if ((!(((uint32_t)L_49) == ((uint32_t)(-1)))))
			{
				goto IL_017a;
			}
		}

IL_015b:
		{
			GameObject_t1756533147 * L_50 = GameObject_Find_m836511350(NULL /*static, unused*/, _stringLiteral459719467, /*hidden argument*/NULL);
			V_4 = L_50;
			GameObject_t1756533147 * L_51 = V_4;
			IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
			bool L_52 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_51, /*hidden argument*/NULL);
			if (!L_52)
			{
				goto IL_017a;
			}
		}

IL_0173:
		{
			GameObject_t1756533147 * L_53 = V_4;
			IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
			Object_Destroy_m4145850038(NULL /*static, unused*/, L_53, /*hidden argument*/NULL);
		}

IL_017a:
		{
			int32_t L_54 = V_3;
			StaticLevelContainer_t3714456515 * L_55 = V_1;
			NullCheck(L_55);
			GameState_t2029468607 * L_56 = L_55->get_gameState_1();
			NullCheck(L_56);
			LevelU5BU5D_t1839895005* L_57 = L_56->get_levels_0();
			int32_t L_58 = V_2;
			NullCheck(L_57);
			int32_t L_59 = L_58;
			Level_t1066665556 * L_60 = (L_57)->GetAt(static_cast<il2cpp_array_size_t>(L_59));
			NullCheck(L_60);
			int32_t L_61 = L_60->get_status_5();
			if ((((int32_t)L_54) <= ((int32_t)L_61)))
			{
				goto IL_01a5;
			}
		}

IL_0192:
		{
			StaticLevelContainer_t3714456515 * L_62 = V_1;
			NullCheck(L_62);
			GameState_t2029468607 * L_63 = L_62->get_gameState_1();
			NullCheck(L_63);
			LevelU5BU5D_t1839895005* L_64 = L_63->get_levels_0();
			int32_t L_65 = V_2;
			NullCheck(L_64);
			int32_t L_66 = L_65;
			Level_t1066665556 * L_67 = (L_64)->GetAt(static_cast<il2cpp_array_size_t>(L_66));
			int32_t L_68 = V_3;
			NullCheck(L_67);
			L_67->set_status_5(L_68);
		}

IL_01a5:
		{
			StaticLevelContainer_t3714456515 * L_69 = V_1;
			NullCheck(L_69);
			GameState_t2029468607 * L_70 = L_69->get_gameState_1();
			IL2CPP_RUNTIME_CLASS_INIT(Constants_t3271648343_il2cpp_TypeInfo_var);
			String_t* L_71 = ((Constants_t3271648343_StaticFields*)Constants_t3271648343_il2cpp_TypeInfo_var->static_fields)->get_SAVE_GAME_PATH_0();
			NullCheck(L_70);
			GameState_Export_m2636018477(L_70, L_71, /*hidden argument*/NULL);
			goto IL_01d9;
		}

IL_01ba:
		{
			GameObject_t1756533147 * L_72 = GameObject_Find_m836511350(NULL /*static, unused*/, _stringLiteral459719467, /*hidden argument*/NULL);
			V_5 = L_72;
			GameObject_t1756533147 * L_73 = V_5;
			IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
			bool L_74 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_73, /*hidden argument*/NULL);
			if (!L_74)
			{
				goto IL_01d9;
			}
		}

IL_01d2:
		{
			GameObject_t1756533147 * L_75 = V_5;
			IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
			Object_Destroy_m4145850038(NULL /*static, unused*/, L_75, /*hidden argument*/NULL);
		}

IL_01d9:
		{
			goto IL_01ef;
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1927440687 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (NullReferenceException_t3156209119_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_01de;
		throw e;
	}

CATCH_01de:
	{ // begin catch(System.NullReferenceException)
		V_6 = ((NullReferenceException_t3156209119 *)__exception_local);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_LogError_m3715728798(NULL /*static, unused*/, _stringLiteral2925530383, /*hidden argument*/NULL);
		goto IL_01ef;
	} // end catch (depth: 1)

IL_01ef:
	{
		return;
	}
}
// System.Void GameMaster::CheckForTouches()
extern Il2CppClass* Input_t1785128008_il2cpp_TypeInfo_var;
extern Il2CppClass* Physics2D_t2540166467_il2cpp_TypeInfo_var;
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32_t2071877448_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisRemovable_t2119088671_m2134440904_MethodInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisAnimator_t69676727_m2717502299_MethodInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisText_t356221433_m1217399699_MethodInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisExplode_t13292173_m1264009640_MethodInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisTutorialNoNo_t3242399842_m3271948131_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral2033965610;
extern Il2CppCodeGenString* _stringLiteral4136222740;
extern Il2CppCodeGenString* _stringLiteral2033965613;
extern Il2CppCodeGenString* _stringLiteral2033965612;
extern Il2CppCodeGenString* _stringLiteral3599784509;
extern Il2CppCodeGenString* _stringLiteral1187738806;
extern Il2CppCodeGenString* _stringLiteral1712688227;
extern Il2CppCodeGenString* _stringLiteral2933275870;
extern const uint32_t GameMaster_CheckForTouches_m1815965391_MetadataUsageId;
extern "C"  void GameMaster_CheckForTouches_m1815965391 (GameMaster_t3159518556 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GameMaster_CheckForTouches_m1815965391_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector2_t2243707579  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector3_t2243707580  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Vector3_t2243707580  V_2;
	memset(&V_2, 0, sizeof(V_2));
	RaycastHit2D_t4063908774  V_3;
	memset(&V_3, 0, sizeof(V_3));
	GameObject_t1756533147 * V_4 = NULL;
	Removable_t2119088671 * V_5 = NULL;
	GameObject_t1756533147 * V_6 = NULL;
	GameObject_t1756533147 * V_7 = NULL;
	GameObject_t1756533147 * V_8 = NULL;
	Explode_t13292173 * V_9 = NULL;
	TutorialNoNo_t3242399842 * V_10 = NULL;
	GameObject_t1756533147 * V_11 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1785128008_il2cpp_TypeInfo_var);
		bool L_0 = Input_GetMouseButtonDown_m47917805(NULL /*static, unused*/, 0, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_01da;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1785128008_il2cpp_TypeInfo_var);
		Vector3_t2243707580  L_1 = Input_get_mousePosition_m146923508(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_1 = L_1;
		float L_2 = (&V_1)->get_x_1();
		Vector3_t2243707580  L_3 = Input_get_mousePosition_m146923508(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_2 = L_3;
		float L_4 = (&V_2)->get_y_2();
		Vector2__ctor_m3067419446((&V_0), L_2, L_4, /*hidden argument*/NULL);
		Camera_t189460977 * L_5 = Camera_get_main_m475173995(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector2_t2243707579  L_6 = V_0;
		Vector3_t2243707580  L_7 = Vector2_op_Implicit_m176791411(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		NullCheck(L_5);
		Vector3_t2243707580  L_8 = Camera_ScreenToWorldPoint_m929392728(L_5, L_7, /*hidden argument*/NULL);
		Vector2_t2243707579  L_9 = Vector2_op_Implicit_m1064335535(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
		Vector2_t2243707579  L_10 = Vector2_get_zero_m3966848876(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Physics2D_t2540166467_il2cpp_TypeInfo_var);
		RaycastHit2D_t4063908774  L_11 = Physics2D_Raycast_m2560154475(NULL /*static, unused*/, L_9, L_10, /*hidden argument*/NULL);
		V_3 = L_11;
		RaycastHit2D_t4063908774  L_12 = V_3;
		bool L_13 = RaycastHit2D_op_Implicit_m596912073(NULL /*static, unused*/, L_12, /*hidden argument*/NULL);
		if (!L_13)
		{
			goto IL_01da;
		}
	}
	{
		Collider2D_t646061738 * L_14 = RaycastHit2D_get_collider_m2568504212((&V_3), /*hidden argument*/NULL);
		NullCheck(L_14);
		Transform_t3275118058 * L_15 = Component_get_transform_m2697483695(L_14, /*hidden argument*/NULL);
		NullCheck(L_15);
		GameObject_t1756533147 * L_16 = Component_get_gameObject_m3105766835(L_15, /*hidden argument*/NULL);
		V_4 = L_16;
		GameObject_t1756533147 * L_17 = V_4;
		NullCheck(L_17);
		Removable_t2119088671 * L_18 = GameObject_GetComponent_TisRemovable_t2119088671_m2134440904(L_17, /*hidden argument*/GameObject_GetComponent_TisRemovable_t2119088671_m2134440904_MethodInfo_var);
		V_5 = L_18;
		Removable_t2119088671 * L_19 = V_5;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_20 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_19, /*hidden argument*/NULL);
		if (!L_20)
		{
			goto IL_019c;
		}
	}
	{
		Removable_t2119088671 * L_21 = V_5;
		NullCheck(L_21);
		bool L_22 = Behaviour_get_enabled_m4079055610(L_21, /*hidden argument*/NULL);
		if (!L_22)
		{
			goto IL_019c;
		}
	}
	{
		Removable_t2119088671 * L_23 = V_5;
		NullCheck(L_23);
		Removable_Remove_m1955228078(L_23, /*hidden argument*/NULL);
		int32_t L_24 = __this->get_numberOfTaps_10();
		__this->set_numberOfTaps_10(((int32_t)((int32_t)L_24+(int32_t)1)));
		int32_t L_25 = __this->get_numberOfTaps_10();
		int32_t L_26 = __this->get_maxTapsFor3Stars_25();
		if ((((int32_t)L_25) < ((int32_t)((int32_t)((int32_t)L_26+(int32_t)1)))))
		{
			goto IL_00dd;
		}
	}
	{
		GameObject_t1756533147 * L_27 = GameObject_Find_m836511350(NULL /*static, unused*/, _stringLiteral2033965610, /*hidden argument*/NULL);
		V_6 = L_27;
		GameObject_t1756533147 * L_28 = V_6;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_29 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_28, /*hidden argument*/NULL);
		if (!L_29)
		{
			goto IL_00dd;
		}
	}
	{
		GameObject_t1756533147 * L_30 = V_6;
		NullCheck(L_30);
		Animator_t69676727 * L_31 = GameObject_GetComponent_TisAnimator_t69676727_m2717502299(L_30, /*hidden argument*/GameObject_GetComponent_TisAnimator_t69676727_m2717502299_MethodInfo_var);
		NullCheck(L_31);
		Animator_SetBool_m2305662531(L_31, _stringLiteral4136222740, (bool)1, /*hidden argument*/NULL);
	}

IL_00dd:
	{
		int32_t L_32 = __this->get_numberOfTaps_10();
		int32_t L_33 = __this->get_maxTapsFor2Stars_26();
		if ((((int32_t)L_32) < ((int32_t)((int32_t)((int32_t)L_33+(int32_t)1)))))
		{
			goto IL_011a;
		}
	}
	{
		GameObject_t1756533147 * L_34 = GameObject_Find_m836511350(NULL /*static, unused*/, _stringLiteral2033965613, /*hidden argument*/NULL);
		V_7 = L_34;
		GameObject_t1756533147 * L_35 = V_7;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_36 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_35, /*hidden argument*/NULL);
		if (!L_36)
		{
			goto IL_011a;
		}
	}
	{
		GameObject_t1756533147 * L_37 = V_7;
		NullCheck(L_37);
		Animator_t69676727 * L_38 = GameObject_GetComponent_TisAnimator_t69676727_m2717502299(L_37, /*hidden argument*/GameObject_GetComponent_TisAnimator_t69676727_m2717502299_MethodInfo_var);
		NullCheck(L_38);
		Animator_SetBool_m2305662531(L_38, _stringLiteral4136222740, (bool)1, /*hidden argument*/NULL);
	}

IL_011a:
	{
		int32_t L_39 = __this->get_numberOfTaps_10();
		int32_t L_40 = __this->get_maxTapsFor1Star_27();
		if ((((int32_t)L_39) < ((int32_t)((int32_t)((int32_t)L_40+(int32_t)1)))))
		{
			goto IL_0157;
		}
	}
	{
		GameObject_t1756533147 * L_41 = GameObject_Find_m836511350(NULL /*static, unused*/, _stringLiteral2033965612, /*hidden argument*/NULL);
		V_8 = L_41;
		GameObject_t1756533147 * L_42 = V_8;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_43 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_42, /*hidden argument*/NULL);
		if (!L_43)
		{
			goto IL_0157;
		}
	}
	{
		GameObject_t1756533147 * L_44 = V_8;
		NullCheck(L_44);
		Animator_t69676727 * L_45 = GameObject_GetComponent_TisAnimator_t69676727_m2717502299(L_44, /*hidden argument*/GameObject_GetComponent_TisAnimator_t69676727_m2717502299_MethodInfo_var);
		NullCheck(L_45);
		Animator_SetBool_m2305662531(L_45, _stringLiteral4136222740, (bool)1, /*hidden argument*/NULL);
	}

IL_0157:
	{
		GameObject_t1756533147 * L_46 = GameObject_Find_m836511350(NULL /*static, unused*/, _stringLiteral3599784509, /*hidden argument*/NULL);
		NullCheck(L_46);
		Text_t356221433 * L_47 = GameObject_GetComponent_TisText_t356221433_m1217399699(L_46, /*hidden argument*/GameObject_GetComponent_TisText_t356221433_m1217399699_MethodInfo_var);
		int32_t L_48 = __this->get_numberOfTaps_10();
		int32_t L_49 = L_48;
		Il2CppObject * L_50 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_49);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_51 = String_Concat_m56707527(NULL /*static, unused*/, L_50, _stringLiteral1187738806, /*hidden argument*/NULL);
		NullCheck(L_47);
		VirtActionInvoker1< String_t* >::Invoke(72 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_47, L_51);
		GameObject_t1756533147 * L_52 = V_4;
		NullCheck(L_52);
		Explode_t13292173 * L_53 = GameObject_GetComponent_TisExplode_t13292173_m1264009640(L_52, /*hidden argument*/GameObject_GetComponent_TisExplode_t13292173_m1264009640_MethodInfo_var);
		V_9 = L_53;
		Explode_t13292173 * L_54 = V_9;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_55 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_54, /*hidden argument*/NULL);
		if (!L_55)
		{
			goto IL_019c;
		}
	}
	{
		Explode_t13292173 * L_56 = V_9;
		NullCheck(L_56);
		Explode_Explosion_m1359633813(L_56, /*hidden argument*/NULL);
	}

IL_019c:
	{
		GameObject_t1756533147 * L_57 = V_4;
		NullCheck(L_57);
		TutorialNoNo_t3242399842 * L_58 = GameObject_GetComponent_TisTutorialNoNo_t3242399842_m3271948131(L_57, /*hidden argument*/GameObject_GetComponent_TisTutorialNoNo_t3242399842_m3271948131_MethodInfo_var);
		V_10 = L_58;
		TutorialNoNo_t3242399842 * L_59 = V_10;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_60 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_59, /*hidden argument*/NULL);
		if (!L_60)
		{
			goto IL_01da;
		}
	}
	{
		GameObject_t1756533147 * L_61 = GameObject_Find_m836511350(NULL /*static, unused*/, _stringLiteral1712688227, /*hidden argument*/NULL);
		V_11 = L_61;
		GameObject_t1756533147 * L_62 = V_11;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_63 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_62, /*hidden argument*/NULL);
		if (!L_63)
		{
			goto IL_01da;
		}
	}
	{
		GameObject_t1756533147 * L_64 = V_11;
		NullCheck(L_64);
		Animator_t69676727 * L_65 = GameObject_GetComponent_TisAnimator_t69676727_m2717502299(L_64, /*hidden argument*/GameObject_GetComponent_TisAnimator_t69676727_m2717502299_MethodInfo_var);
		NullCheck(L_65);
		Animator_SetTrigger_m3418492570(L_65, _stringLiteral2933275870, /*hidden argument*/NULL);
	}

IL_01da:
	{
		return;
	}
}
// System.Void GameMaster::goToNextLevel()
extern Il2CppClass* StaticLevelContainer_t3714456515_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32_t2071877448_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1594891052;
extern const uint32_t GameMaster_goToNextLevel_m4166812671_MetadataUsageId;
extern "C"  void GameMaster_goToNextLevel_m4166812671 (GameMaster_t3159518556 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GameMaster_goToNextLevel_m4166812671_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(StaticLevelContainer_t3714456515_il2cpp_TypeInfo_var);
		StaticLevelContainer_t3714456515 * L_0 = StaticLevelContainer_getInstance_m3684656527(NULL /*static, unused*/, /*hidden argument*/NULL);
		StaticLevelContainer_t3714456515 * L_1 = StaticLevelContainer_getInstance_m3684656527(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_1);
		int32_t L_2 = L_1->get_levelToLoad_0();
		NullCheck(L_0);
		L_0->set_levelToLoad_0(((int32_t)((int32_t)L_2+(int32_t)1)));
		StaticLevelContainer_t3714456515 * L_3 = StaticLevelContainer_getInstance_m3684656527(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_3);
		int32_t L_4 = L_3->get_levelToLoad_0();
		int32_t L_5 = L_4;
		Il2CppObject * L_6 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_5);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_7 = String_Concat_m56707527(NULL /*static, unused*/, _stringLiteral1594891052, L_6, /*hidden argument*/NULL);
		SceneManager_LoadScene_m1619949821(NULL /*static, unused*/, L_7, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GoToScene::.ctor()
extern "C"  void GoToScene__ctor_m3313655624 (GoToScene_t3084403383 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GoToScene::LoadLevel(System.String)
extern "C"  void GoToScene_LoadLevel_m3689284180 (GoToScene_t3084403383 * __this, String_t* ___scene0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___scene0;
		SceneManager_LoadScene_m1619949821(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ImportExportUtilities.Character::.ctor()
extern "C"  void Character__ctor_m1925457927 (Character_t2307506699 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ImportExportUtilities.Character::.ctor(ImportExportUtilities.Character)
extern "C"  void Character__ctor_m1119584827 (Character_t2307506699 * __this, Character_t2307506699 * ___other0, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		Character_t2307506699 * L_0 = ___other0;
		NullCheck(L_0);
		Vector2_t2243707579 * L_1 = L_0->get_address_of_Position_0();
		float L_2 = L_1->get_x_0();
		Character_t2307506699 * L_3 = ___other0;
		NullCheck(L_3);
		Vector2_t2243707579 * L_4 = L_3->get_address_of_Position_0();
		float L_5 = L_4->get_y_1();
		Vector2_t2243707579  L_6;
		memset(&L_6, 0, sizeof(L_6));
		Vector2__ctor_m3067419446(&L_6, L_2, L_5, /*hidden argument*/NULL);
		__this->set_Position_0(L_6);
		Character_t2307506699 * L_7 = ___other0;
		NullCheck(L_7);
		float L_8 = L_7->get_Rotation_1();
		__this->set_Rotation_1(L_8);
		Character_t2307506699 * L_9 = ___other0;
		NullCheck(L_9);
		float L_10 = L_9->get_Scale_2();
		__this->set_Scale_2(L_10);
		Character_t2307506699 * L_11 = ___other0;
		NullCheck(L_11);
		int32_t L_12 = L_11->get_GravityDirection_7();
		__this->set_GravityDirection_7(L_12);
		Character_t2307506699 * L_13 = ___other0;
		NullCheck(L_13);
		int32_t L_14 = L_13->get_Type_13();
		__this->set_Type_13(L_14);
		Character_t2307506699 * L_15 = ___other0;
		NullCheck(L_15);
		int32_t L_16 = L_15->get_Shape_16();
		__this->set_Shape_16(L_16);
		return;
	}
}
// System.Void ImportExportUtilities.ExportedLevel::.ctor(ImportExportUtilities.Level)
extern Il2CppClass* CharacterU5BU5D_t2288121194_il2cpp_TypeInfo_var;
extern Il2CppClass* PlatformU5BU5D_t3340127992_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_get_Count_m2453964909_MethodInfo_var;
extern const MethodInfo* List_1_GetEnumerator_m377810456_MethodInfo_var;
extern const MethodInfo* Enumerator_get_Current_m2360690042_MethodInfo_var;
extern const MethodInfo* Enumerator_MoveNext_m105615144_MethodInfo_var;
extern const MethodInfo* Enumerator_Dispose_m2730639556_MethodInfo_var;
extern const MethodInfo* List_1_get_Count_m1746284091_MethodInfo_var;
extern const MethodInfo* List_1_GetEnumerator_m2527438760_MethodInfo_var;
extern const MethodInfo* Enumerator_get_Current_m943113238_MethodInfo_var;
extern const MethodInfo* Enumerator_MoveNext_m2719952820_MethodInfo_var;
extern const MethodInfo* Enumerator_Dispose_m2140154946_MethodInfo_var;
extern const uint32_t ExportedLevel__ctor_m3218447752_MetadataUsageId;
extern "C"  void ExportedLevel__ctor_m3218447752 (ExportedLevel_t702369075 * __this, Level_t336836020 * ___level0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ExportedLevel__ctor_m3218447752_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Character_t2307506699 * V_0 = NULL;
	Enumerator_t1211357505  V_1;
	memset(&V_1, 0, sizeof(V_1));
	int32_t V_2 = 0;
	Platform_t1003211029 * V_3 = NULL;
	Enumerator_t4202029131  V_4;
	memset(&V_4, 0, sizeof(V_4));
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		Level_t336836020 * L_0 = ___level0;
		NullCheck(L_0);
		List_1_t1676627831 * L_1 = Level_get_characters_m1863299254(L_0, /*hidden argument*/NULL);
		NullCheck(L_1);
		int32_t L_2 = List_1_get_Count_m2453964909(L_1, /*hidden argument*/List_1_get_Count_m2453964909_MethodInfo_var);
		__this->set_characters_0(((CharacterU5BU5D_t2288121194*)SZArrayNew(CharacterU5BU5D_t2288121194_il2cpp_TypeInfo_var, (uint32_t)L_2)));
		__this->set_numberOfCharacters_2((-1));
		Level_t336836020 * L_3 = ___level0;
		NullCheck(L_3);
		List_1_t1676627831 * L_4 = Level_get_characters_m1863299254(L_3, /*hidden argument*/NULL);
		NullCheck(L_4);
		Enumerator_t1211357505  L_5 = List_1_GetEnumerator_m377810456(L_4, /*hidden argument*/List_1_GetEnumerator_m377810456_MethodInfo_var);
		V_1 = L_5;
	}

IL_002f:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0055;
		}

IL_0034:
		{
			Character_t2307506699 * L_6 = Enumerator_get_Current_m2360690042((&V_1), /*hidden argument*/Enumerator_get_Current_m2360690042_MethodInfo_var);
			V_0 = L_6;
			CharacterU5BU5D_t2288121194* L_7 = __this->get_characters_0();
			int32_t L_8 = __this->get_numberOfCharacters_2();
			int32_t L_9 = ((int32_t)((int32_t)L_8+(int32_t)1));
			V_2 = L_9;
			__this->set_numberOfCharacters_2(L_9);
			int32_t L_10 = V_2;
			Character_t2307506699 * L_11 = V_0;
			NullCheck(L_7);
			ArrayElementTypeCheck (L_7, L_11);
			(L_7)->SetAt(static_cast<il2cpp_array_size_t>(L_10), (Character_t2307506699 *)L_11);
		}

IL_0055:
		{
			bool L_12 = Enumerator_MoveNext_m105615144((&V_1), /*hidden argument*/Enumerator_MoveNext_m105615144_MethodInfo_var);
			if (L_12)
			{
				goto IL_0034;
			}
		}

IL_0061:
		{
			IL2CPP_LEAVE(0x74, FINALLY_0066);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_0066;
	}

FINALLY_0066:
	{ // begin finally (depth: 1)
		Enumerator_Dispose_m2730639556((&V_1), /*hidden argument*/Enumerator_Dispose_m2730639556_MethodInfo_var);
		IL2CPP_END_FINALLY(102)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(102)
	{
		IL2CPP_JUMP_TBL(0x74, IL_0074)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_0074:
	{
		int32_t L_13 = __this->get_numberOfCharacters_2();
		__this->set_numberOfCharacters_2(((int32_t)((int32_t)L_13+(int32_t)1)));
		Level_t336836020 * L_14 = ___level0;
		NullCheck(L_14);
		List_1_t372332161 * L_15 = Level_get_platforms_m4054011684(L_14, /*hidden argument*/NULL);
		NullCheck(L_15);
		int32_t L_16 = List_1_get_Count_m1746284091(L_15, /*hidden argument*/List_1_get_Count_m1746284091_MethodInfo_var);
		__this->set_platforms_1(((PlatformU5BU5D_t3340127992*)SZArrayNew(PlatformU5BU5D_t3340127992_il2cpp_TypeInfo_var, (uint32_t)L_16)));
		__this->set_numberOfPlatforms_3((-1));
		Level_t336836020 * L_17 = ___level0;
		NullCheck(L_17);
		List_1_t372332161 * L_18 = Level_get_platforms_m4054011684(L_17, /*hidden argument*/NULL);
		NullCheck(L_18);
		Enumerator_t4202029131  L_19 = List_1_GetEnumerator_m2527438760(L_18, /*hidden argument*/List_1_GetEnumerator_m2527438760_MethodInfo_var);
		V_4 = L_19;
	}

IL_00ac:
	try
	{ // begin try (depth: 1)
		{
			goto IL_00d2;
		}

IL_00b1:
		{
			Platform_t1003211029 * L_20 = Enumerator_get_Current_m943113238((&V_4), /*hidden argument*/Enumerator_get_Current_m943113238_MethodInfo_var);
			V_3 = L_20;
			PlatformU5BU5D_t3340127992* L_21 = __this->get_platforms_1();
			int32_t L_22 = __this->get_numberOfPlatforms_3();
			int32_t L_23 = ((int32_t)((int32_t)L_22+(int32_t)1));
			V_2 = L_23;
			__this->set_numberOfPlatforms_3(L_23);
			int32_t L_24 = V_2;
			Platform_t1003211029 * L_25 = V_3;
			NullCheck(L_21);
			ArrayElementTypeCheck (L_21, L_25);
			(L_21)->SetAt(static_cast<il2cpp_array_size_t>(L_24), (Platform_t1003211029 *)L_25);
		}

IL_00d2:
		{
			bool L_26 = Enumerator_MoveNext_m2719952820((&V_4), /*hidden argument*/Enumerator_MoveNext_m2719952820_MethodInfo_var);
			if (L_26)
			{
				goto IL_00b1;
			}
		}

IL_00de:
		{
			IL2CPP_LEAVE(0xF1, FINALLY_00e3);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_00e3;
	}

FINALLY_00e3:
	{ // begin finally (depth: 1)
		Enumerator_Dispose_m2140154946((&V_4), /*hidden argument*/Enumerator_Dispose_m2140154946_MethodInfo_var);
		IL2CPP_END_FINALLY(227)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(227)
	{
		IL2CPP_JUMP_TBL(0xF1, IL_00f1)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_00f1:
	{
		int32_t L_27 = __this->get_numberOfPlatforms_3();
		__this->set_numberOfPlatforms_3(((int32_t)((int32_t)L_27+(int32_t)1)));
		return;
	}
}
// System.Void ImportExportUtilities.Level::.ctor()
extern Il2CppClass* List_1_t1676627831_il2cpp_TypeInfo_var;
extern Il2CppClass* List_1_t372332161_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m2330312249_MethodInfo_var;
extern const MethodInfo* List_1__ctor_m1493086971_MethodInfo_var;
extern const uint32_t Level__ctor_m3742255388_MetadataUsageId;
extern "C"  void Level__ctor_m3742255388 (Level_t336836020 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Level__ctor_m3742255388_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		List_1_t1676627831 * L_0 = (List_1_t1676627831 *)il2cpp_codegen_object_new(List_1_t1676627831_il2cpp_TypeInfo_var);
		List_1__ctor_m2330312249(L_0, /*hidden argument*/List_1__ctor_m2330312249_MethodInfo_var);
		__this->set__characters_0(L_0);
		List_1_t372332161 * L_1 = (List_1_t372332161 *)il2cpp_codegen_object_new(List_1_t372332161_il2cpp_TypeInfo_var);
		List_1__ctor_m1493086971(L_1, /*hidden argument*/List_1__ctor_m1493086971_MethodInfo_var);
		__this->set__platforms_1(L_1);
		return;
	}
}
// System.Void ImportExportUtilities.Level::.ctor(System.String)
extern Il2CppClass* List_1_t1676627831_il2cpp_TypeInfo_var;
extern Il2CppClass* List_1_t372332161_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m2330312249_MethodInfo_var;
extern const MethodInfo* List_1__ctor_m1493086971_MethodInfo_var;
extern const MethodInfo* JsonUtility_FromJson_TisExportedLevel_t702369075_m2447477843_MethodInfo_var;
extern const MethodInfo* List_1_Add_m928605237_MethodInfo_var;
extern const MethodInfo* List_1_Add_m2207374671_MethodInfo_var;
extern const uint32_t Level__ctor_m442090598_MetadataUsageId;
extern "C"  void Level__ctor_m442090598 (Level_t336836020 * __this, String_t* ___path0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Level__ctor_m442090598_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	ExportedLevel_t702369075 * V_1 = NULL;
	Character_t2307506699 * V_2 = NULL;
	CharacterU5BU5D_t2288121194* V_3 = NULL;
	int32_t V_4 = 0;
	Platform_t1003211029 * V_5 = NULL;
	PlatformU5BU5D_t3340127992* V_6 = NULL;
	int32_t V_7 = 0;
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		List_1_t1676627831 * L_0 = (List_1_t1676627831 *)il2cpp_codegen_object_new(List_1_t1676627831_il2cpp_TypeInfo_var);
		List_1__ctor_m2330312249(L_0, /*hidden argument*/List_1__ctor_m2330312249_MethodInfo_var);
		__this->set__characters_0(L_0);
		List_1_t372332161 * L_1 = (List_1_t372332161 *)il2cpp_codegen_object_new(List_1_t372332161_il2cpp_TypeInfo_var);
		List_1__ctor_m1493086971(L_1, /*hidden argument*/List_1__ctor_m1493086971_MethodInfo_var);
		__this->set__platforms_1(L_1);
		String_t* L_2 = ___path0;
		String_t* L_3 = File_ReadAllText_m1018286608(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		String_t* L_4 = V_0;
		ExportedLevel_t702369075 * L_5 = JsonUtility_FromJson_TisExportedLevel_t702369075_m2447477843(NULL /*static, unused*/, L_4, /*hidden argument*/JsonUtility_FromJson_TisExportedLevel_t702369075_m2447477843_MethodInfo_var);
		V_1 = L_5;
		ExportedLevel_t702369075 * L_6 = V_1;
		NullCheck(L_6);
		CharacterU5BU5D_t2288121194* L_7 = L_6->get_characters_0();
		V_3 = L_7;
		V_4 = 0;
		goto IL_0050;
	}

IL_0039:
	{
		CharacterU5BU5D_t2288121194* L_8 = V_3;
		int32_t L_9 = V_4;
		NullCheck(L_8);
		int32_t L_10 = L_9;
		Character_t2307506699 * L_11 = (L_8)->GetAt(static_cast<il2cpp_array_size_t>(L_10));
		V_2 = L_11;
		List_1_t1676627831 * L_12 = __this->get__characters_0();
		Character_t2307506699 * L_13 = V_2;
		NullCheck(L_12);
		List_1_Add_m928605237(L_12, L_13, /*hidden argument*/List_1_Add_m928605237_MethodInfo_var);
		int32_t L_14 = V_4;
		V_4 = ((int32_t)((int32_t)L_14+(int32_t)1));
	}

IL_0050:
	{
		int32_t L_15 = V_4;
		CharacterU5BU5D_t2288121194* L_16 = V_3;
		NullCheck(L_16);
		if ((((int32_t)L_15) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_16)->max_length)))))))
		{
			goto IL_0039;
		}
	}
	{
		ExportedLevel_t702369075 * L_17 = V_1;
		NullCheck(L_17);
		PlatformU5BU5D_t3340127992* L_18 = L_17->get_platforms_1();
		V_6 = L_18;
		V_7 = 0;
		goto IL_0084;
	}

IL_006a:
	{
		PlatformU5BU5D_t3340127992* L_19 = V_6;
		int32_t L_20 = V_7;
		NullCheck(L_19);
		int32_t L_21 = L_20;
		Platform_t1003211029 * L_22 = (L_19)->GetAt(static_cast<il2cpp_array_size_t>(L_21));
		V_5 = L_22;
		List_1_t372332161 * L_23 = __this->get__platforms_1();
		Platform_t1003211029 * L_24 = V_5;
		NullCheck(L_23);
		List_1_Add_m2207374671(L_23, L_24, /*hidden argument*/List_1_Add_m2207374671_MethodInfo_var);
		int32_t L_25 = V_7;
		V_7 = ((int32_t)((int32_t)L_25+(int32_t)1));
	}

IL_0084:
	{
		int32_t L_26 = V_7;
		PlatformU5BU5D_t3340127992* L_27 = V_6;
		NullCheck(L_27);
		if ((((int32_t)L_26) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_27)->max_length)))))))
		{
			goto IL_006a;
		}
	}
	{
		return;
	}
}
// System.Void ImportExportUtilities.Level::.ctor(ImportExportUtilities.Level)
extern Il2CppClass* List_1_t1676627831_il2cpp_TypeInfo_var;
extern Il2CppClass* List_1_t372332161_il2cpp_TypeInfo_var;
extern Il2CppClass* Character_t2307506699_il2cpp_TypeInfo_var;
extern Il2CppClass* Platform_t1003211029_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m2330312249_MethodInfo_var;
extern const MethodInfo* List_1__ctor_m1493086971_MethodInfo_var;
extern const MethodInfo* List_1_GetEnumerator_m377810456_MethodInfo_var;
extern const MethodInfo* Enumerator_get_Current_m2360690042_MethodInfo_var;
extern const MethodInfo* List_1_Add_m928605237_MethodInfo_var;
extern const MethodInfo* Enumerator_MoveNext_m105615144_MethodInfo_var;
extern const MethodInfo* Enumerator_Dispose_m2730639556_MethodInfo_var;
extern const MethodInfo* List_1_GetEnumerator_m2527438760_MethodInfo_var;
extern const MethodInfo* Enumerator_get_Current_m943113238_MethodInfo_var;
extern const MethodInfo* List_1_Add_m2207374671_MethodInfo_var;
extern const MethodInfo* Enumerator_MoveNext_m2719952820_MethodInfo_var;
extern const MethodInfo* Enumerator_Dispose_m2140154946_MethodInfo_var;
extern const uint32_t Level__ctor_m1909254975_MetadataUsageId;
extern "C"  void Level__ctor_m1909254975 (Level_t336836020 * __this, Level_t336836020 * ___level0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Level__ctor_m1909254975_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Character_t2307506699 * V_0 = NULL;
	Enumerator_t1211357505  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Platform_t1003211029 * V_2 = NULL;
	Enumerator_t4202029131  V_3;
	memset(&V_3, 0, sizeof(V_3));
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		List_1_t1676627831 * L_0 = (List_1_t1676627831 *)il2cpp_codegen_object_new(List_1_t1676627831_il2cpp_TypeInfo_var);
		List_1__ctor_m2330312249(L_0, /*hidden argument*/List_1__ctor_m2330312249_MethodInfo_var);
		__this->set__characters_0(L_0);
		List_1_t372332161 * L_1 = (List_1_t372332161 *)il2cpp_codegen_object_new(List_1_t372332161_il2cpp_TypeInfo_var);
		List_1__ctor_m1493086971(L_1, /*hidden argument*/List_1__ctor_m1493086971_MethodInfo_var);
		__this->set__platforms_1(L_1);
		Level_t336836020 * L_2 = ___level0;
		NullCheck(L_2);
		List_1_t1676627831 * L_3 = Level_get_characters_m1863299254(L_2, /*hidden argument*/NULL);
		NullCheck(L_3);
		Enumerator_t1211357505  L_4 = List_1_GetEnumerator_m377810456(L_3, /*hidden argument*/List_1_GetEnumerator_m377810456_MethodInfo_var);
		V_1 = L_4;
	}

IL_0028:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0046;
		}

IL_002d:
		{
			Character_t2307506699 * L_5 = Enumerator_get_Current_m2360690042((&V_1), /*hidden argument*/Enumerator_get_Current_m2360690042_MethodInfo_var);
			V_0 = L_5;
			List_1_t1676627831 * L_6 = __this->get__characters_0();
			Character_t2307506699 * L_7 = V_0;
			Character_t2307506699 * L_8 = (Character_t2307506699 *)il2cpp_codegen_object_new(Character_t2307506699_il2cpp_TypeInfo_var);
			Character__ctor_m1119584827(L_8, L_7, /*hidden argument*/NULL);
			NullCheck(L_6);
			List_1_Add_m928605237(L_6, L_8, /*hidden argument*/List_1_Add_m928605237_MethodInfo_var);
		}

IL_0046:
		{
			bool L_9 = Enumerator_MoveNext_m105615144((&V_1), /*hidden argument*/Enumerator_MoveNext_m105615144_MethodInfo_var);
			if (L_9)
			{
				goto IL_002d;
			}
		}

IL_0052:
		{
			IL2CPP_LEAVE(0x65, FINALLY_0057);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_0057;
	}

FINALLY_0057:
	{ // begin finally (depth: 1)
		Enumerator_Dispose_m2730639556((&V_1), /*hidden argument*/Enumerator_Dispose_m2730639556_MethodInfo_var);
		IL2CPP_END_FINALLY(87)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(87)
	{
		IL2CPP_JUMP_TBL(0x65, IL_0065)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_0065:
	{
		Level_t336836020 * L_10 = ___level0;
		NullCheck(L_10);
		List_1_t372332161 * L_11 = Level_get_platforms_m4054011684(L_10, /*hidden argument*/NULL);
		NullCheck(L_11);
		Enumerator_t4202029131  L_12 = List_1_GetEnumerator_m2527438760(L_11, /*hidden argument*/List_1_GetEnumerator_m2527438760_MethodInfo_var);
		V_3 = L_12;
	}

IL_0071:
	try
	{ // begin try (depth: 1)
		{
			goto IL_008f;
		}

IL_0076:
		{
			Platform_t1003211029 * L_13 = Enumerator_get_Current_m943113238((&V_3), /*hidden argument*/Enumerator_get_Current_m943113238_MethodInfo_var);
			V_2 = L_13;
			List_1_t372332161 * L_14 = __this->get__platforms_1();
			Platform_t1003211029 * L_15 = V_2;
			Platform_t1003211029 * L_16 = (Platform_t1003211029 *)il2cpp_codegen_object_new(Platform_t1003211029_il2cpp_TypeInfo_var);
			Platform__ctor_m1662664651(L_16, L_15, /*hidden argument*/NULL);
			NullCheck(L_14);
			List_1_Add_m2207374671(L_14, L_16, /*hidden argument*/List_1_Add_m2207374671_MethodInfo_var);
		}

IL_008f:
		{
			bool L_17 = Enumerator_MoveNext_m2719952820((&V_3), /*hidden argument*/Enumerator_MoveNext_m2719952820_MethodInfo_var);
			if (L_17)
			{
				goto IL_0076;
			}
		}

IL_009b:
		{
			IL2CPP_LEAVE(0xAE, FINALLY_00a0);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_00a0;
	}

FINALLY_00a0:
	{ // begin finally (depth: 1)
		Enumerator_Dispose_m2140154946((&V_3), /*hidden argument*/Enumerator_Dispose_m2140154946_MethodInfo_var);
		IL2CPP_END_FINALLY(160)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(160)
	{
		IL2CPP_JUMP_TBL(0xAE, IL_00ae)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_00ae:
	{
		return;
	}
}
// System.Collections.Generic.List`1<ImportExportUtilities.Character> ImportExportUtilities.Level::get_characters()
extern "C"  List_1_t1676627831 * Level_get_characters_m1863299254 (Level_t336836020 * __this, const MethodInfo* method)
{
	{
		List_1_t1676627831 * L_0 = __this->get__characters_0();
		return L_0;
	}
}
// System.Collections.Generic.List`1<ImportExportUtilities.Platform> ImportExportUtilities.Level::get_platforms()
extern "C"  List_1_t372332161 * Level_get_platforms_m4054011684 (Level_t336836020 * __this, const MethodInfo* method)
{
	{
		List_1_t372332161 * L_0 = __this->get__platforms_1();
		return L_0;
	}
}
// System.Void ImportExportUtilities.Level::addCharacter(ImportExportUtilities.Character)
extern const MethodInfo* List_1_Add_m928605237_MethodInfo_var;
extern const uint32_t Level_addCharacter_m1611917466_MetadataUsageId;
extern "C"  void Level_addCharacter_m1611917466 (Level_t336836020 * __this, Character_t2307506699 * ___obj0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Level_addCharacter_m1611917466_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		List_1_t1676627831 * L_0 = __this->get__characters_0();
		Character_t2307506699 * L_1 = ___obj0;
		NullCheck(L_0);
		List_1_Add_m928605237(L_0, L_1, /*hidden argument*/List_1_Add_m928605237_MethodInfo_var);
		return;
	}
}
// System.Void ImportExportUtilities.Level::addPlatform(ImportExportUtilities.Platform)
extern const MethodInfo* List_1_Add_m2207374671_MethodInfo_var;
extern const uint32_t Level_addPlatform_m2853649974_MetadataUsageId;
extern "C"  void Level_addPlatform_m2853649974 (Level_t336836020 * __this, Platform_t1003211029 * ___obj0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Level_addPlatform_m2853649974_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		List_1_t372332161 * L_0 = __this->get__platforms_1();
		Platform_t1003211029 * L_1 = ___obj0;
		NullCheck(L_0);
		List_1_Add_m2207374671(L_0, L_1, /*hidden argument*/List_1_Add_m2207374671_MethodInfo_var);
		return;
	}
}
// System.Void ImportExportUtilities.Level::Export(System.String)
extern Il2CppClass* ExportedLevel_t702369075_il2cpp_TypeInfo_var;
extern Il2CppClass* StreamWriter_t3858580635_il2cpp_TypeInfo_var;
extern const uint32_t Level_Export_m2384450406_MetadataUsageId;
extern "C"  void Level_Export_m2384450406 (Level_t336836020 * __this, String_t* ___path0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Level_Export_m2384450406_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ExportedLevel_t702369075 * V_0 = NULL;
	String_t* V_1 = NULL;
	StreamWriter_t3858580635 * V_2 = NULL;
	{
		ExportedLevel_t702369075 * L_0 = (ExportedLevel_t702369075 *)il2cpp_codegen_object_new(ExportedLevel_t702369075_il2cpp_TypeInfo_var);
		ExportedLevel__ctor_m3218447752(L_0, __this, /*hidden argument*/NULL);
		V_0 = L_0;
		ExportedLevel_t702369075 * L_1 = V_0;
		String_t* L_2 = JsonUtility_ToJson_m1232500921(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		V_1 = L_2;
		String_t* L_3 = ___path0;
		StreamWriter_t3858580635 * L_4 = (StreamWriter_t3858580635 *)il2cpp_codegen_object_new(StreamWriter_t3858580635_il2cpp_TypeInfo_var);
		StreamWriter__ctor_m1804264596(L_4, L_3, /*hidden argument*/NULL);
		V_2 = L_4;
		StreamWriter_t3858580635 * L_5 = V_2;
		String_t* L_6 = V_1;
		NullCheck(L_5);
		VirtActionInvoker1< String_t* >::Invoke(14 /* System.Void System.IO.TextWriter::WriteLine(System.String) */, L_5, L_6);
		StreamWriter_t3858580635 * L_7 = V_2;
		NullCheck(L_7);
		VirtActionInvoker0::Invoke(5 /* System.Void System.IO.TextWriter::Close() */, L_7);
		return;
	}
}
// System.Void ImportExportUtilities.Level::GenerateToScene(ImportExportUtilities.Level)
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_GetEnumerator_m377810456_MethodInfo_var;
extern const MethodInfo* Enumerator_get_Current_m2360690042_MethodInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisGameMaster_t3159518556_m3423764161_MethodInfo_var;
extern const MethodInfo* Object_Instantiate_TisGameObject_t1756533147_m3664764861_MethodInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisTransform_t3275118058_m791278180_MethodInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisCustomGravity_t2378668691_m2385254694_MethodInfo_var;
extern const MethodInfo* Enumerator_MoveNext_m105615144_MethodInfo_var;
extern const MethodInfo* Enumerator_Dispose_m2730639556_MethodInfo_var;
extern const MethodInfo* List_1_GetEnumerator_m2527438760_MethodInfo_var;
extern const MethodInfo* Enumerator_get_Current_m943113238_MethodInfo_var;
extern const MethodInfo* Enumerator_MoveNext_m2719952820_MethodInfo_var;
extern const MethodInfo* Enumerator_Dispose_m2140154946_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral2830371216;
extern Il2CppCodeGenString* _stringLiteral3887597074;
extern const uint32_t Level_GenerateToScene_m306945417_MetadataUsageId;
extern "C"  void Level_GenerateToScene_m306945417 (Il2CppObject * __this /* static, unused */, Level_t336836020 * ___level0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Level_GenerateToScene_m306945417_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	GameObject_t1756533147 * V_0 = NULL;
	Character_t2307506699 * V_1 = NULL;
	Enumerator_t1211357505  V_2;
	memset(&V_2, 0, sizeof(V_2));
	GameObject_t1756533147 * V_3 = NULL;
	GameObject_t1756533147 * V_4 = NULL;
	int32_t V_5 = 0;
	Platform_t1003211029 * V_6 = NULL;
	Enumerator_t4202029131  V_7;
	memset(&V_7, 0, sizeof(V_7));
	GameObject_t1756533147 * V_8 = NULL;
	GameObject_t1756533147 * V_9 = NULL;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		GameObject_t1756533147 * L_0 = GameObject_Find_m836511350(NULL /*static, unused*/, _stringLiteral2830371216, /*hidden argument*/NULL);
		V_0 = L_0;
		goto IL_0026;
	}

IL_0010:
	{
		GameObject_t1756533147 * L_1 = V_0;
		NullCheck(L_1);
		Transform_t3275118058 * L_2 = GameObject_get_transform_m909382139(L_1, /*hidden argument*/NULL);
		NullCheck(L_2);
		Transform_t3275118058 * L_3 = Transform_GetChild_m3838588184(L_2, 0, /*hidden argument*/NULL);
		NullCheck(L_3);
		GameObject_t1756533147 * L_4 = Component_get_gameObject_m3105766835(L_3, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		Object_DestroyImmediate_m95027445(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
	}

IL_0026:
	{
		GameObject_t1756533147 * L_5 = V_0;
		NullCheck(L_5);
		Transform_t3275118058 * L_6 = GameObject_get_transform_m909382139(L_5, /*hidden argument*/NULL);
		NullCheck(L_6);
		int32_t L_7 = Transform_get_childCount_m881385315(L_6, /*hidden argument*/NULL);
		if ((((int32_t)L_7) > ((int32_t)0)))
		{
			goto IL_0010;
		}
	}
	{
		Level_t336836020 * L_8 = ___level0;
		NullCheck(L_8);
		List_1_t1676627831 * L_9 = Level_get_characters_m1863299254(L_8, /*hidden argument*/NULL);
		NullCheck(L_9);
		Enumerator_t1211357505  L_10 = List_1_GetEnumerator_m377810456(L_9, /*hidden argument*/List_1_GetEnumerator_m377810456_MethodInfo_var);
		V_2 = L_10;
	}

IL_0043:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0305;
		}

IL_0048:
		{
			Character_t2307506699 * L_11 = Enumerator_get_Current_m2360690042((&V_2), /*hidden argument*/Enumerator_get_Current_m2360690042_MethodInfo_var);
			V_1 = L_11;
			V_3 = (GameObject_t1756533147 *)NULL;
			V_4 = (GameObject_t1756533147 *)NULL;
			Character_t2307506699 * L_12 = V_1;
			NullCheck(L_12);
			int32_t L_13 = L_12->get_Type_13();
			if (L_13)
			{
				goto IL_00ad;
			}
		}

IL_0060:
		{
			Character_t2307506699 * L_14 = V_1;
			NullCheck(L_14);
			int32_t L_15 = L_14->get_Shape_16();
			if (L_15)
			{
				goto IL_0086;
			}
		}

IL_006b:
		{
			GameObject_t1756533147 * L_16 = GameObject_Find_m836511350(NULL /*static, unused*/, _stringLiteral3887597074, /*hidden argument*/NULL);
			NullCheck(L_16);
			GameMaster_t3159518556 * L_17 = GameObject_GetComponent_TisGameMaster_t3159518556_m3423764161(L_16, /*hidden argument*/GameObject_GetComponent_TisGameMaster_t3159518556_m3423764161_MethodInfo_var);
			NullCheck(L_17);
			GameObject_t1756533147 * L_18 = L_17->get_blueSquare_12();
			V_4 = L_18;
			goto IL_00a8;
		}

IL_0086:
		{
			Character_t2307506699 * L_19 = V_1;
			NullCheck(L_19);
			int32_t L_20 = L_19->get_Shape_16();
			if ((!(((uint32_t)L_20) == ((uint32_t)1))))
			{
				goto IL_00a8;
			}
		}

IL_0092:
		{
			GameObject_t1756533147 * L_21 = GameObject_Find_m836511350(NULL /*static, unused*/, _stringLiteral3887597074, /*hidden argument*/NULL);
			NullCheck(L_21);
			GameMaster_t3159518556 * L_22 = GameObject_GetComponent_TisGameMaster_t3159518556_m3423764161(L_21, /*hidden argument*/GameObject_GetComponent_TisGameMaster_t3159518556_m3423764161_MethodInfo_var);
			NullCheck(L_22);
			GameObject_t1756533147 * L_23 = L_22->get_blueCircle_13();
			V_4 = L_23;
		}

IL_00a8:
		{
			goto IL_020c;
		}

IL_00ad:
		{
			Character_t2307506699 * L_24 = V_1;
			NullCheck(L_24);
			int32_t L_25 = L_24->get_Type_13();
			if ((!(((uint32_t)L_25) == ((uint32_t)1))))
			{
				goto IL_0106;
			}
		}

IL_00b9:
		{
			Character_t2307506699 * L_26 = V_1;
			NullCheck(L_26);
			int32_t L_27 = L_26->get_Shape_16();
			if (L_27)
			{
				goto IL_00df;
			}
		}

IL_00c4:
		{
			GameObject_t1756533147 * L_28 = GameObject_Find_m836511350(NULL /*static, unused*/, _stringLiteral3887597074, /*hidden argument*/NULL);
			NullCheck(L_28);
			GameMaster_t3159518556 * L_29 = GameObject_GetComponent_TisGameMaster_t3159518556_m3423764161(L_28, /*hidden argument*/GameObject_GetComponent_TisGameMaster_t3159518556_m3423764161_MethodInfo_var);
			NullCheck(L_29);
			GameObject_t1756533147 * L_30 = L_29->get_redSquare_14();
			V_4 = L_30;
			goto IL_0101;
		}

IL_00df:
		{
			Character_t2307506699 * L_31 = V_1;
			NullCheck(L_31);
			int32_t L_32 = L_31->get_Shape_16();
			if ((!(((uint32_t)L_32) == ((uint32_t)1))))
			{
				goto IL_0101;
			}
		}

IL_00eb:
		{
			GameObject_t1756533147 * L_33 = GameObject_Find_m836511350(NULL /*static, unused*/, _stringLiteral3887597074, /*hidden argument*/NULL);
			NullCheck(L_33);
			GameMaster_t3159518556 * L_34 = GameObject_GetComponent_TisGameMaster_t3159518556_m3423764161(L_33, /*hidden argument*/GameObject_GetComponent_TisGameMaster_t3159518556_m3423764161_MethodInfo_var);
			NullCheck(L_34);
			GameObject_t1756533147 * L_35 = L_34->get_redCircle_15();
			V_4 = L_35;
		}

IL_0101:
		{
			goto IL_020c;
		}

IL_0106:
		{
			Character_t2307506699 * L_36 = V_1;
			NullCheck(L_36);
			int32_t L_37 = L_36->get_Type_13();
			if ((!(((uint32_t)L_37) == ((uint32_t)2))))
			{
				goto IL_015f;
			}
		}

IL_0112:
		{
			Character_t2307506699 * L_38 = V_1;
			NullCheck(L_38);
			int32_t L_39 = L_38->get_Shape_16();
			if (L_39)
			{
				goto IL_0138;
			}
		}

IL_011d:
		{
			GameObject_t1756533147 * L_40 = GameObject_Find_m836511350(NULL /*static, unused*/, _stringLiteral3887597074, /*hidden argument*/NULL);
			NullCheck(L_40);
			GameMaster_t3159518556 * L_41 = GameObject_GetComponent_TisGameMaster_t3159518556_m3423764161(L_40, /*hidden argument*/GameObject_GetComponent_TisGameMaster_t3159518556_m3423764161_MethodInfo_var);
			NullCheck(L_41);
			GameObject_t1756533147 * L_42 = L_41->get_darkRedSquare_16();
			V_4 = L_42;
			goto IL_015a;
		}

IL_0138:
		{
			Character_t2307506699 * L_43 = V_1;
			NullCheck(L_43);
			int32_t L_44 = L_43->get_Shape_16();
			if ((!(((uint32_t)L_44) == ((uint32_t)1))))
			{
				goto IL_015a;
			}
		}

IL_0144:
		{
			GameObject_t1756533147 * L_45 = GameObject_Find_m836511350(NULL /*static, unused*/, _stringLiteral3887597074, /*hidden argument*/NULL);
			NullCheck(L_45);
			GameMaster_t3159518556 * L_46 = GameObject_GetComponent_TisGameMaster_t3159518556_m3423764161(L_45, /*hidden argument*/GameObject_GetComponent_TisGameMaster_t3159518556_m3423764161_MethodInfo_var);
			NullCheck(L_46);
			GameObject_t1756533147 * L_47 = L_46->get_darkRedCircle_17();
			V_4 = L_47;
		}

IL_015a:
		{
			goto IL_020c;
		}

IL_015f:
		{
			Character_t2307506699 * L_48 = V_1;
			NullCheck(L_48);
			int32_t L_49 = L_48->get_Type_13();
			if ((!(((uint32_t)L_49) == ((uint32_t)3))))
			{
				goto IL_01b8;
			}
		}

IL_016b:
		{
			Character_t2307506699 * L_50 = V_1;
			NullCheck(L_50);
			int32_t L_51 = L_50->get_Shape_16();
			if (L_51)
			{
				goto IL_0191;
			}
		}

IL_0176:
		{
			GameObject_t1756533147 * L_52 = GameObject_Find_m836511350(NULL /*static, unused*/, _stringLiteral3887597074, /*hidden argument*/NULL);
			NullCheck(L_52);
			GameMaster_t3159518556 * L_53 = GameObject_GetComponent_TisGameMaster_t3159518556_m3423764161(L_52, /*hidden argument*/GameObject_GetComponent_TisGameMaster_t3159518556_m3423764161_MethodInfo_var);
			NullCheck(L_53);
			GameObject_t1756533147 * L_54 = L_53->get_yellowSquare_18();
			V_4 = L_54;
			goto IL_01b3;
		}

IL_0191:
		{
			Character_t2307506699 * L_55 = V_1;
			NullCheck(L_55);
			int32_t L_56 = L_55->get_Shape_16();
			if ((!(((uint32_t)L_56) == ((uint32_t)1))))
			{
				goto IL_01b3;
			}
		}

IL_019d:
		{
			GameObject_t1756533147 * L_57 = GameObject_Find_m836511350(NULL /*static, unused*/, _stringLiteral3887597074, /*hidden argument*/NULL);
			NullCheck(L_57);
			GameMaster_t3159518556 * L_58 = GameObject_GetComponent_TisGameMaster_t3159518556_m3423764161(L_57, /*hidden argument*/GameObject_GetComponent_TisGameMaster_t3159518556_m3423764161_MethodInfo_var);
			NullCheck(L_58);
			GameObject_t1756533147 * L_59 = L_58->get_yellowCircle_19();
			V_4 = L_59;
		}

IL_01b3:
		{
			goto IL_020c;
		}

IL_01b8:
		{
			Character_t2307506699 * L_60 = V_1;
			NullCheck(L_60);
			int32_t L_61 = L_60->get_Type_13();
			if ((!(((uint32_t)L_61) == ((uint32_t)4))))
			{
				goto IL_020c;
			}
		}

IL_01c4:
		{
			Character_t2307506699 * L_62 = V_1;
			NullCheck(L_62);
			int32_t L_63 = L_62->get_Shape_16();
			if (L_63)
			{
				goto IL_01ea;
			}
		}

IL_01cf:
		{
			GameObject_t1756533147 * L_64 = GameObject_Find_m836511350(NULL /*static, unused*/, _stringLiteral3887597074, /*hidden argument*/NULL);
			NullCheck(L_64);
			GameMaster_t3159518556 * L_65 = GameObject_GetComponent_TisGameMaster_t3159518556_m3423764161(L_64, /*hidden argument*/GameObject_GetComponent_TisGameMaster_t3159518556_m3423764161_MethodInfo_var);
			NullCheck(L_65);
			GameObject_t1756533147 * L_66 = L_65->get_greenSquare_20();
			V_4 = L_66;
			goto IL_020c;
		}

IL_01ea:
		{
			Character_t2307506699 * L_67 = V_1;
			NullCheck(L_67);
			int32_t L_68 = L_67->get_Shape_16();
			if ((!(((uint32_t)L_68) == ((uint32_t)1))))
			{
				goto IL_020c;
			}
		}

IL_01f6:
		{
			GameObject_t1756533147 * L_69 = GameObject_Find_m836511350(NULL /*static, unused*/, _stringLiteral3887597074, /*hidden argument*/NULL);
			NullCheck(L_69);
			GameMaster_t3159518556 * L_70 = GameObject_GetComponent_TisGameMaster_t3159518556_m3423764161(L_69, /*hidden argument*/GameObject_GetComponent_TisGameMaster_t3159518556_m3423764161_MethodInfo_var);
			NullCheck(L_70);
			GameObject_t1756533147 * L_71 = L_70->get_greenCircle_21();
			V_4 = L_71;
		}

IL_020c:
		{
			GameObject_t1756533147 * L_72 = V_4;
			IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
			bool L_73 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_72, /*hidden argument*/NULL);
			if (!L_73)
			{
				goto IL_0305;
			}
		}

IL_0218:
		{
			GameObject_t1756533147 * L_74 = V_4;
			IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
			GameObject_t1756533147 * L_75 = Object_Instantiate_TisGameObject_t1756533147_m3664764861(NULL /*static, unused*/, L_74, /*hidden argument*/Object_Instantiate_TisGameObject_t1756533147_m3664764861_MethodInfo_var);
			V_3 = L_75;
			GameObject_t1756533147 * L_76 = V_3;
			NullCheck(L_76);
			Transform_t3275118058 * L_77 = GameObject_get_transform_m909382139(L_76, /*hidden argument*/NULL);
			GameObject_t1756533147 * L_78 = V_0;
			NullCheck(L_78);
			Transform_t3275118058 * L_79 = GameObject_get_transform_m909382139(L_78, /*hidden argument*/NULL);
			NullCheck(L_77);
			Transform_set_parent_m3281327839(L_77, L_79, /*hidden argument*/NULL);
			GameObject_t1756533147 * L_80 = V_3;
			NullCheck(L_80);
			Transform_t3275118058 * L_81 = GameObject_GetComponent_TisTransform_t3275118058_m791278180(L_80, /*hidden argument*/GameObject_GetComponent_TisTransform_t3275118058_m791278180_MethodInfo_var);
			Character_t2307506699 * L_82 = V_1;
			NullCheck(L_82);
			Vector2_t2243707579 * L_83 = L_82->get_address_of_Position_0();
			float L_84 = L_83->get_x_0();
			Character_t2307506699 * L_85 = V_1;
			NullCheck(L_85);
			Vector2_t2243707579 * L_86 = L_85->get_address_of_Position_0();
			float L_87 = L_86->get_y_1();
			Vector3_t2243707580  L_88;
			memset(&L_88, 0, sizeof(L_88));
			Vector3__ctor_m2638739322(&L_88, L_84, L_87, (0.0f), /*hidden argument*/NULL);
			NullCheck(L_81);
			Transform_set_localPosition_m1026930133(L_81, L_88, /*hidden argument*/NULL);
			GameObject_t1756533147 * L_89 = V_3;
			NullCheck(L_89);
			Transform_t3275118058 * L_90 = GameObject_GetComponent_TisTransform_t3275118058_m791278180(L_89, /*hidden argument*/GameObject_GetComponent_TisTransform_t3275118058_m791278180_MethodInfo_var);
			Character_t2307506699 * L_91 = V_1;
			NullCheck(L_91);
			float L_92 = L_91->get_Rotation_1();
			Vector3_t2243707580  L_93;
			memset(&L_93, 0, sizeof(L_93));
			Vector3__ctor_m2638739322(&L_93, (0.0f), (0.0f), L_92, /*hidden argument*/NULL);
			NullCheck(L_90);
			Transform_set_localEulerAngles_m2927195985(L_90, L_93, /*hidden argument*/NULL);
			GameObject_t1756533147 * L_94 = V_3;
			NullCheck(L_94);
			Transform_t3275118058 * L_95 = GameObject_GetComponent_TisTransform_t3275118058_m791278180(L_94, /*hidden argument*/GameObject_GetComponent_TisTransform_t3275118058_m791278180_MethodInfo_var);
			Character_t2307506699 * L_96 = V_1;
			NullCheck(L_96);
			float L_97 = L_96->get_Scale_2();
			Character_t2307506699 * L_98 = V_1;
			NullCheck(L_98);
			float L_99 = L_98->get_Scale_2();
			Vector3_t2243707580  L_100;
			memset(&L_100, 0, sizeof(L_100));
			Vector3__ctor_m2638739322(&L_100, L_97, L_99, (1.0f), /*hidden argument*/NULL);
			NullCheck(L_95);
			Transform_set_localScale_m2325460848(L_95, L_100, /*hidden argument*/NULL);
			Character_t2307506699 * L_101 = V_1;
			NullCheck(L_101);
			int32_t L_102 = L_101->get_GravityDirection_7();
			V_5 = L_102;
			int32_t L_103 = V_5;
			if (L_103 == 0)
			{
				goto IL_02c1;
			}
			if (L_103 == 1)
			{
				goto IL_02d2;
			}
			if (L_103 == 2)
			{
				goto IL_02e3;
			}
			if (L_103 == 3)
			{
				goto IL_02f4;
			}
		}

IL_02bc:
		{
			goto IL_0305;
		}

IL_02c1:
		{
			GameObject_t1756533147 * L_104 = V_3;
			NullCheck(L_104);
			CustomGravity_t2378668691 * L_105 = GameObject_GetComponent_TisCustomGravity_t2378668691_m2385254694(L_104, /*hidden argument*/GameObject_GetComponent_TisCustomGravity_t2378668691_m2385254694_MethodInfo_var);
			NullCheck(L_105);
			L_105->set_gravityDirection_2(1);
			goto IL_0305;
		}

IL_02d2:
		{
			GameObject_t1756533147 * L_106 = V_3;
			NullCheck(L_106);
			CustomGravity_t2378668691 * L_107 = GameObject_GetComponent_TisCustomGravity_t2378668691_m2385254694(L_106, /*hidden argument*/GameObject_GetComponent_TisCustomGravity_t2378668691_m2385254694_MethodInfo_var);
			NullCheck(L_107);
			L_107->set_gravityDirection_2(0);
			goto IL_0305;
		}

IL_02e3:
		{
			GameObject_t1756533147 * L_108 = V_3;
			NullCheck(L_108);
			CustomGravity_t2378668691 * L_109 = GameObject_GetComponent_TisCustomGravity_t2378668691_m2385254694(L_108, /*hidden argument*/GameObject_GetComponent_TisCustomGravity_t2378668691_m2385254694_MethodInfo_var);
			NullCheck(L_109);
			L_109->set_gravityDirection_2(2);
			goto IL_0305;
		}

IL_02f4:
		{
			GameObject_t1756533147 * L_110 = V_3;
			NullCheck(L_110);
			CustomGravity_t2378668691 * L_111 = GameObject_GetComponent_TisCustomGravity_t2378668691_m2385254694(L_110, /*hidden argument*/GameObject_GetComponent_TisCustomGravity_t2378668691_m2385254694_MethodInfo_var);
			NullCheck(L_111);
			L_111->set_gravityDirection_2(3);
			goto IL_0305;
		}

IL_0305:
		{
			bool L_112 = Enumerator_MoveNext_m105615144((&V_2), /*hidden argument*/Enumerator_MoveNext_m105615144_MethodInfo_var);
			if (L_112)
			{
				goto IL_0048;
			}
		}

IL_0311:
		{
			IL2CPP_LEAVE(0x324, FINALLY_0316);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_0316;
	}

FINALLY_0316:
	{ // begin finally (depth: 1)
		Enumerator_Dispose_m2730639556((&V_2), /*hidden argument*/Enumerator_Dispose_m2730639556_MethodInfo_var);
		IL2CPP_END_FINALLY(790)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(790)
	{
		IL2CPP_JUMP_TBL(0x324, IL_0324)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_0324:
	{
		Level_t336836020 * L_113 = ___level0;
		NullCheck(L_113);
		List_1_t372332161 * L_114 = Level_get_platforms_m4054011684(L_113, /*hidden argument*/NULL);
		NullCheck(L_114);
		Enumerator_t4202029131  L_115 = List_1_GetEnumerator_m2527438760(L_114, /*hidden argument*/List_1_GetEnumerator_m2527438760_MethodInfo_var);
		V_7 = L_115;
	}

IL_0331:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0452;
		}

IL_0336:
		{
			Platform_t1003211029 * L_116 = Enumerator_get_Current_m943113238((&V_7), /*hidden argument*/Enumerator_get_Current_m943113238_MethodInfo_var);
			V_6 = L_116;
			V_8 = (GameObject_t1756533147 *)NULL;
			V_9 = (GameObject_t1756533147 *)NULL;
			Platform_t1003211029 * L_117 = V_6;
			NullCheck(L_117);
			int32_t L_118 = L_117->get_Type_7();
			if (L_118)
			{
				goto IL_036c;
			}
		}

IL_0351:
		{
			GameObject_t1756533147 * L_119 = GameObject_Find_m836511350(NULL /*static, unused*/, _stringLiteral3887597074, /*hidden argument*/NULL);
			NullCheck(L_119);
			GameMaster_t3159518556 * L_120 = GameObject_GetComponent_TisGameMaster_t3159518556_m3423764161(L_119, /*hidden argument*/GameObject_GetComponent_TisGameMaster_t3159518556_m3423764161_MethodInfo_var);
			NullCheck(L_120);
			GameObject_t1756533147 * L_121 = L_120->get_bluePlatform_23();
			V_9 = L_121;
			goto IL_03b7;
		}

IL_036c:
		{
			Platform_t1003211029 * L_122 = V_6;
			NullCheck(L_122);
			int32_t L_123 = L_122->get_Type_7();
			if ((!(((uint32_t)L_123) == ((uint32_t)1))))
			{
				goto IL_0394;
			}
		}

IL_0379:
		{
			GameObject_t1756533147 * L_124 = GameObject_Find_m836511350(NULL /*static, unused*/, _stringLiteral3887597074, /*hidden argument*/NULL);
			NullCheck(L_124);
			GameMaster_t3159518556 * L_125 = GameObject_GetComponent_TisGameMaster_t3159518556_m3423764161(L_124, /*hidden argument*/GameObject_GetComponent_TisGameMaster_t3159518556_m3423764161_MethodInfo_var);
			NullCheck(L_125);
			GameObject_t1756533147 * L_126 = L_125->get_redPlatform_22();
			V_9 = L_126;
			goto IL_03b7;
		}

IL_0394:
		{
			Platform_t1003211029 * L_127 = V_6;
			NullCheck(L_127);
			int32_t L_128 = L_127->get_Type_7();
			if ((!(((uint32_t)L_128) == ((uint32_t)4))))
			{
				goto IL_03b7;
			}
		}

IL_03a1:
		{
			GameObject_t1756533147 * L_129 = GameObject_Find_m836511350(NULL /*static, unused*/, _stringLiteral3887597074, /*hidden argument*/NULL);
			NullCheck(L_129);
			GameMaster_t3159518556 * L_130 = GameObject_GetComponent_TisGameMaster_t3159518556_m3423764161(L_129, /*hidden argument*/GameObject_GetComponent_TisGameMaster_t3159518556_m3423764161_MethodInfo_var);
			NullCheck(L_130);
			GameObject_t1756533147 * L_131 = L_130->get_purplePlatform_24();
			V_9 = L_131;
		}

IL_03b7:
		{
			GameObject_t1756533147 * L_132 = V_9;
			IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
			bool L_133 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_132, /*hidden argument*/NULL);
			if (!L_133)
			{
				goto IL_0452;
			}
		}

IL_03c3:
		{
			GameObject_t1756533147 * L_134 = V_9;
			IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
			GameObject_t1756533147 * L_135 = Object_Instantiate_TisGameObject_t1756533147_m3664764861(NULL /*static, unused*/, L_134, /*hidden argument*/Object_Instantiate_TisGameObject_t1756533147_m3664764861_MethodInfo_var);
			V_8 = L_135;
			GameObject_t1756533147 * L_136 = V_8;
			NullCheck(L_136);
			Transform_t3275118058 * L_137 = GameObject_get_transform_m909382139(L_136, /*hidden argument*/NULL);
			GameObject_t1756533147 * L_138 = V_0;
			NullCheck(L_138);
			Transform_t3275118058 * L_139 = GameObject_get_transform_m909382139(L_138, /*hidden argument*/NULL);
			NullCheck(L_137);
			Transform_set_parent_m3281327839(L_137, L_139, /*hidden argument*/NULL);
			GameObject_t1756533147 * L_140 = V_8;
			NullCheck(L_140);
			Transform_t3275118058 * L_141 = GameObject_GetComponent_TisTransform_t3275118058_m791278180(L_140, /*hidden argument*/GameObject_GetComponent_TisTransform_t3275118058_m791278180_MethodInfo_var);
			Platform_t1003211029 * L_142 = V_6;
			NullCheck(L_142);
			Vector2_t2243707579 * L_143 = L_142->get_address_of_Position_0();
			float L_144 = L_143->get_x_0();
			Platform_t1003211029 * L_145 = V_6;
			NullCheck(L_145);
			Vector2_t2243707579 * L_146 = L_145->get_address_of_Position_0();
			float L_147 = L_146->get_y_1();
			Vector3_t2243707580  L_148;
			memset(&L_148, 0, sizeof(L_148));
			Vector3__ctor_m2638739322(&L_148, L_144, L_147, (0.0f), /*hidden argument*/NULL);
			NullCheck(L_141);
			Transform_set_localPosition_m1026930133(L_141, L_148, /*hidden argument*/NULL);
			GameObject_t1756533147 * L_149 = V_8;
			NullCheck(L_149);
			Transform_t3275118058 * L_150 = GameObject_GetComponent_TisTransform_t3275118058_m791278180(L_149, /*hidden argument*/GameObject_GetComponent_TisTransform_t3275118058_m791278180_MethodInfo_var);
			Platform_t1003211029 * L_151 = V_6;
			NullCheck(L_151);
			float L_152 = L_151->get_Rotation_1();
			Vector3_t2243707580  L_153;
			memset(&L_153, 0, sizeof(L_153));
			Vector3__ctor_m2638739322(&L_153, (0.0f), (0.0f), L_152, /*hidden argument*/NULL);
			NullCheck(L_150);
			Transform_set_localEulerAngles_m2927195985(L_150, L_153, /*hidden argument*/NULL);
			GameObject_t1756533147 * L_154 = V_8;
			NullCheck(L_154);
			Transform_t3275118058 * L_155 = GameObject_GetComponent_TisTransform_t3275118058_m791278180(L_154, /*hidden argument*/GameObject_GetComponent_TisTransform_t3275118058_m791278180_MethodInfo_var);
			Platform_t1003211029 * L_156 = V_6;
			NullCheck(L_156);
			float L_157 = L_156->get_Scale_2();
			Platform_t1003211029 * L_158 = V_6;
			NullCheck(L_158);
			float L_159 = L_158->get_Scale_2();
			Vector3_t2243707580  L_160;
			memset(&L_160, 0, sizeof(L_160));
			Vector3__ctor_m2638739322(&L_160, L_157, L_159, (1.0f), /*hidden argument*/NULL);
			NullCheck(L_155);
			Transform_set_localScale_m2325460848(L_155, L_160, /*hidden argument*/NULL);
		}

IL_0452:
		{
			bool L_161 = Enumerator_MoveNext_m2719952820((&V_7), /*hidden argument*/Enumerator_MoveNext_m2719952820_MethodInfo_var);
			if (L_161)
			{
				goto IL_0336;
			}
		}

IL_045e:
		{
			IL2CPP_LEAVE(0x471, FINALLY_0463);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_0463;
	}

FINALLY_0463:
	{ // begin finally (depth: 1)
		Enumerator_Dispose_m2140154946((&V_7), /*hidden argument*/Enumerator_Dispose_m2140154946_MethodInfo_var);
		IL2CPP_END_FINALLY(1123)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(1123)
	{
		IL2CPP_JUMP_TBL(0x471, IL_0471)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_0471:
	{
		return;
	}
}
// ImportExportUtilities.Level ImportExportUtilities.Level::GenerateFromScene()
extern Il2CppClass* Level_t336836020_il2cpp_TypeInfo_var;
extern Il2CppClass* Character_t2307506699_il2cpp_TypeInfo_var;
extern Il2CppClass* Platform_t1003211029_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisCustomGravity_t2378668691_m2385254694_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral2830371216;
extern Il2CppCodeGenString* _stringLiteral159138813;
extern Il2CppCodeGenString* _stringLiteral4055698954;
extern Il2CppCodeGenString* _stringLiteral4173139095;
extern Il2CppCodeGenString* _stringLiteral3021629811;
extern Il2CppCodeGenString* _stringLiteral2395476974;
extern Il2CppCodeGenString* _stringLiteral777220966;
extern Il2CppCodeGenString* _stringLiteral3510846499;
extern Il2CppCodeGenString* _stringLiteral2328219853;
extern Il2CppCodeGenString* _stringLiteral4158023012;
extern Il2CppCodeGenString* _stringLiteral22442200;
extern const uint32_t Level_GenerateFromScene_m2982569127_MetadataUsageId;
extern "C"  Level_t336836020 * Level_GenerateFromScene_m2982569127 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Level_GenerateFromScene_m2982569127_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	GameObject_t1756533147 * V_0 = NULL;
	Level_t336836020 * V_1 = NULL;
	int32_t V_2 = 0;
	GameObject_t1756533147 * V_3 = NULL;
	String_t* V_4 = NULL;
	Character_t2307506699 * V_5 = NULL;
	Vector3_t2243707580  V_6;
	memset(&V_6, 0, sizeof(V_6));
	Vector3_t2243707580  V_7;
	memset(&V_7, 0, sizeof(V_7));
	Quaternion_t4030073918  V_8;
	memset(&V_8, 0, sizeof(V_8));
	Vector3_t2243707580  V_9;
	memset(&V_9, 0, sizeof(V_9));
	CustomGravity_t2378668691 * V_10 = NULL;
	int32_t V_11 = 0;
	Platform_t1003211029 * V_12 = NULL;
	Vector3_t2243707580  V_13;
	memset(&V_13, 0, sizeof(V_13));
	Vector3_t2243707580  V_14;
	memset(&V_14, 0, sizeof(V_14));
	Quaternion_t4030073918  V_15;
	memset(&V_15, 0, sizeof(V_15));
	Vector3_t2243707580  V_16;
	memset(&V_16, 0, sizeof(V_16));
	{
		GameObject_t1756533147 * L_0 = GameObject_Find_m836511350(NULL /*static, unused*/, _stringLiteral2830371216, /*hidden argument*/NULL);
		V_0 = L_0;
		Level_t336836020 * L_1 = (Level_t336836020 *)il2cpp_codegen_object_new(Level_t336836020_il2cpp_TypeInfo_var);
		Level__ctor_m3742255388(L_1, /*hidden argument*/NULL);
		V_1 = L_1;
		V_2 = 0;
		goto IL_02d0;
	}

IL_0018:
	{
		GameObject_t1756533147 * L_2 = V_0;
		NullCheck(L_2);
		Transform_t3275118058 * L_3 = GameObject_get_transform_m909382139(L_2, /*hidden argument*/NULL);
		int32_t L_4 = V_2;
		NullCheck(L_3);
		Transform_t3275118058 * L_5 = Transform_GetChild_m3838588184(L_3, L_4, /*hidden argument*/NULL);
		NullCheck(L_5);
		GameObject_t1756533147 * L_6 = Component_get_gameObject_m3105766835(L_5, /*hidden argument*/NULL);
		V_3 = L_6;
		GameObject_t1756533147 * L_7 = V_3;
		NullCheck(L_7);
		Transform_t3275118058 * L_8 = GameObject_get_transform_m909382139(L_7, /*hidden argument*/NULL);
		NullCheck(L_8);
		String_t* L_9 = Object_get_name_m2079638459(L_8, /*hidden argument*/NULL);
		V_4 = L_9;
		String_t* L_10 = V_4;
		NullCheck(L_10);
		bool L_11 = String_Contains_m4017059963(L_10, _stringLiteral159138813, /*hidden argument*/NULL);
		if (!L_11)
		{
			goto IL_01de;
		}
	}
	{
		Character_t2307506699 * L_12 = (Character_t2307506699 *)il2cpp_codegen_object_new(Character_t2307506699_il2cpp_TypeInfo_var);
		Character__ctor_m1925457927(L_12, /*hidden argument*/NULL);
		V_5 = L_12;
		Character_t2307506699 * L_13 = V_5;
		GameObject_t1756533147 * L_14 = V_3;
		NullCheck(L_14);
		Transform_t3275118058 * L_15 = GameObject_get_transform_m909382139(L_14, /*hidden argument*/NULL);
		NullCheck(L_15);
		Vector3_t2243707580  L_16 = Transform_get_position_m1104419803(L_15, /*hidden argument*/NULL);
		V_6 = L_16;
		float L_17 = (&V_6)->get_x_1();
		GameObject_t1756533147 * L_18 = V_3;
		NullCheck(L_18);
		Transform_t3275118058 * L_19 = GameObject_get_transform_m909382139(L_18, /*hidden argument*/NULL);
		NullCheck(L_19);
		Vector3_t2243707580  L_20 = Transform_get_position_m1104419803(L_19, /*hidden argument*/NULL);
		V_7 = L_20;
		float L_21 = (&V_7)->get_y_2();
		Vector2_t2243707579  L_22;
		memset(&L_22, 0, sizeof(L_22));
		Vector2__ctor_m3067419446(&L_22, L_17, L_21, /*hidden argument*/NULL);
		NullCheck(L_13);
		L_13->set_Position_0(L_22);
		Character_t2307506699 * L_23 = V_5;
		GameObject_t1756533147 * L_24 = V_3;
		NullCheck(L_24);
		Transform_t3275118058 * L_25 = GameObject_get_transform_m909382139(L_24, /*hidden argument*/NULL);
		NullCheck(L_25);
		Quaternion_t4030073918  L_26 = Transform_get_rotation_m1033555130(L_25, /*hidden argument*/NULL);
		V_8 = L_26;
		float L_27 = (&V_8)->get_z_2();
		NullCheck(L_23);
		L_23->set_Rotation_1(L_27);
		Character_t2307506699 * L_28 = V_5;
		GameObject_t1756533147 * L_29 = V_3;
		NullCheck(L_29);
		Transform_t3275118058 * L_30 = GameObject_get_transform_m909382139(L_29, /*hidden argument*/NULL);
		NullCheck(L_30);
		Vector3_t2243707580  L_31 = Transform_get_localScale_m3074381503(L_30, /*hidden argument*/NULL);
		V_9 = L_31;
		float L_32 = (&V_9)->get_x_1();
		NullCheck(L_28);
		L_28->set_Scale_2(L_32);
		GameObject_t1756533147 * L_33 = V_3;
		NullCheck(L_33);
		CustomGravity_t2378668691 * L_34 = GameObject_GetComponent_TisCustomGravity_t2378668691_m2385254694(L_33, /*hidden argument*/GameObject_GetComponent_TisCustomGravity_t2378668691_m2385254694_MethodInfo_var);
		V_10 = L_34;
		CustomGravity_t2378668691 * L_35 = V_10;
		NullCheck(L_35);
		int32_t L_36 = L_35->get_gravityDirection_2();
		V_11 = L_36;
		int32_t L_37 = V_11;
		if (L_37 == 0)
		{
			goto IL_00f3;
		}
		if (L_37 == 1)
		{
			goto IL_00e6;
		}
		if (L_37 == 2)
		{
			goto IL_0100;
		}
		if (L_37 == 3)
		{
			goto IL_010d;
		}
	}
	{
		goto IL_011a;
	}

IL_00e6:
	{
		Character_t2307506699 * L_38 = V_5;
		NullCheck(L_38);
		L_38->set_GravityDirection_7(0);
		goto IL_011a;
	}

IL_00f3:
	{
		Character_t2307506699 * L_39 = V_5;
		NullCheck(L_39);
		L_39->set_GravityDirection_7(1);
		goto IL_011a;
	}

IL_0100:
	{
		Character_t2307506699 * L_40 = V_5;
		NullCheck(L_40);
		L_40->set_GravityDirection_7(2);
		goto IL_011a;
	}

IL_010d:
	{
		Character_t2307506699 * L_41 = V_5;
		NullCheck(L_41);
		L_41->set_GravityDirection_7(3);
		goto IL_011a;
	}

IL_011a:
	{
		String_t* L_42 = V_4;
		NullCheck(L_42);
		bool L_43 = String_Contains_m4017059963(L_42, _stringLiteral4055698954, /*hidden argument*/NULL);
		if (!L_43)
		{
			goto IL_0138;
		}
	}
	{
		Character_t2307506699 * L_44 = V_5;
		NullCheck(L_44);
		L_44->set_Shape_16(1);
		goto IL_0140;
	}

IL_0138:
	{
		Character_t2307506699 * L_45 = V_5;
		NullCheck(L_45);
		L_45->set_Shape_16(0);
	}

IL_0140:
	{
		String_t* L_46 = V_4;
		NullCheck(L_46);
		bool L_47 = String_Contains_m4017059963(L_46, _stringLiteral4173139095, /*hidden argument*/NULL);
		if (!L_47)
		{
			goto IL_015e;
		}
	}
	{
		Character_t2307506699 * L_48 = V_5;
		NullCheck(L_48);
		L_48->set_Type_13(2);
		goto IL_01d1;
	}

IL_015e:
	{
		String_t* L_49 = V_4;
		NullCheck(L_49);
		bool L_50 = String_Contains_m4017059963(L_49, _stringLiteral3021629811, /*hidden argument*/NULL);
		if (!L_50)
		{
			goto IL_017c;
		}
	}
	{
		Character_t2307506699 * L_51 = V_5;
		NullCheck(L_51);
		L_51->set_Type_13(1);
		goto IL_01d1;
	}

IL_017c:
	{
		String_t* L_52 = V_4;
		NullCheck(L_52);
		bool L_53 = String_Contains_m4017059963(L_52, _stringLiteral2395476974, /*hidden argument*/NULL);
		if (!L_53)
		{
			goto IL_019a;
		}
	}
	{
		Character_t2307506699 * L_54 = V_5;
		NullCheck(L_54);
		L_54->set_Type_13(0);
		goto IL_01d1;
	}

IL_019a:
	{
		String_t* L_55 = V_4;
		NullCheck(L_55);
		bool L_56 = String_Contains_m4017059963(L_55, _stringLiteral777220966, /*hidden argument*/NULL);
		if (!L_56)
		{
			goto IL_01b8;
		}
	}
	{
		Character_t2307506699 * L_57 = V_5;
		NullCheck(L_57);
		L_57->set_Type_13(3);
		goto IL_01d1;
	}

IL_01b8:
	{
		String_t* L_58 = V_4;
		NullCheck(L_58);
		bool L_59 = String_Contains_m4017059963(L_58, _stringLiteral3510846499, /*hidden argument*/NULL);
		if (!L_59)
		{
			goto IL_01d1;
		}
	}
	{
		Character_t2307506699 * L_60 = V_5;
		NullCheck(L_60);
		L_60->set_Type_13(4);
	}

IL_01d1:
	{
		Level_t336836020 * L_61 = V_1;
		Character_t2307506699 * L_62 = V_5;
		NullCheck(L_61);
		Level_addCharacter_m1611917466(L_61, L_62, /*hidden argument*/NULL);
		goto IL_02cc;
	}

IL_01de:
	{
		String_t* L_63 = V_4;
		NullCheck(L_63);
		bool L_64 = String_Contains_m4017059963(L_63, _stringLiteral2328219853, /*hidden argument*/NULL);
		if (!L_64)
		{
			goto IL_02c2;
		}
	}
	{
		Platform_t1003211029 * L_65 = (Platform_t1003211029 *)il2cpp_codegen_object_new(Platform_t1003211029_il2cpp_TypeInfo_var);
		Platform__ctor_m1128244901(L_65, /*hidden argument*/NULL);
		V_12 = L_65;
		Platform_t1003211029 * L_66 = V_12;
		GameObject_t1756533147 * L_67 = V_3;
		NullCheck(L_67);
		Transform_t3275118058 * L_68 = GameObject_get_transform_m909382139(L_67, /*hidden argument*/NULL);
		NullCheck(L_68);
		Vector3_t2243707580  L_69 = Transform_get_position_m1104419803(L_68, /*hidden argument*/NULL);
		V_13 = L_69;
		float L_70 = (&V_13)->get_x_1();
		GameObject_t1756533147 * L_71 = V_3;
		NullCheck(L_71);
		Transform_t3275118058 * L_72 = GameObject_get_transform_m909382139(L_71, /*hidden argument*/NULL);
		NullCheck(L_72);
		Vector3_t2243707580  L_73 = Transform_get_position_m1104419803(L_72, /*hidden argument*/NULL);
		V_14 = L_73;
		float L_74 = (&V_14)->get_y_2();
		Vector2_t2243707579  L_75;
		memset(&L_75, 0, sizeof(L_75));
		Vector2__ctor_m3067419446(&L_75, L_70, L_74, /*hidden argument*/NULL);
		NullCheck(L_66);
		L_66->set_Position_0(L_75);
		Platform_t1003211029 * L_76 = V_12;
		GameObject_t1756533147 * L_77 = V_3;
		NullCheck(L_77);
		Transform_t3275118058 * L_78 = GameObject_get_transform_m909382139(L_77, /*hidden argument*/NULL);
		NullCheck(L_78);
		Quaternion_t4030073918  L_79 = Transform_get_rotation_m1033555130(L_78, /*hidden argument*/NULL);
		V_15 = L_79;
		float L_80 = (&V_15)->get_z_2();
		NullCheck(L_76);
		L_76->set_Rotation_1(L_80);
		Platform_t1003211029 * L_81 = V_12;
		GameObject_t1756533147 * L_82 = V_3;
		NullCheck(L_82);
		Transform_t3275118058 * L_83 = GameObject_get_transform_m909382139(L_82, /*hidden argument*/NULL);
		NullCheck(L_83);
		Vector3_t2243707580  L_84 = Transform_get_localScale_m3074381503(L_83, /*hidden argument*/NULL);
		V_16 = L_84;
		float L_85 = (&V_16)->get_x_1();
		NullCheck(L_81);
		L_81->set_Scale_2(L_85);
		String_t* L_86 = V_4;
		NullCheck(L_86);
		bool L_87 = String_Contains_m4017059963(L_86, _stringLiteral2395476974, /*hidden argument*/NULL);
		if (!L_87)
		{
			goto IL_027e;
		}
	}
	{
		Platform_t1003211029 * L_88 = V_12;
		NullCheck(L_88);
		L_88->set_Type_7(0);
		goto IL_02b5;
	}

IL_027e:
	{
		String_t* L_89 = V_4;
		NullCheck(L_89);
		bool L_90 = String_Contains_m4017059963(L_89, _stringLiteral3021629811, /*hidden argument*/NULL);
		if (!L_90)
		{
			goto IL_029c;
		}
	}
	{
		Platform_t1003211029 * L_91 = V_12;
		NullCheck(L_91);
		L_91->set_Type_7(1);
		goto IL_02b5;
	}

IL_029c:
	{
		String_t* L_92 = V_4;
		NullCheck(L_92);
		bool L_93 = String_Contains_m4017059963(L_92, _stringLiteral4158023012, /*hidden argument*/NULL);
		if (!L_93)
		{
			goto IL_02b5;
		}
	}
	{
		Platform_t1003211029 * L_94 = V_12;
		NullCheck(L_94);
		L_94->set_Type_7(4);
	}

IL_02b5:
	{
		Level_t336836020 * L_95 = V_1;
		Platform_t1003211029 * L_96 = V_12;
		NullCheck(L_95);
		Level_addPlatform_m2853649974(L_95, L_96, /*hidden argument*/NULL);
		goto IL_02cc;
	}

IL_02c2:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, _stringLiteral22442200, /*hidden argument*/NULL);
	}

IL_02cc:
	{
		int32_t L_97 = V_2;
		V_2 = ((int32_t)((int32_t)L_97+(int32_t)1));
	}

IL_02d0:
	{
		int32_t L_98 = V_2;
		GameObject_t1756533147 * L_99 = V_0;
		NullCheck(L_99);
		Transform_t3275118058 * L_100 = GameObject_get_transform_m909382139(L_99, /*hidden argument*/NULL);
		NullCheck(L_100);
		int32_t L_101 = Transform_get_childCount_m881385315(L_100, /*hidden argument*/NULL);
		if ((((int32_t)L_98) < ((int32_t)L_101)))
		{
			goto IL_0018;
		}
	}
	{
		Level_t336836020 * L_102 = V_1;
		return L_102;
	}
}
// System.Void ImportExportUtilities.Platform::.ctor()
extern "C"  void Platform__ctor_m1128244901 (Platform_t1003211029 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ImportExportUtilities.Platform::.ctor(ImportExportUtilities.Platform)
extern "C"  void Platform__ctor_m1662664651 (Platform_t1003211029 * __this, Platform_t1003211029 * ___other0, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		Platform_t1003211029 * L_0 = ___other0;
		NullCheck(L_0);
		Vector2_t2243707579 * L_1 = L_0->get_address_of_Position_0();
		float L_2 = L_1->get_x_0();
		Platform_t1003211029 * L_3 = ___other0;
		NullCheck(L_3);
		Vector2_t2243707579 * L_4 = L_3->get_address_of_Position_0();
		float L_5 = L_4->get_y_1();
		Vector2_t2243707579  L_6;
		memset(&L_6, 0, sizeof(L_6));
		Vector2__ctor_m3067419446(&L_6, L_2, L_5, /*hidden argument*/NULL);
		__this->set_Position_0(L_6);
		Platform_t1003211029 * L_7 = ___other0;
		NullCheck(L_7);
		float L_8 = L_7->get_Rotation_1();
		__this->set_Rotation_1(L_8);
		Platform_t1003211029 * L_9 = ___other0;
		NullCheck(L_9);
		float L_10 = L_9->get_Scale_2();
		__this->set_Scale_2(L_10);
		Platform_t1003211029 * L_11 = ___other0;
		NullCheck(L_11);
		int32_t L_12 = L_11->get_Type_7();
		__this->set_Type_7(L_12);
		return;
	}
}
// System.Void LevelLoader::.ctor()
extern "C"  void LevelLoader__ctor_m3293766304 (LevelLoader_t433131637 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void LevelLoader::Start()
extern "C"  void LevelLoader_Start_m862454864 (LevelLoader_t433131637 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void LevelLoader::Update()
extern "C"  void LevelLoader_Update_m3214809813 (LevelLoader_t433131637 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void Line::.ctor()
extern "C"  void Line__ctor_m3651664013 (Line_t2729441502 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void LoadSave.GameState::.ctor(System.Int32)
extern Il2CppClass* LevelU5BU5D_t1839895005_il2cpp_TypeInfo_var;
extern Il2CppClass* Level_t1066665556_il2cpp_TypeInfo_var;
extern const uint32_t GameState__ctor_m4157959086_MetadataUsageId;
extern "C"  void GameState__ctor_m4157959086 (GameState_t2029468607 * __this, int32_t ___numberOfLevels0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GameState__ctor_m4157959086_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___numberOfLevels0;
		___numberOfLevels0 = ((int32_t)((int32_t)L_0+(int32_t)1));
		int32_t L_1 = ___numberOfLevels0;
		__this->set_levels_0(((LevelU5BU5D_t1839895005*)SZArrayNew(LevelU5BU5D_t1839895005_il2cpp_TypeInfo_var, (uint32_t)L_1)));
		V_0 = 0;
		goto IL_003d;
	}

IL_001e:
	{
		LevelU5BU5D_t1839895005* L_2 = __this->get_levels_0();
		int32_t L_3 = V_0;
		Level_t1066665556 * L_4 = (Level_t1066665556 *)il2cpp_codegen_object_new(Level_t1066665556_il2cpp_TypeInfo_var);
		Level__ctor_m2252576272(L_4, /*hidden argument*/NULL);
		NullCheck(L_2);
		ArrayElementTypeCheck (L_2, L_4);
		(L_2)->SetAt(static_cast<il2cpp_array_size_t>(L_3), (Level_t1066665556 *)L_4);
		LevelU5BU5D_t1839895005* L_5 = __this->get_levels_0();
		int32_t L_6 = V_0;
		NullCheck(L_5);
		int32_t L_7 = L_6;
		Level_t1066665556 * L_8 = (L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_7));
		int32_t L_9 = V_0;
		NullCheck(L_8);
		L_8->set_number_6(L_9);
		int32_t L_10 = V_0;
		V_0 = ((int32_t)((int32_t)L_10+(int32_t)1));
	}

IL_003d:
	{
		int32_t L_11 = V_0;
		int32_t L_12 = ___numberOfLevels0;
		if ((((int32_t)L_11) < ((int32_t)L_12)))
		{
			goto IL_001e;
		}
	}
	{
		return;
	}
}
// System.Void LoadSave.GameState::.ctor(LoadSave.GameState)
extern Il2CppClass* LevelU5BU5D_t1839895005_il2cpp_TypeInfo_var;
extern Il2CppClass* Level_t1066665556_il2cpp_TypeInfo_var;
extern const uint32_t GameState__ctor_m3504319115_MetadataUsageId;
extern "C"  void GameState__ctor_m3504319115 (GameState_t2029468607 * __this, GameState_t2029468607 * ___other0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GameState__ctor_m3504319115_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		GameState_t2029468607 * L_0 = ___other0;
		NullCheck(L_0);
		LevelU5BU5D_t1839895005* L_1 = L_0->get_levels_0();
		NullCheck(L_1);
		__this->set_levels_0(((LevelU5BU5D_t1839895005*)SZArrayNew(LevelU5BU5D_t1839895005_il2cpp_TypeInfo_var, (uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_1)->max_length)))))));
		V_0 = 0;
		goto IL_0039;
	}

IL_0020:
	{
		LevelU5BU5D_t1839895005* L_2 = __this->get_levels_0();
		int32_t L_3 = V_0;
		GameState_t2029468607 * L_4 = ___other0;
		NullCheck(L_4);
		LevelU5BU5D_t1839895005* L_5 = L_4->get_levels_0();
		int32_t L_6 = V_0;
		NullCheck(L_5);
		int32_t L_7 = L_6;
		Level_t1066665556 * L_8 = (L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_7));
		Level_t1066665556 * L_9 = (Level_t1066665556 *)il2cpp_codegen_object_new(Level_t1066665556_il2cpp_TypeInfo_var);
		Level__ctor_m2689317099(L_9, L_8, /*hidden argument*/NULL);
		NullCheck(L_2);
		ArrayElementTypeCheck (L_2, L_9);
		(L_2)->SetAt(static_cast<il2cpp_array_size_t>(L_3), (Level_t1066665556 *)L_9);
		int32_t L_10 = V_0;
		V_0 = ((int32_t)((int32_t)L_10+(int32_t)1));
	}

IL_0039:
	{
		int32_t L_11 = V_0;
		LevelU5BU5D_t1839895005* L_12 = __this->get_levels_0();
		NullCheck(L_12);
		if ((((int32_t)L_11) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_12)->max_length)))))))
		{
			goto IL_0020;
		}
	}
	{
		return;
	}
}
// System.Void LoadSave.GameState::.ctor(System.String)
extern Il2CppClass* LevelU5BU5D_t1839895005_il2cpp_TypeInfo_var;
extern Il2CppClass* Level_t1066665556_il2cpp_TypeInfo_var;
extern const MethodInfo* JsonUtility_FromJson_TisGameState_t2029468607_m3146932469_MethodInfo_var;
extern const uint32_t GameState__ctor_m3330283085_MetadataUsageId;
extern "C"  void GameState__ctor_m3330283085 (GameState_t2029468607 * __this, String_t* ___path0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GameState__ctor_m3330283085_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	GameState_t2029468607 * V_1 = NULL;
	int32_t V_2 = 0;
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___path0;
		String_t* L_1 = File_ReadAllText_m1018286608(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		String_t* L_2 = V_0;
		GameState_t2029468607 * L_3 = JsonUtility_FromJson_TisGameState_t2029468607_m3146932469(NULL /*static, unused*/, L_2, /*hidden argument*/JsonUtility_FromJson_TisGameState_t2029468607_m3146932469_MethodInfo_var);
		V_1 = L_3;
		GameState_t2029468607 * L_4 = V_1;
		NullCheck(L_4);
		LevelU5BU5D_t1839895005* L_5 = L_4->get_levels_0();
		NullCheck(L_5);
		__this->set_levels_0(((LevelU5BU5D_t1839895005*)SZArrayNew(LevelU5BU5D_t1839895005_il2cpp_TypeInfo_var, (uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_5)->max_length)))))));
		V_2 = 0;
		goto IL_0047;
	}

IL_002e:
	{
		LevelU5BU5D_t1839895005* L_6 = __this->get_levels_0();
		int32_t L_7 = V_2;
		GameState_t2029468607 * L_8 = V_1;
		NullCheck(L_8);
		LevelU5BU5D_t1839895005* L_9 = L_8->get_levels_0();
		int32_t L_10 = V_2;
		NullCheck(L_9);
		int32_t L_11 = L_10;
		Level_t1066665556 * L_12 = (L_9)->GetAt(static_cast<il2cpp_array_size_t>(L_11));
		Level_t1066665556 * L_13 = (Level_t1066665556 *)il2cpp_codegen_object_new(Level_t1066665556_il2cpp_TypeInfo_var);
		Level__ctor_m2689317099(L_13, L_12, /*hidden argument*/NULL);
		NullCheck(L_6);
		ArrayElementTypeCheck (L_6, L_13);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(L_7), (Level_t1066665556 *)L_13);
		int32_t L_14 = V_2;
		V_2 = ((int32_t)((int32_t)L_14+(int32_t)1));
	}

IL_0047:
	{
		int32_t L_15 = V_2;
		LevelU5BU5D_t1839895005* L_16 = __this->get_levels_0();
		NullCheck(L_16);
		if ((((int32_t)L_15) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_16)->max_length)))))))
		{
			goto IL_002e;
		}
	}
	{
		return;
	}
}
// System.Void LoadSave.GameState::Export(System.String)
extern Il2CppClass* StreamWriter_t3858580635_il2cpp_TypeInfo_var;
extern const uint32_t GameState_Export_m2636018477_MetadataUsageId;
extern "C"  void GameState_Export_m2636018477 (GameState_t2029468607 * __this, String_t* ___path0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GameState_Export_m2636018477_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	StreamWriter_t3858580635 * V_1 = NULL;
	{
		String_t* L_0 = JsonUtility_ToJson_m1232500921(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		V_0 = L_0;
		String_t* L_1 = ___path0;
		StreamWriter_t3858580635 * L_2 = (StreamWriter_t3858580635 *)il2cpp_codegen_object_new(StreamWriter_t3858580635_il2cpp_TypeInfo_var);
		StreamWriter__ctor_m1804264596(L_2, L_1, /*hidden argument*/NULL);
		V_1 = L_2;
		StreamWriter_t3858580635 * L_3 = V_1;
		String_t* L_4 = V_0;
		NullCheck(L_3);
		VirtActionInvoker1< String_t* >::Invoke(14 /* System.Void System.IO.TextWriter::WriteLine(System.String) */, L_3, L_4);
		StreamWriter_t3858580635 * L_5 = V_1;
		NullCheck(L_5);
		VirtActionInvoker0::Invoke(5 /* System.Void System.IO.TextWriter::Close() */, L_5);
		return;
	}
}
// System.Void LoadSave.Level::.ctor()
extern "C"  void Level__ctor_m2252576272 (Level_t1066665556 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		__this->set_status_5((-1));
		__this->set_number_6(0);
		return;
	}
}
// System.Void LoadSave.Level::.ctor(LoadSave.Level)
extern "C"  void Level__ctor_m2689317099 (Level_t1066665556 * __this, Level_t1066665556 * ___other0, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		Level_t1066665556 * L_0 = ___other0;
		NullCheck(L_0);
		int32_t L_1 = L_0->get_status_5();
		__this->set_status_5(L_1);
		Level_t1066665556 * L_2 = ___other0;
		NullCheck(L_2);
		int32_t L_3 = L_2->get_number_6();
		__this->set_number_6(L_3);
		return;
	}
}
// System.Void MustDie::.ctor()
extern "C"  void MustDie__ctor_m2648985360 (MustDie_t3564324517 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void MustDie::Start()
extern "C"  void MustDie_Start_m746272128 (MustDie_t3564324517 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void MustDie::Update()
extern "C"  void MustDie_Update_m39114213 (MustDie_t3564324517 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void MustNotDie::.ctor()
extern "C"  void MustNotDie__ctor_m897372945 (MustNotDie_t791179140 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void MustNotDie::Start()
extern "C"  void MustNotDie_Start_m2660101073 (MustNotDie_t791179140 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void MustNotDie::Update()
extern "C"  void MustNotDie_Update_m2193848914 (MustNotDie_t791179140 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void PausePress::.ctor()
extern "C"  void PausePress__ctor_m2591755784 (PausePress_t1918135203 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PausePress::Start()
extern "C"  void PausePress_Start_m1942734468 (PausePress_t1918135203 * __this, const MethodInfo* method)
{
	{
		__this->set_isPaused_2((bool)0);
		return;
	}
}
// System.Void PausePress::Update()
extern Il2CppClass* Input_t1785128008_il2cpp_TypeInfo_var;
extern const uint32_t PausePress_Update_m3699104615_MetadataUsageId;
extern "C"  void PausePress_Update_m3699104615 (PausePress_t1918135203 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PausePress_Update_m3699104615_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1785128008_il2cpp_TypeInfo_var);
		bool L_0 = Input_GetKey_m3849524999(NULL /*static, unused*/, ((int32_t)27), /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0012;
		}
	}
	{
		PausePress_pressPauseButton_m2414244273(__this, /*hidden argument*/NULL);
	}

IL_0012:
	{
		return;
	}
}
// System.Void PausePress::pressPauseButton()
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisText_t356221433_m1217399699_MethodInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisImage_t2042527209_m4162535761_MethodInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisAnimator_t69676727_m2717502299_MethodInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisSpriteRenderer_t1209076198_m1184556631_MethodInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisRigidbody2D_t502193897_m812242143_MethodInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisBoxCollider2D_t948534547_m1829960061_MethodInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisCircleCollider2D_t13116344_m3645672388_MethodInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisConstantForce2D_t3766199213_m3058379513_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral1994460817;
extern Il2CppCodeGenString* _stringLiteral2033965610;
extern Il2CppCodeGenString* _stringLiteral2033965613;
extern Il2CppCodeGenString* _stringLiteral2033965612;
extern Il2CppCodeGenString* _stringLiteral1876604234;
extern Il2CppCodeGenString* _stringLiteral1460660268;
extern Il2CppCodeGenString* _stringLiteral3558534422;
extern Il2CppCodeGenString* _stringLiteral678270948;
extern Il2CppCodeGenString* _stringLiteral2843951784;
extern Il2CppCodeGenString* _stringLiteral2830371216;
extern const uint32_t PausePress_pressPauseButton_m2414244273_MetadataUsageId;
extern "C"  void PausePress_pressPauseButton_m2414244273 (PausePress_t1918135203 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PausePress_pressPauseButton_m2414244273_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	GameObject_t1756533147 * V_0 = NULL;
	GameObject_t1756533147 * V_1 = NULL;
	GameObject_t1756533147 * V_2 = NULL;
	GameObject_t1756533147 * V_3 = NULL;
	Animator_t69676727 * V_4 = NULL;
	Animator_t69676727 * V_5 = NULL;
	GameObject_t1756533147 * V_6 = NULL;
	int32_t V_7 = 0;
	GameObject_t1756533147 * V_8 = NULL;
	SpriteRenderer_t1209076198 * V_9 = NULL;
	int32_t V_10 = 0;
	GameObject_t1756533147 * V_11 = NULL;
	SpriteRenderer_t1209076198 * V_12 = NULL;
	Rigidbody2D_t502193897 * V_13 = NULL;
	BoxCollider2D_t948534547 * V_14 = NULL;
	CircleCollider2D_t13116344 * V_15 = NULL;
	ConstantForce2D_t3766199213 * V_16 = NULL;
	{
		bool L_0 = __this->get_isPaused_2();
		__this->set_isPaused_2((bool)((((int32_t)L_0) == ((int32_t)0))? 1 : 0));
		GameObject_t1756533147 * L_1 = GameObject_Find_m836511350(NULL /*static, unused*/, _stringLiteral1994460817, /*hidden argument*/NULL);
		V_0 = L_1;
		GameObject_t1756533147 * L_2 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_3 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0039;
		}
	}
	{
		GameObject_t1756533147 * L_4 = V_0;
		NullCheck(L_4);
		Text_t356221433 * L_5 = GameObject_GetComponent_TisText_t356221433_m1217399699(L_4, /*hidden argument*/GameObject_GetComponent_TisText_t356221433_m1217399699_MethodInfo_var);
		bool L_6 = __this->get_isPaused_2();
		NullCheck(L_5);
		Behaviour_set_enabled_m1796096907(L_5, (bool)((((int32_t)L_6) == ((int32_t)0))? 1 : 0), /*hidden argument*/NULL);
	}

IL_0039:
	{
		GameObject_t1756533147 * L_7 = GameObject_Find_m836511350(NULL /*static, unused*/, _stringLiteral2033965610, /*hidden argument*/NULL);
		V_1 = L_7;
		GameObject_t1756533147 * L_8 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_9 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
		if (!L_9)
		{
			goto IL_0063;
		}
	}
	{
		GameObject_t1756533147 * L_10 = V_1;
		NullCheck(L_10);
		Image_t2042527209 * L_11 = GameObject_GetComponent_TisImage_t2042527209_m4162535761(L_10, /*hidden argument*/GameObject_GetComponent_TisImage_t2042527209_m4162535761_MethodInfo_var);
		bool L_12 = __this->get_isPaused_2();
		NullCheck(L_11);
		Behaviour_set_enabled_m1796096907(L_11, (bool)((((int32_t)L_12) == ((int32_t)0))? 1 : 0), /*hidden argument*/NULL);
	}

IL_0063:
	{
		GameObject_t1756533147 * L_13 = GameObject_Find_m836511350(NULL /*static, unused*/, _stringLiteral2033965613, /*hidden argument*/NULL);
		V_2 = L_13;
		GameObject_t1756533147 * L_14 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_15 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_14, /*hidden argument*/NULL);
		if (!L_15)
		{
			goto IL_008d;
		}
	}
	{
		GameObject_t1756533147 * L_16 = V_2;
		NullCheck(L_16);
		Image_t2042527209 * L_17 = GameObject_GetComponent_TisImage_t2042527209_m4162535761(L_16, /*hidden argument*/GameObject_GetComponent_TisImage_t2042527209_m4162535761_MethodInfo_var);
		bool L_18 = __this->get_isPaused_2();
		NullCheck(L_17);
		Behaviour_set_enabled_m1796096907(L_17, (bool)((((int32_t)L_18) == ((int32_t)0))? 1 : 0), /*hidden argument*/NULL);
	}

IL_008d:
	{
		GameObject_t1756533147 * L_19 = GameObject_Find_m836511350(NULL /*static, unused*/, _stringLiteral2033965612, /*hidden argument*/NULL);
		V_3 = L_19;
		GameObject_t1756533147 * L_20 = V_3;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_21 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_20, /*hidden argument*/NULL);
		if (!L_21)
		{
			goto IL_00b7;
		}
	}
	{
		GameObject_t1756533147 * L_22 = V_3;
		NullCheck(L_22);
		Image_t2042527209 * L_23 = GameObject_GetComponent_TisImage_t2042527209_m4162535761(L_22, /*hidden argument*/GameObject_GetComponent_TisImage_t2042527209_m4162535761_MethodInfo_var);
		bool L_24 = __this->get_isPaused_2();
		NullCheck(L_23);
		Behaviour_set_enabled_m1796096907(L_23, (bool)((((int32_t)L_24) == ((int32_t)0))? 1 : 0), /*hidden argument*/NULL);
	}

IL_00b7:
	{
		GameObject_t1756533147 * L_25 = GameObject_Find_m836511350(NULL /*static, unused*/, _stringLiteral1876604234, /*hidden argument*/NULL);
		NullCheck(L_25);
		Animator_t69676727 * L_26 = GameObject_GetComponent_TisAnimator_t69676727_m2717502299(L_25, /*hidden argument*/GameObject_GetComponent_TisAnimator_t69676727_m2717502299_MethodInfo_var);
		V_4 = L_26;
		bool L_27 = __this->get_isPaused_2();
		if (!L_27)
		{
			goto IL_00e4;
		}
	}
	{
		Animator_t69676727 * L_28 = V_4;
		NullCheck(L_28);
		Animator_SetTrigger_m3418492570(L_28, _stringLiteral1460660268, /*hidden argument*/NULL);
		goto IL_00f0;
	}

IL_00e4:
	{
		Animator_t69676727 * L_29 = V_4;
		NullCheck(L_29);
		Animator_SetTrigger_m3418492570(L_29, _stringLiteral3558534422, /*hidden argument*/NULL);
	}

IL_00f0:
	{
		Animator_t69676727 * L_30 = V_4;
		bool L_31 = __this->get_isPaused_2();
		NullCheck(L_30);
		Animator_SetBool_m2305662531(L_30, _stringLiteral678270948, L_31, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_32 = GameObject_Find_m836511350(NULL /*static, unused*/, _stringLiteral2843951784, /*hidden argument*/NULL);
		NullCheck(L_32);
		Animator_t69676727 * L_33 = GameObject_GetComponent_TisAnimator_t69676727_m2717502299(L_32, /*hidden argument*/GameObject_GetComponent_TisAnimator_t69676727_m2717502299_MethodInfo_var);
		V_5 = L_33;
		bool L_34 = __this->get_isPaused_2();
		if (!L_34)
		{
			goto IL_012f;
		}
	}
	{
		Animator_t69676727 * L_35 = V_5;
		NullCheck(L_35);
		Animator_SetTrigger_m3418492570(L_35, _stringLiteral3558534422, /*hidden argument*/NULL);
		goto IL_013b;
	}

IL_012f:
	{
		Animator_t69676727 * L_36 = V_5;
		NullCheck(L_36);
		Animator_SetTrigger_m3418492570(L_36, _stringLiteral1460660268, /*hidden argument*/NULL);
	}

IL_013b:
	{
		Animator_t69676727 * L_37 = V_5;
		bool L_38 = __this->get_isPaused_2();
		NullCheck(L_37);
		Animator_SetBool_m2305662531(L_37, _stringLiteral678270948, L_38, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_39 = GameObject_Find_m836511350(NULL /*static, unused*/, _stringLiteral2830371216, /*hidden argument*/NULL);
		V_6 = L_39;
		GameObject_t1756533147 * L_40 = V_6;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_41 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_40, /*hidden argument*/NULL);
		if (!L_41)
		{
			goto IL_02cf;
		}
	}
	{
		V_7 = 0;
		goto IL_02bc;
	}

IL_016d:
	{
		GameObject_t1756533147 * L_42 = V_6;
		NullCheck(L_42);
		Transform_t3275118058 * L_43 = GameObject_get_transform_m909382139(L_42, /*hidden argument*/NULL);
		int32_t L_44 = V_7;
		NullCheck(L_43);
		Transform_t3275118058 * L_45 = Transform_GetChild_m3838588184(L_43, L_44, /*hidden argument*/NULL);
		NullCheck(L_45);
		GameObject_t1756533147 * L_46 = Component_get_gameObject_m3105766835(L_45, /*hidden argument*/NULL);
		V_8 = L_46;
		GameObject_t1756533147 * L_47 = V_8;
		NullCheck(L_47);
		SpriteRenderer_t1209076198 * L_48 = GameObject_GetComponent_TisSpriteRenderer_t1209076198_m1184556631(L_47, /*hidden argument*/GameObject_GetComponent_TisSpriteRenderer_t1209076198_m1184556631_MethodInfo_var);
		V_9 = L_48;
		SpriteRenderer_t1209076198 * L_49 = V_9;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_50 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_49, /*hidden argument*/NULL);
		if (!L_50)
		{
			goto IL_01a7;
		}
	}
	{
		SpriteRenderer_t1209076198 * L_51 = V_9;
		bool L_52 = __this->get_isPaused_2();
		NullCheck(L_51);
		Renderer_set_enabled_m142717579(L_51, (bool)((((int32_t)L_52) == ((int32_t)0))? 1 : 0), /*hidden argument*/NULL);
	}

IL_01a7:
	{
		GameObject_t1756533147 * L_53 = V_8;
		NullCheck(L_53);
		Transform_t3275118058 * L_54 = GameObject_get_transform_m909382139(L_53, /*hidden argument*/NULL);
		NullCheck(L_54);
		int32_t L_55 = Transform_get_childCount_m881385315(L_54, /*hidden argument*/NULL);
		if ((((int32_t)L_55) <= ((int32_t)0)))
		{
			goto IL_0214;
		}
	}
	{
		V_10 = 0;
		goto IL_0201;
	}

IL_01c1:
	{
		GameObject_t1756533147 * L_56 = V_8;
		NullCheck(L_56);
		Transform_t3275118058 * L_57 = GameObject_get_transform_m909382139(L_56, /*hidden argument*/NULL);
		int32_t L_58 = V_10;
		NullCheck(L_57);
		Transform_t3275118058 * L_59 = Transform_GetChild_m3838588184(L_57, L_58, /*hidden argument*/NULL);
		NullCheck(L_59);
		GameObject_t1756533147 * L_60 = Component_get_gameObject_m3105766835(L_59, /*hidden argument*/NULL);
		V_11 = L_60;
		GameObject_t1756533147 * L_61 = V_11;
		NullCheck(L_61);
		SpriteRenderer_t1209076198 * L_62 = GameObject_GetComponent_TisSpriteRenderer_t1209076198_m1184556631(L_61, /*hidden argument*/GameObject_GetComponent_TisSpriteRenderer_t1209076198_m1184556631_MethodInfo_var);
		V_12 = L_62;
		SpriteRenderer_t1209076198 * L_63 = V_12;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_64 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_63, /*hidden argument*/NULL);
		if (!L_64)
		{
			goto IL_01fb;
		}
	}
	{
		SpriteRenderer_t1209076198 * L_65 = V_12;
		bool L_66 = __this->get_isPaused_2();
		NullCheck(L_65);
		Renderer_set_enabled_m142717579(L_65, (bool)((((int32_t)L_66) == ((int32_t)0))? 1 : 0), /*hidden argument*/NULL);
	}

IL_01fb:
	{
		int32_t L_67 = V_10;
		V_10 = ((int32_t)((int32_t)L_67+(int32_t)1));
	}

IL_0201:
	{
		int32_t L_68 = V_10;
		GameObject_t1756533147 * L_69 = V_8;
		NullCheck(L_69);
		Transform_t3275118058 * L_70 = GameObject_get_transform_m909382139(L_69, /*hidden argument*/NULL);
		NullCheck(L_70);
		int32_t L_71 = Transform_get_childCount_m881385315(L_70, /*hidden argument*/NULL);
		if ((((int32_t)L_68) < ((int32_t)L_71)))
		{
			goto IL_01c1;
		}
	}

IL_0214:
	{
		GameObject_t1756533147 * L_72 = V_8;
		NullCheck(L_72);
		Rigidbody2D_t502193897 * L_73 = GameObject_GetComponent_TisRigidbody2D_t502193897_m812242143(L_72, /*hidden argument*/GameObject_GetComponent_TisRigidbody2D_t502193897_m812242143_MethodInfo_var);
		V_13 = L_73;
		Rigidbody2D_t502193897 * L_74 = V_13;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_75 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_74, /*hidden argument*/NULL);
		if (!L_75)
		{
			goto IL_0247;
		}
	}
	{
		bool L_76 = __this->get_isPaused_2();
		if (!L_76)
		{
			goto IL_0240;
		}
	}
	{
		Rigidbody2D_t502193897 * L_77 = V_13;
		NullCheck(L_77);
		Rigidbody2D_Sleep_m3196697853(L_77, /*hidden argument*/NULL);
		goto IL_0247;
	}

IL_0240:
	{
		Rigidbody2D_t502193897 * L_78 = V_13;
		NullCheck(L_78);
		Rigidbody2D_WakeUp_m1402807111(L_78, /*hidden argument*/NULL);
	}

IL_0247:
	{
		GameObject_t1756533147 * L_79 = V_8;
		NullCheck(L_79);
		BoxCollider2D_t948534547 * L_80 = GameObject_GetComponent_TisBoxCollider2D_t948534547_m1829960061(L_79, /*hidden argument*/GameObject_GetComponent_TisBoxCollider2D_t948534547_m1829960061_MethodInfo_var);
		V_14 = L_80;
		BoxCollider2D_t948534547 * L_81 = V_14;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_82 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_81, /*hidden argument*/NULL);
		if (!L_82)
		{
			goto IL_026c;
		}
	}
	{
		BoxCollider2D_t948534547 * L_83 = V_14;
		bool L_84 = __this->get_isPaused_2();
		NullCheck(L_83);
		Behaviour_set_enabled_m1796096907(L_83, (bool)((((int32_t)L_84) == ((int32_t)0))? 1 : 0), /*hidden argument*/NULL);
	}

IL_026c:
	{
		GameObject_t1756533147 * L_85 = V_8;
		NullCheck(L_85);
		CircleCollider2D_t13116344 * L_86 = GameObject_GetComponent_TisCircleCollider2D_t13116344_m3645672388(L_85, /*hidden argument*/GameObject_GetComponent_TisCircleCollider2D_t13116344_m3645672388_MethodInfo_var);
		V_15 = L_86;
		CircleCollider2D_t13116344 * L_87 = V_15;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_88 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_87, /*hidden argument*/NULL);
		if (!L_88)
		{
			goto IL_0291;
		}
	}
	{
		CircleCollider2D_t13116344 * L_89 = V_15;
		bool L_90 = __this->get_isPaused_2();
		NullCheck(L_89);
		Behaviour_set_enabled_m1796096907(L_89, (bool)((((int32_t)L_90) == ((int32_t)0))? 1 : 0), /*hidden argument*/NULL);
	}

IL_0291:
	{
		GameObject_t1756533147 * L_91 = V_8;
		NullCheck(L_91);
		ConstantForce2D_t3766199213 * L_92 = GameObject_GetComponent_TisConstantForce2D_t3766199213_m3058379513(L_91, /*hidden argument*/GameObject_GetComponent_TisConstantForce2D_t3766199213_m3058379513_MethodInfo_var);
		V_16 = L_92;
		ConstantForce2D_t3766199213 * L_93 = V_16;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_94 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_93, /*hidden argument*/NULL);
		if (!L_94)
		{
			goto IL_02b6;
		}
	}
	{
		ConstantForce2D_t3766199213 * L_95 = V_16;
		bool L_96 = __this->get_isPaused_2();
		NullCheck(L_95);
		Behaviour_set_enabled_m1796096907(L_95, (bool)((((int32_t)L_96) == ((int32_t)0))? 1 : 0), /*hidden argument*/NULL);
	}

IL_02b6:
	{
		int32_t L_97 = V_7;
		V_7 = ((int32_t)((int32_t)L_97+(int32_t)1));
	}

IL_02bc:
	{
		int32_t L_98 = V_7;
		GameObject_t1756533147 * L_99 = V_6;
		NullCheck(L_99);
		Transform_t3275118058 * L_100 = GameObject_get_transform_m909382139(L_99, /*hidden argument*/NULL);
		NullCheck(L_100);
		int32_t L_101 = Transform_get_childCount_m881385315(L_100, /*hidden argument*/NULL);
		if ((((int32_t)L_98) < ((int32_t)L_101)))
		{
			goto IL_016d;
		}
	}

IL_02cf:
	{
		return;
	}
}
// System.Void PausePress::RestartLevel()
extern "C"  void PausePress_RestartLevel_m1753174107 (PausePress_t1918135203 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	Scene_t1684909666  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		Scene_t1684909666  L_0 = SceneManager_GetActiveScene_m2964039490(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_1 = L_0;
		int32_t L_1 = Scene_get_buildIndex_m3735680091((&V_1), /*hidden argument*/NULL);
		V_0 = L_1;
		int32_t L_2 = V_0;
		SceneManager_LoadScene_m592643733(NULL /*static, unused*/, L_2, 0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PausePress::GoToMenu()
extern Il2CppCodeGenString* _stringLiteral694877711;
extern const uint32_t PausePress_GoToMenu_m1225061222_MetadataUsageId;
extern "C"  void PausePress_GoToMenu_m1225061222 (PausePress_t1918135203 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PausePress_GoToMenu_m1225061222_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		SceneManager_LoadScene_m1619949821(NULL /*static, unused*/, _stringLiteral694877711, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Removable::.ctor()
extern "C"  void Removable__ctor_m2484126636 (Removable_t2119088671 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Removable::Start()
extern "C"  void Removable_Start_m1691861904 (Removable_t2119088671 * __this, const MethodInfo* method)
{
	{
		__this->set_toBeDestroyed_2((bool)0);
		return;
	}
}
// System.Void Removable::Update()
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const MethodInfo* Component_GetComponent_TisSpriteRenderer_t1209076198_m2178781570_MethodInfo_var;
extern const uint32_t Removable_Update_m2682633095_MetadataUsageId;
extern "C"  void Removable_Update_m2682633095 (Removable_t2119088671 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Removable_Update_m2682633095_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_t2243707580  V_0;
	memset(&V_0, 0, sizeof(V_0));
	SpriteRenderer_t1209076198 * V_1 = NULL;
	Color_t2020392075  V_2;
	memset(&V_2, 0, sizeof(V_2));
	SpriteRenderer_t1209076198 * V_3 = NULL;
	Color_t2020392075  V_4;
	memset(&V_4, 0, sizeof(V_4));
	{
		bool L_0 = __this->get_toBeDestroyed_2();
		if (!L_0)
		{
			goto IL_009e;
		}
	}
	{
		Transform_t3275118058 * L_1 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_1);
		Vector3_t2243707580  L_2 = Transform_get_localScale_m3074381503(L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		float L_3 = (&V_0)->get_x_1();
		if ((!(((float)L_3) < ((float)(0.001f)))))
		{
			goto IL_0033;
		}
	}
	{
		GameObject_t1756533147 * L_4 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		Object_Destroy_m4145850038(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
	}

IL_0033:
	{
		SpriteRenderer_t1209076198 * L_5 = Component_GetComponent_TisSpriteRenderer_t1209076198_m2178781570(__this, /*hidden argument*/Component_GetComponent_TisSpriteRenderer_t1209076198_m2178781570_MethodInfo_var);
		V_1 = L_5;
		SpriteRenderer_t1209076198 * L_6 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_7 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_0068;
		}
	}
	{
		SpriteRenderer_t1209076198 * L_8 = V_1;
		NullCheck(L_8);
		Color_t2020392075  L_9 = SpriteRenderer_get_color_m345525162(L_8, /*hidden argument*/NULL);
		V_2 = L_9;
		float L_10 = (&V_2)->get_a_3();
		if ((!(((float)L_10) < ((float)(0.001f)))))
		{
			goto IL_0068;
		}
	}
	{
		GameObject_t1756533147 * L_11 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		Object_Destroy_m4145850038(NULL /*static, unused*/, L_11, /*hidden argument*/NULL);
	}

IL_0068:
	{
		SpriteRenderer_t1209076198 * L_12 = Component_GetComponent_TisSpriteRenderer_t1209076198_m2178781570(__this, /*hidden argument*/Component_GetComponent_TisSpriteRenderer_t1209076198_m2178781570_MethodInfo_var);
		V_3 = L_12;
		SpriteRenderer_t1209076198 * L_13 = V_3;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_14 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_13, /*hidden argument*/NULL);
		if (!L_14)
		{
			goto IL_009e;
		}
	}
	{
		SpriteRenderer_t1209076198 * L_15 = V_3;
		NullCheck(L_15);
		Color_t2020392075  L_16 = SpriteRenderer_get_color_m345525162(L_15, /*hidden argument*/NULL);
		V_4 = L_16;
		float L_17 = (&V_4)->get_a_3();
		if ((!(((float)L_17) < ((float)(0.001f)))))
		{
			goto IL_009e;
		}
	}
	{
		GameObject_t1756533147 * L_18 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		Object_Destroy_m4145850038(NULL /*static, unused*/, L_18, /*hidden argument*/NULL);
	}

IL_009e:
	{
		return;
	}
}
// System.Void Removable::Remove()
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const MethodInfo* Component_GetComponent_TisAnimator_t69676727_m475627522_MethodInfo_var;
extern const MethodInfo* Component_GetComponent_TisConstantForce2D_t3766199213_m2952855213_MethodInfo_var;
extern const MethodInfo* Component_GetComponent_TisBoxCollider2D_t948534547_m324820273_MethodInfo_var;
extern const MethodInfo* Component_GetComponent_TisCircleCollider2D_t13116344_m1279094442_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral4136222740;
extern const uint32_t Removable_Remove_m1955228078_MetadataUsageId;
extern "C"  void Removable_Remove_m1955228078 (Removable_t2119088671 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Removable_Remove_m1955228078_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ConstantForce2D_t3766199213 * V_0 = NULL;
	BoxCollider2D_t948534547 * V_1 = NULL;
	CircleCollider2D_t13116344 * V_2 = NULL;
	{
		__this->set_toBeDestroyed_2((bool)1);
		Animator_t69676727 * L_0 = Component_GetComponent_TisAnimator_t69676727_m475627522(__this, /*hidden argument*/Component_GetComponent_TisAnimator_t69676727_m475627522_MethodInfo_var);
		NullCheck(L_0);
		Animator_SetBool_m2305662531(L_0, _stringLiteral4136222740, (bool)1, /*hidden argument*/NULL);
		ConstantForce2D_t3766199213 * L_1 = Component_GetComponent_TisConstantForce2D_t3766199213_m2952855213(__this, /*hidden argument*/Component_GetComponent_TisConstantForce2D_t3766199213_m2952855213_MethodInfo_var);
		V_0 = L_1;
		ConstantForce2D_t3766199213 * L_2 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_3 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0031;
		}
	}
	{
		ConstantForce2D_t3766199213 * L_4 = V_0;
		NullCheck(L_4);
		Behaviour_set_enabled_m1796096907(L_4, (bool)0, /*hidden argument*/NULL);
	}

IL_0031:
	{
		BoxCollider2D_t948534547 * L_5 = Component_GetComponent_TisBoxCollider2D_t948534547_m324820273(__this, /*hidden argument*/Component_GetComponent_TisBoxCollider2D_t948534547_m324820273_MethodInfo_var);
		V_1 = L_5;
		BoxCollider2D_t948534547 * L_6 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_7 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_0049;
		}
	}
	{
		BoxCollider2D_t948534547 * L_8 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		Object_Destroy_m4145850038(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
	}

IL_0049:
	{
		CircleCollider2D_t13116344 * L_9 = Component_GetComponent_TisCircleCollider2D_t13116344_m1279094442(__this, /*hidden argument*/Component_GetComponent_TisCircleCollider2D_t13116344_m1279094442_MethodInfo_var);
		V_2 = L_9;
		CircleCollider2D_t13116344 * L_10 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_11 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_10, /*hidden argument*/NULL);
		if (!L_11)
		{
			goto IL_0061;
		}
	}
	{
		CircleCollider2D_t13116344 * L_12 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		Object_Destroy_m4145850038(NULL /*static, unused*/, L_12, /*hidden argument*/NULL);
	}

IL_0061:
	{
		return;
	}
}
// System.Void SkipToNext::.ctor()
extern "C"  void SkipToNext__ctor_m266695162 (SkipToNext_t3002738695 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SkipToNext::Update()
extern "C"  void SkipToNext_Update_m1537027891 (SkipToNext_t3002738695 * __this, const MethodInfo* method)
{
	{
		float L_0 = __this->get_timeoutToSkip_3();
		float L_1 = Time_get_deltaTime_m2233168104(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_timeoutToSkip_3(((float)((float)L_0-(float)L_1)));
		float L_2 = __this->get_timeoutToSkip_3();
		if ((!(((float)L_2) < ((float)(0.0f)))))
		{
			goto IL_0028;
		}
	}
	{
		SkipToNext_SkipScene_m1440608173(__this, /*hidden argument*/NULL);
	}

IL_0028:
	{
		return;
	}
}
// System.Void SkipToNext::SkipScene()
extern "C"  void SkipToNext_SkipScene_m1440608173 (SkipToNext_t3002738695 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_nextScene_2();
		SceneManager_LoadScene_m1619949821(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Spawner::.ctor()
extern Il2CppClass* SingleU5BU5D_t577127397_il2cpp_TypeInfo_var;
extern FieldInfo* U3CPrivateImplementationDetailsU3E_t1486305142____U24fieldU2DA55AA7C523DD2ED84D18AEF0C5B0A88A70618491_0_FieldInfo_var;
extern const uint32_t Spawner__ctor_m1860016179_MetadataUsageId;
extern "C"  void Spawner__ctor_m1860016179 (Spawner_t534830648 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Spawner__ctor_m1860016179_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		__this->set_numberOfLevels_2(((int32_t)50));
		SingleU5BU5D_t577127397* L_0 = ((SingleU5BU5D_t577127397*)SZArrayNew(SingleU5BU5D_t577127397_il2cpp_TypeInfo_var, (uint32_t)2));
		NullCheck(L_0);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (float)(65.0f));
		SingleU5BU5D_t577127397* L_1 = L_0;
		NullCheck(L_1);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(1), (float)(-65.0f));
		__this->set_levelButtonPozitionsY_4(L_1);
		SingleU5BU5D_t577127397* L_2 = ((SingleU5BU5D_t577127397*)SZArrayNew(SingleU5BU5D_t577127397_il2cpp_TypeInfo_var, (uint32_t)5));
		RuntimeHelpers_InitializeArray_m3920580167(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_2, LoadFieldToken(U3CPrivateImplementationDetailsU3E_t1486305142____U24fieldU2DA55AA7C523DD2ED84D18AEF0C5B0A88A70618491_0_FieldInfo_var), /*hidden argument*/NULL);
		__this->set_levelButtonPozitionsX_5(L_2);
		__this->set_pageIdentifierPositionsY_6((-150.0f));
		__this->set_distanceBetweenPages_7((15.0f));
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Spawner::Start()
extern Il2CppClass* StaticLevelContainer_t3714456515_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32_t2071877448_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* GameObjectU5BU5D_t3057952154_il2cpp_TypeInfo_var;
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const MethodInfo* Object_Instantiate_TisGameObject_t1756533147_m3066053529_MethodInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisRectTransform_t3349966182_m132995507_MethodInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisCurrentPage_t1988855378_m3044519405_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral1030128889;
extern Il2CppCodeGenString* _stringLiteral1458397508;
extern Il2CppCodeGenString* _stringLiteral1531322534;
extern Il2CppCodeGenString* _stringLiteral2515825212;
extern Il2CppCodeGenString* _stringLiteral2328220407;
extern const uint32_t Spawner_Start_m3183106495_MetadataUsageId;
extern "C"  void Spawner_Start_m3183106495 (Spawner_t534830648 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Spawner_Start_m3183106495_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	GameObject_t1756533147 * V_1 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(StaticLevelContainer_t3714456515_il2cpp_TypeInfo_var);
		GameState_t2029468607 * L_0 = StaticLevelContainer_loadLevelFromStorage_m1234179298(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_currentGameState_12(L_0);
		StaticLevelContainer_t3714456515 * L_1 = StaticLevelContainer_getInstance_m3684656527(NULL /*static, unused*/, /*hidden argument*/NULL);
		GameState_t2029468607 * L_2 = __this->get_currentGameState_12();
		NullCheck(L_1);
		L_1->set_gameState_1(L_2);
		GameState_t2029468607 * L_3 = __this->get_currentGameState_12();
		NullCheck(L_3);
		LevelU5BU5D_t1839895005* L_4 = L_3->get_levels_0();
		NullCheck(L_4);
		__this->set_numberOfLevels_2(((int32_t)((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_4)->max_length))))-(int32_t)1)));
		int32_t L_5 = __this->get_numberOfLevels_2();
		__this->set_numberOfPages_3(((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_5-(int32_t)1))/(int32_t)((int32_t)10)))+(int32_t)1)));
		ObjectU5BU5D_t3614634134* L_6 = ((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_6);
		ArrayElementTypeCheck (L_6, _stringLiteral1030128889);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral1030128889);
		ObjectU5BU5D_t3614634134* L_7 = L_6;
		int32_t L_8 = __this->get_numberOfLevels_2();
		int32_t L_9 = L_8;
		Il2CppObject * L_10 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_9);
		NullCheck(L_7);
		ArrayElementTypeCheck (L_7, L_10);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_10);
		ObjectU5BU5D_t3614634134* L_11 = L_7;
		NullCheck(L_11);
		ArrayElementTypeCheck (L_11, _stringLiteral1458397508);
		(L_11)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral1458397508);
		ObjectU5BU5D_t3614634134* L_12 = L_11;
		int32_t L_13 = __this->get_numberOfPages_3();
		int32_t L_14 = L_13;
		Il2CppObject * L_15 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_14);
		NullCheck(L_12);
		ArrayElementTypeCheck (L_12, L_15);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_15);
		ObjectU5BU5D_t3614634134* L_16 = L_12;
		NullCheck(L_16);
		ArrayElementTypeCheck (L_16, _stringLiteral1531322534);
		(L_16)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)_stringLiteral1531322534);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_17 = String_Concat_m3881798623(NULL /*static, unused*/, L_16, /*hidden argument*/NULL);
		DeveloperLog_LogMessage_m3766158779(NULL /*static, unused*/, L_17, /*hidden argument*/NULL);
		__this->set_currentPage_11(0);
		int32_t L_18 = __this->get_numberOfPages_3();
		__this->set_pages_10(((GameObjectU5BU5D_t3057952154*)SZArrayNew(GameObjectU5BU5D_t3057952154_il2cpp_TypeInfo_var, (uint32_t)L_18)));
		V_0 = 0;
		goto IL_0141;
	}

IL_00a6:
	{
		GameObject_t1756533147 * L_19 = __this->get_pageIdentifier_9();
		GameObject_t1756533147 * L_20 = GameObject_Find_m836511350(NULL /*static, unused*/, _stringLiteral2515825212, /*hidden argument*/NULL);
		NullCheck(L_20);
		Transform_t3275118058 * L_21 = GameObject_get_transform_m909382139(L_20, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		GameObject_t1756533147 * L_22 = Object_Instantiate_TisGameObject_t1756533147_m3066053529(NULL /*static, unused*/, L_19, L_21, /*hidden argument*/Object_Instantiate_TisGameObject_t1756533147_m3066053529_MethodInfo_var);
		V_1 = L_22;
		GameObject_t1756533147 * L_23 = V_1;
		NullCheck(L_23);
		RectTransform_t3349966182 * L_24 = GameObject_GetComponent_TisRectTransform_t3349966182_m132995507(L_23, /*hidden argument*/GameObject_GetComponent_TisRectTransform_t3349966182_m132995507_MethodInfo_var);
		int32_t L_25 = V_0;
		float L_26 = __this->get_distanceBetweenPages_7();
		int32_t L_27 = __this->get_numberOfPages_3();
		float L_28 = __this->get_distanceBetweenPages_7();
		float L_29 = __this->get_pageIdentifierPositionsY_6();
		Vector2_t2243707579  L_30;
		memset(&L_30, 0, sizeof(L_30));
		Vector2__ctor_m3067419446(&L_30, ((float)((float)((float)((float)(((float)((float)L_25)))*(float)L_26))-(float)((float)((float)((float)((float)(((float)((float)L_27)))*(float)L_28))/(float)(2.0f))))), L_29, /*hidden argument*/NULL);
		Vector3_t2243707580  L_31 = Vector2_op_Implicit_m176791411(NULL /*static, unused*/, L_30, /*hidden argument*/NULL);
		NullCheck(L_24);
		Transform_set_localPosition_m1026930133(L_24, L_31, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_32 = V_1;
		NullCheck(L_32);
		RectTransform_t3349966182 * L_33 = GameObject_GetComponent_TisRectTransform_t3349966182_m132995507(L_32, /*hidden argument*/GameObject_GetComponent_TisRectTransform_t3349966182_m132995507_MethodInfo_var);
		Vector2_t2243707579  L_34;
		memset(&L_34, 0, sizeof(L_34));
		Vector2__ctor_m3067419446(&L_34, (1.2f), (1.2f), /*hidden argument*/NULL);
		Vector3_t2243707580  L_35 = Vector2_op_Implicit_m176791411(NULL /*static, unused*/, L_34, /*hidden argument*/NULL);
		NullCheck(L_33);
		Transform_set_localScale_m2325460848(L_33, L_35, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_36 = V_1;
		NullCheck(L_36);
		Transform_t3275118058 * L_37 = GameObject_get_transform_m909382139(L_36, /*hidden argument*/NULL);
		int32_t L_38 = V_0;
		int32_t L_39 = L_38;
		Il2CppObject * L_40 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_39);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_41 = String_Concat_m56707527(NULL /*static, unused*/, _stringLiteral2328220407, L_40, /*hidden argument*/NULL);
		NullCheck(L_37);
		Object_set_name_m4157836998(L_37, L_41, /*hidden argument*/NULL);
		GameObjectU5BU5D_t3057952154* L_42 = __this->get_pages_10();
		int32_t L_43 = V_0;
		GameObject_t1756533147 * L_44 = V_1;
		NullCheck(L_42);
		ArrayElementTypeCheck (L_42, L_44);
		(L_42)->SetAt(static_cast<il2cpp_array_size_t>(L_43), (GameObject_t1756533147 *)L_44);
		int32_t L_45 = V_0;
		V_0 = ((int32_t)((int32_t)L_45+(int32_t)1));
	}

IL_0141:
	{
		int32_t L_46 = V_0;
		int32_t L_47 = __this->get_numberOfPages_3();
		if ((((int32_t)L_46) < ((int32_t)L_47)))
		{
			goto IL_00a6;
		}
	}
	{
		GameObjectU5BU5D_t3057952154* L_48 = __this->get_pages_10();
		NullCheck(L_48);
		int32_t L_49 = 0;
		GameObject_t1756533147 * L_50 = (L_48)->GetAt(static_cast<il2cpp_array_size_t>(L_49));
		NullCheck(L_50);
		CurrentPage_t1988855378 * L_51 = GameObject_GetComponent_TisCurrentPage_t1988855378_m3044519405(L_50, /*hidden argument*/GameObject_GetComponent_TisCurrentPage_t1988855378_m3044519405_MethodInfo_var);
		NullCheck(L_51);
		L_51->set_isCurrentPage_4((bool)1);
		Spawner_redrawLevels_m1471550761(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Spawner::Update()
extern "C"  void Spawner_Update_m1485478966 (Spawner_t534830648 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void Spawner::GoPageLeft()
extern const MethodInfo* GameObject_GetComponent_TisCurrentPage_t1988855378_m3044519405_MethodInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisAnimator_t69676727_m2717502299_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral2515825212;
extern Il2CppCodeGenString* _stringLiteral286381437;
extern const uint32_t Spawner_GoPageLeft_m1142194739_MetadataUsageId;
extern "C"  void Spawner_GoPageLeft_m1142194739 (Spawner_t534830648 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Spawner_GoPageLeft_m1142194739_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = __this->get_currentPage_11();
		if ((((int32_t)L_0) <= ((int32_t)0)))
		{
			goto IL_0069;
		}
	}
	{
		GameObjectU5BU5D_t3057952154* L_1 = __this->get_pages_10();
		int32_t L_2 = __this->get_currentPage_11();
		NullCheck(L_1);
		int32_t L_3 = L_2;
		GameObject_t1756533147 * L_4 = (L_1)->GetAt(static_cast<il2cpp_array_size_t>(L_3));
		NullCheck(L_4);
		CurrentPage_t1988855378 * L_5 = GameObject_GetComponent_TisCurrentPage_t1988855378_m3044519405(L_4, /*hidden argument*/GameObject_GetComponent_TisCurrentPage_t1988855378_m3044519405_MethodInfo_var);
		NullCheck(L_5);
		L_5->set_isCurrentPage_4((bool)0);
		int32_t L_6 = __this->get_currentPage_11();
		__this->set_currentPage_11(((int32_t)((int32_t)L_6-(int32_t)1)));
		GameObjectU5BU5D_t3057952154* L_7 = __this->get_pages_10();
		int32_t L_8 = __this->get_currentPage_11();
		NullCheck(L_7);
		int32_t L_9 = L_8;
		GameObject_t1756533147 * L_10 = (L_7)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		NullCheck(L_10);
		CurrentPage_t1988855378 * L_11 = GameObject_GetComponent_TisCurrentPage_t1988855378_m3044519405(L_10, /*hidden argument*/GameObject_GetComponent_TisCurrentPage_t1988855378_m3044519405_MethodInfo_var);
		NullCheck(L_11);
		L_11->set_isCurrentPage_4((bool)1);
		GameObject_t1756533147 * L_12 = GameObject_Find_m836511350(NULL /*static, unused*/, _stringLiteral2515825212, /*hidden argument*/NULL);
		NullCheck(L_12);
		Animator_t69676727 * L_13 = GameObject_GetComponent_TisAnimator_t69676727_m2717502299(L_12, /*hidden argument*/GameObject_GetComponent_TisAnimator_t69676727_m2717502299_MethodInfo_var);
		NullCheck(L_13);
		Animator_SetTrigger_m3418492570(L_13, _stringLiteral286381437, /*hidden argument*/NULL);
		Spawner_redrawLevels_m1471550761(__this, /*hidden argument*/NULL);
	}

IL_0069:
	{
		return;
	}
}
// System.Void Spawner::GoPageRight()
extern const MethodInfo* GameObject_GetComponent_TisCurrentPage_t1988855378_m3044519405_MethodInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisAnimator_t69676727_m2717502299_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral2515825212;
extern Il2CppCodeGenString* _stringLiteral286381437;
extern const uint32_t Spawner_GoPageRight_m3093617094_MetadataUsageId;
extern "C"  void Spawner_GoPageRight_m3093617094 (Spawner_t534830648 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Spawner_GoPageRight_m3093617094_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = __this->get_currentPage_11();
		int32_t L_1 = __this->get_numberOfPages_3();
		if ((((int32_t)L_0) >= ((int32_t)((int32_t)((int32_t)L_1-(int32_t)1)))))
		{
			goto IL_0070;
		}
	}
	{
		GameObjectU5BU5D_t3057952154* L_2 = __this->get_pages_10();
		int32_t L_3 = __this->get_currentPage_11();
		NullCheck(L_2);
		int32_t L_4 = L_3;
		GameObject_t1756533147 * L_5 = (L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4));
		NullCheck(L_5);
		CurrentPage_t1988855378 * L_6 = GameObject_GetComponent_TisCurrentPage_t1988855378_m3044519405(L_5, /*hidden argument*/GameObject_GetComponent_TisCurrentPage_t1988855378_m3044519405_MethodInfo_var);
		NullCheck(L_6);
		L_6->set_isCurrentPage_4((bool)0);
		int32_t L_7 = __this->get_currentPage_11();
		__this->set_currentPage_11(((int32_t)((int32_t)L_7+(int32_t)1)));
		GameObjectU5BU5D_t3057952154* L_8 = __this->get_pages_10();
		int32_t L_9 = __this->get_currentPage_11();
		NullCheck(L_8);
		int32_t L_10 = L_9;
		GameObject_t1756533147 * L_11 = (L_8)->GetAt(static_cast<il2cpp_array_size_t>(L_10));
		NullCheck(L_11);
		CurrentPage_t1988855378 * L_12 = GameObject_GetComponent_TisCurrentPage_t1988855378_m3044519405(L_11, /*hidden argument*/GameObject_GetComponent_TisCurrentPage_t1988855378_m3044519405_MethodInfo_var);
		NullCheck(L_12);
		L_12->set_isCurrentPage_4((bool)1);
		GameObject_t1756533147 * L_13 = GameObject_Find_m836511350(NULL /*static, unused*/, _stringLiteral2515825212, /*hidden argument*/NULL);
		NullCheck(L_13);
		Animator_t69676727 * L_14 = GameObject_GetComponent_TisAnimator_t69676727_m2717502299(L_13, /*hidden argument*/GameObject_GetComponent_TisAnimator_t69676727_m2717502299_MethodInfo_var);
		NullCheck(L_14);
		Animator_SetTrigger_m3418492570(L_14, _stringLiteral286381437, /*hidden argument*/NULL);
		Spawner_redrawLevels_m1471550761(__this, /*hidden argument*/NULL);
	}

IL_0070:
	{
		return;
	}
}
// System.Void Spawner::redrawLevels()
extern Il2CppClass* Int32_t2071877448_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const MethodInfo* Object_Instantiate_TisGameObject_t1756533147_m3066053529_MethodInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisRectTransform_t3349966182_m132995507_MethodInfo_var;
extern const MethodInfo* Component_GetComponent_TisText_t356221433_m1342661039_MethodInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisStarShower_t3709226430_m1692839511_MethodInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisButton_t2872111280_m1008560876_MethodInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisImage_t2042527209_m4162535761_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral1594932300;
extern Il2CppCodeGenString* _stringLiteral2515825212;
extern Il2CppCodeGenString* _stringLiteral3423762541;
extern Il2CppCodeGenString* _stringLiteral1539295160;
extern Il2CppCodeGenString* _stringLiteral497497017;
extern const uint32_t Spawner_redrawLevels_m1471550761_MetadataUsageId;
extern "C"  void Spawner_redrawLevels_m1471550761 (Spawner_t534830648 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Spawner_redrawLevels_m1471550761_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	GameObject_t1756533147 * V_1 = NULL;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	GameObject_t1756533147 * V_4 = NULL;
	int32_t V_5 = 0;
	{
		V_0 = 1;
		goto IL_0037;
	}

IL_0007:
	{
		int32_t L_0 = V_0;
		int32_t L_1 = L_0;
		Il2CppObject * L_2 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_1);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_3 = String_Concat_m56707527(NULL /*static, unused*/, _stringLiteral1594932300, L_2, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_4 = GameObject_Find_m836511350(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		V_1 = L_4;
		GameObject_t1756533147 * L_5 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_6 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0033;
		}
	}
	{
		GameObject_t1756533147 * L_7 = V_1;
		NullCheck(L_7);
		GameObject_t1756533147 * L_8 = GameObject_get_gameObject_m3662236595(L_7, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		Object_Destroy_m4145850038(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
	}

IL_0033:
	{
		int32_t L_9 = V_0;
		V_0 = ((int32_t)((int32_t)L_9+(int32_t)1));
	}

IL_0037:
	{
		int32_t L_10 = V_0;
		if ((((int32_t)L_10) <= ((int32_t)((int32_t)10))))
		{
			goto IL_0007;
		}
	}
	{
		V_2 = 0;
		goto IL_01d4;
	}

IL_0046:
	{
		V_3 = 0;
		goto IL_01c2;
	}

IL_004d:
	{
		int32_t L_11 = __this->get_currentPage_11();
		int32_t L_12 = V_2;
		int32_t L_13 = V_3;
		int32_t L_14 = __this->get_numberOfLevels_2();
		if ((((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)10)*(int32_t)L_11))+(int32_t)((int32_t)((int32_t)5*(int32_t)L_12))))+(int32_t)L_13))+(int32_t)1))) <= ((int32_t)L_14)))
		{
			goto IL_006e;
		}
	}
	{
		goto IL_01d0;
	}

IL_006e:
	{
		GameObject_t1756533147 * L_15 = __this->get_levelButton_8();
		GameObject_t1756533147 * L_16 = GameObject_Find_m836511350(NULL /*static, unused*/, _stringLiteral2515825212, /*hidden argument*/NULL);
		NullCheck(L_16);
		Transform_t3275118058 * L_17 = GameObject_get_transform_m909382139(L_16, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		GameObject_t1756533147 * L_18 = Object_Instantiate_TisGameObject_t1756533147_m3066053529(NULL /*static, unused*/, L_15, L_17, /*hidden argument*/Object_Instantiate_TisGameObject_t1756533147_m3066053529_MethodInfo_var);
		V_4 = L_18;
		GameObject_t1756533147 * L_19 = V_4;
		NullCheck(L_19);
		RectTransform_t3349966182 * L_20 = GameObject_GetComponent_TisRectTransform_t3349966182_m132995507(L_19, /*hidden argument*/GameObject_GetComponent_TisRectTransform_t3349966182_m132995507_MethodInfo_var);
		SingleU5BU5D_t577127397* L_21 = __this->get_levelButtonPozitionsX_5();
		int32_t L_22 = V_3;
		NullCheck(L_21);
		int32_t L_23 = L_22;
		float L_24 = (L_21)->GetAt(static_cast<il2cpp_array_size_t>(L_23));
		SingleU5BU5D_t577127397* L_25 = __this->get_levelButtonPozitionsY_4();
		int32_t L_26 = V_2;
		NullCheck(L_25);
		int32_t L_27 = L_26;
		float L_28 = (L_25)->GetAt(static_cast<il2cpp_array_size_t>(L_27));
		Vector2_t2243707579  L_29;
		memset(&L_29, 0, sizeof(L_29));
		Vector2__ctor_m3067419446(&L_29, L_24, L_28, /*hidden argument*/NULL);
		Vector3_t2243707580  L_30 = Vector2_op_Implicit_m176791411(NULL /*static, unused*/, L_29, /*hidden argument*/NULL);
		NullCheck(L_20);
		Transform_set_localPosition_m1026930133(L_20, L_30, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_31 = V_4;
		NullCheck(L_31);
		RectTransform_t3349966182 * L_32 = GameObject_GetComponent_TisRectTransform_t3349966182_m132995507(L_31, /*hidden argument*/GameObject_GetComponent_TisRectTransform_t3349966182_m132995507_MethodInfo_var);
		Vector2_t2243707579  L_33;
		memset(&L_33, 0, sizeof(L_33));
		Vector2__ctor_m3067419446(&L_33, (1.0f), (1.0f), /*hidden argument*/NULL);
		Vector3_t2243707580  L_34 = Vector2_op_Implicit_m176791411(NULL /*static, unused*/, L_33, /*hidden argument*/NULL);
		NullCheck(L_32);
		Transform_set_localScale_m2325460848(L_32, L_34, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_35 = V_4;
		NullCheck(L_35);
		Transform_t3275118058 * L_36 = GameObject_get_transform_m909382139(L_35, /*hidden argument*/NULL);
		int32_t L_37 = V_2;
		int32_t L_38 = V_3;
		int32_t L_39 = ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)5*(int32_t)L_37))+(int32_t)L_38))+(int32_t)1));
		Il2CppObject * L_40 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_39);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_41 = String_Concat_m56707527(NULL /*static, unused*/, _stringLiteral1594932300, L_40, /*hidden argument*/NULL);
		NullCheck(L_36);
		Object_set_name_m4157836998(L_36, L_41, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_42 = V_4;
		NullCheck(L_42);
		Transform_t3275118058 * L_43 = GameObject_get_transform_m909382139(L_42, /*hidden argument*/NULL);
		NullCheck(L_43);
		Transform_t3275118058 * L_44 = Transform_FindChild_m2677714886(L_43, _stringLiteral3423762541, /*hidden argument*/NULL);
		NullCheck(L_44);
		Text_t356221433 * L_45 = Component_GetComponent_TisText_t356221433_m1342661039(L_44, /*hidden argument*/Component_GetComponent_TisText_t356221433_m1342661039_MethodInfo_var);
		String_t* L_46 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		int32_t L_47 = __this->get_currentPage_11();
		int32_t L_48 = V_2;
		int32_t L_49 = V_3;
		int32_t L_50 = ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)10)*(int32_t)L_47))+(int32_t)((int32_t)((int32_t)5*(int32_t)L_48))))+(int32_t)L_49))+(int32_t)1));
		Il2CppObject * L_51 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_50);
		String_t* L_52 = String_Concat_m56707527(NULL /*static, unused*/, L_46, L_51, /*hidden argument*/NULL);
		NullCheck(L_45);
		VirtActionInvoker1< String_t* >::Invoke(72 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_45, L_52);
		GameObject_t1756533147 * L_53 = V_4;
		NullCheck(L_53);
		StarShower_t3709226430 * L_54 = GameObject_GetComponent_TisStarShower_t3709226430_m1692839511(L_53, /*hidden argument*/GameObject_GetComponent_TisStarShower_t3709226430_m1692839511_MethodInfo_var);
		NullCheck(L_54);
		L_54->set_numberOfStars_2(3);
		int32_t L_55 = __this->get_currentPage_11();
		int32_t L_56 = V_2;
		int32_t L_57 = V_3;
		V_5 = ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)10)*(int32_t)L_55))+(int32_t)((int32_t)((int32_t)5*(int32_t)L_56))))+(int32_t)L_57))+(int32_t)1));
		GameState_t2029468607 * L_58 = __this->get_currentGameState_12();
		NullCheck(L_58);
		LevelU5BU5D_t1839895005* L_59 = L_58->get_levels_0();
		int32_t L_60 = V_5;
		NullCheck(L_59);
		int32_t L_61 = L_60;
		Level_t1066665556 * L_62 = (L_59)->GetAt(static_cast<il2cpp_array_size_t>(L_61));
		NullCheck(L_62);
		int32_t L_63 = L_62->get_status_5();
		if ((!(((uint32_t)L_63) == ((uint32_t)(-1)))))
		{
			goto IL_0178;
		}
	}
	{
		GameObject_t1756533147 * L_64 = V_4;
		NullCheck(L_64);
		StarShower_t3709226430 * L_65 = GameObject_GetComponent_TisStarShower_t3709226430_m1692839511(L_64, /*hidden argument*/GameObject_GetComponent_TisStarShower_t3709226430_m1692839511_MethodInfo_var);
		NullCheck(L_65);
		L_65->set_locked_3((bool)1);
		goto IL_01a4;
	}

IL_0178:
	{
		GameObject_t1756533147 * L_66 = V_4;
		NullCheck(L_66);
		StarShower_t3709226430 * L_67 = GameObject_GetComponent_TisStarShower_t3709226430_m1692839511(L_66, /*hidden argument*/GameObject_GetComponent_TisStarShower_t3709226430_m1692839511_MethodInfo_var);
		NullCheck(L_67);
		L_67->set_locked_3((bool)0);
		GameObject_t1756533147 * L_68 = V_4;
		NullCheck(L_68);
		StarShower_t3709226430 * L_69 = GameObject_GetComponent_TisStarShower_t3709226430_m1692839511(L_68, /*hidden argument*/GameObject_GetComponent_TisStarShower_t3709226430_m1692839511_MethodInfo_var);
		GameState_t2029468607 * L_70 = __this->get_currentGameState_12();
		NullCheck(L_70);
		LevelU5BU5D_t1839895005* L_71 = L_70->get_levels_0();
		int32_t L_72 = V_5;
		NullCheck(L_71);
		int32_t L_73 = L_72;
		Level_t1066665556 * L_74 = (L_71)->GetAt(static_cast<il2cpp_array_size_t>(L_73));
		NullCheck(L_74);
		int32_t L_75 = L_74->get_status_5();
		NullCheck(L_69);
		L_69->set_numberOfStars_2(L_75);
	}

IL_01a4:
	{
		GameObject_t1756533147 * L_76 = V_4;
		NullCheck(L_76);
		StarShower_t3709226430 * L_77 = GameObject_GetComponent_TisStarShower_t3709226430_m1692839511(L_76, /*hidden argument*/GameObject_GetComponent_TisStarShower_t3709226430_m1692839511_MethodInfo_var);
		int32_t L_78 = V_5;
		NullCheck(L_77);
		L_77->set_levelNumber_6(L_78);
		GameObject_t1756533147 * L_79 = V_4;
		NullCheck(L_79);
		StarShower_t3709226430 * L_80 = GameObject_GetComponent_TisStarShower_t3709226430_m1692839511(L_79, /*hidden argument*/GameObject_GetComponent_TisStarShower_t3709226430_m1692839511_MethodInfo_var);
		NullCheck(L_80);
		StarShower_FlushStars_m968795006(L_80, /*hidden argument*/NULL);
		int32_t L_81 = V_3;
		V_3 = ((int32_t)((int32_t)L_81+(int32_t)1));
	}

IL_01c2:
	{
		int32_t L_82 = V_3;
		SingleU5BU5D_t577127397* L_83 = __this->get_levelButtonPozitionsX_5();
		NullCheck(L_83);
		if ((((int32_t)L_82) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_83)->max_length)))))))
		{
			goto IL_004d;
		}
	}

IL_01d0:
	{
		int32_t L_84 = V_2;
		V_2 = ((int32_t)((int32_t)L_84+(int32_t)1));
	}

IL_01d4:
	{
		int32_t L_85 = V_2;
		SingleU5BU5D_t577127397* L_86 = __this->get_levelButtonPozitionsY_4();
		NullCheck(L_86);
		if ((((int32_t)L_85) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_86)->max_length)))))))
		{
			goto IL_0046;
		}
	}
	{
		int32_t L_87 = __this->get_currentPage_11();
		if (L_87)
		{
			goto IL_021c;
		}
	}
	{
		GameObject_t1756533147 * L_88 = GameObject_Find_m836511350(NULL /*static, unused*/, _stringLiteral1539295160, /*hidden argument*/NULL);
		NullCheck(L_88);
		Button_t2872111280 * L_89 = GameObject_GetComponent_TisButton_t2872111280_m1008560876(L_88, /*hidden argument*/GameObject_GetComponent_TisButton_t2872111280_m1008560876_MethodInfo_var);
		NullCheck(L_89);
		Behaviour_set_enabled_m1796096907(L_89, (bool)0, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_90 = GameObject_Find_m836511350(NULL /*static, unused*/, _stringLiteral1539295160, /*hidden argument*/NULL);
		NullCheck(L_90);
		Image_t2042527209 * L_91 = GameObject_GetComponent_TisImage_t2042527209_m4162535761(L_90, /*hidden argument*/GameObject_GetComponent_TisImage_t2042527209_m4162535761_MethodInfo_var);
		NullCheck(L_91);
		Behaviour_set_enabled_m1796096907(L_91, (bool)0, /*hidden argument*/NULL);
		goto IL_0246;
	}

IL_021c:
	{
		GameObject_t1756533147 * L_92 = GameObject_Find_m836511350(NULL /*static, unused*/, _stringLiteral1539295160, /*hidden argument*/NULL);
		NullCheck(L_92);
		Button_t2872111280 * L_93 = GameObject_GetComponent_TisButton_t2872111280_m1008560876(L_92, /*hidden argument*/GameObject_GetComponent_TisButton_t2872111280_m1008560876_MethodInfo_var);
		NullCheck(L_93);
		Behaviour_set_enabled_m1796096907(L_93, (bool)1, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_94 = GameObject_Find_m836511350(NULL /*static, unused*/, _stringLiteral1539295160, /*hidden argument*/NULL);
		NullCheck(L_94);
		Image_t2042527209 * L_95 = GameObject_GetComponent_TisImage_t2042527209_m4162535761(L_94, /*hidden argument*/GameObject_GetComponent_TisImage_t2042527209_m4162535761_MethodInfo_var);
		NullCheck(L_95);
		Behaviour_set_enabled_m1796096907(L_95, (bool)1, /*hidden argument*/NULL);
	}

IL_0246:
	{
		int32_t L_96 = __this->get_currentPage_11();
		int32_t L_97 = __this->get_numberOfPages_3();
		if ((!(((uint32_t)L_96) == ((uint32_t)((int32_t)((int32_t)L_97-(int32_t)1))))))
		{
			goto IL_0288;
		}
	}
	{
		GameObject_t1756533147 * L_98 = GameObject_Find_m836511350(NULL /*static, unused*/, _stringLiteral497497017, /*hidden argument*/NULL);
		NullCheck(L_98);
		Button_t2872111280 * L_99 = GameObject_GetComponent_TisButton_t2872111280_m1008560876(L_98, /*hidden argument*/GameObject_GetComponent_TisButton_t2872111280_m1008560876_MethodInfo_var);
		NullCheck(L_99);
		Behaviour_set_enabled_m1796096907(L_99, (bool)0, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_100 = GameObject_Find_m836511350(NULL /*static, unused*/, _stringLiteral497497017, /*hidden argument*/NULL);
		NullCheck(L_100);
		Image_t2042527209 * L_101 = GameObject_GetComponent_TisImage_t2042527209_m4162535761(L_100, /*hidden argument*/GameObject_GetComponent_TisImage_t2042527209_m4162535761_MethodInfo_var);
		NullCheck(L_101);
		Behaviour_set_enabled_m1796096907(L_101, (bool)0, /*hidden argument*/NULL);
		goto IL_02b2;
	}

IL_0288:
	{
		GameObject_t1756533147 * L_102 = GameObject_Find_m836511350(NULL /*static, unused*/, _stringLiteral497497017, /*hidden argument*/NULL);
		NullCheck(L_102);
		Button_t2872111280 * L_103 = GameObject_GetComponent_TisButton_t2872111280_m1008560876(L_102, /*hidden argument*/GameObject_GetComponent_TisButton_t2872111280_m1008560876_MethodInfo_var);
		NullCheck(L_103);
		Behaviour_set_enabled_m1796096907(L_103, (bool)1, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_104 = GameObject_Find_m836511350(NULL /*static, unused*/, _stringLiteral497497017, /*hidden argument*/NULL);
		NullCheck(L_104);
		Image_t2042527209 * L_105 = GameObject_GetComponent_TisImage_t2042527209_m4162535761(L_104, /*hidden argument*/GameObject_GetComponent_TisImage_t2042527209_m4162535761_MethodInfo_var);
		NullCheck(L_105);
		Behaviour_set_enabled_m1796096907(L_105, (bool)1, /*hidden argument*/NULL);
	}

IL_02b2:
	{
		return;
	}
}
// System.Void StarShower::.ctor()
extern "C"  void StarShower__ctor_m4067111103 (StarShower_t3709226430 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void StarShower::FlushStars()
extern const MethodInfo* Component_GetComponent_TisImage_t2042527209_m2189462422_MethodInfo_var;
extern const MethodInfo* Component_GetComponent_TisRectTransform_t3349966182_m1310250299_MethodInfo_var;
extern const MethodInfo* Component_GetComponent_TisText_t356221433_m1342661039_MethodInfo_var;
extern const MethodInfo* Component_GetComponent_TisButton_t2872111280_m3412601438_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral2858757941;
extern Il2CppCodeGenString* _stringLiteral2858757940;
extern Il2CppCodeGenString* _stringLiteral2858757939;
extern Il2CppCodeGenString* _stringLiteral3423762541;
extern const uint32_t StarShower_FlushStars_m968795006_MetadataUsageId;
extern "C"  void StarShower_FlushStars_m968795006 (StarShower_t3709226430 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (StarShower_FlushStars_m968795006_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	RectTransform_t3349966182 * V_0 = NULL;
	Vector2_t2243707579  V_1;
	memset(&V_1, 0, sizeof(V_1));
	RectTransform_t3349966182 * V_2 = NULL;
	Vector2_t2243707579  V_3;
	memset(&V_3, 0, sizeof(V_3));
	{
		bool L_0 = __this->get_locked_3();
		if (!L_0)
		{
			goto IL_00d4;
		}
	}
	{
		Transform_t3275118058 * L_1 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_1);
		Transform_t3275118058 * L_2 = Transform_FindChild_m2677714886(L_1, _stringLiteral2858757941, /*hidden argument*/NULL);
		NullCheck(L_2);
		Image_t2042527209 * L_3 = Component_GetComponent_TisImage_t2042527209_m2189462422(L_2, /*hidden argument*/Component_GetComponent_TisImage_t2042527209_m2189462422_MethodInfo_var);
		NullCheck(L_3);
		Behaviour_set_enabled_m1796096907(L_3, (bool)0, /*hidden argument*/NULL);
		Transform_t3275118058 * L_4 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_4);
		Transform_t3275118058 * L_5 = Transform_FindChild_m2677714886(L_4, _stringLiteral2858757940, /*hidden argument*/NULL);
		NullCheck(L_5);
		Image_t2042527209 * L_6 = Component_GetComponent_TisImage_t2042527209_m2189462422(L_5, /*hidden argument*/Component_GetComponent_TisImage_t2042527209_m2189462422_MethodInfo_var);
		NullCheck(L_6);
		Behaviour_set_enabled_m1796096907(L_6, (bool)0, /*hidden argument*/NULL);
		Transform_t3275118058 * L_7 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_7);
		Transform_t3275118058 * L_8 = Transform_FindChild_m2677714886(L_7, _stringLiteral2858757939, /*hidden argument*/NULL);
		NullCheck(L_8);
		Image_t2042527209 * L_9 = Component_GetComponent_TisImage_t2042527209_m2189462422(L_8, /*hidden argument*/Component_GetComponent_TisImage_t2042527209_m2189462422_MethodInfo_var);
		NullCheck(L_9);
		Behaviour_set_enabled_m1796096907(L_9, (bool)0, /*hidden argument*/NULL);
		Image_t2042527209 * L_10 = Component_GetComponent_TisImage_t2042527209_m2189462422(__this, /*hidden argument*/Component_GetComponent_TisImage_t2042527209_m2189462422_MethodInfo_var);
		Sprite_t309593783 * L_11 = __this->get_lockedSprite_4();
		NullCheck(L_10);
		Image_set_sprite_m1800056820(L_10, L_11, /*hidden argument*/NULL);
		Transform_t3275118058 * L_12 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_12);
		Transform_t3275118058 * L_13 = Transform_FindChild_m2677714886(L_12, _stringLiteral3423762541, /*hidden argument*/NULL);
		NullCheck(L_13);
		RectTransform_t3349966182 * L_14 = Component_GetComponent_TisRectTransform_t3349966182_m1310250299(L_13, /*hidden argument*/Component_GetComponent_TisRectTransform_t3349966182_m1310250299_MethodInfo_var);
		V_0 = L_14;
		RectTransform_t3349966182 * L_15 = V_0;
		NullCheck(L_15);
		Vector3_t2243707580  L_16 = Transform_get_localPosition_m2533925116(L_15, /*hidden argument*/NULL);
		Vector2_t2243707579  L_17 = Vector2_op_Implicit_m1064335535(NULL /*static, unused*/, L_16, /*hidden argument*/NULL);
		V_1 = L_17;
		RectTransform_t3349966182 * L_18 = V_0;
		float L_19 = (&V_1)->get_x_0();
		Vector2_t2243707579  L_20;
		memset(&L_20, 0, sizeof(L_20));
		Vector2__ctor_m3067419446(&L_20, L_19, (5.0f), /*hidden argument*/NULL);
		Vector3_t2243707580  L_21 = Vector2_op_Implicit_m176791411(NULL /*static, unused*/, L_20, /*hidden argument*/NULL);
		NullCheck(L_18);
		Transform_set_localPosition_m1026930133(L_18, L_21, /*hidden argument*/NULL);
		Transform_t3275118058 * L_22 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_22);
		Transform_t3275118058 * L_23 = Transform_FindChild_m2677714886(L_22, _stringLiteral3423762541, /*hidden argument*/NULL);
		NullCheck(L_23);
		Text_t356221433 * L_24 = Component_GetComponent_TisText_t356221433_m1342661039(L_23, /*hidden argument*/Component_GetComponent_TisText_t356221433_m1342661039_MethodInfo_var);
		NullCheck(L_24);
		Text_set_fontSize_m2101304336(L_24, ((int32_t)30), /*hidden argument*/NULL);
		Button_t2872111280 * L_25 = Component_GetComponent_TisButton_t2872111280_m3412601438(__this, /*hidden argument*/Component_GetComponent_TisButton_t2872111280_m3412601438_MethodInfo_var);
		NullCheck(L_25);
		Selectable_set_interactable_m63718297(L_25, (bool)0, /*hidden argument*/NULL);
		return;
	}

IL_00d4:
	{
		Transform_t3275118058 * L_26 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_26);
		Transform_t3275118058 * L_27 = Transform_FindChild_m2677714886(L_26, _stringLiteral3423762541, /*hidden argument*/NULL);
		NullCheck(L_27);
		RectTransform_t3349966182 * L_28 = Component_GetComponent_TisRectTransform_t3349966182_m1310250299(L_27, /*hidden argument*/Component_GetComponent_TisRectTransform_t3349966182_m1310250299_MethodInfo_var);
		V_2 = L_28;
		RectTransform_t3349966182 * L_29 = V_2;
		NullCheck(L_29);
		Vector3_t2243707580  L_30 = Transform_get_localPosition_m2533925116(L_29, /*hidden argument*/NULL);
		Vector2_t2243707579  L_31 = Vector2_op_Implicit_m1064335535(NULL /*static, unused*/, L_30, /*hidden argument*/NULL);
		V_3 = L_31;
		RectTransform_t3349966182 * L_32 = V_2;
		float L_33 = (&V_3)->get_x_0();
		Vector2_t2243707579  L_34;
		memset(&L_34, 0, sizeof(L_34));
		Vector2__ctor_m3067419446(&L_34, L_33, (8.0f), /*hidden argument*/NULL);
		Vector3_t2243707580  L_35 = Vector2_op_Implicit_m176791411(NULL /*static, unused*/, L_34, /*hidden argument*/NULL);
		NullCheck(L_32);
		Transform_set_localPosition_m1026930133(L_32, L_35, /*hidden argument*/NULL);
		Transform_t3275118058 * L_36 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_36);
		Transform_t3275118058 * L_37 = Transform_FindChild_m2677714886(L_36, _stringLiteral3423762541, /*hidden argument*/NULL);
		NullCheck(L_37);
		Text_t356221433 * L_38 = Component_GetComponent_TisText_t356221433_m1342661039(L_37, /*hidden argument*/Component_GetComponent_TisText_t356221433_m1342661039_MethodInfo_var);
		NullCheck(L_38);
		Text_set_fontSize_m2101304336(L_38, ((int32_t)40), /*hidden argument*/NULL);
		Image_t2042527209 * L_39 = Component_GetComponent_TisImage_t2042527209_m2189462422(__this, /*hidden argument*/Component_GetComponent_TisImage_t2042527209_m2189462422_MethodInfo_var);
		Sprite_t309593783 * L_40 = __this->get_unlockedSprite_5();
		NullCheck(L_39);
		Image_set_sprite_m1800056820(L_39, L_40, /*hidden argument*/NULL);
		int32_t L_41 = __this->get_numberOfStars_2();
		if ((((int32_t)L_41) <= ((int32_t)0)))
		{
			goto IL_016b;
		}
	}
	{
		Transform_t3275118058 * L_42 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_42);
		Transform_t3275118058 * L_43 = Transform_FindChild_m2677714886(L_42, _stringLiteral2858757941, /*hidden argument*/NULL);
		NullCheck(L_43);
		Image_t2042527209 * L_44 = Component_GetComponent_TisImage_t2042527209_m2189462422(L_43, /*hidden argument*/Component_GetComponent_TisImage_t2042527209_m2189462422_MethodInfo_var);
		NullCheck(L_44);
		Behaviour_set_enabled_m1796096907(L_44, (bool)1, /*hidden argument*/NULL);
		goto IL_0186;
	}

IL_016b:
	{
		Transform_t3275118058 * L_45 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_45);
		Transform_t3275118058 * L_46 = Transform_FindChild_m2677714886(L_45, _stringLiteral2858757941, /*hidden argument*/NULL);
		NullCheck(L_46);
		Image_t2042527209 * L_47 = Component_GetComponent_TisImage_t2042527209_m2189462422(L_46, /*hidden argument*/Component_GetComponent_TisImage_t2042527209_m2189462422_MethodInfo_var);
		NullCheck(L_47);
		Behaviour_set_enabled_m1796096907(L_47, (bool)0, /*hidden argument*/NULL);
	}

IL_0186:
	{
		int32_t L_48 = __this->get_numberOfStars_2();
		if ((((int32_t)L_48) <= ((int32_t)1)))
		{
			goto IL_01b2;
		}
	}
	{
		Transform_t3275118058 * L_49 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_49);
		Transform_t3275118058 * L_50 = Transform_FindChild_m2677714886(L_49, _stringLiteral2858757940, /*hidden argument*/NULL);
		NullCheck(L_50);
		Image_t2042527209 * L_51 = Component_GetComponent_TisImage_t2042527209_m2189462422(L_50, /*hidden argument*/Component_GetComponent_TisImage_t2042527209_m2189462422_MethodInfo_var);
		NullCheck(L_51);
		Behaviour_set_enabled_m1796096907(L_51, (bool)1, /*hidden argument*/NULL);
		goto IL_01cd;
	}

IL_01b2:
	{
		Transform_t3275118058 * L_52 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_52);
		Transform_t3275118058 * L_53 = Transform_FindChild_m2677714886(L_52, _stringLiteral2858757940, /*hidden argument*/NULL);
		NullCheck(L_53);
		Image_t2042527209 * L_54 = Component_GetComponent_TisImage_t2042527209_m2189462422(L_53, /*hidden argument*/Component_GetComponent_TisImage_t2042527209_m2189462422_MethodInfo_var);
		NullCheck(L_54);
		Behaviour_set_enabled_m1796096907(L_54, (bool)0, /*hidden argument*/NULL);
	}

IL_01cd:
	{
		int32_t L_55 = __this->get_numberOfStars_2();
		if ((((int32_t)L_55) <= ((int32_t)2)))
		{
			goto IL_01f9;
		}
	}
	{
		Transform_t3275118058 * L_56 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_56);
		Transform_t3275118058 * L_57 = Transform_FindChild_m2677714886(L_56, _stringLiteral2858757939, /*hidden argument*/NULL);
		NullCheck(L_57);
		Image_t2042527209 * L_58 = Component_GetComponent_TisImage_t2042527209_m2189462422(L_57, /*hidden argument*/Component_GetComponent_TisImage_t2042527209_m2189462422_MethodInfo_var);
		NullCheck(L_58);
		Behaviour_set_enabled_m1796096907(L_58, (bool)1, /*hidden argument*/NULL);
		goto IL_0214;
	}

IL_01f9:
	{
		Transform_t3275118058 * L_59 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_59);
		Transform_t3275118058 * L_60 = Transform_FindChild_m2677714886(L_59, _stringLiteral2858757939, /*hidden argument*/NULL);
		NullCheck(L_60);
		Image_t2042527209 * L_61 = Component_GetComponent_TisImage_t2042527209_m2189462422(L_60, /*hidden argument*/Component_GetComponent_TisImage_t2042527209_m2189462422_MethodInfo_var);
		NullCheck(L_61);
		Behaviour_set_enabled_m1796096907(L_61, (bool)0, /*hidden argument*/NULL);
	}

IL_0214:
	{
		return;
	}
}
// System.Void StarShower::GoToLevel()
extern Il2CppClass* StaticLevelContainer_t3714456515_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32_t2071877448_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1594891052;
extern const uint32_t StarShower_GoToLevel_m2221394174_MetadataUsageId;
extern "C"  void StarShower_GoToLevel_m2221394174 (StarShower_t3709226430 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (StarShower_GoToLevel_m2221394174_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(StaticLevelContainer_t3714456515_il2cpp_TypeInfo_var);
		StaticLevelContainer_t3714456515 * L_0 = StaticLevelContainer_getInstance_m3684656527(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_1 = __this->get_levelNumber_6();
		NullCheck(L_0);
		L_0->set_levelToLoad_0(L_1);
		int32_t L_2 = __this->get_levelNumber_6();
		int32_t L_3 = L_2;
		Il2CppObject * L_4 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_3);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_5 = String_Concat_m56707527(NULL /*static, unused*/, _stringLiteral1594891052, L_4, /*hidden argument*/NULL);
		SceneManager_LoadScene_m1619949821(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		return;
	}
}
// System.Void StaticData.Constants::.ctor()
extern "C"  void Constants__ctor_m1506138744 (Constants_t3271648343 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void StaticData.Constants::.cctor()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Constants_t3271648343_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral981845120;
extern const uint32_t Constants__cctor_m3623256087_MetadataUsageId;
extern "C"  void Constants__cctor_m3623256087 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Constants__cctor_m3623256087_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = Application_get_persistentDataPath_m3129298355(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_1 = String_Concat_m2596409543(NULL /*static, unused*/, L_0, _stringLiteral981845120, /*hidden argument*/NULL);
		((Constants_t3271648343_StaticFields*)Constants_t3271648343_il2cpp_TypeInfo_var->static_fields)->set_SAVE_GAME_PATH_0(L_1);
		return;
	}
}
// System.Void StaticData.StaticLevelContainer::.ctor()
extern "C"  void StaticLevelContainer__ctor_m1801706126 (StaticLevelContainer_t3714456515 * __this, const MethodInfo* method)
{
	{
		__this->set_maxNumberOfReplays_3(5);
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		__this->set_levelToLoad_0(0);
		int32_t L_0 = __this->get_maxNumberOfReplays_3();
		__this->set_adsCounter_2(L_0);
		return;
	}
}
// System.Boolean StaticData.StaticLevelContainer::playAd()
extern "C"  bool StaticLevelContainer_playAd_m164333081 (StaticLevelContainer_t3714456515 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_adsCounter_2();
		__this->set_adsCounter_2(((int32_t)((int32_t)L_0-(int32_t)1)));
		int32_t L_1 = __this->get_adsCounter_2();
		if ((((int32_t)L_1) >= ((int32_t)0)))
		{
			goto IL_0028;
		}
	}
	{
		int32_t L_2 = __this->get_maxNumberOfReplays_3();
		__this->set_adsCounter_2(L_2);
		return (bool)1;
	}

IL_0028:
	{
		return (bool)0;
	}
}
// StaticData.StaticLevelContainer StaticData.StaticLevelContainer::getInstance()
extern Il2CppClass* StaticLevelContainer_t3714456515_il2cpp_TypeInfo_var;
extern const uint32_t StaticLevelContainer_getInstance_m3684656527_MetadataUsageId;
extern "C"  StaticLevelContainer_t3714456515 * StaticLevelContainer_getInstance_m3684656527 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (StaticLevelContainer_getInstance_m3684656527_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(StaticLevelContainer_t3714456515_il2cpp_TypeInfo_var);
		StaticLevelContainer_t3714456515 * L_0 = ((StaticLevelContainer_t3714456515_StaticFields*)StaticLevelContainer_t3714456515_il2cpp_TypeInfo_var->static_fields)->get__instance_4();
		if (L_0)
		{
			goto IL_0014;
		}
	}
	{
		StaticLevelContainer_t3714456515 * L_1 = (StaticLevelContainer_t3714456515 *)il2cpp_codegen_object_new(StaticLevelContainer_t3714456515_il2cpp_TypeInfo_var);
		StaticLevelContainer__ctor_m1801706126(L_1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(StaticLevelContainer_t3714456515_il2cpp_TypeInfo_var);
		((StaticLevelContainer_t3714456515_StaticFields*)StaticLevelContainer_t3714456515_il2cpp_TypeInfo_var->static_fields)->set__instance_4(L_1);
	}

IL_0014:
	{
		IL2CPP_RUNTIME_CLASS_INIT(StaticLevelContainer_t3714456515_il2cpp_TypeInfo_var);
		StaticLevelContainer_t3714456515 * L_2 = ((StaticLevelContainer_t3714456515_StaticFields*)StaticLevelContainer_t3714456515_il2cpp_TypeInfo_var->static_fields)->get__instance_4();
		return L_2;
	}
}
// LoadSave.GameState StaticData.StaticLevelContainer::loadLevelFromStorage()
extern Il2CppClass* Constants_t3271648343_il2cpp_TypeInfo_var;
extern Il2CppClass* GameState_t2029468607_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern Il2CppClass* FileNotFoundException_t4200667904_il2cpp_TypeInfo_var;
extern Il2CppClass* IsolatedStorageException_t2011779685_il2cpp_TypeInfo_var;
extern Il2CppClass* Exception_t1927440687_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3712056248;
extern Il2CppCodeGenString* _stringLiteral3878670209;
extern const uint32_t StaticLevelContainer_loadLevelFromStorage_m1234179298_MetadataUsageId;
extern "C"  GameState_t2029468607 * StaticLevelContainer_loadLevelFromStorage_m1234179298 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (StaticLevelContainer_loadLevelFromStorage_m1234179298_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	GameState_t2029468607 * V_0 = NULL;
	FileNotFoundException_t4200667904 * V_1 = NULL;
	Exception_t1927440687 * V_2 = NULL;
	IsolatedStorageException_t2011779685 * V_3 = NULL;
	Exception_t1927440687 * V_4 = NULL;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		V_0 = (GameState_t2029468607 *)NULL;
	}

IL_0002:
	try
	{ // begin try (depth: 1)
		IL2CPP_RUNTIME_CLASS_INIT(Constants_t3271648343_il2cpp_TypeInfo_var);
		String_t* L_0 = ((Constants_t3271648343_StaticFields*)Constants_t3271648343_il2cpp_TypeInfo_var->static_fields)->get_SAVE_GAME_PATH_0();
		GameState_t2029468607 * L_1 = (GameState_t2029468607 *)il2cpp_codegen_object_new(GameState_t2029468607_il2cpp_TypeInfo_var);
		GameState__ctor_m3330283085(L_1, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, _stringLiteral3712056248, /*hidden argument*/NULL);
		goto IL_00a7;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1927440687 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (FileNotFoundException_t4200667904_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_001c;
		if(il2cpp_codegen_class_is_assignable_from (IsolatedStorageException_t2011779685_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_005e;
		throw e;
	}

CATCH_001c:
	{ // begin catch(System.IO.FileNotFoundException)
		{
			V_1 = ((FileNotFoundException_t4200667904 *)__exception_local);
		}

IL_001d:
		try
		{ // begin try (depth: 2)
			GameState_t2029468607 * L_2 = (GameState_t2029468607 *)il2cpp_codegen_object_new(GameState_t2029468607_il2cpp_TypeInfo_var);
			GameState__ctor_m4157959086(L_2, ((int32_t)50), /*hidden argument*/NULL);
			V_0 = L_2;
			GameState_t2029468607 * L_3 = V_0;
			NullCheck(L_3);
			LevelU5BU5D_t1839895005* L_4 = L_3->get_levels_0();
			NullCheck(L_4);
			int32_t L_5 = 1;
			Level_t1066665556 * L_6 = (L_4)->GetAt(static_cast<il2cpp_array_size_t>(L_5));
			NullCheck(L_6);
			L_6->set_status_5(0);
			GameState_t2029468607 * L_7 = V_0;
			IL2CPP_RUNTIME_CLASS_INIT(Constants_t3271648343_il2cpp_TypeInfo_var);
			String_t* L_8 = ((Constants_t3271648343_StaticFields*)Constants_t3271648343_il2cpp_TypeInfo_var->static_fields)->get_SAVE_GAME_PATH_0();
			NullCheck(L_7);
			GameState_Export_m2636018477(L_7, L_8, /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
			Debug_Log_m920475918(NULL /*static, unused*/, _stringLiteral3878670209, /*hidden argument*/NULL);
			goto IL_0059;
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__exception_local = (Exception_t1927440687 *)e.ex;
			if(il2cpp_codegen_class_is_assignable_from (Exception_t1927440687_il2cpp_TypeInfo_var, e.ex->object.klass))
				goto CATCH_004d;
			throw e;
		}

CATCH_004d:
		{ // begin catch(System.Exception)
			V_2 = ((Exception_t1927440687 *)__exception_local);
			Exception_t1927440687 * L_9 = V_2;
			IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
			Debug_Log_m920475918(NULL /*static, unused*/, L_9, /*hidden argument*/NULL);
			goto IL_0059;
		} // end catch (depth: 2)

IL_0059:
		{
			goto IL_00a7;
		}
	} // end catch (depth: 1)

CATCH_005e:
	{ // begin catch(System.IO.IsolatedStorage.IsolatedStorageException)
		{
			V_3 = ((IsolatedStorageException_t2011779685 *)__exception_local);
		}

IL_005f:
		try
		{ // begin try (depth: 2)
			GameState_t2029468607 * L_10 = (GameState_t2029468607 *)il2cpp_codegen_object_new(GameState_t2029468607_il2cpp_TypeInfo_var);
			GameState__ctor_m4157959086(L_10, ((int32_t)50), /*hidden argument*/NULL);
			V_0 = L_10;
			GameState_t2029468607 * L_11 = V_0;
			NullCheck(L_11);
			LevelU5BU5D_t1839895005* L_12 = L_11->get_levels_0();
			NullCheck(L_12);
			int32_t L_13 = 1;
			Level_t1066665556 * L_14 = (L_12)->GetAt(static_cast<il2cpp_array_size_t>(L_13));
			NullCheck(L_14);
			L_14->set_status_5(0);
			GameState_t2029468607 * L_15 = V_0;
			IL2CPP_RUNTIME_CLASS_INIT(Constants_t3271648343_il2cpp_TypeInfo_var);
			String_t* L_16 = ((Constants_t3271648343_StaticFields*)Constants_t3271648343_il2cpp_TypeInfo_var->static_fields)->get_SAVE_GAME_PATH_0();
			NullCheck(L_15);
			GameState_Export_m2636018477(L_15, L_16, /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
			Debug_Log_m920475918(NULL /*static, unused*/, _stringLiteral3878670209, /*hidden argument*/NULL);
			goto IL_00a2;
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__exception_local = (Exception_t1927440687 *)e.ex;
			if(il2cpp_codegen_class_is_assignable_from (Exception_t1927440687_il2cpp_TypeInfo_var, e.ex->object.klass))
				goto CATCH_008f;
			throw e;
		}

CATCH_008f:
		{ // begin catch(System.Exception)
			V_4 = ((Exception_t1927440687 *)__exception_local);
			Exception_t1927440687 * L_17 = V_4;
			NullCheck(L_17);
			String_t* L_18 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_17);
			IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
			Debug_Log_m920475918(NULL /*static, unused*/, L_18, /*hidden argument*/NULL);
			goto IL_00a2;
		} // end catch (depth: 2)

IL_00a2:
		{
			goto IL_00a7;
		}
	} // end catch (depth: 1)

IL_00a7:
	{
		GameState_t2029468607 * L_19 = V_0;
		return L_19;
	}
}
// System.Void StaticData.StaticLevelContainer::.cctor()
extern "C"  void StaticLevelContainer__cctor_m2563202227 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void StoryLineNextLevel::.ctor()
extern "C"  void StoryLineNextLevel__ctor_m3258329023 (StoryLineNextLevel_t1393071564 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void StoryLineNextLevel::Start()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4033375497;
extern const uint32_t StoryLineNextLevel_Start_m1126760907_MetadataUsageId;
extern "C"  void StoryLineNextLevel_Start_m1126760907 (StoryLineNextLevel_t1393071564 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (StoryLineNextLevel_Start_m1126760907_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Scene_t1684909666  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Scene_t1684909666  L_0 = SceneManager_GetActiveScene_m2964039490(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_0;
		String_t* L_1 = Scene_get_name_m745914591((&V_0), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_2 = String_Concat_m2596409543(NULL /*static, unused*/, _stringLiteral4033375497, L_1, /*hidden argument*/NULL);
		__this->set_nextSceneName_2(L_2);
		float L_3 = __this->get_secondsTillEnd_3();
		__this->set_timer_4(L_3);
		return;
	}
}
// System.Void StoryLineNextLevel::Update()
extern "C"  void StoryLineNextLevel_Update_m2797851598 (StoryLineNextLevel_t1393071564 * __this, const MethodInfo* method)
{
	{
		float L_0 = __this->get_timer_4();
		float L_1 = Time_get_deltaTime_m2233168104(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_timer_4(((float)((float)L_0-(float)L_1)));
		float L_2 = __this->get_timer_4();
		if ((!(((float)L_2) < ((float)(0.0f)))))
		{
			goto IL_0028;
		}
	}
	{
		StoryLineNextLevel_ChangeToNextLevel_m3463279619(__this, /*hidden argument*/NULL);
	}

IL_0028:
	{
		return;
	}
}
// System.Void StoryLineNextLevel::ChangeToNextLevel()
extern "C"  void StoryLineNextLevel_ChangeToNextLevel_m3463279619 (StoryLineNextLevel_t1393071564 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_nextSceneName_2();
		SceneManager_LoadScene_m1619949821(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void TutorialFollow::.ctor()
extern "C"  void TutorialFollow__ctor_m3454975770 (TutorialFollow_t1280665443 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void TutorialFollow::Start()
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const MethodInfo* Component_GetComponent_TisSpriteRenderer_t1209076198_m2178781570_MethodInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisText_t356221433_m1217399699_MethodInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisRemovable_t2119088671_m2134440904_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral2830371216;
extern Il2CppCodeGenString* _stringLiteral1994460817;
extern const uint32_t TutorialFollow_Start_m2699853582_MetadataUsageId;
extern "C"  void TutorialFollow_Start_m2699853582 (TutorialFollow_t1280665443 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TutorialFollow_Start_m2699853582_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	GameObject_t1756533147 * V_0 = NULL;
	int32_t V_1 = 0;
	GameObject_t1756533147 * V_2 = NULL;
	Removable_t2119088671 * V_3 = NULL;
	{
		SpriteRenderer_t1209076198 * L_0 = Component_GetComponent_TisSpriteRenderer_t1209076198_m2178781570(__this, /*hidden argument*/Component_GetComponent_TisSpriteRenderer_t1209076198_m2178781570_MethodInfo_var);
		bool L_1 = __this->get_isFirstTutorial_4();
		NullCheck(L_0);
		Renderer_set_enabled_m142717579(L_0, L_1, /*hidden argument*/NULL);
		bool L_2 = __this->get_isFirstTutorial_4();
		__this->set_isCurrentTutorial_5(L_2);
		bool L_3 = __this->get_isFirstTutorial_4();
		if (!L_3)
		{
			goto IL_00b0;
		}
	}
	{
		GameObject_t1756533147 * L_4 = GameObject_Find_m836511350(NULL /*static, unused*/, _stringLiteral2830371216, /*hidden argument*/NULL);
		V_0 = L_4;
		GameObject_t1756533147 * L_5 = GameObject_Find_m836511350(NULL /*static, unused*/, _stringLiteral1994460817, /*hidden argument*/NULL);
		NullCheck(L_5);
		Text_t356221433 * L_6 = GameObject_GetComponent_TisText_t356221433_m1217399699(L_5, /*hidden argument*/GameObject_GetComponent_TisText_t356221433_m1217399699_MethodInfo_var);
		String_t* L_7 = __this->get_tutorialText_7();
		NullCheck(L_6);
		VirtActionInvoker1< String_t* >::Invoke(72 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_6, L_7);
		GameObject_t1756533147 * L_8 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_9 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
		if (!L_9)
		{
			goto IL_00b0;
		}
	}
	{
		V_1 = 0;
		goto IL_009f;
	}

IL_005f:
	{
		GameObject_t1756533147 * L_10 = V_0;
		NullCheck(L_10);
		Transform_t3275118058 * L_11 = GameObject_get_transform_m909382139(L_10, /*hidden argument*/NULL);
		int32_t L_12 = V_1;
		NullCheck(L_11);
		Transform_t3275118058 * L_13 = Transform_GetChild_m3838588184(L_11, L_12, /*hidden argument*/NULL);
		NullCheck(L_13);
		GameObject_t1756533147 * L_14 = Component_get_gameObject_m3105766835(L_13, /*hidden argument*/NULL);
		V_2 = L_14;
		GameObject_t1756533147 * L_15 = V_2;
		GameObject_t1756533147 * L_16 = __this->get_followPosition_2();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_17 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_15, L_16, /*hidden argument*/NULL);
		if (!L_17)
		{
			goto IL_009b;
		}
	}
	{
		GameObject_t1756533147 * L_18 = V_2;
		NullCheck(L_18);
		Removable_t2119088671 * L_19 = GameObject_GetComponent_TisRemovable_t2119088671_m2134440904(L_18, /*hidden argument*/GameObject_GetComponent_TisRemovable_t2119088671_m2134440904_MethodInfo_var);
		V_3 = L_19;
		Removable_t2119088671 * L_20 = V_3;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_21 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_20, /*hidden argument*/NULL);
		if (!L_21)
		{
			goto IL_009b;
		}
	}
	{
		Removable_t2119088671 * L_22 = V_3;
		NullCheck(L_22);
		Behaviour_set_enabled_m1796096907(L_22, (bool)0, /*hidden argument*/NULL);
	}

IL_009b:
	{
		int32_t L_23 = V_1;
		V_1 = ((int32_t)((int32_t)L_23+(int32_t)1));
	}

IL_009f:
	{
		int32_t L_24 = V_1;
		GameObject_t1756533147 * L_25 = V_0;
		NullCheck(L_25);
		Transform_t3275118058 * L_26 = GameObject_get_transform_m909382139(L_25, /*hidden argument*/NULL);
		NullCheck(L_26);
		int32_t L_27 = Transform_get_childCount_m881385315(L_26, /*hidden argument*/NULL);
		if ((((int32_t)L_24) < ((int32_t)L_27)))
		{
			goto IL_005f;
		}
	}

IL_00b0:
	{
		return;
	}
}
// System.Void TutorialFollow::Update()
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisPausePress_t1918135203_m3658604164_MethodInfo_var;
extern const MethodInfo* Component_GetComponent_TisSpriteRenderer_t1209076198_m2178781570_MethodInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisSpriteRenderer_t1209076198_m1184556631_MethodInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisTutorialFollow_t1280665443_m4229171454_MethodInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisRemovable_t2119088671_m2134440904_MethodInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisText_t356221433_m1217399699_MethodInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisGameMaster_t3159518556_m3423764161_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral2843951784;
extern Il2CppCodeGenString* _stringLiteral1994460817;
extern Il2CppCodeGenString* _stringLiteral2830371216;
extern Il2CppCodeGenString* _stringLiteral3887597074;
extern const uint32_t TutorialFollow_Update_m3281037067_MetadataUsageId;
extern "C"  void TutorialFollow_Update_m3281037067 (TutorialFollow_t1280665443 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TutorialFollow_Update_m3281037067_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	GameObject_t1756533147 * V_1 = NULL;
	int32_t V_2 = 0;
	GameObject_t1756533147 * V_3 = NULL;
	Removable_t2119088671 * V_4 = NULL;
	SpriteRenderer_t1209076198 * G_B2_0 = NULL;
	SpriteRenderer_t1209076198 * G_B1_0 = NULL;
	int32_t G_B3_0 = 0;
	SpriteRenderer_t1209076198 * G_B3_1 = NULL;
	SpriteRenderer_t1209076198 * G_B6_0 = NULL;
	SpriteRenderer_t1209076198 * G_B5_0 = NULL;
	int32_t G_B7_0 = 0;
	SpriteRenderer_t1209076198 * G_B7_1 = NULL;
	{
		GameObject_t1756533147 * L_0 = GameObject_Find_m836511350(NULL /*static, unused*/, _stringLiteral2843951784, /*hidden argument*/NULL);
		NullCheck(L_0);
		PausePress_t1918135203 * L_1 = GameObject_GetComponent_TisPausePress_t1918135203_m3658604164(L_0, /*hidden argument*/GameObject_GetComponent_TisPausePress_t1918135203_m3658604164_MethodInfo_var);
		NullCheck(L_1);
		bool L_2 = L_1->get_isPaused_2();
		V_0 = L_2;
		SpriteRenderer_t1209076198 * L_3 = Component_GetComponent_TisSpriteRenderer_t1209076198_m2178781570(__this, /*hidden argument*/Component_GetComponent_TisSpriteRenderer_t1209076198_m2178781570_MethodInfo_var);
		bool L_4 = __this->get_isCurrentTutorial_5();
		G_B1_0 = L_3;
		if (!L_4)
		{
			G_B2_0 = L_3;
			goto IL_002c;
		}
	}
	{
		bool L_5 = V_0;
		G_B3_0 = ((((int32_t)L_5) == ((int32_t)0))? 1 : 0);
		G_B3_1 = G_B1_0;
		goto IL_002d;
	}

IL_002c:
	{
		G_B3_0 = 0;
		G_B3_1 = G_B2_0;
	}

IL_002d:
	{
		NullCheck(G_B3_1);
		Renderer_set_enabled_m142717579(G_B3_1, (bool)G_B3_0, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_6 = __this->get_hand_6();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_7 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_0064;
		}
	}
	{
		GameObject_t1756533147 * L_8 = __this->get_hand_6();
		NullCheck(L_8);
		SpriteRenderer_t1209076198 * L_9 = GameObject_GetComponent_TisSpriteRenderer_t1209076198_m1184556631(L_8, /*hidden argument*/GameObject_GetComponent_TisSpriteRenderer_t1209076198_m1184556631_MethodInfo_var);
		bool L_10 = __this->get_isCurrentTutorial_5();
		G_B5_0 = L_9;
		if (!L_10)
		{
			G_B6_0 = L_9;
			goto IL_005e;
		}
	}
	{
		bool L_11 = V_0;
		G_B7_0 = ((((int32_t)L_11) == ((int32_t)0))? 1 : 0);
		G_B7_1 = G_B5_0;
		goto IL_005f;
	}

IL_005e:
	{
		G_B7_0 = 0;
		G_B7_1 = G_B6_0;
	}

IL_005f:
	{
		NullCheck(G_B7_1);
		Renderer_set_enabled_m142717579(G_B7_1, (bool)G_B7_0, /*hidden argument*/NULL);
	}

IL_0064:
	{
		GameObject_t1756533147 * L_12 = __this->get_followPosition_2();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_13 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_12, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_13)
		{
			goto IL_018a;
		}
	}
	{
		TutorialFollow_t1280665443 * L_14 = __this->get_nextTutorial_3();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_15 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_14, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_15)
		{
			goto IL_00f6;
		}
	}
	{
		TutorialFollow_t1280665443 * L_16 = __this->get_nextTutorial_3();
		NullCheck(L_16);
		GameObject_t1756533147 * L_17 = Component_get_gameObject_m3105766835(L_16, /*hidden argument*/NULL);
		NullCheck(L_17);
		SpriteRenderer_t1209076198 * L_18 = GameObject_GetComponent_TisSpriteRenderer_t1209076198_m1184556631(L_17, /*hidden argument*/GameObject_GetComponent_TisSpriteRenderer_t1209076198_m1184556631_MethodInfo_var);
		NullCheck(L_18);
		Renderer_set_enabled_m142717579(L_18, (bool)1, /*hidden argument*/NULL);
		TutorialFollow_t1280665443 * L_19 = __this->get_nextTutorial_3();
		NullCheck(L_19);
		GameObject_t1756533147 * L_20 = Component_get_gameObject_m3105766835(L_19, /*hidden argument*/NULL);
		NullCheck(L_20);
		TutorialFollow_t1280665443 * L_21 = GameObject_GetComponent_TisTutorialFollow_t1280665443_m4229171454(L_20, /*hidden argument*/GameObject_GetComponent_TisTutorialFollow_t1280665443_m4229171454_MethodInfo_var);
		NullCheck(L_21);
		L_21->set_isCurrentTutorial_5((bool)1);
		TutorialFollow_t1280665443 * L_22 = __this->get_nextTutorial_3();
		NullCheck(L_22);
		GameObject_t1756533147 * L_23 = L_22->get_followPosition_2();
		NullCheck(L_23);
		Removable_t2119088671 * L_24 = GameObject_GetComponent_TisRemovable_t2119088671_m2134440904(L_23, /*hidden argument*/GameObject_GetComponent_TisRemovable_t2119088671_m2134440904_MethodInfo_var);
		NullCheck(L_24);
		Behaviour_set_enabled_m1796096907(L_24, (bool)1, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_25 = GameObject_Find_m836511350(NULL /*static, unused*/, _stringLiteral1994460817, /*hidden argument*/NULL);
		NullCheck(L_25);
		Text_t356221433 * L_26 = GameObject_GetComponent_TisText_t356221433_m1217399699(L_25, /*hidden argument*/GameObject_GetComponent_TisText_t356221433_m1217399699_MethodInfo_var);
		TutorialFollow_t1280665443 * L_27 = __this->get_nextTutorial_3();
		NullCheck(L_27);
		GameObject_t1756533147 * L_28 = Component_get_gameObject_m3105766835(L_27, /*hidden argument*/NULL);
		NullCheck(L_28);
		TutorialFollow_t1280665443 * L_29 = GameObject_GetComponent_TisTutorialFollow_t1280665443_m4229171454(L_28, /*hidden argument*/GameObject_GetComponent_TisTutorialFollow_t1280665443_m4229171454_MethodInfo_var);
		NullCheck(L_29);
		String_t* L_30 = L_29->get_tutorialText_7();
		NullCheck(L_26);
		VirtActionInvoker1< String_t* >::Invoke(72 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_26, L_30);
		goto IL_017e;
	}

IL_00f6:
	{
		GameObject_t1756533147 * L_31 = GameObject_Find_m836511350(NULL /*static, unused*/, _stringLiteral2830371216, /*hidden argument*/NULL);
		V_1 = L_31;
		GameObject_t1756533147 * L_32 = GameObject_Find_m836511350(NULL /*static, unused*/, _stringLiteral1994460817, /*hidden argument*/NULL);
		NullCheck(L_32);
		Text_t356221433 * L_33 = GameObject_GetComponent_TisText_t356221433_m1217399699(L_32, /*hidden argument*/GameObject_GetComponent_TisText_t356221433_m1217399699_MethodInfo_var);
		GameObject_t1756533147 * L_34 = GameObject_Find_m836511350(NULL /*static, unused*/, _stringLiteral3887597074, /*hidden argument*/NULL);
		NullCheck(L_34);
		GameMaster_t3159518556 * L_35 = GameObject_GetComponent_TisGameMaster_t3159518556_m3423764161(L_34, /*hidden argument*/GameObject_GetComponent_TisGameMaster_t3159518556_m3423764161_MethodInfo_var);
		NullCheck(L_35);
		String_t* L_36 = L_35->get_defaultTutorialString_28();
		NullCheck(L_33);
		VirtActionInvoker1< String_t* >::Invoke(72 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_33, L_36);
		GameObject_t1756533147 * L_37 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_38 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_37, /*hidden argument*/NULL);
		if (!L_38)
		{
			goto IL_017e;
		}
	}
	{
		V_2 = 0;
		goto IL_016d;
	}

IL_013b:
	{
		GameObject_t1756533147 * L_39 = V_1;
		NullCheck(L_39);
		Transform_t3275118058 * L_40 = GameObject_get_transform_m909382139(L_39, /*hidden argument*/NULL);
		int32_t L_41 = V_2;
		NullCheck(L_40);
		Transform_t3275118058 * L_42 = Transform_GetChild_m3838588184(L_40, L_41, /*hidden argument*/NULL);
		NullCheck(L_42);
		GameObject_t1756533147 * L_43 = Component_get_gameObject_m3105766835(L_42, /*hidden argument*/NULL);
		V_3 = L_43;
		GameObject_t1756533147 * L_44 = V_3;
		NullCheck(L_44);
		Removable_t2119088671 * L_45 = GameObject_GetComponent_TisRemovable_t2119088671_m2134440904(L_44, /*hidden argument*/GameObject_GetComponent_TisRemovable_t2119088671_m2134440904_MethodInfo_var);
		V_4 = L_45;
		Removable_t2119088671 * L_46 = V_4;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_47 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_46, /*hidden argument*/NULL);
		if (!L_47)
		{
			goto IL_0169;
		}
	}
	{
		Removable_t2119088671 * L_48 = V_4;
		NullCheck(L_48);
		Behaviour_set_enabled_m1796096907(L_48, (bool)1, /*hidden argument*/NULL);
	}

IL_0169:
	{
		int32_t L_49 = V_2;
		V_2 = ((int32_t)((int32_t)L_49+(int32_t)1));
	}

IL_016d:
	{
		int32_t L_50 = V_2;
		GameObject_t1756533147 * L_51 = V_1;
		NullCheck(L_51);
		Transform_t3275118058 * L_52 = GameObject_get_transform_m909382139(L_51, /*hidden argument*/NULL);
		NullCheck(L_52);
		int32_t L_53 = Transform_get_childCount_m881385315(L_52, /*hidden argument*/NULL);
		if ((((int32_t)L_50) < ((int32_t)L_53)))
		{
			goto IL_013b;
		}
	}

IL_017e:
	{
		GameObject_t1756533147 * L_54 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		Object_Destroy_m4145850038(NULL /*static, unused*/, L_54, /*hidden argument*/NULL);
		return;
	}

IL_018a:
	{
		Transform_t3275118058 * L_55 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_56 = __this->get_followPosition_2();
		NullCheck(L_56);
		Transform_t3275118058 * L_57 = GameObject_get_transform_m909382139(L_56, /*hidden argument*/NULL);
		NullCheck(L_57);
		Vector3_t2243707580  L_58 = Transform_get_localPosition_m2533925116(L_57, /*hidden argument*/NULL);
		NullCheck(L_55);
		Transform_set_localPosition_m1026930133(L_55, L_58, /*hidden argument*/NULL);
		return;
	}
}
// System.Void TutorialNoNo::.ctor()
extern "C"  void TutorialNoNo__ctor_m1849162099 (TutorialNoNo_t3242399842 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void TutorialNoNo::Start()
extern "C"  void TutorialNoNo_Start_m799199791 (TutorialNoNo_t3242399842 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void TutorialNoNo::Update()
extern "C"  void TutorialNoNo_Update_m1424635276 (TutorialNoNo_t3242399842 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
