﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// DeveloperTools.DeveloperLog
struct DeveloperLog_t2073088872;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void DeveloperTools.DeveloperLog::.ctor()
extern "C"  void DeveloperLog__ctor_m1268392924 (DeveloperLog_t2073088872 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DeveloperTools.DeveloperLog::LogMessage(System.String)
extern "C"  void DeveloperLog_LogMessage_m3766158779 (Il2CppObject * __this /* static, unused */, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
