﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Removable
struct Removable_t2119088671;

#include "codegen/il2cpp-codegen.h"

// System.Void Removable::.ctor()
extern "C"  void Removable__ctor_m2484126636 (Removable_t2119088671 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Removable::Start()
extern "C"  void Removable_Start_m1691861904 (Removable_t2119088671 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Removable::Update()
extern "C"  void Removable_Update_m2682633095 (Removable_t2119088671 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Removable::Remove()
extern "C"  void Removable_Remove_m1955228078 (Removable_t2119088671 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
