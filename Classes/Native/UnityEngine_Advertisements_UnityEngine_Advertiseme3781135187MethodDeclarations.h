﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.Advertisements.Editor.Platform/<Initialize>c__AnonStorey0
struct U3CInitializeU3Ec__AnonStorey0_t3781135187;
// System.IAsyncResult
struct IAsyncResult_t1999651008;

#include "codegen/il2cpp-codegen.h"

// System.Void UnityEngine.Advertisements.Editor.Platform/<Initialize>c__AnonStorey0::.ctor()
extern "C"  void U3CInitializeU3Ec__AnonStorey0__ctor_m2993356163 (U3CInitializeU3Ec__AnonStorey0_t3781135187 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Advertisements.Editor.Platform/<Initialize>c__AnonStorey0::<>m__0(System.IAsyncResult)
extern "C"  void U3CInitializeU3Ec__AnonStorey0_U3CU3Em__0_m130738225 (U3CInitializeU3Ec__AnonStorey0_t3781135187 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
