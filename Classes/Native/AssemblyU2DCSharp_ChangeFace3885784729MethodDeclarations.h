﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ChangeFace
struct ChangeFace_t3885784729;

#include "codegen/il2cpp-codegen.h"

// System.Void ChangeFace::.ctor()
extern "C"  void ChangeFace__ctor_m2504436316 (ChangeFace_t3885784729 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChangeFace::Start()
extern "C"  void ChangeFace_Start_m3655810180 (ChangeFace_t3885784729 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChangeFace::Update()
extern "C"  void ChangeFace_Update_m2745119365 (ChangeFace_t3885784729 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
