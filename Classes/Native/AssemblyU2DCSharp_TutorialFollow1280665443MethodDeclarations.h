﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TutorialFollow
struct TutorialFollow_t1280665443;

#include "codegen/il2cpp-codegen.h"

// System.Void TutorialFollow::.ctor()
extern "C"  void TutorialFollow__ctor_m3454975770 (TutorialFollow_t1280665443 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TutorialFollow::Start()
extern "C"  void TutorialFollow_Start_m2699853582 (TutorialFollow_t1280665443 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TutorialFollow::Update()
extern "C"  void TutorialFollow_Update_m3281037067 (TutorialFollow_t1280665443 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
