﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Spawner
struct Spawner_t534830648;

#include "codegen/il2cpp-codegen.h"

// System.Void Spawner::.ctor()
extern "C"  void Spawner__ctor_m1860016179 (Spawner_t534830648 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Spawner::Start()
extern "C"  void Spawner_Start_m3183106495 (Spawner_t534830648 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Spawner::Update()
extern "C"  void Spawner_Update_m1485478966 (Spawner_t534830648 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Spawner::GoPageLeft()
extern "C"  void Spawner_GoPageLeft_m1142194739 (Spawner_t534830648 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Spawner::GoPageRight()
extern "C"  void Spawner_GoPageRight_m3093617094 (Spawner_t534830648 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Spawner::redrawLevels()
extern "C"  void Spawner_redrawLevels_m1471550761 (Spawner_t534830648 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
