﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.Advertisements.iOS.Platform/<UnityAdsReady>c__AnonStorey0
struct U3CUnityAdsReadyU3Ec__AnonStorey0_t1627639653;
// UnityEngine.Advertisements.CallbackExecutor
struct CallbackExecutor_t2947320436;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_Advertisements_UnityEngine_Advertiseme2947320436.h"

// System.Void UnityEngine.Advertisements.iOS.Platform/<UnityAdsReady>c__AnonStorey0::.ctor()
extern "C"  void U3CUnityAdsReadyU3Ec__AnonStorey0__ctor_m377072553 (U3CUnityAdsReadyU3Ec__AnonStorey0_t1627639653 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Advertisements.iOS.Platform/<UnityAdsReady>c__AnonStorey0::<>m__0(UnityEngine.Advertisements.CallbackExecutor)
extern "C"  void U3CUnityAdsReadyU3Ec__AnonStorey0_U3CU3Em__0_m1952225827 (U3CUnityAdsReadyU3Ec__AnonStorey0_t1627639653 * __this, CallbackExecutor_t2947320436 * ___executor0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
