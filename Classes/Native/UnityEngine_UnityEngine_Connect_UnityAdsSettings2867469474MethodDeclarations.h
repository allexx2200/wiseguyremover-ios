﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_RuntimePlatform1869584967.h"

// System.Boolean UnityEngine.Connect.UnityAdsSettings::get_enabled()
extern "C"  bool UnityAdsSettings_get_enabled_m2021662085 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Connect.UnityAdsSettings::IsPlatformEnabled(UnityEngine.RuntimePlatform)
extern "C"  bool UnityAdsSettings_IsPlatformEnabled_m2937279829 (Il2CppObject * __this /* static, unused */, int32_t ___platform0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Connect.UnityAdsSettings::get_initializeOnStartup()
extern "C"  bool UnityAdsSettings_get_initializeOnStartup_m4039407970 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Connect.UnityAdsSettings::get_testMode()
extern "C"  bool UnityAdsSettings_get_testMode_m2074656677 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.Connect.UnityAdsSettings::GetGameId(UnityEngine.RuntimePlatform)
extern "C"  String_t* UnityAdsSettings_GetGameId_m2635423069 (Il2CppObject * __this /* static, unused */, int32_t ___platform0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
