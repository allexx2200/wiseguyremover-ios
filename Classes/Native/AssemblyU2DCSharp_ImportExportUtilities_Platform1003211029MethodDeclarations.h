﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ImportExportUtilities.Platform
struct Platform_t1003211029;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_ImportExportUtilities_Platform1003211029.h"

// System.Void ImportExportUtilities.Platform::.ctor()
extern "C"  void Platform__ctor_m1128244901 (Platform_t1003211029 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ImportExportUtilities.Platform::.ctor(ImportExportUtilities.Platform)
extern "C"  void Platform__ctor_m1662664651 (Platform_t1003211029 * __this, Platform_t1003211029 * ___other0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
