﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CustomGravity
struct CustomGravity_t2378668691;

#include "codegen/il2cpp-codegen.h"

// System.Void CustomGravity::.ctor()
extern "C"  void CustomGravity__ctor_m2276211818 (CustomGravity_t2378668691 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CustomGravity::Start()
extern "C"  void CustomGravity_Start_m1628625966 (CustomGravity_t2378668691 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CustomGravity::Update()
extern "C"  void CustomGravity_Update_m735476087 (CustomGravity_t2378668691 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
