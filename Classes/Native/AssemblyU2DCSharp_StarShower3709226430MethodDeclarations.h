﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// StarShower
struct StarShower_t3709226430;

#include "codegen/il2cpp-codegen.h"

// System.Void StarShower::.ctor()
extern "C"  void StarShower__ctor_m4067111103 (StarShower_t3709226430 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void StarShower::FlushStars()
extern "C"  void StarShower_FlushStars_m968795006 (StarShower_t3709226430 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void StarShower::GoToLevel()
extern "C"  void StarShower_GoToLevel_m2221394174 (StarShower_t3709226430 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
