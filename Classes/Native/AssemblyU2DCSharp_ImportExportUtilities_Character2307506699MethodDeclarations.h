﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ImportExportUtilities.Character
struct Character_t2307506699;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_ImportExportUtilities_Character2307506699.h"

// System.Void ImportExportUtilities.Character::.ctor()
extern "C"  void Character__ctor_m1925457927 (Character_t2307506699 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ImportExportUtilities.Character::.ctor(ImportExportUtilities.Character)
extern "C"  void Character__ctor_m1119584827 (Character_t2307506699 * __this, Character_t2307506699 * ___other0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
