﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GoToScene
struct GoToScene_t3084403383;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void GoToScene::.ctor()
extern "C"  void GoToScene__ctor_m3313655624 (GoToScene_t3084403383 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GoToScene::LoadLevel(System.String)
extern "C"  void GoToScene_LoadLevel_m3689284180 (GoToScene_t3084403383 * __this, String_t* ___scene0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
