﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Object2689449295.h"
#include "AssemblyU2DCSharp_U3CPrivateImplementationDetailsU2731437132.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>
struct  U3CPrivateImplementationDetailsU3E_t1486305142  : public Il2CppObject
{
public:

public:
};

struct U3CPrivateImplementationDetailsU3E_t1486305142_StaticFields
{
public:
	// <PrivateImplementationDetails>/$ArrayType=20 <PrivateImplementationDetails>::$field-A55AA7C523DD2ED84D18AEF0C5B0A88A70618491
	U24ArrayTypeU3D20_t2731437132  ___U24fieldU2DA55AA7C523DD2ED84D18AEF0C5B0A88A70618491_0;

public:
	inline static int32_t get_offset_of_U24fieldU2DA55AA7C523DD2ED84D18AEF0C5B0A88A70618491_0() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t1486305142_StaticFields, ___U24fieldU2DA55AA7C523DD2ED84D18AEF0C5B0A88A70618491_0)); }
	inline U24ArrayTypeU3D20_t2731437132  get_U24fieldU2DA55AA7C523DD2ED84D18AEF0C5B0A88A70618491_0() const { return ___U24fieldU2DA55AA7C523DD2ED84D18AEF0C5B0A88A70618491_0; }
	inline U24ArrayTypeU3D20_t2731437132 * get_address_of_U24fieldU2DA55AA7C523DD2ED84D18AEF0C5B0A88A70618491_0() { return &___U24fieldU2DA55AA7C523DD2ED84D18AEF0C5B0A88A70618491_0; }
	inline void set_U24fieldU2DA55AA7C523DD2ED84D18AEF0C5B0A88A70618491_0(U24ArrayTypeU3D20_t2731437132  value)
	{
		___U24fieldU2DA55AA7C523DD2ED84D18AEF0C5B0A88A70618491_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
