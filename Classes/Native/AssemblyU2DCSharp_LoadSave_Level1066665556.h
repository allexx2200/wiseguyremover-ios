﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LoadSave.Level
struct  Level_t1066665556  : public Il2CppObject
{
public:
	// System.Int32 LoadSave.Level::status
	int32_t ___status_5;
	// System.Int32 LoadSave.Level::number
	int32_t ___number_6;

public:
	inline static int32_t get_offset_of_status_5() { return static_cast<int32_t>(offsetof(Level_t1066665556, ___status_5)); }
	inline int32_t get_status_5() const { return ___status_5; }
	inline int32_t* get_address_of_status_5() { return &___status_5; }
	inline void set_status_5(int32_t value)
	{
		___status_5 = value;
	}

	inline static int32_t get_offset_of_number_6() { return static_cast<int32_t>(offsetof(Level_t1066665556, ___number_6)); }
	inline int32_t get_number_6() const { return ___number_6; }
	inline int32_t* get_address_of_number_6() { return &___number_6; }
	inline void set_number_6(int32_t value)
	{
		___number_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
