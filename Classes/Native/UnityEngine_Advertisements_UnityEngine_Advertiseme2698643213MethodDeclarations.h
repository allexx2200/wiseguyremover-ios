﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.Advertisements.Advertisement/<Show>c__AnonStorey0
struct U3CShowU3Ec__AnonStorey0_t2698643213;

#include "codegen/il2cpp-codegen.h"

// System.Void UnityEngine.Advertisements.Advertisement/<Show>c__AnonStorey0::.ctor()
extern "C"  void U3CShowU3Ec__AnonStorey0__ctor_m4194757089 (U3CShowU3Ec__AnonStorey0_t2698643213 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
