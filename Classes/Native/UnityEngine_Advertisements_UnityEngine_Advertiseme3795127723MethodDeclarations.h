﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.Advertisements.iOS.Platform/<UnityAdsDidFinish>c__AnonStorey3
struct U3CUnityAdsDidFinishU3Ec__AnonStorey3_t3795127723;

#include "codegen/il2cpp-codegen.h"

// System.Void UnityEngine.Advertisements.iOS.Platform/<UnityAdsDidFinish>c__AnonStorey3::.ctor()
extern "C"  void U3CUnityAdsDidFinishU3Ec__AnonStorey3__ctor_m2021361755 (U3CUnityAdsDidFinishU3Ec__AnonStorey3_t3795127723 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
